/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QSize>
#include <QPixmap>

class Settings : public QObject
{
    Q_OBJECT
public:
    Settings();

    static QString elementPath(const QString & element)
    { return ":images/weather_elements/" + element; }

    static QSize windowSize();
    static bool scaledImages();

    static qreal referenceWidth() { return 480.0; }
    static qreal referenceHeight() { return 864.0; }

    static qreal widthFactor() { return qreal(windowSize().width()) / referenceWidth(); }
    static qreal heightFactor() { return qreal(windowSize().height()) / referenceHeight(); }

    static qreal scaleWidth(qreal width) { return width * widthFactor(); }
    static qreal scaleHeight(qreal height) { return height * heightFactor(); }

    static QStringList getDemoCities();
    static QStringList getCurrentCities();
    static void setCurrentCities(QStringList cities);

private:
    QSettings m_settings;
    static Settings *instance();

    static QStringList getCities(const QString &type);

};

#endif // SETTINGS_H
