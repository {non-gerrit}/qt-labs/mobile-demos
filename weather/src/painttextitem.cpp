/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "painttextitem.h"
#include <QPainter>
#include <QDebug>

TextPainter::TextPainter(int fontSize, QColor color, const QString &text)
    : m_pen(color)
    , m_brush(color)
    , m_text(text)
    , m_quoted(false)
    , m_maxWidth(-1)
{
    m_font.setFamily("Nokia Sans");
    m_font.setPixelSize(fontSize);
    m_font.setStyleStrategy(QFont::PreferAntialias);
    m_pen.setJoinStyle(Qt::RoundJoin);
}

int TextPainter::width() const
{
    QFontMetrics m(m_font);
    int result = 0;
    if (m_quoted)
        result += 2 * m.width('"');
    QString text = m_maxWidth > 0 ? m.elidedText(m_text, Qt::ElideRight, m_maxWidth - result)
                                  : m_text;
    return result + m.width(text);
}

void TextPainter::paint(QPainter *painter)
{
    QFontMetrics m(m_font);
    QString text;
    if (m_quoted) {
        int quote = 2 * m.width('"');
        text = m_maxWidth > 0 ? m.elidedText(m_text, Qt::ElideRight, m_maxWidth - quote) : m_text;
        text = '"' + text + '"';
    } else
        text = m_maxWidth > 0 ? m.elidedText(m_text, Qt::ElideRight, m_maxWidth) : m_text;
    painter->setFont(m_font);
    painter->setPen(m_pen);
    painter->setBrush(m_brush);

    painter->drawText(QPointF(m_pos.x(), m_pos.y() + m.ascent()), text);
}

void TextPainter::locateAtCenter(TextPainter *item, qreal left, qreal top, qreal width)
{
    item->setPos(left + (width - item->width()) / 2, top);
}

void TextPainter::locateAtCenter(QList<TextPainter*> items, qreal left, qreal top, qreal width)
{
    static const int margin = 5;

    if (items.isEmpty())
        return;

    int itemsWidth = (items.count() - 1) * margin;
    for (int i = 0; i < items.count(); ++i)
        itemsWidth += items[i]->width();

    left += (width - itemsWidth) / 2;
    for (int i = 0; i < items.count(); ++i) {
        items[i]->setPos(left, top);
        left += items[i]->width() + margin;
    }
}
