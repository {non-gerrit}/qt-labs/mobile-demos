/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef FAKECONTENTSCREEN_H
#define FAKECONTENTSCREEN_H

#include <QObject>
#include <QGraphicsItem>

class FakeContentScreen : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    FakeContentScreen(QGraphicsItem *parent = 0);
    static int loadImages();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    const QRectF m_boundingRect;
    QPixmap m_left;
    QPixmap m_right;

signals:
    void userAccepted();
};

#endif // FAKECONTENTSCREEN_H
