/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef FORECASTDATA_H
#define FORECASTDATA_H

#include <QString>
#include "forecast.h"
#include "yahooweatherresponse.h"

#include <QSharedPointer>

class YahooWeatherResponse;

class ForecastData
{
public:
    ForecastData() : m_error(false), m_data(0) {}
    ForecastData(YahooWeatherResponse *data) : m_error(!data), m_data(data) {}

    Forecast::ForecastType type() const;
    bool night() const;
    bool error() const { return m_error; }
    QString key() const { return m_data ? m_data->locationCode() : QString(); }
    QString cityName() const { return m_data ? m_data->location().city() : QString(); }
    int lower() const { return m_data ? m_data->forecast(0).low() : 0; }
    int current() const { return m_data ? m_data->condition().temperature() : 0; }
    int upper() const { return m_data ? m_data->forecast(0).high() : 0; }
    bool isNull() const { return !m_error && !m_data; }

    inline bool operator==(const ForecastData &other);
    bool operator!=(const ForecastData &other) { return !operator==(other); }

private:
    bool m_error;

    QSharedPointer<YahooWeatherResponse> m_data;
};

bool ForecastData::operator==(const ForecastData &other)
{
    return    type() == other.type() && night() == other.night() && error() == other.error()
           && key() == other.key() && cityName() == other.cityName() && lower() == other.lower()
           && current() == other.current() && upper() == other.upper();
}

#endif // FORECASTDATA_H
