/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef PIXMAPBUTTON_H
#define PIXMAPBUTTON_H

#include <QObject>
#include <QGraphicsPixmapItem>

class PixmapButton: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity);
public:
    PixmapButton(qreal minSize, const QPixmap &pixmap, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    QPainterPath shape() const;

public slots:
    void doHide() { hide(); }

signals:
    void clicked();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    const qreal m_minSize;
};

#endif // PIXMAPBUTTON_H
