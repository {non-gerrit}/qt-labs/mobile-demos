/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef FORECAST_H
#define FORECAST_H

namespace Forecast
{
    enum ForecastType
    {
        MostlyCloudy = 0,
        Cloudy,
        MostlySunny,
        PartlyCloudy,
        Sunny,
        Flurries,
        Fog,
        Haze,
        Sand,
        Dust,
        Icy,
        Sleet,
        ChanceOfSleet,
        Snow,
        ChanceOfSnow,
        Mist,
        Rain,
        ChanceOfRain,
        Storm,
        ChanceOfStorm,
        Thunderstorm,
        ChanceOfThunderstorm,
        UnknownForecast
    };

    enum Effect
    {
        FogEffect = 0,
        HazeEffect,
        NoEffect
    };
}

#endif // FORECAST_H
