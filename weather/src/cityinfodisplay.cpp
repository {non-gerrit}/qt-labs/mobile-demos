/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "cityinfodisplay.h"
#include "settings.h"
#include "pixmaploader.h"

#include <QFont>
#include <QPen>
#include <QFontMetrics>
#include <QDebug>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

#define CURRENT_TEMP_FONT_SIZE  (Settings::scaleHeight(170.0))
#define TEMP_BOUND_FONT_SIZE    (Settings::scaleHeight(40.0))
#define CITY_NAME_FONT_SIZE     (Settings::scaleHeight(50.0))
#define MAX_CITY_NAME_WIDTH     (Settings::scaleWidth(440.0))
#define CITY_NAME_TOP           (Settings::scaleHeight(235.0))

// CurrentTemperatureDisplay

CurrentTemperatureDisplay::CurrentTemperatureDisplay()
    : m_brush(Qt::white)
    , m_sign(PixmapLoader::getPic("minus_sign"))
    , m_unit(PixmapLoader::getPic("centigrades"))
    , m_value(-1)
    , m_pos(0.0, 0.0)
{
    m_font.setFamily("Nokia Sans");
    m_font.setBold(true);
    m_font.setPixelSize(CURRENT_TEMP_FONT_SIZE);
    m_font.setStyleStrategy(QFont::PreferAntialias);

    m_pen.setColor(Qt::white);
    m_pen.setBrush(Qt::white);
    m_pen.setJoinStyle(Qt::RoundJoin);

    setTemperature(0);
}

QRectF CurrentTemperatureDisplay::boundingRect () const
{
    return m_boundingRect;
}

void CurrentTemperatureDisplay::paint(QPainter *painter)
{
    if (m_value < 0)
        painter->drawPixmap(m_signPos, m_sign);

    painter->setPen(m_pen);
    painter->setBrush(m_brush);
    painter->setFont(m_font);
    painter->drawText(m_numberPos, m_text);

    painter->drawPixmap(m_unitPos, m_unit);
}

void CurrentTemperatureDisplay::setTemperature(int value)
{
    static const int maxValue = 300;
    value = qMax(qMin(maxValue, value), -maxValue);

    if (m_value == value)
        return;

    m_value = value;
    m_text = QString::number(qAbs(m_value));

    QFontMetrics metrics(m_font);

    int textWidth = metrics.width(m_text);
    int width = textWidth;
    int left = 0;

    if (m_value < 0) {
        left = -(width >> 1);
        m_numberPos = QPointF(left, m_unit.height());
        width += m_sign.width();
        left -= m_sign.width();
        m_signPos = QPointF(left, 0.0);
        m_unitPos = QPointF(left + width, 0.0);
        width += m_unit.width();
    } else {
        width += m_unit.width();
        int left = -(width >> 1);
        m_numberPos = QPointF(left, m_unit.height());
        m_unitPos = QPointF(left + textWidth, 0.0);
    }
    m_boundingRect = QRectF(left, 0.0, width, m_unit.height());

}

// TemperatureBoundDisplay

TemperatureBoundDisplay::TemperatureBoundDisplay(bool lowerBound)
    : m_brush(Qt::white)
    , m_icon(PixmapLoader::getPic(lowerBound ? "icon_min" : "icon_max"))
    , m_value(-1)
    , m_lowerBound(lowerBound)
    , m_pos(0.0, 0.0)
{
    m_font.setFamily("Nokia Sans");
    m_font.setPixelSize(TEMP_BOUND_FONT_SIZE);
    m_font.setStyleStrategy(QFont::PreferAntialias);

    QColor color(255, 255, 255, 178);

    m_pen.setColor(color);
    m_pen.setBrush(color);
    m_pen.setJoinStyle(Qt::RoundJoin);

    m_brush.setColor(color);

    setValue(0);
}

QRectF TemperatureBoundDisplay::boundingRect () const
{
    return m_boundingRect;
}

void TemperatureBoundDisplay::paint(QPainter *painter)
{
    QRectF rect(m_boundingRect);
    painter->drawPixmap(rect.topLeft(), m_icon);
    rect.setLeft(rect.left() + m_icon.width());

    painter->setPen(m_pen);
    painter->setBrush(m_brush);
    painter->setFont(m_font);
    QPointF textPos(rect.left(), m_icon.height());
    painter->drawText(textPos, m_text);
}

void TemperatureBoundDisplay::setValue(int value)
{
    static const int maxValue = 300;
    value = qMax(qMin(maxValue, value), -maxValue);

    if (m_value == value)
        return;

    m_value = value;
    m_text = QString::number(m_value) + QChar(176) + 'c';

    QFontMetrics metrics(m_font);
    m_boundingRect = QRectF(0.0, 0.0, m_icon.width() + metrics.width(m_text), m_icon.height());
}

// TemperatureDisplay

TemperatureDisplay::TemperatureDisplay()
    : m_line(PixmapLoader::getPic("division_line"))
    , m_lowerBound(true)
    , m_upperBound(false)
    , m_pos(0.0, 0.0)
{
    setTemperature(0, 0, 0);
}

void TemperatureDisplay::setTemperature(int lowerBound, int upperBound, int current)
{
    m_lowerBound.setValue(lowerBound);
    m_upperBound.setValue(upperBound);
    m_current.setTemperature(current);

    m_current.setPos(0.0, 0.0);

    const qreal lineRight = m_line.width() >> 1;
    const qreal right = qMax(lineRight, m_current.boundingRect().right());
    const qreal left = qMin(-lineRight, m_current.boundingRect().left());

    qreal height = m_current.boundingRect().height() + 8.0;

    m_linePos = QPointF(-lineRight, height);
    height += m_line.height() + 8.0;

    m_upperBound.setPos(-lineRight, height);
    m_lowerBound.setPos(lineRight - m_lowerBound.boundingRect().width(), height);

    height += qMax(m_lowerBound.boundingRect().height(), m_upperBound.boundingRect().height());

    m_boundingRect = QRectF(left, 0.0, right - left, height);
}

QRectF TemperatureDisplay::boundingRect () const
{
    return m_boundingRect;
}

void TemperatureDisplay::paint(QPainter *painter)
{
    painter->translate(m_current.pos());
    m_current.paint(painter);

    painter->translate(m_upperBound.pos() - m_current.pos());
    m_upperBound.paint(painter);

    painter->translate(m_lowerBound.pos() - m_upperBound.pos());
    m_lowerBound.paint(painter);

    painter->translate(-m_lowerBound.pos());

    painter->drawPixmap(m_linePos, m_line);
}

// CityInfoDisplay

CityInfoDisplay::CityInfoDisplay(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_brush(Qt::white)
{
    setCacheMode(ItemCoordinateCache);
    m_temperature.setPos(0.0, 0.0);

    m_font.setFamily("Nokia Sans");
    m_font.setPixelSize(CITY_NAME_FONT_SIZE);
    m_font.setStyleStrategy(QFont::PreferAntialias);

    m_pen.setColor(Qt::white);
    m_pen.setBrush(Qt::white);
    m_pen.setJoinStyle(Qt::RoundJoin);

    setCityName("unknown");
}

void CityInfoDisplay::updateBoundingRect()
{
    const qreal right = qMax(m_temperature.boundingRect().right(), -m_textPos.x());
    const qreal left = qMin(m_temperature.boundingRect().left(), m_textPos.x());
    m_boundingRect = QRectF(left, 0.0, right - left, m_nameRect.bottom());
}

void CityInfoDisplay::setTemperature(int lowerBound, int upperBound, int current)
{
    prepareGeometryChange();
    m_temperature.setTemperature(lowerBound, upperBound, current);
    updateBoundingRect();
    update();
}

void CityInfoDisplay::setCityName(const QString &name)
{
    prepareGeometryChange();
    QFontMetrics metrics(m_font);
    m_text = metrics.elidedText(name, Qt::ElideRight, MAX_CITY_NAME_WIDTH);

    int width = metrics.width(m_text);
    int right = width >> 1;
    qreal top = CITY_NAME_TOP;
    m_textPos = QPointF(-right, top + metrics.height());
    m_nameRect = QRectF(-right, top, 2 * right, metrics.height() + metrics.descent());

    updateBoundingRect();
    update();
}

QRectF CityInfoDisplay::boundingRect () const
{
    return m_boundingRect;
}

void CityInfoDisplay::paint(QPainter *painter, const QStyleOptionGraphicsItem *opt, QWidget *widget)
{
    Q_UNUSED(opt);
    Q_UNUSED(widget);

    painter->setPen(m_pen);
    painter->setBrush(m_brush);
    painter->setFont(m_font);
    painter->drawText(m_textPos, m_text);

    painter->translate(m_temperature.pos());
    m_temperature.paint(painter);
}

void CityInfoDisplay::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_nameRect.contains(event->pos())) {
        emit nameClicked();
    } else
        event->ignore();
}

int CityInfoDisplay::loadImages()
{
    PixmapLoader::load("minus_sign");
    PixmapLoader::load("centigrades");
    PixmapLoader::load("icon_min");
    PixmapLoader::load("icon_max");
    PixmapLoader::load("division_line");
    return 5;
}
