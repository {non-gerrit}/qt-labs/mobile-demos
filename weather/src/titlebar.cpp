/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "titlebar.h"
#include "settings.h"
#include "pixmaploader.h"

#include <QFont>
#include <QCoreApplication>

// CloseButton

CloseButton::CloseButton(QGraphicsItem *parent)
    : PixmapButton(80.0, PixmapLoader::getPic("button_close"), parent)
{
}

void CloseButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    QCoreApplication::instance()->quit();
}

TitleBar::TitleBar(QGraphicsItem *parent)
    : QGraphicsPixmapItem(PixmapLoader::getPic("title_bar"), parent)
{
    QGraphicsTextItem *text = new QGraphicsTextItem(this);
    QFont font = text->font();
    font.setFamily("Nokia sans regular");
    font.setPixelSize(Settings::scaleHeight(25));
    text->setFont(font);
    text->setDefaultTextColor(QColor("white"));
    text->setPlainText("Weather");
    QRectF rect(boundingRect());
    qreal x = rect.left() + (rect.width() - text->boundingRect().width()) / 2;
    text->setPos(x, Settings::scaleHeight(5.0));

    CloseButton *button = new CloseButton(this);
    button->setPos(Settings::scaleWidth(442.0), Settings::scaleHeight(14.0));
}

int TitleBar::loadImages()
{
    PixmapLoader::load("button_close");
    PixmapLoader::load("title_bar");
    return 2;
}
