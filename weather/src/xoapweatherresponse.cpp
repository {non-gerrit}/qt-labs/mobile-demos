/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "xoapweatherresponse.h"

#include <QDebug>

static inline int strToInt(const QString &str, int defaultValue)
{
    bool ok;
    int result = str.toInt(&ok);
    return ok ? result : defaultValue;
}

static inline QString getNodeContentText(QDomNode node)
{
    if (node.childNodes().count() == 0 || !node.childNodes().at(0).isText())
        return QString();
    return node.childNodes().at(0).nodeValue();
}

void XoapWeatherResponse::Item::read(QDomElement element)
{
    m_id = element.attribute("id");
    m_type = strToInt(element.attribute("type"), 0);
    m_texts.clear();
    m_texts << getNodeContentText(element).split(",", QString::SkipEmptyParts);
}

XoapWeatherResponse::XoapWeatherResponse()
{
}

void XoapWeatherResponse::read(QDomElement element)
{
    m_version = element.attribute("ver");
    for (int i = 0; i < element.childNodes().count(); ++i) {
        if (element.childNodes().at(i).isElement()) {
            Item item;
            item.read(element.childNodes().at(i).toElement());
            m_list.append(item);
        }
    }
}

void XoapWeatherResponse::print()
{
    /*
    qDebug() << m_version << m_list.count() << " items";
    for (int i = 0; i < m_list.count(); ++i)
        qDebug() << "item " << i << " : " << m_list[i].id() << m_list[i].type() << m_list[i].texts();
        */
}
