/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef LOADING_H
#define LOADING_H

#include <QObject>
#include <QGraphicsItem>
#include <QBasicTimer>
#include <QPainter>

class Loading : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    Loading(QGraphicsItem *parent = 0);

    static void loadImages();

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    void start() { m_ticker.start(50, this); }
    void stop() { m_ticker.stop(); }

protected:
    void timerEvent(QTimerEvent *event);

private:
    int m_image;
    const QRectF m_boundingRect;
    QBasicTimer m_ticker;
};

#endif // LOADING_H
