/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "bootmanager.h"
#include "settings.h"
#include "forecastprovider.h"
#include "pixmaploader.h"
#include "forecasthungitem.h"
#include "forecastrain.h"
#include "forecastsnow.h"
#include "forecaststars.h"
#include "cityinfodisplay.h"
#include "titlebar.h"
#include "citycarroussel.h"
#include "scrollbar.h"
#include "citylist.h"
#include "addcitytool.h"

#include <QDebug>

BootManager::BootManager(QObject *parent)
    : QObject(parent)
    , m_imagesLoaded(false)
{
}

void BootManager::run(const QStringList &locations)
{
    m_keys = locations;
    ForecastProvider::connectToResponseSignal(this, SLOT(forecastResponse(int, ForecastData)));
    foreach (const QString & city, m_keys) {
        m_requests.append(ForecastProvider::getForecast(city, true));
        m_data.append(ForecastData());
    }
    PixmapLoader::connectToOnIdleSignal(this, SLOT(pixmapLoaderIsIdle()));
    int count =  ForecastHungItem::loadImages();
    count += ForecastRain::loadImages();
    count += ForecastSnow::loadImages();
    count += ForecastStars::loadImages();
    count += CityInfoDisplay::loadImages();
    count += TitleBar::loadImages();
    count += CityCarroussel::loadImages();
    count += ScrollBar::loadImages();
    count += CityList::loadImages();
    count += AddCityTool::loadImages();

    m_imagesLoaded = count == 0;
    if (m_imagesLoaded)
        PixmapLoader::disconnectReceiver(this);
}

void BootManager::forecastResponse(int reqId, const ForecastData &forecast)
{
    if (m_requests.removeAll(reqId)) {
        int idx = m_keys.indexOf(forecast.key());
        if (idx >= 0)
            m_data[idx] = forecast;
    }
    if (m_requests.isEmpty()) {
        ForecastProvider::disconnectReceiver(this);
        if (m_imagesLoaded)
            emit ready();
    }
}

void BootManager::pixmapLoaderIsIdle()
{
    PixmapLoader::disconnectReceiver(this);
    m_imagesLoaded = true;
    if (m_requests.isEmpty())
        emit ready();
}
