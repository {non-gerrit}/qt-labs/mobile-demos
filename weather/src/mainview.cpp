/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "mainview.h"
#include "settings.h"

#include "pixmaploader.h"

// MainView

MainView::MainView(bool connected, QWidget *parent)
    : QGraphicsView(parent)
    , m_carroussel(0)
    , m_loading(0)
    , m_bootManager(0)
    , m_manager(0)
    , m_fakeContentScreen(0)
    , m_connected(connected)
{
    setScene(&m_scene);
    setFrameShape(QFrame::NoFrame);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform |
                   QPainter::TextAntialiasing);
    setWindowTitle("Weather");
    m_scene.setSceneRect(QRectF(QPointF(0.0, 0.0), Settings::windowSize()));
    resize(Settings::windowSize());
    m_scene.setBackgroundBrush(Qt::black);

    PixmapLoader::connectToOnIdleSignal(this, SLOT(pixmapLoaderIsIdle()));
    Loading::loadImages();
    if (!m_connected)
        FakeContentScreen::loadImages();
}

MainView::~MainView()
{
}

void MainView::pixmapLoaderIsIdle()
{
    PixmapLoader::disconnectReceiver(this);
    if (m_connected)
        startBoot();
    else {
        m_fakeContentScreen = new FakeContentScreen();
        connect(m_fakeContentScreen, SIGNAL(userAccepted()), this, SLOT(startBoot()));
        m_fakeContentScreen->setPos(0.0, 0.0);
        m_scene.addItem(m_fakeContentScreen);
    }
}

void MainView::startBoot()
{
    delete m_fakeContentScreen;
    m_fakeContentScreen = 0;
    m_loading = new Loading();
    m_loading->setPos((geometry().width() - m_loading->boundingRect().width()) / 2,
                      (geometry().height() - m_loading->boundingRect().height()) / 2);
    m_scene.addItem(m_loading);
    m_loading->start();
    m_bootManager = new BootManager(this);
    connect(m_bootManager, SIGNAL(ready()), this, SLOT(bootEnd()));
    m_bootManager->run(m_connected ? Settings::getCurrentCities() : Settings::getDemoCities());
}

void MainView::bootEnd()
{
    m_loading->deleteLater();
    m_loading = 0;
    showCarroussel();
    createCityManager();
    m_bootManager->deleteLater();
    m_bootManager = 0;
}

void MainView::showCarroussel()
{
    QList<ForecastData> data = m_bootManager->data();
    m_carroussel = new CityCarroussel();
    connect(m_carroussel, SIGNAL(cityNameClicked()), this, SLOT(cityNameClicked()));

    connect(this, SIGNAL(moveLeft()), m_carroussel, SLOT(moveLeft()));
    connect(this, SIGNAL(moveRight()), m_carroussel, SLOT(moveRight()));

    m_carroussel->update(data);
    m_carroussel->setPos(0.0, 0.0);
    m_carroussel->setZValue(0.0);
    m_scene.addItem(m_carroussel);
}

void MainView::createCityManager()
{
    m_manager = new CityManager(m_bootManager->data());
    m_manager->setPos(0.0, 0.0);
    m_manager->setZValue(1.0);
    m_manager->hide();

}

void MainView::cityNameClicked()
{
    if (!m_manager)
        return;
    m_carroussel->setActive(false);
    connect(m_manager, SIGNAL(terminated()), this, SLOT(closeCityManager()));
    m_scene.addItem(m_manager);
    m_manager->show();
    m_manager->showManager(m_carroussel->selected());
}

void MainView::closeCityManager()
{
    if (!m_manager)
        return;
    m_manager->hide();
    m_scene.removeItem(m_manager);
    disconnect(m_manager, SIGNAL(terminated()), this, SLOT(closeCityManager()));
    saveManagerList();
    m_carroussel->update(m_manager->forecastList());
    m_carroussel->setActive(true);
}

void MainView::saveManagerList()
{
    if (m_connected) {
        QStringList list;
        foreach(const ForecastData &data, m_manager->forecastList())
            list.append(data.key());
        Settings::setCurrentCities(list);
    }
}

void MainView::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Left)
        emit moveLeft();
    if (event->key() == Qt::Key_Right)
        emit moveRight();
    QGraphicsView::keyPressEvent(event);
}
