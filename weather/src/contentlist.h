/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CONTENTLIST_H
#define CONTENTLIST_H

#include <QList>
#include <QObject>
#include <QGraphicsObject>
#include <QAbstractAnimation>
#include <QPointer>

#define ITEM_TOP_PROPERTY_NAME  "top"

class ContentListItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_PROPERTY(qreal top READ getTop WRITE setTop);
    Q_INTERFACES(QGraphicsItem);
public:
    ContentListItem(QGraphicsItem *parent = 0);
    virtual qreal contentHeight() const = 0;
    QRectF boundingRect() const;

    virtual QAbstractAnimation *getShowAnimation() = 0;
    virtual QAbstractAnimation *getHideAnimation() = 0;

protected:
    void updateGeometry();
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
private:
    friend class ContentList;
    QRectF m_geometry;
    bool m_geometryReady;
    qreal getTop() { return pos().y(); }
    void setTop(qreal top) { setPos(pos().x(), top); }

};

class ContentListActivity;
class RemoveActivity;
class InsertActivity;

class ContentList : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem);
public:
    ContentList(QGraphicsItem *parent = 0);
    ContentList(QList<ContentListItem*> items, QGraphicsItem *parent = 0);
    ~ContentList();

    bool insertItem(int idx, ContentListItem*);
    bool addItem(ContentListItem *item) { return insertItem(m_items.count(), item); }
    bool removeItem(int idx);
    bool removeItem(ContentListItem* item) { return removeItem(getItemIndex(item)); }
    bool moveItem(int from, int to);
    bool moveItem(ContentListItem* item, int to) { return moveItem(getItemIndex(item), to); }
    void appendItems(QList<ContentListItem*> items);

    int itemCount() const { return m_items.count(); }
    ContentListItem* getItem(int idx) { return m_items.at(idx); }
    int getItemIndex(ContentListItem* item) { return m_items.indexOf(item); }

    bool busy() { return m_queue.count() > 0; }
    void addActivity(ContentListActivity *);

    void updateItems();

    QRectF boundingRect() const;
    qreal width() const;
    void setWidth(qreal width);

signals:
    void newContentItem(QGraphicsItem *item);
    void contentItemRemoved(QGraphicsItem *item);

protected:
    virtual QAbstractAnimation *getInsertAnimation(int idx, qreal height) = 0;
    virtual QAbstractAnimation *getRemoveAnimation(int idx) = 0;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    QList<ContentListItem*> getItems() { return m_items; }

    typedef bool (*ContentListItemCompare)(const ContentListItem* s1, const ContentListItem* s2);
    void sortItems(ContentListItemCompare compare);

private:
    friend class ContentListActivity;
    friend class RemoveActivity;
    friend class InsertActivity;
    friend class SortActivity;
    friend class AppendItemsActivity;
    QList<ContentListItem*> m_items;
    QList<ContentListActivity*> m_queue;

    QRectF m_boundingRect;

    void updateGeometry();

    void checkQueue();
    void activityEnd();
    void doRemoveItem(int idx, bool notify);
    void doInsertItem(int idx, ContentListItem* item, bool notify);
    void doAppendItems(QList<ContentListItem*> items, bool notify);

};

class ContentListActivity : public QObject
{
    Q_OBJECT
public:
    ContentListActivity(ContentList &list) : m_list(list), m_insertPos(0) {}
    virtual bool run() = 0;
    virtual bool active();

protected:
    ContentList &m_list;
    void addActivity(ContentListActivity*);

protected slots:
    void activityEnd() { m_list.activityEnd(); }

private:
    int m_insertPos;

};

class SignalActivity : public ContentListActivity
{
    Q_OBJECT
public:
    SignalActivity(ContentList &list) : ContentListActivity(list) {}
    bool run();

signals:
    void notify();
};

class SortActivity : public ContentListActivity
{
    Q_OBJECT
public:
    SortActivity(ContentList &list, ContentList::ContentListItemCompare compare)
        : ContentListActivity(list), m_compare(compare) {}
    bool run();

private:
    ContentList::ContentListItemCompare m_compare;
};

class AppendItemsActivity : public ContentListActivity
{
    Q_OBJECT
public:
    AppendItemsActivity(ContentList &list, QList<ContentListItem*> items)
        : ContentListActivity(list), m_items(items) {}
    bool run();

private:
    QList<ContentListItem*> m_items;
};

class AnimationActivity : public ContentListActivity
{
    Q_OBJECT
public:
    AnimationActivity(QAbstractAnimation *animation, ContentList &list);
    ~AnimationActivity();
    bool run();
    bool active() { return m_animation && m_animation->state() != QAbstractAnimation::Stopped; }

private slots:
    void animationEnd();

private:
    QAbstractAnimation *m_animation;

};

class RemoveActivity : public ContentListActivity
{
    Q_OBJECT
public:
    RemoveActivity(int idx, bool destroyItem, bool notify, ContentList &list);
    bool run();
    bool active() { return m_active; }

private slots:
    void hideEnd();

private:
    int m_idx;
    bool m_destroyItem;
    ContentListItem *m_item;
    bool m_active;
    const bool m_notify;

};

class InsertActivity : public ContentListActivity
{
    Q_OBJECT
public:
    InsertActivity(int idx, ContentListItem* item, bool notify, ContentList &list);
    bool run();
    bool active() { return m_active; }

private slots:
    void showItem();

private:
    int m_idx;
    QPointer<ContentListItem> m_item;
    bool m_active;
    const bool m_notify;

};

class MoveActivity : public ContentListActivity
{
public:
    MoveActivity(int from, int to, ContentList &list);
    bool run();

private:
     int m_from;
     int m_to;

};

#endif // CONTENTLIST_H
