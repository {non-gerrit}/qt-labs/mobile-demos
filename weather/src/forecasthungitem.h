/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef FORECASTHUNGITEM_H
#define FORECASTHUNGITEM_H

#include <QObject>
#include <QGraphicsItem>
#include <QAbstractAnimation>

class ForecastHungItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
    Q_PROPERTY(qreal picTop READ picTop WRITE setPicTop)
public:
    enum ItemType {
        Cloud1 = 0,
        Cloud2,
        Cloud3,
        CloudRain1,
        CloudRain2,
        CloudRain3,
        CloudStorm1,
        CloudStorm2,
        CloudStorm3,
        CloudTStorm1,
        CloudTStorm2,
        Sun,
        ColdSun,
        Moon,
        TypeCount
    };

    static int loadImages();

    ForecastHungItem(ItemType type, QGraphicsItem *parent = 0);
    QAbstractAnimation *getAnimation();

    qreal lineLenght() const;
    void setLineLenght(qreal lenght);
    void setLinePos(qreal linePos);

    QPointF picPos() const;
    void setPicPos(QPointF pos);
    void setPicPos(qreal x, qreal y) { setPicPos(QPointF(x, y)); }

    void reset();

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

private:
    QGraphicsPixmapItem *m_pic;
    QGraphicsPixmapItem *m_line;
    ItemType m_type;
    qreal m_targetPicTop;

    qreal picTop() const { return m_pic->pos().y(); }
    void setPicTop(qreal top) { m_pic->setPos(m_pic->pos().x(), top); }

};

#endif // FORECASTHUNGITEM_H
