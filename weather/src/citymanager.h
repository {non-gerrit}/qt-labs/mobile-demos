/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CITYMANAGER_H
#define CITYMANAGER_H

#include <QGraphicsItem>
#include <QGraphicsPixmapItem>

#include "addcitytool.h"
#include "citylist.h"

class CityManagerContent : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
    Q_PROPERTY(qreal top READ getTop WRITE setTop);
public:
    CityManagerContent(QList<ForecastData> &contentList, QGraphicsItem *parent = 0);
    void select(const QString &selected) { m_list->select(selected); }

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    qreal getHiddenTop();

private:
    const QRectF m_boundingRect;
    AddCityTool *m_addTool;
    CityList *m_list;

    qreal getTop() { return pos().y(); }
    void setTop(qreal top) { setPos(pos().x(), top); }

private slots:
    void forecastSelected(ForecastData data);

};

class CityManager : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    CityManager(QList<ForecastData> contentList, QGraphicsItem *parent = 0);
    void showManager(const QString &selected);

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    QList<ForecastData> forecastList() const { return m_contentList; }

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

signals:
    void terminated();

private:
    const QRectF m_boundingRect;
    CityManagerContent *m_content;
    QList<ForecastData> m_contentList;
    QPointer<QAbstractAnimation> m_animation;
    bool m_visible;

    void startAnimation(bool show);
};

#endif // CITYMANAGER_H
