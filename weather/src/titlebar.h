/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef TITLEBAR_H
#define TITLEBAR_H

#include <QGraphicsPixmapItem>
#include "pixmapbutton.h"

class CloseButton: public PixmapButton
{
public:
    CloseButton(QGraphicsItem *parent = 0);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

class TitleBar : public QGraphicsPixmapItem
{
public:
    TitleBar(QGraphicsItem *parent = 0);
    static int loadImages();
};

#endif // TITLEBAR_H
