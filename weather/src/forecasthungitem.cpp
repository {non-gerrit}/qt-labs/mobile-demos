/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "forecasthungitem.h"
#include "settings.h"
#include "pixmaploader.h"

#include <QPropertyAnimation>

#define LINE_NAME_SUFFIX    "_line"

typedef struct
{
    const char * const prefix;
    const qreal x;
    const qreal y;

    QString name() const { return prefix; }
    QString lineName() const { return name() + LINE_NAME_SUFFIX; }

    qreal scaledX() const { return Settings::scaleWidth(x); }
    qreal scaledY() const { return Settings::scaleHeight(y); }

    QPixmap pic() const { return PixmapLoader::getPic(name()); }
    QPixmap linePic() const { return PixmapLoader::getPic(lineName()); }

private:

} HungObjectData;

static HungObjectData HungItemsData[ForecastHungItem::TypeCount] = {
    {"cloud_1",         160.0,  3.0},   // Cloud1
    {"cloud_2",         131.0,  6.0},   // Cloud2
    {"cloud_3",         102.0,  11.0},  // Cloud3
    {"cloud_rain_1",    160.0,  3.0},   // CloudRain1
    {"cloud_rain_2",    131.0,  6.0},   // CloudRain2
    {"cloud_rain_3",    102.0,  11.0},  // CloudRain3
    {"cloud_storm_1",   160.0,  3.0},   // CloudStorm1
    {"cloud_storm_2",   131.0,  6.0},   // CloudStorm2
    {"cloud_storm_3",   102.0,  11.0},  // CloudStorm3
    {"cloud_tstorm_1",  160.0,  3.0},   // CloudTStorm1
    {"cloud_tstorm_2",  131.0,  6.0},   // CloudTStorm2
    {"sun",             198.0,  125.0}, // Sun
    {"cold_sun",        121.5,  65.0},  // ColdSun
    {"moon",            97.5,   18.0}   // Moon
};

// ForecastHungItem

ForecastHungItem::ForecastHungItem(ItemType type, QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_type(type)
    , m_targetPicTop(0)
{
    setFlag(QGraphicsItem::ItemHasNoContents, true);

    HungObjectData &data = HungItemsData[m_type];

    m_pic = new QGraphicsPixmapItem(data.pic(), this);
    m_line = new QGraphicsPixmapItem(data.linePic(), m_pic);

    m_pic->setPos(0.0, 0.0);

    m_line->setPos(data.scaledX(), data.scaledY() - m_line->boundingRect().bottom());
    reset();
}

int ForecastHungItem::loadImages()
{
    int result = 0;
    for (int i = 0; i < ForecastHungItem::TypeCount; ++i) {
        PixmapLoader::load(HungItemsData[i].name());
        PixmapLoader::load(HungItemsData[i].lineName());
        result += 2;
    }
    return result;
}

qreal ForecastHungItem::lineLenght() const
{
    return m_targetPicTop + HungItemsData[m_type].scaledY();
}

void ForecastHungItem::setLineLenght(qreal lenght)
{
    m_targetPicTop = lenght - HungItemsData[m_type].scaledY();
}

void ForecastHungItem::setLinePos(qreal linePos)
{
    setPos(linePos - HungItemsData[m_type].scaledX(), 0.0);
}

QPointF ForecastHungItem::picPos() const
{
    return QPointF(pos().x(), pos().y() + m_targetPicTop);
}

void ForecastHungItem::setPicPos(QPointF picPos)
{
    setPos(picPos.x(), 0.0);
    m_targetPicTop = picPos.y();
}

void ForecastHungItem::reset()
{
    setPicTop(-m_pic->boundingRect().height());
}

QRectF ForecastHungItem::boundingRect () const
{
    return QRectF(0.0, 0.0, m_pic->boundingRect().width(),
                  m_targetPicTop + m_pic->boundingRect().height());
}

void ForecastHungItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *opt, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(opt);
    Q_UNUSED(widget);
}

QAbstractAnimation *ForecastHungItem::getAnimation()
{
    QPropertyAnimation *result = new QPropertyAnimation(this, "picTop");
    result->setStartValue(-m_pic->boundingRect().height());
    result->setEndValue(m_targetPicTop);
    result->setEasingCurve(QEasingCurve::OutBack);
    result->setDuration(500);
    return result;
}
