/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CITYINFODISPLAY_H
#define CITYINFODISPLAY_H

#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QGraphicsSimpleTextItem>
#include <QGraphicsPixmapItem>
#include <QPen>
#include <QBrush>
#include <QFont>

class CurrentTemperatureDisplay
{
public:
    CurrentTemperatureDisplay();

    void setTemperature(int value);

    QPointF pos() const { return m_pos; }
    void setPos(QPointF pos) { m_pos = pos; }
    void setPos(qreal x, qreal y) { setPos(QPointF(x, y)); }

    QRectF boundingRect () const;
    void paint(QPainter *painter);
private:
    QPen m_pen;
    QBrush m_brush;
    QFont m_font;
    QPixmap m_sign;
    QPixmap m_unit;
    QString m_text;
    int m_value;
    QRectF m_boundingRect;
    QPointF m_pos;

    QPointF m_signPos;
    QPointF m_numberPos;
    QPointF m_unitPos;

};

class TemperatureBoundDisplay
{
public:
    TemperatureBoundDisplay(bool lowerBound);

    bool lowerBound() const { return m_lowerBound; }
    int value() const { return m_value; }
    void setValue(int value);

    QPointF pos() const { return m_pos; }
    void setPos(QPointF pos) { m_pos = pos; }
    void setPos(qreal x, qreal y) { setPos(QPointF(x, y)); }

    QRectF boundingRect () const;
    void paint(QPainter *painter);

private:
    QPen m_pen;
    QBrush m_brush;
    QFont m_font;
    QPixmap m_icon;

    QString m_text;
    int m_value;
    const bool m_lowerBound;
    QRectF m_boundingRect;
    QPointF m_pos;
};

class TemperatureDisplay
{
public:
    TemperatureDisplay();

    void setTemperature(int lowerBound, int upperBound, int current);

    QPointF pos() const { return m_pos; }
    void setPos(QPointF pos) { m_pos = pos; }
    void setPos(qreal x, qreal y) { setPos(QPointF(x, y)); }

    QRectF boundingRect () const;
    void paint(QPainter *painter);

private:
    QPixmap m_line;
    TemperatureBoundDisplay m_lowerBound;
    TemperatureBoundDisplay m_upperBound;
    CurrentTemperatureDisplay m_current;

    QRectF m_boundingRect;
    QPointF m_linePos;
    QPointF m_pos;

};

class CityInfoDisplay : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    CityInfoDisplay(QGraphicsItem *parent = 0);

    static int loadImages();

    void setTemperature(int lowerBound, int upperBound, int current);
    void setCityName(const QString &name);

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

signals:
    void nameClicked();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    QRectF m_boundingRect;
    TemperatureDisplay m_temperature;
    QPen m_pen;
    QBrush m_brush;
    QFont m_font;
    QString m_text;
    QPointF m_textPos;
    QRectF m_nameRect;

    void updateBoundingRect();

};

#endif // CITYINFODISPLAY_H
