/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "forecastsnow.h"
#include "settings.h"
#include "pixmaploader.h"

#include <QPainter>

typedef struct
{
    const char * const prefix;
    QString name() const { return prefix; }
    QPixmap pic() const { return PixmapLoader::getPic(name()); }

private:

} SnowFlaketData;

static const int SnowFlakeCount = 10;
static SnowFlaketData SnowFlakeArray[SnowFlakeCount] = {
    {"snow_flake"},
    {"snow_flake_01"},
    {"snow_flake_02"},
    {"snow_flake_03"},
    {"snow_flake_04"},
    {"snow_flake_05"},
    {"snow_flake_06"},
    {"snow_flake_07"},
    {"snow_flake_08"},
    {"snow_flake_09"},
};

static const qreal maxVerticalSpeed = 70.0;
static const qreal minVerticalSpeed = 30.0;
static const qreal mediumVerticalSpeed = (maxVerticalSpeed + minVerticalSpeed) / 2;

static const qreal maxHorizontalSpeed = 100.0;
static const qreal minHorizontalSpeed = 20.0;

static const qreal windSpeed = -50.0;

static const qreal speedDeviation = 0.15;

static const int deviation1 = 2;
static const int deviation2 = 31;

#define RANDOM_FACTOR   (qreal(qrand()) / qreal(RAND_MAX))

// ForecastSnow

ForecastSnow::SnowFlake::SnowFlake(int type, const QSizeF &bounds)
    : m_pixmap(SnowFlakeArray[type].pic())
    , m_bounds(bounds)
    , m_waitingTime(0)
{
    qreal x = minHorizontalSpeed + windSpeed +
              RANDOM_FACTOR * (maxHorizontalSpeed - minHorizontalSpeed);
    qreal y = minVerticalSpeed + RANDOM_FACTOR * (maxVerticalSpeed - minVerticalSpeed);
    m_speed = QPointF(x, y);
}

int ForecastSnow::loadImages()
{
    for (int i = 0; i < SnowFlakeCount;++i)
        PixmapLoader::load(SnowFlakeArray[i].name());
    return SnowFlakeCount;
}

static inline bool doDeviation(qreal &factor)
{
    if (qrand() % deviation2 > deviation1)
        return false;
    factor = 1.0 + ((qrand() % 2) ? speedDeviation : -speedDeviation);
    return true;
}

void ForecastSnow::SnowFlake::timerEvent(int interval_ms)
{
    if (m_waitingTime < 0) {
        m_waitingTime += interval_ms;
        return;
    }
    const qreal interval_s = qreal(interval_ms) / 1000.0;
    qreal x = m_pos.x() + m_speed.x() * interval_s;
    qreal y = m_pos.y() + m_speed.y() * interval_s;

    y = y >= m_bounds.height() ? -m_pixmap.height() : y;
    x = x <= -m_pixmap.width() ? m_bounds.width()
                               : x >= m_bounds.width() ? -m_pixmap.width() : x;

    m_pos = QPointF(x, y);

    qreal factor;
    if (doDeviation(factor))
        m_speed.setY(qMax(qMin(m_speed.y() * factor, maxVerticalSpeed), minVerticalSpeed));
    if (doDeviation(factor)) {
        qreal x = m_speed.x() - windSpeed;
        x = qMax(qMin(x * factor, maxHorizontalSpeed), minHorizontalSpeed) + windSpeed;
        m_speed.setX(x);
    }
}

void ForecastSnow::SnowFlake::paint(QPainter *painter)
{
    if (isVisible() && m_waitingTime >= 0)
        painter->drawPixmap(m_pos, m_pixmap);
}

ForecastSnow::ForecastSnow(int count, const QRectF &bounds, QGraphicsItem *parent)
    : m_bounds(bounds)
    , m_parent(parent)
    , m_visible(false)
{
    qsrand(QTime(0, 0).secsTo(QTime::currentTime()) * qrand());
    for (int i = 0; i < count; ++i)
        m_flakes.append(new SnowFlake(qrand() % 10, m_bounds.size()));
    updateFLakesPositions();
}

void ForecastSnow::updateFLakesPositions()
{
    qsrand(QTime(0, 0).secsTo(QTime::currentTime()) * qrand());

    int maxTime = -1000 * m_bounds.height() / mediumVerticalSpeed;

    foreach(SnowFlake *flake, m_flakes) {

        const qreal height = -flake->height();
        const qreal width = m_bounds.width() - flake->width();

        flake->setPos(width *RANDOM_FACTOR, height *RANDOM_FACTOR);
        flake->setWaitingTime(maxTime * RANDOM_FACTOR);
    }
}

void ForecastSnow::paint(QPainter *painter)
{
    if (!m_visible)
        return;

    painter->translate(m_bounds.topLeft());

    foreach(SnowFlake *flake, m_flakes)
        flake->paint(painter);

    painter->translate(QPointF(-m_bounds.left(), -m_bounds.top()));
}

void ForecastSnow::start()
{
    m_lastTick = QTime::currentTime();
    m_ticker.start(30, this);
    show();
}

void ForecastSnow::stop()
{
    m_ticker.stop();
}

void ForecastSnow::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    QTime now = QTime::currentTime();
    int interval = m_lastTick.msecsTo(now);
    m_lastTick = now;
    foreach(SnowFlake *flake, m_flakes)
        flake->timerEvent(interval);
    if (m_parent)
        m_parent->update(m_bounds);
}
