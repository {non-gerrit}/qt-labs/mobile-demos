/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef FORECASTPROVIDER_H
#define FORECASTPROVIDER_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QList>
#include <QMap>

#include "forecastdata.h"
#include "forecastsource.h"

class ForecastProvider : public QObject
{
    Q_OBJECT
public:
    static int getForecast(const QString &key, bool locationId);

    static void connectToResponseSignal(QObject *receiver, const char *method);
    static void disconnectReceiver(QObject *receiver);

    static void setForecastSource(ForecastSource *source);

signals:
    void forecastResponse(int reqId, const ForecastData &forecast);

private:

    ForecastSource *m_source;

    ForecastProvider();
    ~ForecastProvider();
    static ForecastProvider *instance();

};

#endif // FORECASTPROVIDER_H
