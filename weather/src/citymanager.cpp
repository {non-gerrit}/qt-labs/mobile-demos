/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "citymanager.h"
#include "settings.h"

#include <QGraphicsSceneMouseEvent>
#include <QPropertyAnimation>

static inline qreal getBottomVerticalPos(qreal &bottom, QGraphicsItem *item)
{
    const qreal result = bottom - item->boundingRect().bottom();
    bottom -= item->boundingRect().height();
    return result;
}

// CityManagerContent

CityManagerContent::CityManagerContent(QList<ForecastData> &contentList, QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_boundingRect(QPointF(0.0, 0.0), Settings::windowSize())
{
    setFlag(ItemHasNoContents);

    qreal bottom = m_boundingRect.bottom();
    m_addTool = new AddCityTool(contentList, this);
    m_addTool->setPos(0.0, getBottomVerticalPos(bottom, m_addTool));
    m_addTool->setZValue(10.0);

    m_list = new CityList(contentList, this);
    m_list->setPos(0.0, getBottomVerticalPos(bottom, m_list) + m_list->initialTop() + 1);
    m_list->setZValue(0.0);

    connect(m_addTool, SIGNAL(newForecast(ForecastData)),
            this, SLOT(forecastSelected(ForecastData)));

}

void CityManagerContent::forecastSelected(ForecastData data)
{
    m_list->addForecast(data);
}

QRectF CityManagerContent::boundingRect () const
{
    return m_boundingRect;
}

void CityManagerContent::paint(QPainter *painter,
                               const QStyleOptionGraphicsItem *opt, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(opt);
    Q_UNUSED(widget);
}

qreal CityManagerContent::getHiddenTop()
{
    return m_boundingRect.height() - m_list->pos().y() - 5.0;
}

// CityManager

CityManager::CityManager(QList<ForecastData> contentList, QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_boundingRect(QPointF(0.0, 0.0), Settings::windowSize())
    , m_contentList(contentList)
    , m_visible(false)
{
    m_content = new CityManagerContent(m_contentList, this);
    m_content->setPos(0.0, m_content->getHiddenTop());
}

QRectF CityManager::boundingRect () const
{
    return m_boundingRect;
}

void CityManager::paint(QPainter *painter, const QStyleOptionGraphicsItem *opt, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(opt);
    Q_UNUSED(widget);
}

void CityManager::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    event->accept();
    startAnimation(false);
}

void CityManager::showManager(const QString &selected)
{
    m_content->select(selected);
    startAnimation(true);
}

void CityManager::startAnimation(bool show)
{
    if (show == m_visible)
        return;

    m_visible = show;

    if (m_animation)
        m_animation->stop();

    QPropertyAnimation* animation = new QPropertyAnimation(m_content, "top");

    animation->setEasingCurve(QEasingCurve::OutExpo);
    animation->setEndValue(show ? 0.0 : m_content->getHiddenTop());
    animation->setDuration(500);
    m_animation = animation;
    if (!show)
        connect(animation, SIGNAL(finished()), this, SIGNAL(terminated()));
    m_animation->start(QAbstractAnimation::DeleteWhenStopped);

}
