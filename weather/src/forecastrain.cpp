/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "forecastrain.h"
#include "settings.h"
#include "pixmaploader.h"

typedef struct
{
    const char * const prefix;
    QString name() const { return prefix; }
    QPixmap pic() const { return PixmapLoader::getPic(name()); }

private:

} RainItemData;

static const int RainItemCount = 3;
static RainItemData RainItemArray[RainItemCount] = {
    {"rain_01"},
    {"rain_02"},
    {"rain_03"}
};

ForecastRain::ForecastRain(RainType type, QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_animation(this, "progress")
{
    for (int i = Light; i <= Heavy; ++i) {
        QGraphicsPixmapItem *item = new QGraphicsPixmapItem(RainItemArray[i].pic(), this);
        item->setPos(0.0, -20.0);
        item->hide();
        m_items.append(item);
    }
    m_animation.setStartValue(0.0);
    m_animation.setEndValue(3.0);
    m_animation.setDuration(type == Light ? 3000 : type == Medium ? 2000 : 1000);
    m_animation.setLoopCount(-1);
}

int ForecastRain::loadImages()
{
    for (int i = 0; i < RainItemCount;++i)
        PixmapLoader::load(RainItemArray[i].name());
    return RainItemCount;
}

void ForecastRain::start()
{
    switch (m_animation.state()) {
    case QAbstractAnimation::Stopped:
        m_animation.start();
        break;
    case QAbstractAnimation::Paused:
        m_animation.resume();
        break;
    default:
        break;
    }
}

QRectF ForecastRain::boundingRect () const
{
    return m_items.isEmpty() ? QRectF() : m_items[0]->boundingRect();
}

void ForecastRain::paint(QPainter *painter, const QStyleOptionGraphicsItem *opt, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(opt);
    Q_UNUSED(widget);
}

void ForecastRain::stop()
{
    if (m_items.count() > 1 && m_animation.state() == QAbstractAnimation::Running)
        m_animation.pause();
}

void ForecastRain::setProgress(qreal progress)
{
    m_progress = progress;
    for (int i = 0; i < m_items.count(); ++i) {

        qreal x = progress + i - 1;
        x = x < 0 ? 3.0 + x : x > 3.0 ? x - 3.0 : x;
        const qreal opacity =   x <= 1.0 ? x
                              : x <= 2.0 ? 2.0 - x
                              : 0.0;
        m_items[i]->setOpacity(opacity);
        if ((opacity != 0.0) != m_items[i]->isVisible())
            m_items[i]->setVisible(opacity != 0.0);
    }
}
