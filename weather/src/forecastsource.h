/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef FORECASTSOURCE_H
#define FORECASTSOURCE_H

#include <QObject>
#include "forecastdata.h"

class ForecastSource : public QObject
{
    Q_OBJECT
public:
    ForecastSource(QObject *parent = 0) : QObject(parent) {}
    virtual int getForecast(const QString &key, bool locationId) = 0;

signals:
    void forecastReceived(int id, const ForecastData &data);
};

#endif // FORECASTSOURCE_H
