/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef FORECASTSTARS_H
#define FORECASTSTARS_H

#include <QObject>
#include <QGraphicsItem>
#include <QAbstractAnimation>

class ForecastStars : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
    Q_PROPERTY(qreal progress READ progress WRITE setProgress)
public:
    ForecastStars(int count, QGraphicsItem *parent = 0);
    QAbstractAnimation *getAnimation();

    static int loadImages();

    QRectF rect() const;
    void setRect(QRectF rect);

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

private:
    class Star : public QGraphicsPixmapItem
    {
    public:
        Star(int type, QGraphicsItem *parent);
        const int starType;
    };

    qreal m_progress;
    QRectF m_boundingRect;
    QList<Star*> m_starts;

    void setProgress(qreal progress);
    qreal progress() const { return m_progress; }

    inline bool checkColision(const QRectF &rect1, qreal radius1,
                              const QRectF &rect2, qreal radius2);
    inline bool checkColision(Star *item1, Star *item2);
    inline bool checkColision(Star *star, const QList<Star*> &items);
    inline QPointF getRandomPos(const QRectF &border);
    void updateStarsPositions();

private slots:
    void animationStateChanged(QAbstractAnimation::State newState,
                               QAbstractAnimation::State oldState);
};

#endif // FORECASTSTARS_H
