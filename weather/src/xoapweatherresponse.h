/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef XOAPWEATHERRESPONSE_H
#define XOAPWEATHERRESPONSE_H

#include <QString>
#include <QStringList>
#include <QDomElement>
#include <QTime>
#include <QDate>
#include <QDateTime>

class XoapWeatherResponse
{
public:
    class Item
    {
    public:
        Item() : m_type(0) {}
        void read(QDomElement element);

        QStringList texts() const { return m_texts; }
        int type() const { return m_type; }
        QString id() const { return m_id; }

    private:
        QStringList m_texts;
        int m_type;
        QString m_id;

    };
    XoapWeatherResponse();
    void read(QDomElement element);
    void print();

    QString versionid() const { return m_version; }
    const QList<Item> &items() const { return m_list; }


private:
    QString m_version;
    QList<Item> m_list;
};

#endif // XOAPWEATHERRESPONSE_H
