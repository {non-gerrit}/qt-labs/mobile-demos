/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "fakecontentscreen.h"
#include "pixmaploader.h"
#include "settings.h"
#include "painttextitem.h"

#include <QPainter>
#include <QDebug>
#include <QCoreApplication>
#include <QGraphicsSceneMouseEvent>

#define SCREEN_FONT_SIZE    (Settings::scaleHeight(30.0))
#define BUTTON_FONT_SIZE    (Settings::scaleHeight(40.0))

#define LEFT_BUTTON_NAME    "button_softkey_right"
#define RIGHT_BUTTON_NAME   "button_softkey_left"

#define LEFT_PIXMAP         (PixmapLoader::getPic(LEFT_BUTTON_NAME))
#define RIGHT_PIXMAP        (PixmapLoader::getPic(RIGHT_BUTTON_NAME))

FakeContentScreen::FakeContentScreen(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_boundingRect(QPointF(0.0, 0.0), Settings::windowSize())
    , m_left(LEFT_PIXMAP)
    , m_right(RIGHT_PIXMAP)
{
}

QRectF FakeContentScreen::boundingRect() const
{
    return m_boundingRect;
}

void FakeContentScreen::paint(QPainter *painter,
                              const QStyleOptionGraphicsItem *opt, QWidget *widget)
{
    Q_UNUSED(opt);
    Q_UNUSED(widget);

    static TextPainter text1(SCREEN_FONT_SIZE, QColor(255, 255, 255, 255), "You have no internet");
    static TextPainter text2(SCREEN_FONT_SIZE, QColor(255, 255, 255, 255),
                             "connection. Do you want to");
    static TextPainter text3(SCREEN_FONT_SIZE, QColor(255, 255, 255, 255),
                             "continue to the off-line");
    static TextPainter text4(SCREEN_FONT_SIZE, QColor(255, 255, 255, 255), "weather demo?");

    static TextPainter left(BUTTON_FONT_SIZE, QColor(255, 255, 255, 255), "YES");
    static TextPainter right(BUTTON_FONT_SIZE, QColor(255, 255, 255, 255), "EXIT");

    static const int buttonTop = boundingRect().height() - m_left.height();
    static const int buttonLeft = boundingRect().width() - m_right.width();

    static bool firstTime = true;

    if (firstTime) {
        firstTime = false;
        const qreal top = (boundingRect().height() - 4 * text1.height() - m_left.height()) / 2;
        TextPainter::locateAtCenter(&text1, 0.0, top, boundingRect().width());
        TextPainter::locateAtCenter(&text2, 0.0, top + 1 * text1.height(), boundingRect().width());
        TextPainter::locateAtCenter(&text3, 0.0, top + 2 * text1.height(), boundingRect().width());
        TextPainter::locateAtCenter(&text4, 0.0, top + 3 * text1.height(), boundingRect().width());

        const qreal buttonTextTop = (m_left.height() - left.height()) / 2 + buttonTop;
        left.setPos(30, buttonTextTop);
        right.setPos(boundingRect().width() - right.width() - 30, buttonTextTop);

    }

    painter->fillRect(boundingRect(), QColor(7, 18, 23, 255));

    text1.paint(painter);
    text2.paint(painter);
    text3.paint(painter);
    text4.paint(painter);

    painter->drawPixmap(0, buttonTop, m_left);
    painter->drawPixmap(buttonLeft, buttonTop, m_right);

    left.paint(painter);
    right.paint(painter);

}

int FakeContentScreen::loadImages()
{
    PixmapLoader::load(LEFT_BUTTON_NAME);
    PixmapLoader::load(RIGHT_BUTTON_NAME);
    return 2;
}

void FakeContentScreen::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    static QRectF left(0.0, boundingRect().height() - m_left.height(),
                       m_left.width(), m_left.height());
    static QRectF right(boundingRect().width() - m_right.width(),
                        boundingRect().height() - m_left.height(),
                        m_right.width(), m_right.height());


    if (left.contains(event->pos()))
        emit userAccepted();
    else
        QCoreApplication::instance()->quit();

}
