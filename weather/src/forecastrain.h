/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef FORECASTRAIN_H
#define FORECASTRAIN_H

#include <QObject>
#include <QGraphicsItem>
#include <QPropertyAnimation>
#include <QGraphicsPixmapItem>

#include <QDebug>

class ForecastRain : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
    Q_PROPERTY(qreal progress READ progress WRITE setProgress)
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity)
public:
    enum RainType
    {
        Light = 0,
        Medium,
        Heavy
    };
    ForecastRain(RainType type, QGraphicsItem *parent = 0);

    static int loadImages();

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

public slots:
    void start();
    void stop();
    void show() { QGraphicsItem::show(); }

private:
    QList<QGraphicsPixmapItem*> m_items;
    QPropertyAnimation m_animation;
    qreal m_progress;

    void setProgress(qreal progress);
    qreal progress() const { return m_progress; }
};

#endif // FORECASTRAIN_H
