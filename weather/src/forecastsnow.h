/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef FORECASTSNOW_H
#define FORECASTSNOW_H

#include <QObject>
#include <QGraphicsItem>
#include <QBasicTimer>
#include <QTime>

class ForecastSnow : public QObject
{
    Q_OBJECT
public:
    ForecastSnow(int count, const QRectF &bounds, QGraphicsItem *parent);

    static int loadImages();

    void paint(QPainter *painter);

public slots:
    void start();
    void stop();
    void show() { m_visible = true; }
    void hide() { m_visible = false; }

protected:
    void timerEvent(QTimerEvent *event);

private:
    class SnowFlake
    {
    public:
        SnowFlake(int type, const QSizeF &bounds);
        void timerEvent(int interval_ms);

        qreal width() const { return m_pixmap.width(); }
        qreal height() const { return m_pixmap.height(); }

        void setPos(qreal x, qreal y) { m_pos = QPointF(x, y); }
        void setWaitingTime(int value) { m_waitingTime = value; }

        bool isVisible() const { return !m_pixmap.isNull(); }

        inline void paint(QPainter *painter);

    private:
        QPixmap m_pixmap;
        const QSizeF m_bounds;
        QPointF m_speed;
        QPointF m_pos;
        int m_waitingTime;
    };

    const QRectF m_bounds;
    QGraphicsItem *m_parent;
    QList<SnowFlake*> m_flakes;
    QBasicTimer m_ticker;
    QTime m_lastTick;

    qreal m_visible;

    void updateFLakesPositions();

};

#endif // FORECASTSNOW_H
