/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef __CARROUSSEL_H__
#define __CARROUSSEL_H__

#include <QList>
#include <QDebug>


template<class T> class Carroussel
{
public:
    Carroussel() : m_pos(0) {}
    ~Carroussel() {}
    int count() const { return m_list.count(); }
    int pos() const { return m_pos; }
    void move(int offset) { m_pos = getIndex(m_pos + offset); }
    int add(const T &item);
    void remove(int idx);
    T &operator[](int aIdx) { return m_list[getIndex(aIdx + m_pos)]; }
    const T &operator[](int aIdx) const { return m_list[getIndex(aIdx + m_pos)]; }
    void reset(const QList<T> &list, int newPos);
private:
    int m_pos;
    QList<T> m_list;
    int getIndex(int idx) const;
    int getCarrousselIndex(int idx);
};

class CarrousselGroup
{
public:
    inline ~CarrousselGroup();
    template<class T> void add(T *carroussel);
    inline void remove(void *carroussel);
    inline void move(int offset);

private:
    class BasicCarrosselHandler
    {
    public:
        virtual ~BasicCarrosselHandler() {}
        virtual void move(int offset) = 0;
        virtual bool isEqual(void *value) = 0;
    };
    template<class T> class CarrousselHandler : public BasicCarrosselHandler
    {
    public:
        CarrousselHandler(T *carroussel) : m_carroussel(carroussel) {}
        void move(int offset) { m_carroussel->move(offset); }
        bool isEqual(void *value) { return m_carroussel == value; }
    private:
        T *m_carroussel;
    };

    QList<BasicCarrosselHandler*> m_items;
    inline int find(void *value);
};

// Carroussel

template<class T> int Carroussel<T>::add(const T &item)
{
    int idx = m_list.count() - m_pos;
    m_list.append(item);
    return idx <= m_list.count() / 2 ? idx : idx - m_list.count();
}

template<class T> void Carroussel<T>::remove(int idx)
{
    idx = getIndex(m_pos + idx);
    m_list.removeAt(idx);
    m_pos = getIndex(idx < m_pos ? m_pos - 1 : m_pos);
}

template<class T> void Carroussel<T>::reset(const QList<T> &list, int newPos)
{
    m_list = list;
    m_pos = getIndex(newPos);
}

template<class T> int Carroussel<T>::getIndex(int idx) const
{
    if (m_list.count() == 0)
        return 0;
    idx %= m_list.count();
    return idx >= 0 ? idx : m_list.count() + idx;
}

// CarrousselGroup

CarrousselGroup::~CarrousselGroup()
{
    qDeleteAll(m_items);
}

template<class T> void CarrousselGroup::add(T *carroussel)
{
    if (find(carroussel) == -1)
        m_items.append(new CarrousselHandler<T>(carroussel));
}

void CarrousselGroup::remove(void *carroussel)
{
    int idx = find(carroussel);
    if (idx >= 0)
        delete m_items.takeAt(idx);
}

int CarrousselGroup::find(void *value)
{
    for (int i = 0; i < m_items.count(); ++i)
        if (m_items[i]->isEqual(value))
            return i;
    return -1;
}

void CarrousselGroup::move(int offset)
{
    foreach (BasicCarrosselHandler *handler, m_items)
        handler->move(offset);
}




#endif /* __CARROUSSEL_H__ */
