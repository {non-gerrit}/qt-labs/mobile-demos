/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef DEMOFORECASTSOURCE_H
#define DEMOFORECASTSOURCE_H

#include "forecastsource.h"
#include "yahooweatherresponse.h"

#include <QList>

class DemoForecastSource : public ForecastSource
{
    Q_OBJECT
public:
    DemoForecastSource(int waitTime, QObject *parent = 0);
    int getForecast(const QString &key, bool locationId);

private:

    class Request
    {
    public:
        int m_reqId;
        QString m_locId;
        QString m_cityQuery;
    };

    const int m_waitTime;
    QList<Request> m_requests;
    QList<ForecastData> m_list;
    int m_idSeq;

    void createContentList();
    QString findCity(const QString &name);

    int getId() { return ++m_idSeq; }

    int findByLocId(const QString &locId);
    int findByQuery(const QString &query);

private slots:
    void sendResponse();

};

#endif // DEMOFORECASTSOURCE_H
