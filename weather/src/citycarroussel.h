/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CITYCARROUSSEL_H
#define CITYCARROUSSEL_H

#include "forecast.h"
#include "forecastview.h"
#include "carroussel.h"
#include "gesturebox.h"
#include "cityinfodisplay.h"
#include "forecastdata.h"

#include <QSharedPointer>
#include <QWeakPointer>

class ForecastBackground : public QGraphicsPixmapItem
{
public:
    ForecastBackground(QGraphicsItem *parent = 0);
    Forecast::ForecastType forecast() const { return m_forecast; }
    bool night() const { return m_night; }

    void setForecast(Forecast::ForecastType forecast, bool night);
    void reset() { setForecast(Forecast::UnknownForecast, false); }

    void setReferencePos(qreal pos);
    void setDisplacement(qreal displacement);

    void addCompanion(QGraphicsItem *item);
    void removeCompanion(QGraphicsItem *item);

    QString description() const { return m_description; }

private:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
    Forecast::ForecastType m_forecast;
    bool m_night;
    qreal m_pos;
    qreal m_displacement;
    QList<QGraphicsItem*> m_companion;

    QGraphicsPixmapItem *m_effect;
    QString m_description;

};

class CityCarroussel;

class CityCarrousselGesture : public GestureBox
{
public:
    CityCarrousselGesture(CityCarroussel &carroussel, QGraphicsItem *parent = 0);
    void setActivate(bool active) { m_active = active; }
    void abort() { m_aborted = true; }

protected:
    void gestureMousePress(QPointF pos, bool &startGesture, bool &acceptClick);
    void gestureStart(QPointF pos);
    void gestureMove(QPointF pos, QPointF movement, QPointF speed);
    void gestureEnd(QPointF pos, QPointF speed);

private:
    CityCarroussel &m_carroussel;
    bool m_active;
    bool m_aborted;
    qreal m_startPoint;

};

class CityCarroussel : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
    Q_PROPERTY(qreal displacement READ displacement WRITE setDisplacement)
public:
    CityCarroussel(QGraphicsItem *parent = 0);
    ~CityCarroussel();
    void add(ForecastData item);

    void update(QList<ForecastData> items);

    static int loadImages();

    bool active() const { return m_active; }
    void setActive(bool value);

    QString selected() const { return m_data.count() > 0 ? m_data[0].key() : QString(); }

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

signals:
    void cityNameClicked();

public slots:
    void moveLeft() { move(-1); }
    void moveRight() { move(1); }

private:
    friend class CityCarrousselGesture;

    Carroussel<ForecastData> m_data;
    Carroussel<ForecastBackground*> m_background;
    Carroussel<CityInfoDisplay*> m_cityInfo;

    CarrousselGroup m_carroussel;
    ForecastView *m_view;
    qreal m_displacement;
    CityCarrousselGesture *m_gestureBox;
    const QRectF m_boundingRect;
    const qreal m_backgroundWidth;
    const qreal m_backgroundPos;
    const qreal m_transparencySize;
    const qreal m_distance;
    qreal m_positions[3];

    bool m_active;
    bool m_deleteAfterMove;

    void setGestureDisplacement(qreal displacement);
    void moveBack() { move(0); }
    void move(int direction, bool deleteLastItem = false);
    void moveEnd(int direction);

    void updateBackground(int idx);
    void updateMainItem();

    void setDisplacement(qreal displacement);
    qreal displacement() const { return m_displacement; }

private slots:
    void moveLeftEnd() { moveEnd(-1); }
    void moveRightEnd() { moveEnd(1); }
    void moveBackEnd() { moveEnd(0); }
};

#endif // CITYCARROUSSEL_H
