/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "loading.h"
#include "pixmaploader.h"

// Loading

struct ProgressImageData
{
    const char * const name;
    QPixmap pic() const { return PixmapLoader::getPic(name); }
    QRectF getRect()
    {
        QPixmap pixmap = pic();
        return QRectF(0.0, 0.0, pixmap.width(), pixmap.height());
    }
};

static const int ProgressImageCount = 25;
static ProgressImageData ProgressImageArray[ProgressImageCount] = {
    {"loading_1"},
    {"loading_2"},
    {"loading_3"},
    {"loading_4"},
    {"loading_5"},
    {"loading_6"},
    {"loading_7"},
    {"loading_8"},
    {"loading_9"},
    {"loading_10"},
    {"loading_11"},
    {"loading_12"},
    {"loading_13"},
    {"loading_14"},
    {"loading_15"},
    {"loading_16"},
    {"loading_17"},
    {"loading_18"},
    {"loading_19"},
    {"loading_20"},
    {"loading_21"},
    {"loading_22"},
    {"loading_23"},
    {"loading_24"},
    {"loading_25"}
};

void Loading::loadImages()
{
    for (int i = 0; i < ProgressImageCount; ++i)
        PixmapLoader::load(ProgressImageArray[i].name);
}

Loading::Loading(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_image(0)
    , m_boundingRect(ProgressImageArray[m_image].getRect())
{
}

QRectF Loading::boundingRect () const
{
    return m_boundingRect;
}

void Loading::paint(QPainter *painter, const QStyleOptionGraphicsItem *opt, QWidget *widget)
{
    Q_UNUSED(opt);
    Q_UNUSED(widget);
    painter->drawPixmap(0, 0, ProgressImageArray[m_image].pic());
}

void Loading::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    ++m_image;
    m_image %= ProgressImageCount;
    update();
}
