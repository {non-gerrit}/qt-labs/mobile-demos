/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "pixmapbutton.h"

// PixmapButton

PixmapButton::PixmapButton(qreal minSize, const QPixmap &pixmap, QGraphicsItem *parent)
        : QGraphicsPixmapItem(pixmap, parent)
        , m_minSize(minSize)
{
    setShapeMode(BoundingRectShape);
}

void PixmapButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    emit clicked();
}

QPainterPath PixmapButton::shape() const
{
    return QGraphicsItem::shape();
}

QRectF PixmapButton::boundingRect() const
{
    QRectF result(QGraphicsPixmapItem::boundingRect());
    qreal hMargin = m_minSize < result.width() ? 0 : (m_minSize - result.width()) / 2;
    qreal vMargin = m_minSize < result.height() ? 0 : (m_minSize - result.height()) / 2;
    result.adjust(-hMargin, -vMargin, hMargin, vMargin);
    return result;
}
