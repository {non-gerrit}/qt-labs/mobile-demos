/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef PAINTTEXTITEM_H
#define PAINTTEXTITEM_H

#include <QFont>
#include <QPen>
#include <QBrush>
#include <QFontMetrics>
#include <QGraphicsItem>

class TextPainter
{
public:
    TextPainter(int fontSize, QColor color, const QString &text);
    QFont &font() { return m_font; }
    QPen &pen() { return m_pen; }
    QBrush &brush() { return m_brush; }

    const QString text() const { return m_text; }
    void setText(const QString &text) { m_text = text; }

    QPointF pos() const { return m_pos; }
    void setPos(QPointF pos) { m_pos = pos; }
    void setPos(qreal x, qreal y) { setPos(QPointF(x, y)); }

    bool quoted() const { return m_quoted; }
    void setQuoted(bool quoted) { m_quoted = quoted; }

    int maxWidth() const { return m_maxWidth; }
    void setMaxWidth(int width) { m_maxWidth = width; }

    int width() const;
    int height() const { return QFontMetrics(m_font).height(); }

    void paint(QPainter *painter);

    static void locateAtCenter(QList<TextPainter*> items, qreal left, qreal top, qreal width);
    static void locateAtCenter(TextPainter *item, qreal left, qreal top, qreal width);

private:
    QFont m_font;
    QPen m_pen;
    QBrush m_brush;
    QString m_text;
    QPointF m_pos;
    bool m_quoted;
    int m_maxWidth;
};

#endif // PAINTTEXTITEM_H
