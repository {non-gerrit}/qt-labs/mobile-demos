/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "settings.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QTimer>
#include <QDebug>

#if defined(Q_OS_SYMBIAN)
#define SETTINGS_FILE   "c://data/weather.ini"
#elif defined(Q_WS_MAEMO_5)
#define SETTINGS_FILE   "/usr/share/applications/weather.ini"
#else
#define SETTINGS_FILE   "weather.ini"
#endif

Settings::Settings()
    : m_settings(SETTINGS_FILE, QSettings::IniFormat)
{
}

Settings *Settings::instance()
{
    static Settings * const result = new Settings();
    return result;
}

QSize Settings::windowSize()
{
#ifdef Q_OS_SYMBIAN
    static const QSize result(QApplication::desktop()->screenGeometry().size());
#else
    static const QSize result(instance()->m_settings.value("windowSize").toSize());
#endif
    return result;
}

bool Settings::scaledImages()
{
    QSize size(windowSize());
    return size.width() != 480 || size.height() != 864;
}

class CitySortHelper
{
public:
    CitySortHelper(const QMap<QString, int> &map) : m_map(map) {}
    bool operator()(const QString &city1, const QString &city2)
    {
        return m_map[city1] < m_map[city2];
    }

private:
    const QMap<QString, int> &m_map;
};

QStringList Settings::getCurrentCities()
{
    return getCities("Cities");
}

QStringList Settings::getDemoCities()
{
    return getCities("Demo");
}

QStringList Settings::getCities(const QString &type)
{
    Settings *obj = instance();

    obj->m_settings.beginGroup(type);

    QStringList result = obj->m_settings.allKeys();
    QMap<QString, int> map;
    foreach(const QString &key, result)
        map[key] = obj->m_settings.value(key).toInt();

    obj->m_settings.endGroup();

    qSort(result.begin(), result.end(), CitySortHelper(map));
    return result;
}

void Settings::setCurrentCities(QStringList cities)
{
    Settings *obj = instance();
    obj->m_settings.beginGroup("Cities");
    obj->m_settings.remove("");
    for (int i = 0; i < cities.count(); ++i)
        obj->m_settings.setValue(cities[i], i);
    obj->m_settings.endGroup();
    obj->m_settings.sync();
}
