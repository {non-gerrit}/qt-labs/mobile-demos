/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QPropertyAnimation>
#include <QDebug>

#include "scrollbar.h"
#include "pixmaploader.h"

ScrollBar::ScrollBar(QGraphicsItem* parent)
    : QGraphicsPixmapItem(PixmapLoader::getPic("scroll"), parent)
    , m_knob(PixmapLoader::getPic("scroll_knob"), this)
    , m_height(pixmap().height() - m_knob.pixmap().height())
    , m_value(0.0)
    , m_visible(false)
{
    hide();
    setOpacity(0.0);
    m_knob.setPos(0.0, 0.0);
    m_timer.setSingleShot(true);
    m_timer.setInterval(1000);

    connect(&m_timer, SIGNAL(timeout()), this, SLOT(timeout()));
}

int ScrollBar::loadImages()
{
    PixmapLoader::load("scroll");
    PixmapLoader::load("scroll_knob");
    return 2;
}
bool ScrollBar::startAnimation(bool show)
{
    if (show == m_visible)
        return false;

    m_visible = show;
    m_timer.stop();

    if (m_animation)
        m_animation->stop();

    QPropertyAnimation* animation = new QPropertyAnimation(this, "opacity");
    animation->setEasingCurve(QEasingCurve::OutExpo);
    animation->setDuration(500);
    animation->setEndValue(show ? 1.0 : 0.0);

    if (show)
        connect(animation, SIGNAL(finished()), &m_timer, SLOT(start()));
    else
        connect(animation, SIGNAL(finished()), this, SLOT(hideScrollBar()));

    m_animation = animation;
    animation->start(QAbstractAnimation::DeleteWhenStopped);
    return true;
}

void ScrollBar::setValue(qreal value)
{
    if (value < 0 || value > 1.0)
        return;
    m_value = value;
    m_knob.setPos(0, m_value * m_height);

    show();
    if (!startAnimation(true))
        m_timer.start();
}




