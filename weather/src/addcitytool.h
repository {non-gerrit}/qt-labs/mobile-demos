/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef ADDCITYTOOL_H
#define ADDCITYTOOL_H

#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QBasicTimer>
#include <QGraphicsSimpleTextItem>
#include <QPen>
#include <QBrush>
#include <QFont>
 #include <QGraphicsProxyWidget>
#include <QLineEdit>
#include <QList>

#include "pixmapbutton.h"
#include "forecastprovider.h"
#include "loading.h"
#include "forecastdata.h"

class AddCityScreen : public QGraphicsItem
{
public:
    AddCityScreen(const QRectF &boundingRect, QGraphicsItem *parent = 0);

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

private:
    const QRectF m_boundingRect;
};

class AddCityLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    AddCityLineEdit(QWidget *parent = 0);
    void editReset();
protected:
    void focusInEvent(QFocusEvent *event);
    void focusOutEvent(QFocusEvent *event);
private:
    bool m_clean;
private slots:
    void textEditedSlot(const QString & text);
};


class AddCityFirstScreen : public QObject, public AddCityScreen
{
    Q_OBJECT
public:
    AddCityFirstScreen(const QRectF &boundingRect, QGraphicsItem *parent = 0);
    void clean();

signals:
    void citySelected(const QString &city);

private:
    QGraphicsPixmapItem * const m_textBackground;
    PixmapButton * const m_button;
    QString m_text;
    QGraphicsProxyWidget *m_proxy;
    AddCityLineEdit *m_lineEdit;

private slots:
    void buttonClick();
};

class AddCitySearchScreen : public QObject , public AddCityScreen
{
    Q_OBJECT
public:
    AddCitySearchScreen(const QRectF &boundingRect, QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    void setCityName(const QString &name);
    void cancel() { reset(); }

signals:
    void forecastReceived(const ForecastData &forecast);
    void forecastRequestError(const QString &name);

private:
    QString m_city;
    int m_reqId;
    Loading *m_loading;

    void reset();

private slots:
    void forecastResponse(int reqId, const ForecastData &forecast);
};

class AddCityErrorScreen : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    enum ErrorType
    {
        NotFound,
        AlreadyInList
    };
    AddCityErrorScreen(QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    void setCityName(const QString &name, ErrorType type);

signals:
    void closed();

private:
    PixmapButton * const m_button;
    QString m_city;
    ErrorType m_type;

    void paintNotFound(QPainter *painter);
    void paintAlreadyInList(QPainter *painter);
};
class AddCityTool : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    AddCityTool(const QList<ForecastData> &content, QGraphicsItem *parent = 0);
    ~AddCityTool();
    void cancel();

    static int loadImages();

private:
    const QList<ForecastData> &m_content;
    AddCityFirstScreen * const m_firstScreen;
    AddCitySearchScreen * const m_SearchScreen;
    AddCityErrorScreen * const m_ErrorScreen;

    AddCityFirstScreen *createFirstScreen();
    AddCitySearchScreen *createSearchScreen();
    AddCityErrorScreen *createErrorScreen();

    void setCurrentScreen(QGraphicsItem *screen);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

signals:
    void newForecast(const ForecastData &forecast);

private slots:
    void errorScreenClosed();
    void forecastReceived(const ForecastData &forecast);
    void forecastRequestError(const QString &name);
    void citySelected(const QString &city);

};

#endif // ADDCITYTOOL_H
