/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef YAHOOWEATHERRESPONSE_H
#define YAHOOWEATHERRESPONSE_H

#include <QString>
#include <QDomElement>
#include <QTime>
#include <QDate>
#include <QDateTime>

class YahooWeatherResponse
{
public:
    enum TemperatureUnit { Fahrenheit, Celsius };
    enum DistanceUnit { Miles, Kilometers };
    enum PressureUnit { PoundsPerSquare, Millibars };
    enum SpeedUnit { MilesPerHour, KilometersPerHour };
    enum PressureState { Steady, Rising, Falling };

    class Location
    {
    public:
        Location() {};
        Location(QDomElement element);
        QString city() const { return m_city; }
        QString region() const { return m_region; }
        QString country() const { return m_country; }

    private:
        QString m_city;
        QString m_region;
        QString m_country;
    };

    class Units
    {
    public:
        Units() {};
        Units(QDomElement element);
        TemperatureUnit temperature() const { return m_temperature; }
        DistanceUnit distance() const { return m_distance; }
        PressureUnit pressure() const { return m_pressure; }
        SpeedUnit speed() const { return m_speed; }

    private:
        TemperatureUnit m_temperature;
        DistanceUnit m_distance;
        PressureUnit m_pressure;
        SpeedUnit m_speed;
    };

    class Wind
    {
    public:
        Wind() {};
        Wind(QDomElement element);
        int chill() const { return m_chill; }
        int direction() const { return m_direction; }
        int speed() const { return m_speed; }

    private:
        int m_chill;
        int m_direction;
        int m_speed;
    };

    class Atmosphere
    {
    public:
        Atmosphere() {};
        Atmosphere(QDomElement element);
        int humidity() const { return m_humidity; }
        float visibility() const { return m_visibility; }
        float pressure() const { return m_pressure; }
        PressureState pressureState() const { return m_pressureState; }

    private:
        int m_humidity;
        float m_visibility;
        float m_pressure;
        PressureState m_pressureState;
    };

    class Astronomy
    {
    public:
        Astronomy() {};
        Astronomy(QDomElement element);
        QTime sunrise() const { return m_sunrise; }
        QTime sunset() const { return m_sunset; }

    private:
        QTime m_sunrise;
        QTime m_sunset;
    };

    class Condition
    {
    public:
        Condition() {};
        Condition(QDomElement element);
        QString text() const { return m_text; }
        int code() const { return m_code; }
        int temperature() const { return m_temperature; }
        QDateTime date() const { return m_date; }

    private:
        QString m_text;
        int m_code;
        int m_temperature;
        QDateTime m_date;
    };

    class Forecast
    {
    public:
        Forecast() {};
        Forecast(QDomElement element);

        QDate date() const { return m_date; }
        int low() const { return m_low; }
        int high() const { return m_high; }
        QString text() const { return m_text; }
        int code() const { return m_code; }

    private:
        QDate m_date;
        int m_low;
        int m_high;
        QString m_text;
        int m_code;
    };

public:
    YahooWeatherResponse(const QString &locationCode, QDomElement element);

    QString locationCode() const { return m_locationCode; }

    Location location() const { return m_location; }
    Units units() const { return m_units; }
    Wind wind() const { return m_wind; }
    Atmosphere atmosphere() const { return m_atmosphere; }
    Astronomy astronomy() const { return m_astronomy; }
    Condition condition() const { return m_condition; }
    Forecast forecast(int idx) const { return m_forecast[idx]; }

    QDateTime buildDate() const { return m_buildDate; }
    int timeToLive() const { return m_timeToLive; }
    float latitude() const { return m_latitude; }
    float longitude() const { return m_longitude; }
    QDateTime pubDate() const { return m_pubDate; }

    void print();


private:
    QString m_locationCode;
    QDateTime m_buildDate;
    int m_timeToLive;
    float m_latitude;
    float m_longitude;
    QDateTime m_pubDate;

    Location m_location;
    Units m_units;
    Wind m_wind;
    Atmosphere m_atmosphere;
    Astronomy m_astronomy;
    Condition m_condition;
    Forecast m_forecast[2];

    void readItem(QDomElement item);

//    <guid isPermaLink="false">USCA1116_2009_11_08_18_56_PST</guid>





};

#endif // YAHOOWEATHERRESPONSE_H
