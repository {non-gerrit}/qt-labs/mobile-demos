/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef GESTUREBOX_H
#define GESTUREBOX_H

#include <QGraphicsItem>

class GestureBox : public QGraphicsItem
{
public:
    GestureBox(QGraphicsItem *parent = 0);
    ~GestureBox();

    QRectF rect() const;
    void setRect(QRectF);
    void setRect(qreal, qreal, qreal, qreal);
    QRectF boundingRect() const;

protected:
    virtual void gestureMousePress(QPointF pos, bool &startGesture, bool &acceptClick) = 0;
    virtual void gestureStart(QPointF pos) = 0;
    virtual void gestureMove(QPointF pos, QPointF movement, QPointF speed) = 0;
    virtual void gestureEnd(QPointF pos, QPointF speed) = 0;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
    friend class GestureData;
    friend class ViewData;
    QRectF m_boundingRect;

};

#endif // GESTUREBOX_H
