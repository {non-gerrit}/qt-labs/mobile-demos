/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef BOOTMANAGER_H
#define BOOTMANAGER_H

#include <QObject>
#include <QMap>
#include <QStringList>
#include "forecastdata.h"

class BootManager : public QObject
{
    Q_OBJECT
public:
    BootManager(QObject *parent = 0);
    void run(const QStringList &locations);
    QList<ForecastData> data() const { return m_data; }

signals:
    void ready();

private slots:
    void forecastResponse(int reqId, const ForecastData &forecast);
    void pixmapLoaderIsIdle();

private:
    QStringList m_keys;
    QList<ForecastData> m_data;
    QList<int> m_requests;
    bool m_imagesLoaded;
};

#endif // BOOTMANAGER_H
