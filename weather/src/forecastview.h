/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef FORECASTVIEW_H
#define FORECASTVIEW_H

#include <QObject>
#include <QGraphicsItem>
#include <QAbstractAnimation>

#include "forecasthungitem.h"
#include "forecastsnow.h"
#include "forecastrain.h"
#include "forecaststars.h"
#include "forecast.h"

class HungItemsManager
{
public:
    HungItemsManager(qreal minZ, qreal maxZ)
        : m_minZ(minZ)
        , m_maxZ(maxZ)
        , m_displacement(0.0)
        , m_sun(0)
        , m_sunPos(0) {}
    void setSun(ForecastHungItem *sun);
    ForecastHungItem *sun() { return m_sun; }
    void addItem(ForecastHungItem *item);
    QAbstractAnimation *getAnimation();
    void reset();
    void setElementsDisplacement(qreal displacement);
private:
    const qreal m_minZ;
    const qreal m_maxZ;
    qreal m_displacement;
    ForecastHungItem *m_sun;
    qreal m_sunPos;
    QList<ForecastHungItem*> m_items;
    QList<qreal> m_positions;

    void doSetDisplacement(ForecastHungItem *item, qreal max, qreal pos);
};

class EffectAnimation : public QPropertyAnimation
{
    Q_OBJECT
public:
signals:
    void started();
protected:
    void updateState(QAbstractAnimation::State oldState, QAbstractAnimation::State newState)
    {
        if (oldState == Stopped && newState == Running)
            emit started();
    }

private:
};

class ForecastView : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    ~ForecastView();
    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    Forecast::ForecastType viewType() const { return m_type; }
    bool night() const { return m_night; }

    void setElementsDisplacement(qreal displacement);

    void reset();
    QAbstractAnimation *getAnimation();

    static ForecastView *createView(Forecast::ForecastType type, bool night, QGraphicsItem *parent = 0);


protected:
    ForecastView(Forecast::ForecastType type, bool night, QGraphicsItem *parent = 0);

    void addHungItem(ForecastHungItem::ItemType type, const QPointF &itemPos,
                     int deviation, bool reference = false);
    void setMainHangItem(ForecastHungItem::ItemType);
    void addStars();

    void addSnow(int count);
    void addRain(ForecastRain::RainType);

private:
    const Forecast::ForecastType m_type;
    const bool m_night;
    const QRectF m_boundingRect;
    HungItemsManager m_hungManager;
    ForecastStars *m_stars;
    ForecastHungItem *m_reference;
    ForecastRain *m_rain;
    ForecastSnow *m_snow;

    QRectF getEffectRect();
    template<class T> QAbstractAnimation *createEffectAnimation(T *effect);

};

#endif // FORECASTVIEW_H
