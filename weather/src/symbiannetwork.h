/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef SYMBIAN_NETWORK_H
#define SYMBIAN_NETWORK_H

#include <es_sock.h>
#include <sys/socket.h>
#include <net/if.h>
#include <rconnmon.h>


#include <stdarg.h>
#include <e32std.h>

/*
void writeDebugMsg(const char* msg, ...)
{
	static char buffer[1024];

    va_list args;
    va_start( args, msg);
	vsprintf(buffer, msg, args);
    va_end( args);

	FILE *logfile;
	logfile = fopen("C:\\Data\\weather.log", "a");
	fprintf(logfile, "%s\n", buffer);
	fclose(logfile);
}
*/

QString qt_TDesC2QStringL(const TDesC& aDescriptor) 
{
#ifdef QT_NO_UNICODE
    return QString::fromLocal8Bit(aDescriptor.Ptr(), aDescriptor.Length());
#else
    return QString::fromUtf16(aDescriptor.Ptr(), aDescriptor.Length());
#endif
}

static bool useCurrentConnectionL(RConnectionMonitor &monitor)
{
    TUint count;
    TRequestStatus status;
	monitor.GetConnectionCount(count, status);
	User::WaitForRequest(status);
    User::LeaveIfError(status.Int());
	if (count <= 0)
		return false;
	
    TUint connId;
    TUint subConnCount;
    User::LeaveIfError(monitor.GetConnectionInfo(1, connId, subConnCount));
    TBuf<50> iapName;
    monitor.GetStringAttribute(connId, 0, KIAPName, iapName, status);
    User::WaitForRequest(status);
    User::LeaveIfError(status.Int());

    QString strIapName = qt_TDesC2QStringL(iapName);
    struct ifreq ifReq;
    strcpy(ifReq.ifr_name, strIapName.toLatin1().data());
    User::LeaveIfError(setdefaultif(&ifReq));
    return true;
}

static bool useCurrentConnectionL()
{
    RConnectionMonitor monitor;
    if (monitor.ConnectL() != KErrNone)
    	return false;
	CleanupClosePushL(monitor);
	bool result = useCurrentConnectionL(monitor);
    CleanupStack::PopAndDestroy();
    return result;
}

static bool useCurrentConnection()
{
	bool result = false;
	TRAPD(error, result = useCurrentConnectionL());
	return error == KErrNone && result;
}

static void createNewConnectionL()
{
    RSocketServ server;
    User::LeaveIfError(server.Connect());
    CleanupClosePushL(server);

    RConnection connection;
    User::LeaveIfError(connection.Open(server));
    CleanupClosePushL(connection);
    User::LeaveIfError(connection.Start());

    _LIT(KIapName, "IAP\\Name");
    TBuf8<50> iapName;
    User::LeaveIfError(connection.GetDesSetting(TPtrC(KIapName), iapName));
    connection.Stop();
    CleanupStack::PopAndDestroy(2);

    iapName.ZeroTerminate();
    struct ifreq ifReq;
    strcpy(ifReq.ifr_name, (char*)iapName.Ptr());
    User::LeaveIfError(setdefaultif(&ifReq));
}

static bool createNewConnection()
{
	TRAPD(error, createNewConnectionL());
	return error == KErrNone;
}

static bool connect()
{
	return useCurrentConnection() || createNewConnection();
}

#endif
