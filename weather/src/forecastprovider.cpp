/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "forecastprovider.h"
#include <QTime>
#include <QTimer>

#include "forecast.h"

ForecastProvider::ForecastProvider()
    : m_source(0)
{
}

ForecastProvider::~ForecastProvider()
{
}

int ForecastProvider::getForecast(const QString &key, bool locationId)
{
    ForecastProvider *obj = instance();
    if (obj->m_source)
        return obj->m_source->getForecast(key, locationId);
    return -1;
}

ForecastProvider *ForecastProvider::instance()
{
    static ForecastProvider * const result(new ForecastProvider());
    return result;
}


void ForecastProvider::setForecastSource(ForecastSource *source)
{
    ForecastProvider *obj = instance();
    if (obj->m_source) {
        disconnect(obj->m_source, SIGNAL(forecastReceived(int, ForecastData)),
                   obj, SIGNAL(forecastResponse(int, ForecastData)));
        obj->m_source->deleteLater();
    }
    obj->m_source = source;
    if (obj->m_source)
        connect(obj->m_source, SIGNAL(forecastReceived(int, ForecastData)),
                obj, SIGNAL(forecastResponse(int, ForecastData)), Qt::QueuedConnection);
}

void ForecastProvider::connectToResponseSignal(QObject *receiver, const char *method)
{
    QObject::connect(instance(), SIGNAL(forecastResponse(int, ForecastData)),
                     receiver, method, Qt::QueuedConnection);
}

void ForecastProvider::disconnectReceiver(QObject *receiver)
{
    instance()->disconnect(receiver);
}
