/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef SCROLLBAR_H
#define SCROLLBAR_H

#include <QTimer>
#include <QPointer>
#include <QGraphicsPixmapItem>
#include <QAbstractAnimation>

class ScrollBar: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity);
public:
    ScrollBar(QGraphicsItem* parent = 0);
    qreal value() const { return m_value; }
    void setValue(qreal);
    static int loadImages();

private slots:
    void timeout() { startAnimation(false); }
    void hideScrollBar() { hide(); }

private:
    QGraphicsPixmapItem m_knob;
    const qreal m_height;
    qreal m_value;
    QTimer m_timer;
    QPointer<QAbstractAnimation> m_animation;
    bool m_visible;

    bool startAnimation(bool show);
};

#endif /* SCROLLBAR_H */
