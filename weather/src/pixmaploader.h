/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef PIXMAPLOADER_H
#define PIXMAPLOADER_H

#include <QThread>
#include <QMutex>
#include <QMap>
#include <QStringList>
#include <QPixmap>
#include <QWaitCondition>

class PixmapLoader;

class PixmapLoaderThread : public QThread
{
    Q_OBJECT
public:
    PixmapLoaderThread(PixmapLoader *loader);
signals:
    void imageIsReady(const QString &name, QImage image);
protected:
    void run();
private:
    PixmapLoader * const m_loader;

};

class PixmapLoader : public QObject
{
    Q_OBJECT
public:
    static void load(const QString &name);
    static void connectToOnIdleSignal(QObject *receiver, const char *method);
    static void disconnectReceiver(QObject *receiver);
    static QPixmap getPic(const QString &name);

signals:
    void onIdle();

private:
    friend class PixmapLoaderThread;
    QMutex m_mutex;
    QWaitCondition m_condition;

    QStringList m_queue;
    QStringList m_currentImages;
    QMap<QString, QPixmap> m_store;
    PixmapLoaderThread *m_thread;

    PixmapLoader(QObject *parent = 0);
    static PixmapLoader *instance();

    void enqueue(const QString &name);
    QString doDequeue();
    QString dequeue();

private slots:
    void imageIsReady(const QString &name, QImage image);
};

#endif // PIXMAPLOADER_H
