/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPixmapCache>
#include <QSettings>
#include <QDebug>

#include "system.h"
#include "mainview.h"
#include "forecastdata.h"
#include "settings.h"

#include "forecastprovider.h"
#include "demoforecastsource.h"
#include "networkforecastsource.h"

#if defined (Q_OS_SYMBIAN)
#include "symbiannetwork.h"
#endif


int main(int argc, char **argv)
{
    qRegisterMetaType<ForecastData>("ForecastData");

    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("openBossa");
    QCoreApplication::setOrganizationDomain("openbossa.org");
    QCoreApplication::setApplicationName("weather");

#ifdef Q_OS_SYMBIAN
    const bool connected = connect();
#else
    const bool connected = true;
#endif

    if (connected)
        ForecastProvider::setForecastSource(new NetworkForecastSource());
    else
        ForecastProvider::setForecastSource(new DemoForecastSource(1500));

    MainView mainView(connected);

    System::setViewMode(&mainView, System::PortraitMode);

#if defined(Q_OS_SYMBIAN) || defined(Q_WS_MAEMO_5)
    mainView.showFullScreen();
#else
    mainView.show();
#endif

    return app.exec();
}
