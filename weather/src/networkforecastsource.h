/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef NETWORKFORECASTSOURCE_H
#define NETWORKFORECASTSOURCE_H

#include "forecastdata.h"
#include "forecastsource.h"
#include <QObject>
#include <QMap>
#include <QStringList>
#include <QNetworkAccessManager>

class LocationRequestManager : public QObject
{
    Q_OBJECT
public:
    LocationRequestManager(QObject *parent = 0);
    int addRequest(const QString &query);

signals:
    void newLocationId(int reqId, const QString &locId);
    void locationIdQueryError(int reqId, const QString &query);

private slots:
    void receiveResponse(QNetworkReply *reply);

private:
    QMap<QString, int> m_requests;
    QNetworkAccessManager m_network;

    QString readResponse(const QString &query, QNetworkReply *reply);
};

class ForecastRequestmanager : public QObject
{
    Q_OBJECT
public:
    ForecastRequestmanager(QObject *parent = 0);

    int addRequest(const QString &locId);

public slots:
    void addRequest(int reqId, const QString &locId);

signals:
    void newForecastResponse(int reqId, YahooWeatherResponse *forecast);
    void forecastResponseError(int reqId);

private slots:
    void receiveResponse(QNetworkReply *reply);

private:
    QMap<QString, QList<int> > m_requests;
    QNetworkAccessManager m_network;

    YahooWeatherResponse *readResponse(const QString &query, QNetworkReply *locId);
    void doAddRequest(int reqId, const QString &locId);
};

class NetworkForecastSource : public ForecastSource
{
    Q_OBJECT
public:
    NetworkForecastSource(QObject *parent = 0);
    ~NetworkForecastSource();
    int getForecast(const QString &key, bool locationId);

private:
    LocationRequestManager m_locationManager;
    ForecastRequestmanager m_forecastManager;

private slots:
    void newForecastResponse(int reqId, YahooWeatherResponse *forecast);
    void forecastResponseError(int reqId);
};

#endif // NETWORKFORECASTSOURCE_H
