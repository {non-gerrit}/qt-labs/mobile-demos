/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QGraphicsView>
#include <QKeyEvent>
#include <QGraphicsScene>

#include "titlebar.h"
#include "citycarroussel.h"
#include "forecastdata.h"
#include "bootmanager.h"
#include "loading.h"
#include "citymanager.h"
#include "fakecontentscreen.h"

class MainView: public QGraphicsView
{
    Q_OBJECT

public:
    MainView(bool connected, QWidget *parent = 0);
    ~MainView();

protected:
    void keyPressEvent(QKeyEvent* event);

signals:
    void moveLeft();
    void moveRight();

private slots:
    void cityNameClicked();
    void closeCityManager();

    void pixmapLoaderIsIdle();
    void bootEnd();
    void startBoot();

private:
    QGraphicsScene m_scene;
    CityCarroussel *m_carroussel;
    Loading *m_loading;
    BootManager *m_bootManager;
    CityManager *m_manager;
    FakeContentScreen *m_fakeContentScreen;
    const bool m_connected;

    void showCarroussel();
    void createCityManager();
    void saveManagerList();
};

#endif // MAINVIEW_H

