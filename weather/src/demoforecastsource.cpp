/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "demoforecastsource.h"
#include "forecast.h"

#include <QTimer>
#include <QFile>
#include <QDebug>

struct CityInfo
{
    const char * name;
    const int lower;
    const int temp;
    const int upper;
    const bool night;
};

static const CityInfo cityNames[Forecast::UnknownForecast] = {
    {"Araxa", -7, -4, -1, true}, // MostlyCloudy
    {"Belo Horizonte", 18, 22, 27, false}, // Cloudy
    {"Recife", 25, 31, 34, true}, // MostlySunny
    {"Ipojuca", 22, 29, 36, false}, // PartlyCloudy
    {"Fortaleza", 31, 34, 36, true}, // Sunny
    {"Sao Paulo", -1, 10, 12, false}, // Flurries
    {"Uberlandia", -26, -23, -20, false}, // Fog
    {"Governador Valadares", 3, 17, 18, true}, // Haze
    {"Brasilia", 22, 25, 30, false}, // Sand
    {"Florianopolis", 0, 8, 10, true}, // Dust
    {"Gravata", -10, -2, 3, false}, // Icy
    {"Maceio", 22, 35, 38, true}, // Sleet
    {"Porto Alegre", -8, -6, -4, true}, // ChanceOfSleet
    {"Batatais", -20, -12, -10, false}, // Snow
    {"Ribeirao Preto", -20, -17, 0, true}, // ChanceOfSnow
    {"Campinas", 1, 4, 10, false}, // Mist
    {"Campo belo", 12, 19, 21, true}, // Rain
    {"Sertaozinho", 10, 15, 17, true}, // ChanceOfRain
    {"Pirassununga", -15, -12, 2, false}, // Storm
    {"Goiania", 7, 13, 18, true}, // ChanceOfStorm
    {"Manaus", 35, 41, 44, false}, // Thunderstorm
    {"Belem", -14, -13, -5, true} // ChanceOfThunderstorm
};

DemoForecastSource::DemoForecastSource(int waitTime, QObject *parent)
    : ForecastSource(parent)
    , m_waitTime(waitTime)
    , m_idSeq(0)
{
    createContentList();
}


int DemoForecastSource::findByLocId(const QString &locId)
{
    if (!locId.isEmpty())
        for (int i = 0; i < m_requests.count(); ++i)
            if (m_requests[i].m_locId == locId)
                return i;
    return -1;
}

int DemoForecastSource::findByQuery(const QString &query)
{
    if (!query.isEmpty())
        for (int i = 0; i < m_requests.count(); ++i)
            if (m_requests[i].m_cityQuery == query)
                return i;
    return -1;
}

QString DemoForecastSource::findCity(const QString &name)
{
    QString city = name.toUpper();
    for (int i = 0; i < m_list.count(); ++i) {
        if (city == m_list[i].cityName().toUpper())
            return m_list[i].key();
    }
    return QString();
}

int DemoForecastSource::getForecast(const QString &key, bool locationId)
{

    int idx = locationId ? findByLocId(key) : findByQuery(key);
    if (idx >= 0)
        return m_requests[idx].m_reqId;

    QString locId = locationId ? key : findCity(key);

    Request request;
    request.m_locId = locId;
    request.m_cityQuery = key;
    request.m_reqId = getId();
    m_requests.append(request);
    if (m_waitTime > 0)
        QTimer::singleShot(m_waitTime, this, SLOT(sendResponse()));
    else sendResponse();
    return request.m_reqId;
}

void DemoForecastSource::createContentList()
{
    QFile file(":fake_content.xml");
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "cannot open fake content file.";
        return;
    }
    QDomDocument doc;
    QString msg;
    if (!doc.setContent(&file, &msg)) {
        qWarning() << "fake content parse error: " << msg;
        return;
    }
    if (doc.documentElement().childNodes().count() == 0) {
        qWarning() << "fake content is empty.";
        return;
    }
    QDomElement root = doc.documentElement();
    for (int i = 0; i < root.childNodes().count(); ++i) {
        if (root.childNodes().at(i).isElement()) {
            QDomElement element = root.childNodes().at(i).toElement();
            QString id = element.attribute("locId");
            if (!id.isEmpty() && element.childNodes().count() == 1 &&
                element.childNodes().at(0).isElement()) {
                QDomElement child = element.childNodes().at(0).toElement();
                m_list.append(ForecastData(new YahooWeatherResponse(id, child)));
            }
        }
    }
}

void DemoForecastSource::sendResponse()
{
    if (!m_requests.isEmpty()) {
        Request request = m_requests.takeFirst();
        for (int i = 0; i < m_list.count(); ++i) {
            if (m_list[i].key() == request.m_locId) {
                emit forecastReceived(request.m_reqId, m_list[i]);
                return;
            }
        }
        emit forecastReceived(request.m_reqId, ForecastData(0));
    }
}
