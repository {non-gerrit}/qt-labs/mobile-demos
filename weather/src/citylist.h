/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef CITYLIST_H
#define CITYLIST_H

#include <QBasicTimer>
#include <QTime>
#include <QGraphicsItem>

#include "gesturebox.h"
#include "contentlist.h"
#include "forecastdata.h"
#include "pixmapbutton.h"
#include "scrollbar.h"
#include "painttextitem.h"

// CityListScrollBox

class CityListScrollBox : public QObject, public GestureBox
{
    Q_OBJECT
public:
    CityListScrollBox(QGraphicsItem *content, QGraphicsItem *parent = 0);

signals:
    void updated(qreal, qreal, qreal);

protected:
    void gestureMousePress(QPointF pos, bool &startGesture, bool &acceptClick);
    void gestureStart(QPointF pos);
    void gestureMove(QPointF pos, QPointF movement, QPointF speed);
    void gestureEnd(QPointF pos, QPointF speed);

    void timerEvent(QTimerEvent *event);

private:
    QGraphicsItem * const m_content;
    QBasicTimer m_ticker;
    qreal m_speed;
    qreal m_acceleration;
    QTime m_time;
    QPointF m_startPoint;
    ScrollBar *m_scroll;
    bool move(QPointF movement);
    void contentPositionUpdated();
};

// CityListText

class CityListText : public QGraphicsItem
{
public:
    CityListText(const QString &text, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

private:
    TextPainter m_painter;

};

// CityListItem

class CityContentList;

class CityListItem : public ContentListItem
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity);
public:
    CityListItem(const ForecastData &data, CityContentList *list, bool deleteVisible);
    void select(bool selected);

    const ForecastData &data() const { return m_data; }

    qreal contentHeight() const;
    QAbstractAnimation *getShowAnimation();
    QAbstractAnimation *getHideAnimation();

    QAbstractAnimation *getButtonAnimation(bool hide);

private slots:
    void removeFromList();

private:
    const qreal m_height;
    const ForecastData m_data;
    CityContentList * const m_list;

    QGraphicsPixmapItem * const m_background;
    QGraphicsPixmapItem * const m_check;
    PixmapButton * const m_delete;
    CityListText * const m_text;

    template<class T> static QAbstractAnimation *getFadeAnimation(T *target, bool hide, int msecs);

private slots:
    void doHide() { hide(); }

};

// CityContentList

class CityContentList : public ContentList
{
    Q_OBJECT
    Q_PROPERTY(qreal top READ getTop WRITE setTop);
public:
    CityContentList(QList<ForecastData> &contentList, QObject *holder);
    void addForecast(const ForecastData &data);
    void select(const QString &selected);

protected:
    QAbstractAnimation *getInsertAnimation(int idx, qreal height);
    QAbstractAnimation *getRemoveAnimation(int idx);

private:
    QObject * const m_holder;
    QList<ForecastData> &m_contentList;

    QAbstractAnimation *createCompoundAnimation(QList<QAbstractAnimation*> list);
    QAbstractAnimation *getItemMoveAnimation(int idx, qreal offset);
    QAbstractAnimation *getMoveAnimation(QObject *object, qreal offset);

    qreal getTop() { return pos().y(); }
    void setTop(qreal top) { setPos(pos().x(), top); }

};

// CityList

class CityList : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_PROPERTY(qreal top READ getTop WRITE setTop);
    Q_INTERFACES(QGraphicsItem);
public:
    CityList(QList<ForecastData> &contentList, QGraphicsItem *parent = 0);
    void select(const QString &selected) { m_list->select(selected); }

    QRectF boundingRect () const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    static int loadImages();

    qreal initialTop() const;

public slots:
    void addForecast(const ForecastData &data) { m_list->addForecast(data); }

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    const qreal m_itemHeight;
    const qreal m_topHeight;
    const QRectF m_boundingRect;
    const QRectF m_paintRect;

    inline QRectF getBoundingRect();
    inline QRectF getPaintRect();

    QGraphicsPixmapItem * const m_top;
    CityContentList * const m_list;
    CityListScrollBox * const m_scrollBox;

    qreal getTop() { return pos().y(); }
    void setTop(qreal top) { setPos(pos().x(), top); }
};

#endif // CITYLIST_H
