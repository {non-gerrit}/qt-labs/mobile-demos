/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#include "forecastdata.h"

struct ConditionTypeData
{
    const int id;
    Forecast::ForecastType type;
};

static const int ContitionTypeCount = 47;
static const ConditionTypeData ContitionType[ContitionTypeCount] = {
    {1, Forecast::Storm},
    {2, Forecast::Thunderstorm},
    {3, Forecast::Thunderstorm},
    {4, Forecast::Thunderstorm},
    {5, Forecast::Sleet},
    {6, Forecast::Rain},
    {7, Forecast::Sleet},
    {8, Forecast::Mist},
    {9, Forecast::Mist},
    {10, Forecast::Rain},
    {11, Forecast::Rain},
    {12, Forecast::Rain},
    {13, Forecast::Flurries},
    {14, Forecast::Flurries},
    {15, Forecast::Snow},
    {16, Forecast::Snow},
    {17, Forecast::Snow},
    {18, Forecast::Sleet},
    {19, Forecast::Haze},
    {20, Forecast::Fog},
    {21, Forecast::Haze},
    {22, Forecast::Fog},
    {23, Forecast::Cloudy},
    {24, Forecast::Cloudy},
    {25, Forecast::Cloudy},
    {26, Forecast::Cloudy},
    {27, Forecast::MostlyCloudy},
    {28, Forecast::MostlyCloudy},
    {29, Forecast::PartlyCloudy},
    {30, Forecast::PartlyCloudy},
    {31, Forecast::Sunny},
    {32, Forecast::Sunny},
    {33, Forecast::MostlySunny},
    {34, Forecast::MostlySunny},
    {35, Forecast::Sleet},
    {36, Forecast::Sunny},
    {37, Forecast::Thunderstorm},
    {38, Forecast::Thunderstorm},
    {39, Forecast::Thunderstorm},
    {40, Forecast::Storm},
    {41, Forecast::Snow},
    {42, Forecast::Sleet},
    {43, Forecast::Snow},
    {44, Forecast::PartlyCloudy},
    {45, Forecast::Thunderstorm},
    {46, Forecast::Sleet},
    {47, Forecast::Thunderstorm}
};


Forecast::ForecastType ForecastData::type() const
{
    if (m_data) {
        for (int i = 0; i < ContitionTypeCount; ++i)
            if (ContitionType[i].id == m_data->condition().code())
                return ContitionType[i].type;
    }
    return Forecast::UnknownForecast;
}

bool ForecastData::night() const
{
    if (!m_data)
        return false;
    return m_data->condition().date().time() < m_data->astronomy().sunrise()
        || m_data->condition().date().time() > m_data->astronomy().sunset();
}

