TEMPLATE = app

QT += network xml

TARGET = weather
target.path = $$PREFIX/bin
INSTALLS += target

INSTALLS    += desktop
desktop.path  = /usr/share/applications/hildon
desktop.files  = data/weather.desktop

INSTALLS    += icon64
icon64.path  = /usr/share/icons/hicolor/64x64/apps
icon64.files  = data/icon-weather.png

INSTALLS    += inifile
inifile.path  = /usr/share/applications/
inifile.files  = data/weather.ini

RESOURCES += resources.qrc

OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build
DESTDIR = build
VPATH += src

include(../shared/shared.pri)

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.5

symbian {
    LIBS += -lesock \
        -lconnmon \
        -lcone \
        -lavkon
    addFiles.sources = data/weather.ini
    addFiles.path = c:\data
    DEPLOYMENT += addFiles
    ICON = data/icon-weather.svg
    TARGET.CAPABILITY = NetworkServices
    TARGET.EPOCHEAPSIZE = 0x20000 \
        0x2000000
    TARGET.UID3 = 0xe1234567
}

HEADERS += mainview.h \
    settings.h \
    forecastview.h \
    forecasthungitem.h \
    forecaststars.h \
    forecastsnow.h \
    forecastrain.h \
    forecast.h \
    carroussel.h \
    citycarroussel.h \
    gesturebox_p.h \
    gesturebox.h \
    titlebar.h \
    cityinfodisplay.h \
    pixmaploader.h \
    loading.h \
    forecastprovider.h \
    forecastdata.h \
    bootmanager.h \
    pixmapbutton.h \
    scrollbar.h \
    contentlist.h \
    citylist.h \
    citymanager.h \
    addcitytool.h \
    painttextitem.h \
    forecastsource.h \
    demoforecastsource.h \
    networkforecastsource.h \
    yahooweatherresponse.h \
    xoapweatherresponse.h \
    symbiannetwork.h \
    fakecontentscreen.h
SOURCES += mainview.cpp \
    main.cpp \
    settings.cpp \
    forecastview.cpp \
    forecasthungitem.cpp \
    forecaststars.cpp \
    forecastsnow.cpp \
    forecastrain.cpp \
    citycarroussel.cpp \
    gesturebox.cpp \
    titlebar.cpp \
    cityinfodisplay.cpp \
    pixmaploader.cpp \
    loading.cpp \
    forecastprovider.cpp \
    bootmanager.cpp \
    pixmapbutton.cpp \
    scrollbar.cpp \
    contentlist.cpp \
    citylist.cpp \
    citymanager.cpp \
    addcitytool.cpp \
    painttextitem.cpp \
    demoforecastsource.cpp \
    networkforecastsource.cpp \
    yahooweatherresponse.cpp \
    xoapweatherresponse.cpp \
    forecastdata.cpp \
    fakecontentscreen.cpp

# Maemo 5
linux-g++-maemo5{
  # Targets for debian source and binary package creation
  debian-src.commands = dpkg-buildpackage -S -r -us -uc -d
  debian-bin.commands = dpkg-buildpackage -b -r -uc -d
  debian-all.depends = debian-src debian-bin


  # Clean all but Makefile
  compiler_clean.commands = -$(DEL_FILE) $(TARGET)

  QMAKE_EXTRA_TARGETS += debian-all debian-src debian-bin compiler_clean
}
