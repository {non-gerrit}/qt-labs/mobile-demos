/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "timelinemenu.h"

#include <QApplication>
#include <QDate>
#include <QGraphicsLinearLayout>
#include <QGraphicsSceneMouseEvent>

#include "pdata.h"
#include "button.h"
#include "dataresource.h"
#include "pixmaploader.h"
#include "coingraphicwidget.h"


TimelineMenu::TimelineMenu()
    : QGraphicsWidget(),
      contentsWidget(0)
{
    setFlag(QGraphicsItem::ItemHasNoContents);
    setFlag(QGraphicsItem::ItemClipsChildrenToShape);

    QGraphicsPixmapItem *background =
        new QGraphicsPixmapItem(PixmapLoader::pixmap(":/bg.png"), this);
    background->setPos(0, 0);

    setContentsMargins(0, 0, 0, 0);

    // This widget has the contents, that we will scroll on the
    // screen. TimelineMenu will clip it to the correct region.
    contentsWidget = new QGraphicsWidget(this);
    contentsWidget->setContentsMargins(0, 0, 0, 0);

    QGraphicsLinearLayout *l = new QGraphicsLinearLayout(Qt::Horizontal);
    l->setContentsMargins(0, 0, 0, 0);
    l->setSpacing(0);
    contentsWidget->setLayout(l);

    // This variable defines the area of the screen (counting from the
    // bottom) that is going to be used to drag the timeline itself
    draggingHeight = Resource::intValue("Widgets/TimelineMenu.draggingHeight");
    isDragging = false;

    updateMaxValue();

    db = qApp->property("settings").value<PData*>();
    connect(db, SIGNAL(valueAdded(const QString &)),
            SLOT(updateGraph(const QString &)));
    coin = new QPixmap(QLatin1String(":/coin.png"));

    fillWidget(l);
}

TimelineMenu::~TimelineMenu()
{
    delete coin;
}

qreal TimelineMenu::draggableHeight() const
{
    return draggingHeight;
}

void TimelineMenu::updateGraph(const QString &group)
{
    Q_UNUSED(group);
    updateMaxValue();

    for (int i = 0; i < allCoins.size(); i++) {
        CoinGraphicWidget *coins = allCoins.at(i);
        coins->updateValue(i + 1, overallSum, maxValue);
    }
}

void TimelineMenu::updateMaxValue()
{
    PData *db = qApp->property("settings").value<PData*>();
    const QDate &date = QDate::currentDate();

    db->beginGroup(QLatin1String("date"));
    db->beginGroup(QString::number(date.year()));

    const QStringList &keys = db->allKeys();

    qreal sum = 0;
    QList<qreal> values;
    foreach (const QString &key, keys) {
        const qreal value = db->getValue(key);
        sum += value;
        values << value;
    }

    qSort(values.begin(), values.end());
    maxValue = values.size() > 0 ? values.last() : 0;
    db->endGroup();
    db->endGroup();
    overallSum = sum;
}

void TimelineMenu::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->pos().y() <= (size().height() - draggingHeight))
        event->ignore();
    else
        isDragging = true;
}

void TimelineMenu::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (isDragging) {
        qreal x = contentsWidget->x() + event->pos().x() - event->lastPos().x();
        contentsWidget->setX(qBound<qreal>(-contentsWidget->size().width()
                                           + size().width(), x, 0));
    }
}

void TimelineMenu::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    isDragging = false;
}

void TimelineMenu::resizeEvent(QGraphicsSceneResizeEvent *event)
{
    QGraphicsWidget::resizeEvent(event);

    if (contentsWidget)
        contentsWidget->resize(contentsWidget->size().width(), size().height());
}

void TimelineMenu::fillWidget(QGraphicsLinearLayout *l)
{
    Button *button;
    CoinGraphicWidget *coins;
    QGraphicsLinearLayout *vl;

    QFont buttonFont("Nokia Sans");
    buttonFont.setPixelSize(Resource::intValue("Widgets/MonthButton.fontSize"));

    int year = QDate::currentDate().year();

    for (int i = 1; i <= 12; i++) {
        coins = new CoinGraphicWidget(i, overallSum, maxValue, coin);
        allCoins.append(coins);

        button = new Button(PixmapLoader::pixmap(":/btn_timeline_off.png"));
        button->setFont(buttonFont);
        button->setAcceptedMouseButtons(Qt::NoButton);
        button->setText(QDate(year, i, 1).toString("MMM/yy"));
        button->setCacheMode(QGraphicsItem::ItemCoordinateCache);

        vl = new QGraphicsLinearLayout(Qt::Vertical);
        vl->setSpacing(0);
        vl->setContentsMargins(0, 0, 0, 0);
        vl->addItem(coins);
        vl->setAlignment(coins, Qt::AlignHCenter);
        vl->addItem(button);

        l->addItem(vl);
        l->setAlignment(vl, Qt::AlignBottom);
    }
}
