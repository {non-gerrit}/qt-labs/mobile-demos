/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QGraphicsWidget>

struct Category
{
    Category() : value(0) { }

    QString name;
    qreal value;
    QColor color;
};

class BarChartGraphicsWidget : public QGraphicsWidget
{
    Q_OBJECT

    Q_PROPERTY(qreal stretch READ stretch WRITE setStretch);

public:
    BarChartGraphicsWidget();
    ~BarChartGraphicsWidget();

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                       QWidget *widget = 0);

    void addCategory(const QString &name, qreal value, const QColor &color = QColor());
    void addCategory(const char *name, qreal value, const QColor &color = QColor());
    void updateCategoryAt(int index, qreal value);
    void clear();

    void setScale(qreal scale);

    qreal stretch() const { return _stretch; }
    void setStretch(qreal stretch);

protected:
    QColor defaultColor(int i) const;

    qreal _stretch;
    qreal _scale;
    qreal _automaticScale;
    QList<Category *> _data;
};

inline void BarChartGraphicsWidget::addCategory(const char *name, qreal value,
                                                const QColor &color)
{
    addCategory(QString(name), value, color);
}
