/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "barchart.h"

#include <QPainter>

BarChartGraphicsWidget::BarChartGraphicsWidget() : QGraphicsWidget(), _stretch(1.0),
                                                   _scale(-1), _automaticScale(0)
{
    setCacheMode(ItemCoordinateCache);
    addCategory("Jan", 200);
    addCategory("Feb", 300);
    addCategory("Mar", 250);
    addCategory("Apr", 350);
    addCategory("May", 150);
}

BarChartGraphicsWidget::~BarChartGraphicsWidget()
{

}

void BarChartGraphicsWidget::addCategory(const QString &name, qreal value, const QColor &color)
{
    Category *category = new Category;
    category->name = name;
    category->value = value;
    category->color = color;
    _data << category;

    if (value > _automaticScale)
        _automaticScale = value;

    update();
}

void BarChartGraphicsWidget::clear()
{
    qDeleteAll(_data);
    _data.clear();
    _automaticScale = 0;
    update();
}

void BarChartGraphicsWidget::updateCategoryAt(int index, qreal value)
{
    if ((index < 0) || (index >= _data.count()))
        return;

    qreal oldValue = _data[index]->value;
    _data[index]->value = value;

    if (value > _automaticScale) {
        _automaticScale = value;
    } else if (oldValue == _automaticScale) {
        _automaticScale = 0;
        for (int i = 0; i < _data.count(); ++i) {
            if (_data[i]->value > _automaticScale) {
                _automaticScale = _data[i]->value;
            }
        }
    }

    update();
}

QColor BarChartGraphicsWidget::defaultColor(int i) const
{
    switch (i % 6) {
    case 0:
        return QColor("#004586");
    case 1:
        return QColor("#ff420e");
    case 2:
        return QColor("#ffd320");
    case 3:
        return QColor("#579d1c");
    case 4:
        return QColor("#ff950e");
    case 5:
        return QColor("#c5000b");
    default:
        return QColor();
    }
}

void BarChartGraphicsWidget::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                                   QWidget *widget)
{
    QGraphicsWidget::paint(painter, option, widget);
    painter->save();

    painter->drawLine(0, size().height(), size().width(), size().height());

    qreal barWidth = rect().width() / (2 * _data.count());
    qreal barScale = _stretch * rect().height() / (_scale > 0 ? _scale : _automaticScale);

    painter->setPen(Qt::NoPen);
    for (int i = 0; i < _data.count(); ++i) {
        qreal barSize = _data[i]->value * barScale;
        QColor barColor = _data[i]->color.isValid() ? _data[i]->color : defaultColor(i);
        QRectF barRect((2 * i + 0.5) * barWidth, rect().height() - barSize,
                       barWidth, barSize);

        QLinearGradient gradient(barRect.topRight(), barRect.bottomLeft());
        gradient.setColorAt(0, barColor);
        gradient.setColorAt(1, barColor.darker(150));

        painter->setBrush(gradient);
        painter->drawRect(barRect);
    }

    painter->restore();
}

void BarChartGraphicsWidget::setScale(qreal scale)
{
    if (_scale == scale)
        return;

    _scale = scale;
    update();
}

void BarChartGraphicsWidget::setStretch(qreal stretch)
{
    if (_stretch == stretch)
        return;

    _stretch = stretch;
    update();
}
