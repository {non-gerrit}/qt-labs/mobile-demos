/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QGraphicsLinearLayout>

#include "titlebar.h"
#include "pixmaploader.h"
#include "button.h"
#include "pixmapwidget.h"


TitleBar::TitleBar()
    : QGraphicsWidget()
{
    setFlag(QGraphicsItem::ItemHasNoContents);

    PixmapWidget *title =
        new PixmapWidget(PixmapLoader::pixmap(":/titlebar.png"));

    Button *button = new Button(PixmapLoader::pixmap(":/bt_close_off.png"),
                                PixmapLoader::pixmap(":/bt_close_on.png"));
    connect(button, SIGNAL(clicked()), SIGNAL(closeClicked()));

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout;
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addItem(title);
    layout->addItem(button);

    setLayout(layout);
}

TitleBar::~TitleBar()
{

}
