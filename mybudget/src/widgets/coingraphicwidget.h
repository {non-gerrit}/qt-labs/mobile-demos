/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef _COINWIDGET_H_
#define _COINWIDGET_H_

#include "pdata.h"

#include <QGraphicsWidget>

class CoinGraphicWidget : public QGraphicsWidget
{
public:
    CoinGraphicWidget(int month, qreal overallSum, qreal maxValue, QPixmap *coin);
    ~CoinGraphicWidget();

    void updateValue(int month, qreal overallSum, qreal maxValue);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

private:
    QList<int> offsets;
    int coins;

    QPixmap *coinPixmap;
    QPixmap labelLeft;
    QPixmap labelRight;
    QPixmap labelMiddle;
    QPixmap *graphCache;

    qreal percentValue;
    int labelOffset;
    QFont nokiaFont;
    int maxCoins;

    PData *db;

    int marginForCoins;

    void updateGraphCache(int coins);
};
#endif
