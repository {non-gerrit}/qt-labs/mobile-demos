/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef _HORIZONTALMENU_H_
#define _HORIZONTALMENU_H_

#include <QtCore>
#include <QtGui>

#include "pixmapwidget.h"
#include "pixmaploader.h"


class HorizontalMenu : public PixmapWidget
{
    Q_OBJECT

public:
    enum Option {
        Food,
        Car,
        House,
        Books,
        Clothes,
        Fun,
        Health,
        Travel,
        Overall
    };

    HorizontalMenu(bool hasOverallIcon = false);
    ~HorizontalMenu();

    QString selected() const;

    int count() const;
    void focusMenuItem(int index, bool animated = true);

signals:
    void optionChanged(QString option);

protected:
    void resizeEvent(QGraphicsSceneResizeEvent *event);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    int indexAtPosition(qreal x);
    void fillWidget(bool hasOverallIcon);

private:
    QGraphicsWidget *w;
    QGraphicsWidget *clipper;
    QGraphicsLinearLayout *l;
    QGraphicsRectItem *marker;
    int selectedIndex;
    int yOffset;
    int markOffset;
    int lightOffset;
    bool isSliding;
    int moveThreshold;
    PixmapWidget *light;
    PixmapWidget *markSelection;

    QSequentialAnimationGroup *sequentialAnimation;

    QPropertyAnimation *centralizeAnimation;
    QPropertyAnimation *fadeAnimation;

    int pressedX;

    QList<QPixmap> selectionPixmaps;

private Q_SLOTS:
    void centralizedStarted();
    void optionSelected();
};

class MenuIconWidget : public PixmapWidget
{
public:
    MenuIconWidget(HorizontalMenu::Option option)
        : PixmapWidget()
    {
        QString filename;
        switch (option) {
            case HorizontalMenu::Food:
                filename = ":/ico_food.png";
                break;
            case HorizontalMenu::Car:
                filename = ":/ico_car.png";
                break;
            case HorizontalMenu::House:
                filename = ":/ico_house.png";
                break;
            case HorizontalMenu::Books:
                filename = ":/ico_books.png";
                break;
            case HorizontalMenu::Clothes:
                filename = ":/ico_clothes.png";
                break;
            case HorizontalMenu::Fun:
                filename = ":/ico_fun.png";
                break;
            case HorizontalMenu::Health:
                filename = ":/ico_health.png";
                break;
            case HorizontalMenu::Travel:
                filename = ":/ico_travel.png";
                break;
            case HorizontalMenu::Overall:
                filename = ":/ico_overall.png";
                break;
            default:
                break;
        }
        setPixmap(PixmapLoader::pixmap(filename));
    }
};

#endif
