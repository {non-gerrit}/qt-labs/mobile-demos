/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "horizontalmenu.h"

#include "dataresource.h"
#include "pixmaploader.h"


static const char *optionKeys[] = {
    "food", "car", "house", "books", "clothes", "fun", "health",
    "travel", "overall"
};

HorizontalMenu::HorizontalMenu(bool hasOverallIcon)
    : PixmapWidget(PixmapLoader::pixmap(":/carrousel_bg.png")),
      selectedIndex(0),
      isSliding(false),
      moveThreshold(15)
{
    clipper = new QGraphicsWidget(this);
    clipper->resize(size());
    clipper->setPos(0, 0);
    clipper->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
    clipper->setZValue(2);

    w = new QGraphicsWidget(clipper);
    l = new QGraphicsLinearLayout(Qt::Horizontal);
    w->setLayout(l);

    markSelection = new PixmapWidget(0);
    marker = new QGraphicsRectItem(0, 0, 90, 90, this);

    fillWidget(hasOverallIcon);

    light = new PixmapWidget(Resource::stringValue("Widgets/HorizontalMenu.light"));
    yOffset = Resource::intValue("Widgets/HorizontalMenu.yOffset");
    markOffset = Resource::intValue("Widgets/HorizontalMenu.markOffset");
    lightOffset = Resource::intValue("Widgets/HorizontalMenu.lightOffset");

    light->setParentItem(this);
    light->setZValue(1);

    if (hasOverallIcon)
        w->moveBy(-2000, yOffset); // ###
    else
        w->moveBy(0, yOffset);


    centralizeAnimation = new QPropertyAnimation(w, "x", this);
    centralizeAnimation->setDuration(240);

    // Fade In Animation, from 0 to 1. We use the setDirection method to
    // produce fade out animation.
    fadeAnimation = new QPropertyAnimation(markSelection, "opacity", this);
    fadeAnimation->setStartValue(0.0);
    fadeAnimation->setEndValue(1.0);
    fadeAnimation->setDuration(220);

    // Sequential Animation = Centralize Animation + Fade In Animation
    sequentialAnimation = new QSequentialAnimationGroup(this);
    sequentialAnimation->addAnimation(centralizeAnimation);
    sequentialAnimation->addAnimation(fadeAnimation);

    connect(centralizeAnimation, SIGNAL(stateChanged(QAbstractAnimation::State,
                                                     QAbstractAnimation::State)),
            SLOT(centralizedStarted()));
    connect(centralizeAnimation, SIGNAL(finished()), SLOT(optionSelected()));
}

HorizontalMenu::~HorizontalMenu()
{

}

int HorizontalMenu::count() const
{
    return l->count();
}

void HorizontalMenu::optionSelected()
{
    emit optionChanged(selected());
}

QString HorizontalMenu::selected() const
{
    if (selectedIndex < 0)
        return QString();
    else
        return optionKeys[selectedIndex];
}

void HorizontalMenu::resizeEvent(QGraphicsSceneResizeEvent *event)
{
    PixmapWidget::resizeEvent(event);

    const int w = size().width();
    const int h = size().height();
    const QSizeF &itemSize = l->itemAt(0)->preferredSize();

    clipper->resize(w, h);

    marker->setRect(0, 0, itemSize.width(), h);
    marker->setPos(w / 2 - itemSize.width() / 2, 0);
    markSelection->setGeometry(0, 0, w, markOffset);
    light->setPos(0, lightOffset);

    // ajust selected item
    focusMenuItem(selectedIndex, false);
}

void HorizontalMenu::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    isSliding = false;
    pressedX = event->pos().x();

    // fade out
    fadeAnimation->setDirection(QAbstractAnimation::Backward);
    fadeAnimation->start();
}

void HorizontalMenu::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    const int ex = event->pos().x();

    if (qAbs(ex - pressedX) > moveThreshold)
        isSliding = true;

    if (isSliding) {
        const int mx = marker->pos().x();
        const int mw = marker->rect().width();
        const int fx = w->x() + ex - event->lastPos().x();

        w->setX(qBound<qreal>(mx + mw - w->size().width(), fx, mx));
    }
}

void HorizontalMenu::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    //fade in
    fadeAnimation->setDirection(QAbstractAnimation::Forward);

    if (isSliding) {
        const int index = indexAtPosition(size().width() / 2);
        isSliding = false;
        focusMenuItem(index >= 0 ? index : selectedIndex);
    } else {
        const int index = indexAtPosition(event->pos().x());
        if (index >= 0) {
            selectedIndex = index;
            focusMenuItem(selectedIndex);
        }
    }
}

void HorizontalMenu::fillWidget(bool hasOverallIcon)
{
    markSelection->setParentItem(this);
    markSelection->setCacheMode(NoCache);
    markSelection->setOpacity(0.0);
    markSelection->setPos(0, 0);

    marker->setPen(Qt::NoPen);

    MenuIconWidget *item;

    for (int i = Food; i <= Travel; i++) {
        item = new MenuIconWidget(Option(i));
        l->addItem(item);
        l->setAlignment(item, Qt::AlignCenter);

        selectionPixmaps << QPixmap(PixmapLoader::pixmap(QString(":/carrousel_bg_%1.png")
                                                  .arg(optionKeys[i])));
    }

    if (hasOverallIcon) {
        item = new MenuIconWidget(Overall);
        l->addItem(item);
        l->setAlignment(item, Qt::AlignCenter);

        selectionPixmaps << QPixmap(PixmapLoader::pixmap(":/carrousel_bg_overall.png"));
    }

    l->setContentsMargins(0, 0, 0, 0);
    w->setContentsMargins(0, 0, 0, 0);
    l->setSpacing(item->preferredSize().width() / 3);
}

void HorizontalMenu::centralizedStarted()
{
    markSelection->setPixmap(selectionPixmaps[selectedIndex]);
}

int HorizontalMenu::indexAtPosition(qreal x)
{
    int result = (x - w->x()) / (w->size().width() / l->count());
    return (result < 0 || result >= l->count()) ? -1 : result;
}

void HorizontalMenu::focusMenuItem(int index, bool animated)
{
    const qreal iw = marker->rect().width() + l->spacing();
    const qreal fx = marker->x() - iw * index;

    selectedIndex = index;

    if (!animated) {
        w->setX(fx);
        markSelection->setOpacity(1.0);
        markSelection->setPixmap(selectionPixmaps[selectedIndex]);
        optionSelected();
    } else {
        centralizeAnimation->setStartValue(w->x());
        centralizeAnimation->setEndValue(fx);
        sequentialAnimation->start();
    }
}


