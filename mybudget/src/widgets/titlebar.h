/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef TITLEBAR_H
#define TITLEBAR_H

#include <QGraphicsWidget>

class TitleBar : public QGraphicsWidget
{
    Q_OBJECT;

public:
    TitleBar();
    ~TitleBar();

signals:
    void closeClicked();
};

#endif
