/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QPainter>

#include "dataresource.h"
#include "listexpenses.h"
#include "pixmaploader.h"


ExpensesListViewItem::ExpensesListViewItem(QGraphicsItem *parent)
    : ListViewItem(parent),
      m_fontColor(QColor("#566868")),
      m_divisor(PixmapLoader::pixmap(":/divisor")),
      m_background(PixmapLoader::pixmap(":/List_white.png")),
      m_labelLeft(PixmapLoader::pixmap(":/Label_green_01.png")),
      m_labelMiddle(PixmapLoader::pixmap(":/Label_green_02.png")),
      m_labelRight(PixmapLoader::pixmap(":/Label_green_03.png"))
{
    m_font = QFont("Nokia Sans");
    m_font.setPixelSize(Resource::intValue("Widgets/ListExpenses.fontSize"));
    setPreferredSize(QSizeF(0, m_background.height()));
}

void ExpensesListViewItem::paint(QPainter *painter,
                                 const QStyleOptionGraphicsItem *option,
                                 QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setFont(m_font);
    painter->setPen(m_fontColor);

    painter->drawPixmap(0, 0, m_background);

    const int sw = size().width();
    const int sh = size().height();
    const int border = sw * 0.03;
    const int lrw = m_labelRight.width();
    const int lrh = m_labelRight.height();
    const int ly = sh / 2 - lrh / 2;
    const int x2 = sw - lrw - border - sw * 0.02;
    const int x1 = x2 - m_valueWidth;
    const int x0 = x1 - m_labelLeft.width();

    if (m_isOverall) {
        painter->drawPixmap(0, 10, m_icon);
        painter->drawPixmap(m_icon.width(), 0, m_divisor);
        painter->drawText(m_icon.width() * 1.1, 0, sw - x0, sh,
                          Qt::AlignLeft | Qt::AlignVCenter, m_text);
    } else
        painter->drawText(border, 0, sw, sh,
                          Qt::AlignLeft | Qt::AlignVCenter, m_text);

    painter->drawPixmap(x0, ly, m_labelLeft);
    painter->drawPixmap(x1, ly, m_valueWidth, lrh, m_labelMiddle);
    painter->drawPixmap(x2, ly, m_labelRight);

    painter->setPen(Qt::white);
    painter->drawText(x1, ly, m_valueWidth + lrw - border, lrh,
                      Qt::AlignRight | Qt::AlignVCenter, m_value);
}

void ExpensesListViewItem::updateContents(int idx)
{
    AbstractModel *model = listView()->model();

    if (model) {
        m_text = model->data(idx, "text").toString();
        m_isOverall = model->data(idx, "isOverall").toBool();
        m_value = QString::number(model->data(idx, "value").toDouble(), 'f', 2);

        if (m_isOverall)
            m_icon = PixmapLoader::pixmap(model->data(idx, "icon").toString());

        QFontMetrics fm(m_font);
        m_valueWidth = fm.width(m_value) - 10;

        update();
    }
}

void ExpensesListViewItem::contentsChanged()
{
    updateContents(index());
}

void ExpensesListViewItem::indexChange(int oldIndex, int newIndex)
{
    Q_UNUSED(oldIndex);
    updateContents(newIndex);
}
