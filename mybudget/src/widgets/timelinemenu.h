/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef _TIMELINE_H_
#define _TIMELINE_H_

#include <QGraphicsWidget>

class PData;
class CoinGraphicWidget;
class QGraphicsLinearLayout;


class TimelineMenu : public QGraphicsWidget
{
    Q_OBJECT

public:
    TimelineMenu();
    ~TimelineMenu();

    void updateMaxValue();
    qreal draggableHeight() const;

protected:
    void fillWidget(QGraphicsLinearLayout *l);
    void resizeEvent(QGraphicsSceneResizeEvent *event);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private slots:
    void updateGraph(const QString &group);

private:
    QGraphicsWidget *contentsWidget;
    bool isDragging;

    qreal maxValue;
    qreal overallSum;
    QPixmap *coin;

    PData *db;
    QList<CoinGraphicWidget*> allCoins;

    int draggingHeight;
};
#endif
