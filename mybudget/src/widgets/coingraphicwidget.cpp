/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "coingraphicwidget.h"

#include <QApplication>
#include <QDateTime>
#include <QPainter>
#include <QWidget>

CoinGraphicWidget::CoinGraphicWidget(int month, qreal overallSum, qreal maxValue, QPixmap *coinPx)
    : QGraphicsWidget(), graphCache(0)
{
    nokiaFont.setFamily(QLatin1String("Nokia Sans"));

#ifdef Q_OS_SYMBIAN
    nokiaFont.setPixelSize(18);
    labelOffset = 2;
    maxCoins = 75;
    marginForCoins = 10;
#else
    nokiaFont.setPixelSize(22);
    labelOffset = 5;
    maxCoins = 100;
    marginForCoins = 16;
#endif

    coinPixmap = coinPx;

    // ###
    int width = coinPixmap->size().width() + marginForCoins;
    setMinimumWidth(width);
    setPreferredWidth(width);
    setMaximumWidth(width);
    setMaximumHeight(QWIDGETSIZE_MAX);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

    labelLeft.load(QLatin1String(":/ico_left_%.png"));
    labelMiddle.load(QLatin1String(":/label_bg.png"));
    labelRight.load(QLatin1String(":/label_left_end2.png"));

    db = qApp->property("settings").value<PData*>();
    updateValue(month, overallSum, maxValue);
}

CoinGraphicWidget::~CoinGraphicWidget()
{
    delete graphCache;
}

void CoinGraphicWidget::updateValue(int month, qreal overallSum, qreal maxValue)
{
    QDate date = QDate::currentDate();
    qreal monthValue;

    db->beginGroup(QLatin1String("date"));
    db->beginGroup(QString::number(date.year()));
    monthValue = db->getValue(QString::number(month));
    db->endGroup();
    db->endGroup();

    coins = qRound((monthValue / maxValue) * maxCoins); //maxCoins = 100%
    for (int i = 0; i < coins; ++i)
        offsets << qRound(qrand() % 4);

    percentValue = 100 * monthValue / overallSum;

    updateGraphCache(coins);

    update();
    updateGeometry();
}

void CoinGraphicWidget::updateGraphCache(int coins)
{
    // Reset our cached pixmap
    delete graphCache;

    if (coins == 0) {
        graphCache = new QPixmap();
        setMinimumHeight(0);
        setPreferredHeight(0);
        return;
    }

    int labelHeight = labelLeft.size().height();
    int coinHeight = coinPixmap->size().height();
    int coinWidth = coinPixmap->size().width();

    // More size to cover offsets and %-ballon size
    int newWidth = coinWidth + marginForCoins;
    int newHeight = coinHeight * coins + labelHeight + labelOffset;

    graphCache = new QPixmap(QSize(newWidth, newHeight));
    graphCache->fill(Qt::transparent);

    QPainter p;
    p.begin(graphCache);

    QRect r = graphCache->rect();
    int y = r.y() + labelHeight + labelOffset; // ###
    for (int i = 0; i < coins; ++i) {
        int x = r.x() + ((marginForCoins - 4) / 2) + offsets.at(i);
        y += coinHeight;
        p.drawPixmap(x, y, *coinPixmap);
    }

#ifdef Q_OS_SYMBIAN
    qreal middleScaleFactor = 1.5;
#else
    qreal middleScaleFactor = 2.0;
#endif

    QSize labelSize(labelLeft.width() + middleScaleFactor * labelMiddle.width()
                    + labelRight.width(), labelHeight);
    int labelX = r.x() + (r.width() - labelSize.width()) / 2;
    int labelY = r.y();

    p.setFont(nokiaFont);
    p.setPen(QColor(68, 82, 83));

    p.drawPixmap(labelX, labelY, labelLeft);
    p.drawPixmap(labelX + labelLeft.width(), labelY,
                 labelMiddle.scaled(middleScaleFactor * labelMiddle.width(), labelHeight));
    p.drawPixmap(labelX + labelLeft.width() + middleScaleFactor * labelMiddle.width(),
                 labelY, labelRight);

    p.drawText(labelX + labelLeft.width(),
               labelY + labelHeight - 17,
               QString::number(percentValue, 'f', 2));
    p.end();

    setMinimumHeight(graphCache->size().height());
    setPreferredHeight(graphCache->size().height());
}

void CoinGraphicWidget::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                              QWidget *widget)
{
    QGraphicsWidget::paint(painter, option, widget);

    if (graphCache->isNull())
        return;

    painter->setRenderHint(QPainter::SmoothPixmapTransform);
    painter->setRenderHint(QPainter::Antialiasing);

    // Centralize our cached pixmap
    int x = rect().x();
    int y = rect().bottom() - graphCache->size().height();
    painter->drawPixmap(x, y, *graphCache);
}
