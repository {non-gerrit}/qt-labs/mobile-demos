/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef LISTEXPENSES_H
#define LISTEXPENSES_H

#include "listview.h"


class ExpensesListViewItem : public ListViewItem
{
public:
    ExpensesListViewItem(QGraphicsItem *parent = 0);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);

protected:
    void contentsChanged();
    void indexChange(int oldIndex, int newIndex);

private:
    void updateContents(int idx);

    QFont m_font;
    QColor m_fontColor;

    QPixmap m_divisor;
    QPixmap m_background;
    QPixmap m_labelLeft;
    QPixmap m_labelMiddle;
    QPixmap m_labelRight;

    QString m_text;
    QString m_value;
    QPixmap m_icon;
    bool m_isOverall;
    int m_valueWidth;
};

#endif
