/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef LISTSCREEN_H
#define LISTSCREEN_H

#include "model.h"
#include "pageslider.h"


class PData;
class QPixmap;
class ScrollBar;
class FooterBar;
class StandardModel;
class HorizontalMenu;
class KineticListView;


class ListScreen : public Page
{
    Q_OBJECT

public:
    ListScreen();
    ~ListScreen();

    QString currentCategory() const;
    void setTotalLabelValue(qreal total);

    QRectF flickableRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);

protected slots:
    void onOffsetChanged();
    void updateModel(const QString &category);
    void loadCategory(const QString &category);
    void categoryUpdated(const QString &category);

protected:
    void reconfigureScroll();
    void resizeEvent(QGraphicsSceneResizeEvent *event);

    PData *m_db;
    QHash<QString, qreal> m_totals;
    QHash<QString, StandardModel *> m_models;

    ScrollBar *m_scrollbar;
    HorizontalMenu *m_menu;
    KineticListView *m_listView;
    FooterBar *m_footer;
};

#endif
