/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "listscreen.h"

#include "model.h"
#include "pdata.h"
#include "label.h"
#include "dataresource.h"
#include "listview.h"
#include "titlebar.h"
#include "scrollbar.h"
#include "listexpenses.h"
#include "horizontalmenu.h"
#include "pixmaploader.h"


static const char *overallItemLabels[] = {
    "Food", "Car", "House", "Books", "Clothes", "Fun", "Health", "Travel"
};

static const char *overallItemIcons[] = {
    ":/food", ":/car", ":/house", ":/book" , ":/clothes", ":/fun",
    ":/health", ":/travel"
};

static QPair<int, QString> elapsedTimeLabel(int sec)
{
    if (sec < 60)
        return qMakePair(sec, QString("sec"));
    else if (sec < 3600)
        return qMakePair(sec / 60, QString("min"));
    else if (sec < 86400) {
        int number = sec / 3600;
        return qMakePair(number, QString(number < 2 ? "hour" : "hours"));
    } else if (sec < 604800) {
        int number = sec / 86400;
        return qMakePair(number, QString(number < 2 ? "day" : "days"));
    } else if (sec < 2592000) {
        int number = sec / 604800;
        return qMakePair(number, QString(number < 2 ? "week" : "weeks"));
    } else {
        int number = sec / 2592000;
        return qMakePair(number, QString(number < 2 ? "month" : "months"));
    }
}


class FooterBar : public PixmapWidget
{
    Q_OBJECT

public:
    FooterBar();

    void setLabelText(const QString &text);
    void setLabelValue(const QString &text);

private:
    Label *m_label;
    Label *m_valueLabel;
};


ListScreen::ListScreen()
    : Page()
{
    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout(Qt::Vertical);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    setLayout(layout);

    TitleBar *title = new TitleBar;
    layout->addItem(title);

    m_menu = new HorizontalMenu(true);
    m_menu->setZValue(2);
    layout->addItem(m_menu);

    m_listView = new KineticListView();
    m_listView->setCreator(new ListViewCreator<ExpensesListViewItem>());
    layout->addItem(m_listView);
    connect(m_listView, SIGNAL(offsetChanged()), SLOT(onOffsetChanged()));
    connect(m_listView, SIGNAL(maximumOffsetChanged()), SLOT(onOffsetChanged()));

    m_scrollbar = new ScrollBar(PixmapLoader::pixmap(":/scrollbar_bg.png"),
                                PixmapLoader::pixmap(":/scrollbar_fg.png"), this);
    m_scrollbar->setZValue(2);
    m_scrollbar->setBorders(0, 10, 0, 10);
    m_scrollbar->setKnobBorders(0, 10, 0, 10);

    reconfigureScroll();

    m_footer = new FooterBar;
    layout->addItem(m_footer);

    connect(m_menu, SIGNAL(optionChanged(const QString &)),
            SLOT(loadCategory(const QString &)));
    connect(title, SIGNAL(closeClicked()), qApp, SLOT(quit()));

    m_db = qApp->property("settings").value<PData*>();
    connect(m_db, SIGNAL(valueAdded(QString)), SLOT(categoryUpdated(QString)));

    m_menu->focusMenuItem(m_menu->count() - 1, false);
}

ListScreen::~ListScreen()
{

}

void ListScreen::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                       QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->fillRect(boundingRect(), QColor(0xDB, 0xDD, 0xE1));
}

void ListScreen::onOffsetChanged()
{
    m_scrollbar->setMaximum(m_listView->maximumOffset());
    m_scrollbar->setValue(m_listView->offset());
    m_scrollbar->setPageSize(m_listView->size().height());
}

QString ListScreen::currentCategory() const
{
    return m_menu->selected();
}

void ListScreen::setTotalLabelValue(qreal total)
{
    m_footer->setLabelText(m_menu->selected() != "overall" ?
                           tr("Subtotal") : tr("Total"));
    m_footer->setLabelValue(QString::number(total, 'f', 2));
}

QRectF ListScreen::flickableRect() const
{
    int offset = m_menu->y() + m_menu->size().height();
    return boundingRect().adjusted(0, offset, 0, 0);
}

void ListScreen::loadCategory(const QString &category)
{
    if (!m_models.contains(category))
        updateModel(category);

    m_listView->setModel(m_models.value(category, 0));
    setTotalLabelValue(m_totals.value(category, 0));
}

void ListScreen::categoryUpdated(const QString &category)
{
    updateModel(category);

    /*
      When a category is updated, we need to update the overall list too.
       As the category argument here cames from the edited one, it will never
       be "overall" because there is no edit mode for it.
     */
    updateModel("overall");

    /*
      To update the label when a new value is inserted
      Here we use currentCategory instead of category because
      "overall" can be the current selection in the list menu and, as we said before,
      category will never be overall because there is no edit mode for it.
    */
    setTotalLabelValue(m_totals.value(currentCategory(), 0));
}

void ListScreen::updateModel(const QString &category)
{
    StandardModel *model;

    if (!m_models.contains(category)) {
        model = new StandardModel(this);
        m_models[category] = model;
    } else
        model = m_models[category];

    model->clear();

    if (category == "overall") {
        const QList<qreal> &lst = m_db->loadTotalsByCategory();

        qreal total = 0;
        QHash<QString, QVariant> record;

        for (int i = 0; i < 8 && i < lst.count(); i++) {
            record["text"] = overallItemLabels[i];
            record["icon"] = overallItemIcons[i];
            record["value"] = lst[i];
            record["isOverall"] = true;
            model->append(record);

            total += lst[i];
        }

        m_totals[category] = total;
    } else {
        const QList<QPair<uint, qreal> > &lst = m_db->loadCategory(category);

        qreal total = 0;
        QHash<QString, QVariant> record;
        int currentTime = QDateTime::currentDateTime().toTime_t();

        for (int i = lst.count() - 1; i >= 0; i--) {
            const QPair<uint, qreal> &item = lst[i];
            const int sec = currentTime - item.first;

            const QPair<int, QString> &values = elapsedTimeLabel(sec);
            record["text"] = QString("%1 %2 ago")
                .arg(values.first).arg(values.second);
            record["value"] = item.second;
            record["isOverall"] = false;
            model->append(record);

            total += item.second;
        }

        m_totals[category] = total;
    }
}

void ListScreen::resizeEvent(QGraphicsSceneResizeEvent *event)
{
    Page::resizeEvent(event);
    reconfigureScroll();
}

void ListScreen::reconfigureScroll()
{
    int hborder = m_scrollbar->size().width() * 1.40;
    int vborder = m_listView->size().height() * 0.02;

    m_scrollbar->setPos(m_listView->x() + m_listView->size().width() - hborder,
                        m_listView->y() + vborder);

    m_scrollbar->resize(m_scrollbar->preferredWidth(),
                        m_listView->size().height() - vborder * 2);
}

FooterBar::FooterBar()
    : PixmapWidget(PixmapLoader::pixmap(":/base_green.png"))
{
    QFont font("Nokia Sans");

    const int hborder = preferredWidth() * 0.02;
    const int vborder = preferredHeight() * 0.12;

    m_label = new Label();
    font.setPixelSize(Resource::intValue("List/FooterBar.labelFontSize"));
    m_label->setFont(font);
    m_label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    m_label->setText(tr("Total"));

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout(Qt::Horizontal);
    layout->setSpacing(0);
    layout->setContentsMargins(hborder, vborder, hborder, vborder);
    setLayout(layout);

    layout->addItem(m_label);

    PixmapWidget *tagLabel =
        new PixmapWidget(PixmapLoader::pixmap(":/label_white.png"));
    layout->addItem(tagLabel);

    m_valueLabel = new Label();
    font.setPixelSize(Resource::intValue("List/FooterBar.valueFontSize"));
    m_valueLabel->setFont(font);
    m_valueLabel->setFontColor(QColor(64, 74, 74));
    m_valueLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_valueLabel->setText("0.00");

    QGraphicsLinearLayout *lp = new QGraphicsLinearLayout(Qt::Horizontal);
    lp->setSpacing(0);
    lp->setContentsMargins(tagLabel->preferredWidth() * 0.20, 0,
                           tagLabel->preferredWidth() * 0.04, 0);
    lp->addItem(m_valueLabel);
    tagLabel->setLayout(lp);

    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
}

void FooterBar::setLabelText(const QString &text)
{
    m_label->setText(text);
}

void FooterBar::setLabelValue(const QString &text)
{
    m_valueLabel->setText(text);
}

#include "listscreen.moc"
