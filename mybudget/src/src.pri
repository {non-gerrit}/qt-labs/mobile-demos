HEADERS += src/pdata.h \
           src/pixmaploader.h \
           src/calculator/calculatorscreen.h \
           src/calculator/calculatorengine.h \
           src/chart/chartscreen.h \
           src/lists/listscreen.h \
           src/widgets/barchart.h \
           src/widgets/horizontalmenu.h \
           src/widgets/listexpenses.h \
           src/widgets/coingraphicwidget.h \
           src/widgets/timelinemenu.h \
           src/widgets/titlebar.h


INCLUDEPATH += src src/widgets

SOURCES += src/main.cpp \
           src/pixmaploader.cpp \
           src/calculator/calculatorscreen.cpp \
           src/calculator/calculatorengine.cpp \
           src/chart/chartscreen.cpp \
           src/lists/listscreen.cpp \
           src/widgets/barchart.cpp \
           src/widgets/horizontalmenu.cpp \
           src/widgets/listexpenses.cpp \
           src/widgets/coingraphicwidget.cpp \
           src/widgets/timelinemenu.cpp \
           src/widgets/titlebar.cpp \




