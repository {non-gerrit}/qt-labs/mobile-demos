/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QGraphicsLinearLayout>

#include "calculatorscreen.h"

#include "pdata.h"
#include "label.h"
#include "button.h"
#include "titlebar.h"
#include "dataresource.h"
#include "calculatorengine.h"


CalculatorScreen::CalculatorScreen()
  : Page()
{
    setContentsMargins(0, 0, 0, 0);

    mainLayout = new QGraphicsLinearLayout(Qt::Vertical);
    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);

    m_engine = new CalculatorEngine;
    m_engine->connect(&_digitMapper, SIGNAL(mapped(int)), SLOT(pressDigit(int)));
    m_engine->connect(&_operatorMapper, SIGNAL(mapped(int)), SLOT(pressOperator(int)));

    TitleBar *title = new TitleBar;
    mainLayout->addItem(title);

    createCarousel();
    createDisplay();
    createKeyboard();

    connect(title, SIGNAL(closeClicked()), qApp, SLOT(quit()));

    connect(m_engine, SIGNAL(displayChanged(QString)),
            SLOT(handleDisplayChange(QString)));
}

CalculatorScreen::~CalculatorScreen()
{
    delete m_engine;
}

void CalculatorScreen::setDisplayText(const QString &text)
{
    display->setText(text);
}

QString CalculatorScreen::optionSelected()
{
    return _menu->selected();
}

void CalculatorScreen::createDisplay()
{
    QFont nokiaFont("Nokia Sans");
    nokiaFont.setPixelSize(Resource::intValue("Calculator/Screen.fontSize"));

    display = new Label;
    display->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
    display->setFont(nokiaFont);
    setDisplayText("0.00");

    PixmapWidget *image = \
        new PixmapWidget(PixmapLoader::pixmap(":/calculator_display.png"));
    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout(image);

    layout->addItem(display);

    mainLayout->addItem(image);
}

void CalculatorScreen::createCarousel()
{
    _menu = new HorizontalMenu();
    _menu->setZValue(1);
    mainLayout->addItem(_menu);
}

void CalculatorScreen::createKeyboard()
{
    QStringList buttons;

    // Line: 7 8 9 /
    buttons << "7" << "8" << "9" << "/";
    mainLayout->addItem(createKeyboardLine(buttons));
    buttons.clear();

    // Line: 4 5 6 -
    buttons << "4" << "5" << "6" << "-";
    mainLayout->addItem(createKeyboardLine(buttons));
    buttons.clear();

    // Line: 1 2 3 +
    buttons << "1" << "2" << "3" << "+";
    mainLayout->addItem(createKeyboardLine(buttons));
    buttons.clear();

    // Line: . 0 X =
    buttons << "." << "0" << "x" << "=";
    mainLayout->addItem(createKeyboardLine(buttons));
    buttons.clear();

    // Line: Clear Submit
    buttons << "CLEAR" << "SUBMIT";
    mainLayout->addItem(createKeyboardLine(buttons));
    buttons.clear();
}

QGraphicsLinearLayout *CalculatorScreen::createKeyboardLine(const QStringList &items)
{
    static QString operations = "+-x/.=";

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    foreach (const QString &item, items) {
        Button *button;

        QFont font("Nokia Sans");
        font.setPixelSize(Resource::intValue("Calculator/Button.fontSize"));

        if (operations.contains(item)) {
            button = new Button(PixmapLoader::pixmap(":/bt_02_off.png"),
                                PixmapLoader::pixmap(":/bt_02_on.png"));

            if (item == ".")
                m_engine->connect(button, SIGNAL(clicked()), SLOT(pressDot()));
            else if (item == "=")
                m_engine->connect(button, SIGNAL(clicked()), SLOT(pressEqual()));
            else
                connectButton(&_operatorMapper, button, operations.indexOf(item));
        } else if (item == "SUBMIT" || item == "CLEAR") {
            button = new Button(PixmapLoader::pixmap(":/bt_03_off.png"),
                                PixmapLoader::pixmap(":/bt_03_on.png"));

            if (item == "CLEAR")
                m_engine->connect(button, SIGNAL(clicked()), SLOT(pressClearAll()));
            else {
                connect(button, SIGNAL(clicked()), SLOT(handleSubmit()));
                //Submit also cleans the display
                m_engine->connect(button, SIGNAL(clicked()), SLOT(pressClearAll()));
            }

            font.setPixelSize(Resource::intValue("Calculator/Action.fontSize"));
        } else {
            // digit
            button = new Button(PixmapLoader::pixmap(":/bt_01_off.png"),
                                PixmapLoader::pixmap(":/bt_01_on.png"));
            connectButton(&_digitMapper, button, item.toInt());
        }

        button->setText(item);
        button->setFont(font);

        layout->addItem(button);
    }
    return layout;
}

void CalculatorScreen::connectButton(QSignalMapper *mapper, QGraphicsLayoutItem *item,
                                     int mapping)
{
    QGraphicsWidget *widget = static_cast<QGraphicsWidget *>(item);
    mapper->setMapping(widget, mapping);
    mapper->connect(widget, SIGNAL(clicked()), SLOT(map()));
}

QRectF CalculatorScreen::flickableRect() const
{
    int offset = _menu->y() + _menu->size().height();
    return boundingRect().adjusted(0, offset, 0, 0);
}

void CalculatorScreen::handleDisplayChange(const QString &display)
{
    setDisplayText(display);
}

void CalculatorScreen::handleSubmit()
{
    const qreal value = m_engine->currentValue();

    if (value > 0) {
        const QString &category = optionSelected();
        PData *db = qApp->property("settings").value<PData*>();

        db->addValue(category, m_engine->currentValue());
    }
}
