/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QObject>

class CalculatorEngine : public QObject
{
    Q_OBJECT

    enum Operator { Sum = 0,
                    Subtract,
                    Multiply,
                    Divide };

public:
    CalculatorEngine();
    ~CalculatorEngine();

    qreal currentValue();

public slots:
    void pressDigit(int digit);
    void pressDot();

    void pressOperator(int op);
    void pressEqual();

    void pressClear();
    void pressClearAll();

signals:
    void displayChanged(QString display);

protected:
    qreal firstOperand;
    qreal secondOperand;
    Operator operation;
    bool waitingForOperand;
    bool dirty;
    QString display;
    int digitCounter;
};
