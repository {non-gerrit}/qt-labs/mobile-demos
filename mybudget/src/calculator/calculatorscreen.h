/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CALCULATORSCREEN_H
#define CALCULATORSCREEN_H

#include <QGraphicsWidget>
#include <QSignalMapper>

#include "pageslider.h"
#include "horizontalmenu.h"


class Label;
class CalculatorEngine;
class QGraphicsLinearLayout;


class CalculatorScreen : public Page
{
    Q_OBJECT

public:
    CalculatorScreen();
    ~CalculatorScreen();

    void setDisplayText(const QString &text);
    QString optionSelected();

    QRectF flickableRect() const;

signals:
    void digitPressed(int i);
    void dotPressed();
    void operatorPressed(int i);
    void equalPressed();
    void clearPressed();
    void submitPressed();

protected slots:
    void handleSubmit();
    void handleDisplayChange(const QString &display);

protected:
    void createDisplay();
    void createCarousel();
    void createKeyboard();

    QGraphicsLinearLayout *createKeyboardLine(const QStringList &items);
    void connectButton(QSignalMapper *mapper, QGraphicsLayoutItem *item,
                       int mapping);

    QGraphicsLinearLayout *mainLayout;
    Label *display;
    QSignalMapper _digitMapper;
    QSignalMapper _operatorMapper;
    HorizontalMenu *_menu;
    CalculatorEngine *m_engine;
};

#endif
