/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "calculatorengine.h"

CalculatorEngine::CalculatorEngine() : QObject(), firstOperand(0.0), secondOperand(0.0),
                                       operation(Sum), waitingForOperand(true), display("0"),
                                       digitCounter(0)
{

}

CalculatorEngine::~CalculatorEngine()
{

}

void CalculatorEngine::pressDigit(int digit)
{
    ++digitCounter;
    if (digitCounter > 6)
        return;

    if (waitingForOperand) {
        if ((digit == 0) && (display == "0")) {
            digitCounter = 0;
            return;
        }

        waitingForOperand = false;
        display.clear();
    }
    display += QString::number(digit);
    emit displayChanged(display);
}

void CalculatorEngine::pressDot()
{
    if (display.contains("."))
        return;

    // Why 4? Maximum number is 999999.99,
    // so if when we already have the 6 integer digits,
    // we need more 2 for the cents. When there are less
    // integer digits, we limit cents to 2 digits =)
    digitCounter = 4;

    if (waitingForOperand) {
         waitingForOperand = false;
         display = QString("0");
    }

    display += QString(".");
    emit displayChanged(display);
}

void CalculatorEngine::pressOperator(int op)
{
    if (!waitingForOperand)
        pressEqual();
    operation = Operator(op);
    digitCounter = 0;
}

void CalculatorEngine::pressClear()
{
    digitCounter = 0;
    waitingForOperand = true;
    display = QString("0");
    emit displayChanged(display);
}

void CalculatorEngine::pressClearAll()
{
    digitCounter = 0;
    firstOperand = 0.0;
    operation = Sum;
    waitingForOperand = true;
    display = QString("0");
    emit displayChanged(display);
}

void CalculatorEngine::pressEqual()
{
    if (waitingForOperand == false)
        secondOperand = display.toFloat();

    switch (operation) {
    case Sum:
        firstOperand += secondOperand;
        break;
    case Subtract:
        firstOperand -= secondOperand;
        break;
    case Multiply:
        firstOperand *= secondOperand;
        break;
    case Divide:
        if (secondOperand != 0)
            firstOperand /= secondOperand;
        break;
    }

    waitingForOperand = true;
    display = QString::number(firstOperand, 'f', 2);
    emit displayChanged(display);
}

qreal CalculatorEngine::currentValue()
{
    return display.toFloat();
}
