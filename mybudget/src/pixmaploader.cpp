/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QPixmap>

#include "pixmaploader.h"
#include "dataresource.h"


PixmapLoader::PixmapLoader()
{

}

QPixmap PixmapLoader::pixmap(const QString &fileName)
{
    static QHash<QString, QPixmap> pixmapCache;

    QSize size;

    // try to get size from theme
    const QString &cname = QString("Images/%1").arg(fileName);

    if (Resource::containsValue(cname))
        size = Resource::value(cname).toSize();

    // try to find in cache
    const QString &key = !size.isValid() ? fileName :
        QString("%1:%2x%3").arg(fileName).arg(size.width()).arg(size.height());

    if (pixmapCache.contains(key))
        return pixmapCache.value(key);
    else {
        QPixmap result(fileName);

        if (size.isValid())
            result = result.scaled(size, Qt::IgnoreAspectRatio,
                                   Qt::SmoothTransformation);

        pixmapCache.insert(key, result);
        return result;
    }
}
