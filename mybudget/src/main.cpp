/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QApplication>
#include <QTimer>

#include "pdata.h"
#include "system.h"
#include "dataresource.h"
#include "calculator/calculatorscreen.h"
#include "lists/listscreen.h"
#include "chart/chartscreen.h"


int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("openBossa");
    QCoreApplication::setOrganizationDomain("openbossa.org");
    QCoreApplication::setApplicationName("mybudget");

#ifndef Q_OS_SYMBIAN
    QString themeFile(QLatin1String(":/themes/mybudget.ini"));
#else
    QString themeFile(QLatin1String(":/themes/symbian.ini"));
#endif

    Resource::setIniFile(themeFile);

    QSize windowSize = Resource::value("windowSize").toSize();

    // Database
    PData *settings = new PData;
    qApp->setProperty("settings", QVariant::fromValue(settings));

    // Our contents
    QGraphicsScene scene;
    scene.setItemIndexMethod(QGraphicsScene::NoIndex);

    QGraphicsView *view = new QGraphicsView(&scene);
    view->setFrameShape(QFrame::NoFrame);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setSceneRect(0, 0, windowSize.width(), windowSize.height());
    view->resize(windowSize);
    view->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);

    view->setBackgroundBrush(Qt::black);

    PageSlider *slider = new PageSlider();
    slider->setThreshold(Qt::Vertical, 99999);
    slider->setThreshold(Qt::Horizontal, 70);

    scene.addItem(slider);
    slider->setPos(0, 0);
    slider->resize(windowSize);

    slider->addPage(new CalculatorScreen);
    slider->addPage(new ListScreen);
    slider->addPage(new ChartScreen);

    slider->setCurrentPage(1);

    System::setViewMode(view, System::PortraitMode);

#if defined(Q_OS_SYMBIAN) || defined(Q_WS_MAEMO_5)
    view->showFullScreen();
#else
    view->show();
#endif

    int rc = app.exec();

    delete settings;

    return rc;
}
