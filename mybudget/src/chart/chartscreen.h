/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CHARTSCREEN_H
#define CHARTSCREEN_H

#include "pageslider.h"

class TimelineMenu;


class ChartScreen : public Page
{
    Q_OBJECT

public:
    ChartScreen();
    virtual ~ChartScreen();

    QRectF flickableRect() const;

private:
    TimelineMenu *m_timeline;
};

#endif
