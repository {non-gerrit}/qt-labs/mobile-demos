/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QApplication>
#include <QGraphicsLinearLayout>

#include "chartscreen.h"
#include "titlebar.h"
#include "timelinemenu.h"


ChartScreen::ChartScreen()
    : Page()
{
    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout(Qt::Vertical);
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);

    TitleBar *title = new TitleBar;
    layout->addItem(title);

    m_timeline = new TimelineMenu;
    layout->addItem(m_timeline);
    layout->setAlignment(m_timeline, Qt::AlignBottom);
    layout->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    setLayout(layout);
    connect(title, SIGNAL(closeClicked()), qApp, SLOT(quit()));
}

ChartScreen::~ChartScreen()
{

}

QRectF ChartScreen::flickableRect() const
{
    return boundingRect().adjusted(0, 0, 0, -m_timeline->draggableHeight());
}
