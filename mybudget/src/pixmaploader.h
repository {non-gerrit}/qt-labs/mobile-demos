/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef PIXMAPLOADER_H
#define PIXMAPLOADER_H

#include <QHash>
#include <QSize>
#include <QPixmap>


class QString;

class PixmapLoader
{
public:
    static QPixmap pixmap(const QString &fileName);

private:
    PixmapLoader();
};

#endif
