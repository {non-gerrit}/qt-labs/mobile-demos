/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef _PDATA_H_
#define _PDATA_H_

#include <QSettings>
#include <QStringList>
#include <QString>
#include <QDateTime>

class PData : public QSettings
{
    Q_OBJECT

public:

PData() : QSettings()
        {
            categoryList << "food" << "car" << "house" << "books" << "clothes" << "fun" \
                         << "health" << "travel";

            QStringList groups = childGroups();

            if (!groups.contains("date")) {
                beginGroup("date");
                QDate date = QDate::currentDate();
                beginGroup(QString::number(date.year()));
                setValue(QString::number(date.month()), 0);
                endGroup();
                endGroup();
            }
        }

    void addValue(const QString &group, qreal v)
    {
        uint timestamp = QDateTime::currentDateTime().toTime_t();

        updateMonthValue(v);

        beginGroup(group);
        QStringList keys = allKeys();

        if (keys.size() >= 101)
            remove(keys.at(0));

        setValue(QString::number(timestamp), v);
        endGroup();

        emit valueAdded(group);
    }

    QStringList rows(const QString &group)
    {
        beginGroup(group);
        QStringList keys = allKeys();
        endGroup();

        return keys;
    }

    QList<QPair<uint, qreal> > loadCategory(const QString &group)
    {
        QList<QPair<uint, qreal> >data;

        beginGroup(group);
        QStringList keys = allKeys();

        foreach (QString k, keys) {
            data << qMakePair(k.toUInt(), value(k, 0).value<qreal>());
        }
        endGroup();

        return data;
    }

    //## name
    qreal getValue(const QString &key)
    {
        return value(key, 0).value<qreal>();
    }

    QList<qreal> loadTotalsByCategory()
    {
        QList<qreal> totals;
        foreach (QString k, categoryList) {
            QList<QPair<uint, qreal> > data = loadCategory(k);
            qreal total = 0;
            QList<QPair<uint, qreal> >::const_iterator iter;

            for (iter = data.constBegin(); iter != data.constEnd(); ++iter)
                total += iter->second;

            totals << total;
        }

        return totals;
    }

signals:
    void valueAdded(const QString &group);

protected:

    void updateMonthValue(qreal v)
    {
        QDate date = QDate::currentDate();
        qreal sum = 0;

        beginGroup("date");
        beginGroup(QString::number(date.year()));
        qreal data = value(QString::number(date.month()), 0).value<qreal>();
        sum = data + v;
        setValue(QString::number(date.month()), sum);
        endGroup();
        endGroup();
    }

    QStringList categoryList;
};

Q_DECLARE_METATYPE(PData*)

#endif
