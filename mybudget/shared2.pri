INCLUDEPATH += ../shared
HEADERS += \
           ../shared/model.h \
           ../shared/listview.h \
           ../shared/scrollbar.h \
           ../shared/pageslider.h \
           ../shared/flickablearea.h
SOURCES += \
           ../shared/model.cpp \
           ../shared/listview.cpp \
           ../shared/scrollbar.cpp \
           ../shared/pageslider.cpp \
           ../shared/flickablearea.cpp
