TEMPLATE = app
TARGET =

# All generated files goes same directory
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build
DESTDIR = build

RESOURCES += images/images.qrc

include(src/src.pri)

include(../shared/shared.pri)
include(shared2.pri)

isEmpty(PREFIX) {
    PREFIX = "/usr/local"
}

INSTALLS += target
target.path = $$PREFIX/bin

INSTALLS    += desktop
desktop.path  = /usr/share/applications/hildon
desktop.files  = maemo/mybudget.desktop

INSTALLS    += icon64
icon64.path  = /usr/share/icons/hicolor/64x64/apps
icon64.files  = maemo/mybudget-icon.png

INSTALLS    += initsh
initsh.path  = /usr/bin/
initsh.files  = maemo/start_mybudget.sh

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.5

# S60
symbian {
    isEmpty(NODEPLOYMENT) {
        TARGET.UID3 = 0xe1234566
        TARGET.EPOCHEAPSIZE = 0x020000 0x2000000
        ICON = images/mybudget-icon.svg
        mybudget.sources = mybudget.conf
        mybudget.path = c:\DATA\.config\openBossa
        DEPLOYMENT += mybudget
    }
}

# Maemo 5
linux-g++-maemo5{
  # Targets for debian source and binary package creation
  debian-src.commands = dpkg-buildpackage -S -r -us -uc -d
  debian-bin.commands = dpkg-buildpackage -b -r -uc -d
  debian-all.depends = debian-src debian-bin


  # Clean all but Makefile
  compiler_clean.commands = -$(DEL_FILE) $(TARGET)

  QMAKE_EXTRA_TARGETS += debian-all debian-src debian-bin compiler_clean
}
