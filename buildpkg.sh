#!/bin/bash

# A script to generate the source tar.gz to be
# uploaded to Maemo build bot.
# It is important to note the copy of the shared directory
# to the demo directory. We have to do it in order to
# provide the whole source needed, so the build bot
# can generate the .deb packages.

# This small hack is necessary until the contents
# of shared/ aren't distributed as another package.

if [ $# -eq 0 ]; then
    echo "Usage: buildpkg DEMO"
    exit 1
fi

DEMO_DIR=$1

cp -r shared $DEMO_DIR
cd $DEMO_DIR
sed -i 's/\.\.\/shared/shared/g' shared/shared.pri $DEMO_DIR.pro
if [ -e shared2.pri ]; then
    sed -i 's/\.\.\/shared/shared/g' shared2.pri
fi
mad dpkg-buildpackage -rfakeroot
rm -fr build/ debian/qtmobiledemo-$DEMO_DIR/ shared/

