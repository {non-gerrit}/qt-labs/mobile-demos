TEMPLATE = app
TARGET = hyperui
DEPENDPATH += .

# All generated files goes same directory
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build

DESTDIR = build
VPATH += src
INSTALLS += target
target.path = $$PREFIX/bin ##

INSTALLS    += desktop
desktop.path  = /usr/share/applications/hildon
desktop.files  = data/hyperui.desktop

INSTALLS    += icon64
icon64.path  = /usr/share/icons/hicolor/64x64/apps
icon64.files  = data/hyperui.png

isEmpty(RESOLUTION) {
    # N900 resolution
    RESOLUTION = "800x480"
}

!isEmpty(USE_RASTER) {
    DEFINES += USE_RASTER_GRAPHICS_SYSTEM
}

RESOURCES = resource/$$RESOLUTION/hyperui.qrc

HEADERS += mainwindow.h \
           pagemenu.h \
           view.h \
           global.h \
           pageview.h \
           menuview.h \
           phoneview.h \
           draggablepreview.h \
           clockwidget.h \
           contactlist.h \
           contactresource.h

SOURCES += main.cpp \
           mainwindow.cpp \
           pagemenu.cpp \
           view.cpp \
           global.cpp \
           pageview.cpp \
           menuview.cpp \
           phoneview.cpp \
           draggablepreview.cpp \
           clockwidget.cpp \
           contactlist.cpp \
           contactresource.cpp

include(../shared/shared.pri)

# S60
symbian {
    TARGET.UID3 = 0xe1234569
    ICON = data/hyperui.svg
    isEmpty(RESOLUTION) {
        # N97 resolution
        RESOLUTION = "640x360"
    }
}

# Maemo 5
linux-g++-maemo5{
  # Targets for debian source and binary package creation
  debian-src.commands = dpkg-buildpackage -S -r -us -uc -d
  debian-bin.commands = dpkg-buildpackage -b -r -uc -d
  debian-all.depends = debian-src debian-bin


  # Clean all but Makefile
  compiler_clean.commands = -$(DEL_FILE) $(TARGET)

  QMAKE_EXTRA_TARGETS += debian-all debian-src debian-bin compiler_clean
}
