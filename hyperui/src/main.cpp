/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "system.h"
#include "dataresource.h"
#include "mainwindow.h"


int main(int argc, char **argv)
{
#ifdef USE_RASTER_GRAPHICS_SYSTEM
    QApplication::setGraphicsSystem("raster");
#endif

    Resource::setIniFile(":/hyperui.ini");
    Resource::setPixmapPrefix(":/images/");

    QApplication app(argc, argv);

    const int width = Resource::intValue("window/width");
    const int height = Resource::intValue("window/height");

    QGraphicsScene scene;
    QGraphicsView view(&scene);

    view.setWindowTitle(QObject::tr("Hyper UI"));
    view.setFrameShape(QFrame::NoFrame);
    view.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view.setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);

    view.resize(width, height);
    scene.setSceneRect(0, 0, width, height);

    MainWindow mainWindow;
    scene.addItem(&mainWindow);
    mainWindow.setGeometry(0, 0, width, height);

    System::setViewMode(&view, System::PortraitMode);

#if defined(Q_OS_SYMBIAN) || defined(Q_WS_MAEMO_5)
    view.showFullScreen();
#else
    view.setFixedSize(width, height);
    view.show();
#endif

    return app.exec();
}
