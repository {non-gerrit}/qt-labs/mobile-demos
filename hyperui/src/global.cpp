/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QPainter>
#include <QPropertyAnimation>

#include "global.h"


void drawTextWithShadow(QPainter *painter, int x, int y,
                        const QString &text, const QColor &color)
{
    painter->setPen(QColor(30, 30, 30));
    painter->drawText(x + 2, y + 2, text);

    painter->setPen(color);
    painter->drawText(x, y, text);
}

QPropertyAnimation *propertyAnimation(QObject *obj, const char *property, int time,
                                      QEasingCurve::Type type)
{
    QPropertyAnimation *result = new QPropertyAnimation(obj, property);
    result->setDuration(time);
    result->setEasingCurve(type);
    return result;
}
