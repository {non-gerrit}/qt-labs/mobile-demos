/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef GLOBAL_H
#define GLOBAL_H

#include <QObject>
#include <QString>
#include <QEasingCurve>

class QPainter;
class QPropertyAnimation;

void drawTextWithShadow(QPainter *painter, int x, int y,
                        const QString &text, const QColor &color);

QPropertyAnimation *propertyAnimation(QObject *obj, const char *property, int time,
                                      QEasingCurve::Type type = QEasingCurve::Linear);

#endif
