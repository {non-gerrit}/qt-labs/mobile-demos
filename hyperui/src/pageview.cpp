/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QApplication>
#include <QGraphicsLinearLayout>
#include <QGraphicsSceneResizeEvent>

#include "dataresource.h"
#include "view.h"
#include "button.h"
#include "pagemenu.h"
#include "pageview.h"


class PageSlot : public QGraphicsWidget
{
    Q_OBJECT

public:
    PageSlot(QGraphicsItem *parent = 0);

    View *contents() const;
    void setContents(View *contents);

protected:
    void resizeEvent(QGraphicsSceneResizeEvent *event);

private:
    View *m_contents;
};


PageSlot::PageSlot(QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      m_contents(0)
{
    setFlags(QGraphicsItem::ItemHasNoContents);
}

View *PageSlot::contents() const
{
    return m_contents;
}

void PageSlot::setContents(View *contents)
{
    if (m_contents && m_contents->parentItem() == this)
        m_contents->setParentItem(0);

    m_contents = contents;

    if (contents) {
        contents->setParentItem(this);
        contents->setGeometry(0, 0, size().width(), size().height());
    }
}

void PageSlot::resizeEvent(QGraphicsSceneResizeEvent *event)
{
    QGraphicsWidget::resizeEvent(event);

    if (m_contents)
        m_contents->resize(event->newSize());
}


PageView::PageView(QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      m_isBack(false),
      m_isAnimating(false)
{
    m_topOffset = Resource::intValue("page-view/margin-top");

    setFlag(QGraphicsItem::ItemHasNoContents);

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout(Qt::Vertical);
    layout->setContentsMargins(20 * 0.75, 40, 20 * 0.75, 0);

    QGraphicsLinearLayout *topLayout = new QGraphicsLinearLayout(Qt::Horizontal);
    topLayout->setContentsMargins(0, 0, 0, 0);

    setLayout(layout);
    layout->addItem(topLayout);
    layout->addStretch(1);

    m_menu = new PageMenu();

    m_backButton = new Button(Resource::pixmap("top_bt_back.png"),
                              QPixmap(),
                              Resource::pixmap("top_bt_back_disabled.png"));

    m_optionsButton = new Button(Resource::pixmap("top_bt_options.png"),
                                 QPixmap(),
                                 Resource::pixmap("top_bt_options_disabled.png"));

    connect(m_backButton, SIGNAL(clicked()), SLOT(backClicked()));
    connect(m_optionsButton, SIGNAL(clicked()), SLOT(optionsClicked()));

    topLayout->addItem(m_optionsButton);
    topLayout->addStretch(1);
    topLayout->addItem(m_menu);
    topLayout->addStretch(1);
    topLayout->addItem(m_backButton);

    m_optionsButton->setEnabled(false);

    m_oldSlot = new PageSlot(this);
    m_newSlot = new PageSlot(this);
    m_oldSlot->setPos(0, m_topOffset);
    m_newSlot->setPos(0, m_topOffset);
}

bool PageView::add(View *view, bool keepAlive)
{
    if (!view || isAnimating())
        return false;

    view->setPageView(this);
    m_keepAlive[view] = keepAlive;

    if (m_views.isEmpty()) {
        m_views.push(view);
        m_menu->setText(view->title());
        m_oldSlot->setContents(view);
    } else
        animateTransition(m_views.top(), view, false);

    return true;
}

bool PageView::back()
{
    if (m_views.count() < 2 || isAnimating())
        return false;

    View *oldView = m_views.pop();
    View *newView = m_views.top();

    animateTransition(oldView, newView, true);
    return true;
}

bool PageView::isAnimating() const
{
    return m_isAnimating;
}

void PageView::backClicked()
{
    if (isAnimating())
        return;

    if (m_views.count() < 2)
        QApplication::quit();
    else
        back();
}

void PageView::optionsClicked()
{
    qWarning("options clicked");
}

void PageView::transitionFinished()
{
    View *newView = m_newSlot->contents();
    View *oldView = m_oldSlot->contents();

    disconnect(newView, SIGNAL(transitionInFinished()),
               this, SLOT(transitionFinished()));
    disconnect(oldView, SIGNAL(transitionOutFinished()),
               newView, SLOT(doTransitionOut()));

    if (m_isBack) {
        m_oldSlot->setContents(0);
        bool keepAlive = m_keepAlive[oldView];
        m_keepAlive.remove(oldView);
        if (!keepAlive)
            delete oldView;
    } else {
        oldView->hide();
        m_views.push(newView);
    }

    m_isAnimating = false;
    m_menu->setText(newView->title());
}

void PageView::animateTransition(View *oldView, View *newView, bool isBack)
{
    m_isAnimating = true;

    m_isBack = isBack;
    m_oldSlot->setContents(oldView);
    m_newSlot->setContents(newView);

    newView->show();

    connect(newView, SIGNAL(transitionInFinished()),
            this, SLOT(transitionFinished()));
    connect(oldView, SIGNAL(transitionOutFinished()),
            newView, SLOT(doTransitionIn()));

    oldView->doTransitionOut();
}

void PageView::resizeEvent(QGraphicsSceneResizeEvent *event)
{
    QGraphicsWidget::resizeEvent(event);

    QSizeF newSize = event->newSize();
    newSize.setHeight(newSize.height() - m_topOffset);

    m_oldSlot->resize(newSize);
    m_newSlot->resize(newSize);
}

#include "pageview.moc"
