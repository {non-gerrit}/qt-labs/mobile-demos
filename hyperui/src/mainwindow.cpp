/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QDateTime>
#include <QPainter>
#include <QGraphicsRectItem>

#include "dataresource.h"
#include "mainwindow.h"
#include "pageview.h"
#include "menuview.h"
#include "clockwidget.h"
#include "draggablepreview.h"


MainWindow::MainWindow(QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      m_background(Resource::pixmap("background.png"))
{
    m_clockWidget = new ClockWidget(this);
    m_clockWidget->setPos(0, 0);

    // cache the clock into a pixmap to improve drag performance
    m_clockWidget->setCacheMode(QGraphicsItem::ItemCoordinateCache);

    const int width = Resource::intValue("window/width");
    const int height = Resource::intValue("window/height");

    m_overlay = new QGraphicsRectItem(this);
    m_overlay->setBrush(QColor(0, 0, 0, 100));
    m_overlay->setRect(0, 0, width, height);
    m_overlay->hide();

    m_mainView = new PageView();
    m_mainView->add(new MenuView());
    m_preview = new DraggablePreview(m_mainView, QSize(width, height), this);
    connect(m_preview, SIGNAL(draggableStarted()), SLOT(onDragModeIn()));
    connect(m_preview, SIGNAL(minimizeStarted()), SLOT(onDragModeOut()));
    connect(m_preview, SIGNAL(maximizeFinished()), SLOT(onMaximizeFinished()));

    QGraphicsSimpleTextItem *phoneLabel =
        new QGraphicsSimpleTextItem(tr("T-Mobile"), this);

    m_hourLabel = new QGraphicsSimpleTextItem(this);

    QFont labelFont(Resource::stringValue("default/font-family"));
    labelFont.setBold(true);
    labelFont.setPixelSize(Resource::intValue("topbar/font-size"));
    phoneLabel->setFont(labelFont);
    phoneLabel->setBrush(QColor(Resource::stringValue("default/font-color")));

    m_hourLabel->setFont(labelFont);
    m_hourLabel->setBrush(QColor(Resource::stringValue("default/font-color")));

    QGraphicsPixmapItem *iconBattery =
        new QGraphicsPixmapItem(Resource::pixmap("topbar_battery.png"), this);

    QGraphicsPixmapItem *icon3G =
        new QGraphicsPixmapItem(Resource::pixmap("topbar_3g.png"), this);

    QGraphicsPixmapItem *iconNetwork =
        new QGraphicsPixmapItem(Resource::pixmap("topbar_network.png"), this);

    m_hourLabel->setPos(Resource::value("topbar/hour-label-pos").toPoint());
    phoneLabel->setPos(Resource::value("topbar/label-pos").toPoint());
    iconNetwork->setPos(Resource::value("topbar/icon-network-pos").toPoint());
    icon3G->setPos(Resource::value("topbar/icon-3g-pos").toPoint());
    iconBattery->setPos(Resource::value("topbar/icon-battery-pos").toPoint());

    updateTime();
    createDummyDailyEvents();

    // update time each 30 seconds
    startTimer(30000);
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    updateTime();
}

void MainWindow::updateTime()
{
    m_hourLabel->setText(QDateTime::currentDateTime().toString("hh:mm"));
}

void MainWindow::onDragModeIn()
{
    m_overlay->show();
}

void MainWindow::onDragModeOut()
{
    m_overlay->hide();
}

void MainWindow::onMaximizeFinished()
{
    m_overlay->hide();
    m_clockWidget->hide();
}

void MainWindow::createDummyDailyEvents()
{
    QColor color1("#80A2BF");
    QColor color2("#FF5E74");
    QColor color3("#A05284");

    const QDate &cd = QDate::currentDate();

    m_clockWidget->addEvent(QDateTime(cd, QTime(15, 20, 0)),
                            QDateTime(cd, QTime(16, 30, 0)), color1,
                            tr("Development Meeting"));
    m_clockWidget->addEvent(QDateTime(cd, QTime(16, 34, 0)),
                            QDateTime(cd, QTime(17, 15, 0)), color1,
                            tr("Development Meeting"));
    m_clockWidget->addEvent(QDateTime(cd, QTime(17, 19, 0)),
                            QDateTime(cd, QTime(18, 17, 0)), color1,
                            tr("Development Meeting"));
    m_clockWidget->addEvent(QDateTime(cd, QTime(18, 25, 0)),
                            QDateTime(cd, QTime(18, 53, 0)), color1,
                            tr("Development Meeting"));

    m_clockWidget->addEvent(QDateTime(cd, QTime(18, 40, 0)),
                            QDateTime(cd, QTime(19, 20, 0)), color3,
                            tr("Project Presentation"));
    m_clockWidget->addEvent(QDateTime(cd, QTime(19, 24, 0)),
                            QDateTime(cd, QTime(20, 20, 0)), color3,
                            tr("Project Presentation"));

    m_clockWidget->addEvent(QDateTime(cd, QTime(19, 55, 0)),
                            QDateTime(cd, QTime(20, 45, 0)), color2,
                            tr("Dinner with Managers"));
    m_clockWidget->addEvent(QDateTime(cd, QTime(21, 25, 0)),
                            QDateTime(cd, QTime(22, 20, 0)), color2,
                            tr("Dinner with Managers"));
}

void MainWindow::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                       QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->drawPixmap(0, 0, m_background);
}
