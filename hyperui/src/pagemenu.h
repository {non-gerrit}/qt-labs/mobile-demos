/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef PAGEMENU_H
#define PAGEMENU_H

#include <QColor>
#include <QPixmap>
#include <QGraphicsWidget>


class PageMenu : public QGraphicsWidget
{
    Q_OBJECT

public:
    PageMenu(QGraphicsItem *parent = 0);

    QString text() const;
    void setText(const QString &text);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);

private:
    QString m_text;
    QRect m_textRect;
    QColor m_fontColor;
    QPixmap m_background;
};

#endif
