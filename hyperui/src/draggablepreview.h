/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef DRAGGABLEPREVIEW_H
#define DRAGGABLEPREVIEW_H

#include <QGraphicsWidget>

QT_BEGIN_NAMESPACE
class QState;
class QAbstractAnimation;
QT_END_NAMESPACE


class DraggablePreview : public QGraphicsWidget
{
    Q_OBJECT

public:
    DraggablePreview(QGraphicsWidget *item, const QSize &screenSize,
                     QGraphicsItem *parent = 0);

signals:
    void draggableUpdate();
    void draggableStarted();
    void minimizeStarted();
    void maximizeStarted();
    void maximizeFinished();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *e);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *e);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *e);

private slots:
    void onMaximizeFinished();

private:
    void setupInterface();
    QAbstractAnimation *createAnimation(int time, const char *slot = 0);

    QGraphicsWidget *m_item;
    QSize m_screenSize;

    int m_border;
    int m_topMargin;
    int m_leftMargin;
    int m_maximizeRange;

    QPointF m_lastPos;
    QPointF m_draggablePos;
    int m_minimumOffset;
    int m_maximumOffset;

    QGraphicsPixmapItem *m_background;

    QState *m_minimizedState;
    QState *m_draggableState;
    QState *m_maximizedState;
};


#endif
