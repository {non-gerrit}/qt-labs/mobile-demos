/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef MENUVIEW_H
#define MENUVIEW_H

#include "view.h"

class PhoneView;
class QAbstractAnimation;


class MenuView : public View
{
    Q_OBJECT

public:
    MenuView(QGraphicsItem *parent = 0);

protected slots:
    void onPhoneClicked();

protected:
    int m_vSpacing;
    int m_hSpacing;
    int m_topMargin;
    int m_leftMargin;
    QPointF m_mainIconIPos;
    QPointF m_mainIconFPos;

    Button *m_btnTwitter;
    Button *m_btnEmail;
    Button *m_btnSettings;
    Button *m_btnMusic;
    Button *m_btnNavigation;
    Button *m_btnChat;
    Button *m_btnGames;
    Button *m_btnWeb;
    Button *m_btnFolder;
    Button *m_btnCalendar;
    Button *m_btnCamera;
    Button *m_btnPhone;

    PhoneView *m_phoneView;

    void createStateMachine();
    QAbstractAnimation *createInOutAnimation(bool out);

    Button *addIcon(const QPixmap &pixmap, const QPointF &pos,
                    const char *slot = 0);
};

#endif
