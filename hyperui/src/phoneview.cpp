/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QPainter>
#include <QState>
#include <QStateMachine>
#include <QSignalTransition>
#include <QGraphicsPixmapItem>
#include <QGraphicsLinearLayout>
#include <QPropertyAnimation>
#include <QGraphicsGridLayout>
#include <QSequentialAnimationGroup>

#include "dataresource.h"
#include "label.h"
#include "global.h"
#include "button.h"
#include "phoneview.h"
#include "contactlist.h"
#include "contactresource.h"


static QFont resourceButtonFont()
{
    QFont font(Resource::stringValue("default/font-family"));
    font.setBold(true);
    font.setPixelSize(Resource::intValue("button/font-size"));
    return font;
}


class Overlay : public QObject,
                public QGraphicsRectItem
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity)
    Q_PROPERTY(bool visible READ isVisible WRITE setVisible)

public:
    Overlay(QGraphicsItem *parent = 0)
        : QGraphicsRectItem(parent) {}

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *) { }
};


class DialerWidget : public QGraphicsWidget
{
    Q_OBJECT

public:
    DialerWidget(QGraphicsItem *parent = 0);

signals:
    void buttonClicked(const QString &value);

protected:
    void addButton(const QString &label, int row, int col,
                   const QString &imagePrefix);

private slots:
    void onButtonClicked();

private:
    QGraphicsGridLayout *m_layout;
};


DialerWidget::DialerWidget(QGraphicsItem *parent)
    : QGraphicsWidget(parent)
{
    QGraphicsPixmapItem *background =
        new QGraphicsPixmapItem(Resource::pixmap("dialer/background.png"), this);
    background->setPos(0, 0);
    background->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);

    const int margin = Resource::intValue("dialer-widget/margin");
    const int spacing = Resource::intValue("dialer-widget/spacing");

    m_layout = new QGraphicsGridLayout();
    m_layout->setSpacing(spacing);
    m_layout->setContentsMargins(margin, margin, margin, margin);

    addButton("1", 0, 0, "dialer/top_left_key");
    addButton("2", 0, 1, "dialer/middle_key");
    addButton("3", 0, 2, "dialer/top_right_key");
    addButton("4", 1, 0, "dialer/middle_key");
    addButton("5", 1, 1, "dialer/middle_key");
    addButton("6", 1, 2, "dialer/middle_key");
    addButton("7", 2, 0, "dialer/middle_key");
    addButton("8", 2, 1, "dialer/middle_key");
    addButton("9", 2, 2, "dialer/middle_key");
    addButton("*", 3, 0, "dialer/bottom_left_key");
    addButton("0", 3, 1, "dialer/middle_key");
    addButton("#", 3, 2, "dialer/bottom_right_key");

    setLayout(m_layout);
}

void DialerWidget::addButton(const QString &label, int row, int col,
                             const QString &imagePrefix)
{
    const QString &normalPath = QString("%1.png").arg(imagePrefix);
    const QString &pressedPath = QString("%1_pressed.png").arg(imagePrefix);

    Button *button = new Button(Resource::pixmap(normalPath),
                                Resource::pixmap(pressedPath));
    button->setText(label);
    button->setFont(resourceButtonFont());

    connect(button, SIGNAL(clicked()), SLOT(onButtonClicked()));

    m_layout->addItem(button, row, col);
}

void DialerWidget::onButtonClicked()
{
    Button *button = dynamic_cast<Button *>(sender());

    if (button)
        emit buttonClicked(button->text());
}


class CallBoard : public QGraphicsWidget
{
    Q_OBJECT

public:
    CallBoard(QGraphicsItem *parent = 0);

    void setPhoto(const QPixmap &photo);

    void setName(const QString &name);
    void setPhone(const QString &phone);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);

    QGraphicsWidget *contents;
    QGraphicsWidget *panelWait;
    QGraphicsWidget *panelInCall;

private:
    QPixmap m_top;
    QPixmap m_bottom;
    QPixmap m_middleline;

    QGraphicsPixmapItem *m_icon;
    QGraphicsPixmapItem *m_photo;

    Label *m_callLabel;
    Label *m_nameLabel;
    Label *m_phoneLabel;
    Label *m_bigNameLabel;
};


/*!
    \class CallBoard

    This widget will show call information.
*/
CallBoard::CallBoard(QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      m_top(Resource::pixmap("dialer_bk_top.png")),
      m_bottom(Resource::pixmap("dialer_bk_bottom.png")),
      m_middleline(Resource::pixmap("dialer_bk_lineexpand.png"))
{
    // read settings
    QFont font(Resource::stringValue("default/font-family"));
    const int margin = Resource::intValue("call-board/margin");
    const QString &phoneColor = Resource::stringValue("call-board/phone-font-color");
    const QRect &subPanelRect = Resource::value("call-board/sub-panel-rect").toRect();
    const QPoint &dialPos = Resource::value("call-board/dial-button-pos").toPoint();
    const QPoint &mutePos = Resource::value("call-board/mute-button-pos").toPoint();
    const QPoint &speakerPos = Resource::value("call-board/speaker-button-pos").toPoint();
    const QPoint &dialIconPos = Resource::value("call-board/dial-icon-pos").toPoint();
    const QRect &callLabelRect = Resource::value("call-board/call-label-rect").toRect();
    const QRect &nameLabelRect = Resource::value("call-board/name-label-rect").toRect();
    const QRect &phoneLabelRect = Resource::value("call-board/phone-label-rect").toRect();
    const QRect &bigNameLabelRect = Resource::value("call-board/big-name-label-rect").toRect();

    // initialize interface
    setMinimumSize(m_top.size().width(),
                   m_top.size().height() +
                   m_bottom.size().height() +
                   m_middleline.size().height());

    // create main panel and photo
    contents = new QGraphicsWidget(this);
    contents->setFlag(QGraphicsItem::ItemHasNoContents);
    contents->setGeometry(geometry());

    m_photo = new QGraphicsPixmapItem(contents);
    m_photo->setPos(margin, margin);
    m_photo->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);

    m_icon = new QGraphicsPixmapItem(contents);
    m_icon->setPixmap(Resource::pixmap("dialer_bullet_phone.png"));
    m_icon->setPos(dialIconPos);
    m_icon->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);

    // create 'wait' sub-panel
    panelWait = new QGraphicsWidget(contents);
    panelWait->setFlag(QGraphicsItem::ItemHasNoContents);
    panelWait->setGeometry(subPanelRect);

    font.setPixelSize(Resource::intValue("call-board/small-font-size"));

    m_callLabel = new Label(panelWait);
    m_callLabel->setFont(font);
    m_callLabel->setText(tr("Calling..."));
    m_callLabel->setGeometry(callLabelRect);

    font.setPixelSize(Resource::intValue("call-board/font-size"));

    m_nameLabel = new Label(panelWait);
    m_nameLabel->setFont(font);
    m_nameLabel->setGeometry(nameLabelRect);

    m_phoneLabel = new Label(panelWait);
    m_phoneLabel->setFont(font);
    m_phoneLabel->setFontColor(QColor(phoneColor));
    m_phoneLabel->setGeometry(phoneLabelRect);

    // create 'in-call' sub-panel
    panelInCall = new QGraphicsWidget(contents);
    panelInCall->setFlag(QGraphicsItem::ItemHasNoContents);
    panelInCall->setGeometry(subPanelRect);

    m_bigNameLabel = new Label(panelInCall);
    m_bigNameLabel->setFont(font);
    m_bigNameLabel->setGeometry(bigNameLabelRect);

    Button *dialButton = new Button(Resource::pixmap("dialer_bt_dialer.png"),
                                    panelInCall);
    dialButton->setPos(dialPos);

    Button *muteButton = new Button(Resource::pixmap("dialer_bt_mute.png"),
                                    panelInCall);
    muteButton->setPos(mutePos);

    Button *speakerButton = new Button(Resource::pixmap("dialer_bt_speaker.png"),
                                       panelInCall);
    speakerButton->setPos(speakerPos);
}

/*!
  Sets the contact's name.
*/
void CallBoard::setName(const QString &name)
{
    m_nameLabel->setText(name);
    m_bigNameLabel->setText(name);
}

/*!
  Sets the contact's phone.
*/
void CallBoard::setPhone(const QString &phone)
{
    m_phoneLabel->setText(phone);
}

/*!
  Sets the contact photo.
*/
void CallBoard::setPhoto(const QPixmap &photo)
{
    m_photo->setPixmap(photo);
}

/*!
  \internal
*/
void CallBoard::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                      QWidget *)
{
    const int tw = m_top.width();
    const int th = m_top.height();
    const int by = size().height() - m_bottom.height();

    painter->drawPixmap(0, 0, m_top);
    painter->drawTiledPixmap(0, th, tw, by - th, m_middleline);
    painter->drawPixmap(0, by, m_bottom);
}


class DialerDisplay : public QGraphicsWidget
{
    Q_OBJECT

public:
    DialerDisplay(QGraphicsItem *parent = 0);

    QString text() const;
    void append(const QString &value);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);

public slots:
    void clear();

private:
    Label *m_label;
    Button *m_cancel;
    QPixmap m_background;
};


/*!
    \class DialerDisplay

    This widget is used to display the keys pressed in the dialer. It has a
    backspace key and a default background.
*/
DialerDisplay::DialerDisplay(QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      m_background(Resource::pixmap("dialer_display_background.png"))
{
    setMinimumSize(m_background.size());

    m_label = new Label(this);
    m_label->setElideMode(Qt::ElideLeft);
    m_label->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

    QFont font = m_label->font();
    font.setPixelSize(Resource::intValue("phone-view/display-font-size"));
    m_label->setFont(font);

    m_cancel = new Button(Resource::pixmap("dialer_display_bt_cancel.png"));
    connect(m_cancel, SIGNAL(clicked()), SLOT(clear()));

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout(Qt::Horizontal);
    layout->addItem(m_label);
    layout->addItem(m_cancel);
    setLayout(layout);

    layout->setAlignment(m_cancel, Qt::AlignBottom);
}

/*!
  Returns the current display text.
*/
QString DialerDisplay::text() const
{
    return m_label->text();
}

/*!
  Appends a text in the display.
*/
void DialerDisplay::append(const QString &value)
{
    m_label->setText(m_label->text() + value);
}

/*!
  Clear the last char in the display if it's not empty.
*/
void DialerDisplay::clear()
{
    const QString &value = m_label->text();

    if (!value.isEmpty())
        m_label->setText(value.left(value.length() - 1));
}

/*!
  \internal
*/
void DialerDisplay::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                          QWidget *)
{
    painter->drawPixmap(0, 0, m_background);
}


/*!
    \class PhoneView

    This class provides the view to make calls.
*/

PhoneView::PhoneView(QGraphicsItem *parent)
    : View(parent)
{
    setTitle(tr("CONTACTS"));

    // read settings
    const int callTimeout = Resource::intValue("phone-view/call-timeout");
    const QPoint &displayPos = Resource::value("phone-view/display-pos").toPoint();
    const QPoint &dialerBackPos = Resource::value("phone-view/dialer-back-pos").toPoint();
    const QPoint &callButtonPos = Resource::value("phone-view/call-button-pos").toPoint();
    const QPoint &contactsButtonPos = Resource::value("phone-view/contacts-button-pos").toPoint();

    // initialize interface
    setFlag(QGraphicsItem::ItemHasNoContents);

    m_frame = new QGraphicsWidget(this);

    m_display = new DialerDisplay(m_frame);
    m_display->setPos(displayPos);

    m_contactsButton = new Button(Resource::pixmap("dialer_bt_contacts.png"),
                                  Resource::pixmap("dialer_bt_contacts_over.png"),
                                  m_frame);
    m_contactsButton->setPos(contactsButtonPos);
    m_contactsButton->setFont(resourceButtonFont());

    m_overlay = new Overlay(m_frame);
    m_overlay->setBrush(Qt::black);
    m_overlay->setRect(Resource::value("phone-view/overlay-rect").toRect());

    m_callButton = new Button(Resource::pixmap("dialer_bt_call.png"),
                              Resource::pixmap("dialer_bt_call_over.png"), m_frame);
    m_callButton->setText(tr("CALL"));
    m_callButton->setPos(callButtonPos);
    m_callButton->setFont(resourceButtonFont());

    m_endCallButton = new Button(Resource::pixmap("dialer_bt_endcall.png"),
                                 Resource::pixmap("dialer_bt_endcall_over.png"), m_frame);
    m_endCallButton->setText(tr("END CALL"));
    m_endCallButton->setPos(callButtonPos);
    m_endCallButton->setFont(resourceButtonFont());

    m_board = new CallBoard(m_frame);
    m_board->setPos(dialerBackPos);
    m_board->setPhoto(Resource::pixmap("call_photo_nobody.png"));

    m_dialer = new DialerWidget(m_frame);
    m_dialer->setPos(dialerBackPos);
    connect(m_dialer, SIGNAL(buttonClicked(const QString &)),
            SLOT(dialButtonClicked(const QString &)));

    m_contactList = new ContactList(this);
    m_contactList->setGeometry(Resource::value("phone-view/contactlist-rect").toRect());
    m_contactList->hide();
    connect(m_contactList, SIGNAL(contactClicked(int)), SLOT(contactClicked(int)));

    m_callTimer.setInterval(callTimeout);
    m_callTimer.setSingleShot(true);

    setOpacity(0.0);
    createStateMachine();

    connect(m_callButton, SIGNAL(clicked()), SLOT(callClicked()));
}

/*!
  Handle call click.
*/
void PhoneView::callClicked()
{
    // update phone number
    m_board->setName(m_display->text());
    m_board->setPhone("");
    m_board->setPhoto(Resource::pixmap("call_photo_nobody.png"));

    // simulate call wait
    m_callTimer.start();
}

/*!
  Handle contact click.
*/
void PhoneView::contactClicked(int index)
{
    m_board->setName(ContactResource::name(index));
    m_board->setPhone(ContactResource::phone(index));

    const QString &photo = ContactResource::photo(index, ContactResource::LargePhoto);
    if (!photo.isEmpty())
        m_board->setPhoto(Resource::pixmap(photo));
    else
        m_board->setPhoto(Resource::pixmap("call_photo_nobody.png"));

    emit callContact();

    // simulate call wait
    m_callTimer.start();
}

/*!
  Fill the dialer display.
*/
void PhoneView::dialButtonClicked(const QString &value)
{
    m_display->append(value);
}

/*!
  Creates the all states of the view.
*/
void PhoneView::createStateMachine()
{
    QStateMachine *machine = new QStateMachine(this);

    QState *state0 = new QState();
    state0->assignProperty(this, "opacity", 0.0);

    // create default state
    QState *state1 = new QState();
    state1->assignProperty(this, "opacity", 1.0);
    state1->assignProperty(m_frame, "opacity", 1.0);
    state1->assignProperty(m_frame, "visible", true);
    state1->assignProperty(m_dialer, "opacity", 1.0);
    state1->assignProperty(m_display, "visible", true);
    state1->assignProperty(m_board, "visible", false);
    state1->assignProperty(m_callButton, "visible", true);
    state1->assignProperty(m_endCallButton, "visible", false);
    state1->assignProperty(m_board->contents, "opacity", 0.0);
    state1->assignProperty(m_board, "geometry", m_board->geometry());
    state1->assignProperty(m_overlay, "opacity", 0.0);
    state1->assignProperty(m_overlay, "visible", false);
    state1->assignProperty(m_contactList, "y", m_contactList->size().height() * 1.5);
    state1->assignProperty(m_contactList, "visible", false);

    // create calling state
    QState *state2 = new QState();
    state2->assignProperty(m_frame, "opacity", 1.0);
    state2->assignProperty(m_frame, "visible", true);
    state2->assignProperty(m_dialer, "opacity", 0.0);
    state2->assignProperty(m_display, "visible", false);
    state2->assignProperty(m_board, "visible", true);
    state2->assignProperty(m_callButton, "visible", false);
    state2->assignProperty(m_endCallButton, "visible", true);
    state2->assignProperty(m_board->contents, "opacity", 1.0);
    const int offsetY = -(m_board->pos().y() - m_display->pos().y());
    state2->assignProperty(m_board, "geometry",
                           m_board->geometry().adjusted(0, offsetY, 0, 0));

    state2->assignProperty(m_board->panelWait, "opacity", 1.0);
    state2->assignProperty(m_board->panelInCall, "opacity", 0.0);
    state2->assignProperty(m_board->panelWait, "visible", true);
    state2->assignProperty(m_board->panelInCall, "visible", false);
    state2->assignProperty(m_overlay, "opacity", 0.5);
    state2->assignProperty(m_overlay, "visible", true);
    state2->assignProperty(m_contactList, "y", m_contactList->size().height() * 1.5);
    state2->assignProperty(m_contactList, "visible", false);

    // create in-call state
    QState *state3 = new QState();
    state3->assignProperty(m_board->panelWait, "opacity", 0.0);
    state3->assignProperty(m_board->panelInCall, "opacity", 1.0);
    state3->assignProperty(m_board->panelWait, "visible", false);
    state3->assignProperty(m_board->panelInCall, "visible", true);
    state3->assignProperty(m_overlay, "opacity", 0.5);
    state3->assignProperty(m_overlay, "visible", true);

    // create contact list state
    QState *state4 = new QState();
    state4->assignProperty(m_frame, "opacity", 0.0);
    state4->assignProperty(m_frame, "visible", false);
    state4->assignProperty(m_contactList, "y", 0);
    state4->assignProperty(m_contactList, "visible", true);

    // associates state1-state2 transition
    QSignalTransition *transition1 =
        state1->addTransition(m_callButton, SIGNAL(clicked()), state2);
    transition1->addAnimation(createCallAnimation());

    // associates state2-state1 transition
    QSignalTransition *transition2 =
        state2->addTransition(m_endCallButton, SIGNAL(clicked()), state1);
    transition2->addAnimation(createEndCallAnimation());

    // associates state3-state1 transition
    QSignalTransition *transition3 =
        state3->addTransition(m_endCallButton, SIGNAL(clicked()), state1);
    transition3->addAnimation(createEndCallAnimation());

    // associates state2-state3 transition
    QSignalTransition *transition4 =
        state2->addTransition(&m_callTimer, SIGNAL(timeout()), state3);
    transition4->addAnimation(createInCallAnimation());

    // associates state0-state1 transition
    QSignalTransition *transition5 =
        state0->addTransition(this, SIGNAL(transitionInStarted()), state1);
    transition5->addAnimation(createInOutAnimation(false));

    // associates state1-state0 transition
    QSignalTransition *transition6 =
        state1->addTransition(this, SIGNAL(transitionOutStarted()), state0);
    transition6->addAnimation(createInOutAnimation(true));

    // associates state1-state4 transition
    QSignalTransition *transition7 =
        state1->addTransition(m_contactsButton, SIGNAL(clicked()), state4);
    transition7->addAnimation(createContactAnimation(false));

    // associates state4-state2 transition
    QSignalTransition *transition8 =
        state4->addTransition(this, SIGNAL(callContact()), state2);
    transition8->addAnimation(createContactAnimation(true));

    // associates state4-state0 transition
    QSignalTransition *transition9 =
        state4->addTransition(this, SIGNAL(transitionOutStarted()), state0);
    transition9->addAnimation(createInOutAnimation(true));

    machine->addState(state0);
    machine->addState(state1);
    machine->addState(state2);
    machine->addState(state3);
    machine->addState(state4);

    machine->setInitialState(state0);
    machine->start();
}

QAbstractAnimation *PhoneView::createInOutAnimation(bool out)
{
    QSequentialAnimationGroup *result = new QSequentialAnimationGroup();
    result->addAnimation(propertyAnimation(this, "opacity", 400));

    if (!out)
        connect(result, SIGNAL(finished()), SIGNAL(transitionInFinished()));
    else
        connect(result, SIGNAL(finished()), SIGNAL(transitionOutFinished()));

    return result;
}

/*!
  Creates the animation executed in the contacts action.
*/
QAbstractAnimation *PhoneView::createContactAnimation(bool close)
{
    QSequentialAnimationGroup *result = new QSequentialAnimationGroup();

    if (close) {
        result->addAnimation(propertyAnimation(m_contactList, "y", 600,
                                               QEasingCurve::InQuart));
        result->addAnimation(propertyAnimation(m_contactList, "visible", 0));
        result->addAnimation(propertyAnimation(m_frame, "visible", 0));
        result->addAnimation(propertyAnimation(m_frame, "opacity", 400));
    } else {
        result->addAnimation(propertyAnimation(m_contactList, "visible", 0));
        result->addAnimation(propertyAnimation(m_frame, "opacity", 400));
        result->addAnimation(propertyAnimation(m_contactList, "y", 600,
                                               QEasingCurve::OutQuart));
        result->addAnimation(propertyAnimation(m_frame, "visible", 0));
    }

    return result;
}

/*!
  Creates the animation executed in the call action.
*/
QAbstractAnimation *PhoneView::createCallAnimation()
{
    QSequentialAnimationGroup *result = new QSequentialAnimationGroup();
    result->addAnimation(propertyAnimation(m_dialer, "opacity", 200));
    result->addAnimation(propertyAnimation(m_board, "geometry", 300));
    result->addAnimation(propertyAnimation(m_board->contents, "opacity", 200));
    result->addAnimation(propertyAnimation(m_overlay, "opacity", 200));

    return result;
}

/*!
  Creates the animation executed in the in-call action.
*/
QAbstractAnimation *PhoneView::createInCallAnimation()
{
    QSequentialAnimationGroup *result = new QSequentialAnimationGroup();
    result->addAnimation(propertyAnimation(m_overlay, "opacity", 200));
    result->addAnimation(propertyAnimation(m_board->panelWait, "opacity", 300));
    result->addAnimation(propertyAnimation(m_board->panelWait, "visible", 0));
    result->addAnimation(propertyAnimation(m_board->panelInCall, "opacity", 300));
    result->addAnimation(propertyAnimation(m_overlay, "visible", 0));

    return result;
}

/*!
  Creates the animation executed in the end call action.
*/
QAbstractAnimation *PhoneView::createEndCallAnimation()
{
    QSequentialAnimationGroup *result = new QSequentialAnimationGroup();
    result->addAnimation(propertyAnimation(m_overlay, "opacity", 200));
    result->addAnimation(propertyAnimation(m_board->contents, "opacity", 200));
    result->addAnimation(propertyAnimation(m_board, "geometry", 300));
    result->addAnimation(propertyAnimation(m_dialer, "opacity", 200));
    result->addAnimation(propertyAnimation(m_board, "visible", 0));
    result->addAnimation(propertyAnimation(m_display, "visible", 0));
    result->addAnimation(propertyAnimation(m_overlay, "visible", 0));

    return result;
}

#include "phoneview.moc"
