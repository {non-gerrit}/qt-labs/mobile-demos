/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CONTACTRESOURCE_H
#define CONTACTRESOURCE_H

#include <QHash>
#include <QString>

class ContactResource
{
public:
    enum PhotoSize { SmallPhoto, LargePhoto };

    static int count();

    static QString name(int index);
    static QString phone(int index);
    static QString photo(int index, PhotoSize type = SmallPhoto);

    static int indexFromPhone(const QString &phone);

private:
    ContactResource();
    ~ContactResource();
    static ContactResource *instance();

    QString data(int index, int role) const;

    QList<QHash<int, QString> > m_values;
};

#endif
