/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CONTACTLIST_H
#define CONTACTLIST_H

#include <QGraphicsWidget>

#include "view.h"

class ScrollArea;
class ContactPhoto;
class ContactListItem;


class ContactList : public QGraphicsWidget
{
    Q_OBJECT

public:
    ContactList(QGraphicsItem *parent = 0);

    bool containsLetter(char c) const;

signals:
    void contactClicked(int index);

protected slots:
    void letterPressed(char c);

private:
    ScrollArea *m_scrollArea;
    QHash<char, QGraphicsWidget *> m_labels;

    friend class ContactPhoto;
    friend class ContactListItem;
};

#endif
