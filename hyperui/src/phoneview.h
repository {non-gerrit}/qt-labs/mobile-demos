/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef PHONEVIEW_H
#define PHONEVIEW_H

#include <QTimer>
#include <QPixmap>

#include "view.h"

class Label;
class Overlay;
class CallBoard;
class ContactList;
class DialerWidget;
class DialerDisplay;

QT_BEGIN_NAMESPACE
class QAbstractAnimation;
QT_END_NAMESPACE


class PhoneView : public View
{
    Q_OBJECT

public:
    PhoneView(QGraphicsItem *parent = 0);

signals:
    void callContact();

private:
    void createStateMachine();
    QAbstractAnimation *createCallAnimation();
    QAbstractAnimation *createInCallAnimation();
    QAbstractAnimation *createEndCallAnimation();
    QAbstractAnimation *createInOutAnimation(bool out);
    QAbstractAnimation *createContactAnimation(bool close);

private slots:
    void callClicked();
    void contactClicked(int index);
    void dialButtonClicked(const QString &value);

private:
    QTimer m_callTimer;
    Label *m_label;
    Overlay *m_overlay;
    QGraphicsWidget *m_frame;
    Button *m_callButton;
    Button *m_endCallButton;
    Button *m_contactsButton;
    DialerWidget *m_dialer;
    CallBoard *m_board;
    DialerDisplay *m_display;
    ContactList *m_contactList;
};

#endif
