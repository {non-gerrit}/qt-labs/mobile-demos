/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef PAGEVIEW_H
#define PAGEVIEW_H

#include <QStack>
#include <QPixmap>
#include <QGraphicsWidget>

class View;
class Button;
class PageMenu;
class PageSlot;


class PageView : public QGraphicsWidget
{
    Q_OBJECT

public:
    PageView(QGraphicsItem *parent = 0);

    bool add(View *view, bool keepAlive = false);
    bool back();

    bool isAnimating() const;

protected:
    void animateTransition(View *oldView, View *newView, bool isBack);

    void resizeEvent(QGraphicsSceneResizeEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *) { }

private slots:
    void backClicked();
    void optionsClicked();
    void transitionFinished();

private:
    bool m_isBack;
    int m_topOffset;
    bool m_isAnimating;

    QStack<View *> m_views;
    QHash<View *, bool> m_keepAlive;

    PageMenu *m_menu;
    Button *m_backButton;
    Button *m_optionsButton;

    PageSlot *m_oldSlot;
    PageSlot *m_newSlot;
};

#endif
