/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QPainter>
#include <QFontMetrics>

#include "dataresource.h"
#include "pagemenu.h"


PageMenu::PageMenu(QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      m_background(Resource::pixmap("top_bar_active.png"))
{
    QFont textFont = QFont(Resource::stringValue("default/font-family"));
    textFont.setPixelSize(Resource::intValue("page-menu/font-size"));
    setFont(textFont);

    m_textRect = Resource::value("page-menu/label-rect").toRect();
    m_fontColor = QColor(Resource::stringValue("default/font-color"));

    setMinimumSize(m_background.size());
    setMaximumSize(m_background.size());
}

QString PageMenu::text() const
{
    return m_text;
}

void PageMenu::setText(const QString &text)
{
    if (m_text != text) {
        m_text = text;
        update();
    }
}

void PageMenu::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                     QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->drawPixmap(0, 0, m_background);

    QFontMetrics metrics(font());
    const QString &elidedText = metrics.elidedText(m_text, Qt::ElideRight,
                                                   m_textRect.width());

    painter->setFont(font());
    painter->setPen(m_fontColor);
    painter->drawText(m_textRect, Qt::TextSingleLine | Qt::AlignCenter, elidedText);
}
