/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef CLOCKWIDGET_H
#define CLOCKWIDGET_H

#include <QFont>
#include <QTimer>
#include <QPixmap>
#include <QGraphicsWidget>

class ClockEvent;


class ClockWidget : public QGraphicsWidget
{
    Q_OBJECT

public:
    ClockWidget(QGraphicsItem *parent = 0);
    ~ClockWidget();

    void addEvent(const QDateTime &begin, const QDateTime &end,
                  const QColor &color, const QString &text);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);

private slots:
    void updateTime();
    void updateEvents();

private:
    QFont m_dayFont;
    QFont m_labelFont;
    QFont m_weekDayFont;
    QColor m_fontColor;
    QPixmap m_background;
    QPixmap m_divLine;

    QTimer m_timer;
    QPoint m_knobPoint;
    QPoint m_middlePoint;
    QPoint m_labelPos;
    QRectF m_dayRect;
    QRectF m_weekDayRect;
    QRect m_eventsPixmapRect;
    int m_labelHeight;
    int m_eventsInnerOffset;
    int m_eventsInnerDiameter;

    QPixmap m_middleKnob;
    QPixmap m_hourPointer;
    QPixmap m_minutePointer;

    QPixmap m_eventsPixmap;
    QList<ClockEvent *> m_events;
};

#endif
