/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QState>
#include <QStateMachine>
#include <QSignalTransition>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>

#include "dataresource.h"
#include "global.h"
#include "button.h"
#include "menuview.h"
#include "phoneview.h"


#define FPOS(i, j) QPointF(m_leftMargin + m_vSpacing * i, \
                           m_topMargin + m_hSpacing * j)

#define FNPOS(i, j) QPointF(m_vSpacing * -(i + 3), \
                            m_topMargin + m_hSpacing * (j + 2))


MenuView::MenuView(QGraphicsItem *parent)
    : View(parent)
{
    setTitle(tr("MENU"));
    setFlag(QGraphicsItem::ItemHasNoContents);
    setFlag(QGraphicsItem::ItemClipsChildrenToShape);

    // read settings
    m_topMargin = Resource::intValue("menu-view/margin-top");
    m_leftMargin = Resource::intValue("menu-view/margin-left");
    m_vSpacing = Resource::intValue("menu-view/spacing-vertical");
    m_hSpacing = Resource::intValue("menu-view/spacing-horizontal");
    m_mainIconIPos = Resource::value("menu-view/main-icon-pos").toPoint();
    m_mainIconFPos = Resource::value("menu-view/main-icon-out-pos").toPoint();

    // initialize interface
    m_btnTwitter = addIcon(Resource::pixmap("menu_bt_twitter.png"), FPOS(0, 0));

    m_btnEmail = addIcon(Resource::pixmap("menu_bt_email.png"), FPOS(0, 2));
    m_btnSettings = addIcon(Resource::pixmap("menu_bt_settings.png"), FPOS(1, 1));
    m_btnMusic = addIcon(Resource::pixmap("menu_bt_music.png"), FPOS(2, 0));

    m_btnNavigation = addIcon(Resource::pixmap("menu_bt_navigation.png"), FPOS(0, 4));
    m_btnChat = addIcon(Resource::pixmap("menu_bt_chat.png"), FPOS(1, 3));
    m_btnGames = addIcon(Resource::pixmap("menu_bt_games.png"), FPOS(2, 2));
    m_btnWeb = addIcon(Resource::pixmap("menu_bt_web.png"), FPOS(3, 1));

    m_btnFolder = addIcon(Resource::pixmap("menu_bt_folder.png"), FPOS(1, 5));
    m_btnCalendar = addIcon(Resource::pixmap("menu_bt_calendar.png"), FPOS(2, 4));
    m_btnCamera = addIcon(Resource::pixmap("menu_bt_camera.png"), FPOS(3, 3));

    m_btnPhone = addIcon(Resource::pixmap("menu_bt_phone.png"),
                         m_mainIconIPos, SLOT(onPhoneClicked()));

    createStateMachine();

    // keep always alive to reduce transition time
    m_phoneView = new PhoneView();
    m_phoneView->setParent(this);
}

Button *MenuView::addIcon(const QPixmap &pixmap, const QPointF &pos,
                          const char *slot)
{
    Button *button = new Button(pixmap, this);
    button->setPos(pos);

    if (slot)
        connect(button, SIGNAL(clicked()), slot);

    return button;
}

void MenuView::onPhoneClicked()
{
    pageView()->add(m_phoneView, true);
}

void MenuView::createStateMachine()
{
    QStateMachine *machine = new QStateMachine(this);

    QState *state1 = new QState();
    state1->assignProperty(m_btnTwitter, "pos", FNPOS(2, 3));
    state1->assignProperty(m_btnEmail, "pos", FNPOS(2, 5));
    state1->assignProperty(m_btnSettings, "pos", FNPOS(1, 4));
    state1->assignProperty(m_btnMusic, "pos", FNPOS(0, 3));
    state1->assignProperty(m_btnNavigation, "pos", FNPOS(2, 7));
    state1->assignProperty(m_btnChat, "pos", FNPOS(1, 6));
    state1->assignProperty(m_btnGames, "pos", FNPOS(0, 5));
    state1->assignProperty(m_btnWeb, "pos", FNPOS(-1, 4));
    state1->assignProperty(m_btnFolder, "pos", FNPOS(1, 8));
    state1->assignProperty(m_btnCalendar, "pos", FNPOS(0, 7));
    state1->assignProperty(m_btnCamera, "pos", FNPOS(-1, 6));
    state1->assignProperty(m_btnPhone, "pos", m_mainIconFPos);

    QState *state2 = new QState();
    state2->assignProperty(m_btnTwitter, "pos", FPOS(0, 0));
    state2->assignProperty(m_btnEmail, "pos", FPOS(0, 2));
    state2->assignProperty(m_btnSettings, "pos", FPOS(1, 1));
    state2->assignProperty(m_btnMusic, "pos", FPOS(2, 0));
    state2->assignProperty(m_btnNavigation, "pos", FPOS(0, 4));
    state2->assignProperty(m_btnChat, "pos", FPOS(1, 3));
    state2->assignProperty(m_btnGames, "pos", FPOS(2, 2));
    state2->assignProperty(m_btnWeb, "pos", FPOS(3, 1));
    state2->assignProperty(m_btnFolder, "pos", FPOS(1, 5));
    state2->assignProperty(m_btnCalendar, "pos", FPOS(2, 4));
    state2->assignProperty(m_btnCamera, "pos", FPOS(3, 3));
    state2->assignProperty(m_btnPhone, "pos", m_mainIconIPos);

    QSignalTransition *transition1 =
        state1->addTransition(this, SIGNAL(transitionInStarted()), state2);
    transition1->addAnimation(createInOutAnimation(false));

    QSignalTransition *transition2 =
        state2->addTransition(this, SIGNAL(transitionOutStarted()), state1);
    transition2->addAnimation(createInOutAnimation(true));

    machine->addState(state1);
    machine->addState(state2);

    machine->setInitialState(state2);
    machine->start();
}

QAbstractAnimation *MenuView::createInOutAnimation(bool out)
{
    QParallelAnimationGroup *result = new QParallelAnimationGroup();

    const int t = 200;
    const int d = 50;

    if (!out) {
        const QEasingCurve::Type ec = QEasingCurve::OutQuart;

        result->addAnimation(propertyAnimation(m_btnWeb, "pos", t, ec));
        result->addAnimation(propertyAnimation(m_btnMusic, "pos", t + d, ec));
        result->addAnimation(propertyAnimation(m_btnCamera, "pos", t + 2 * d, ec));
        result->addAnimation(propertyAnimation(m_btnGames, "pos", t + 3 * d, ec));
        result->addAnimation(propertyAnimation(m_btnSettings, "pos", t + 4 * d, ec));
        result->addAnimation(propertyAnimation(m_btnTwitter, "pos", t + 5 * d, ec));
        result->addAnimation(propertyAnimation(m_btnCalendar, "pos", t + 6 * d, ec));
        result->addAnimation(propertyAnimation(m_btnChat, "pos", t + 7 * d, ec));
        result->addAnimation(propertyAnimation(m_btnEmail, "pos", t + 8 * d, ec));
        result->addAnimation(propertyAnimation(m_btnFolder, "pos", t + 9 * d, ec));
        result->addAnimation(propertyAnimation(m_btnNavigation, "pos", t + 10 * d, ec));
        result->addAnimation(propertyAnimation(m_btnPhone, "pos", t + 11 * d, ec));

        connect(result, SIGNAL(finished()), SIGNAL(transitionInFinished()));
    } else {
        const QEasingCurve::Type ec = QEasingCurve::InQuart;

        result->addAnimation(propertyAnimation(m_btnPhone, "pos", t, ec));
        result->addAnimation(propertyAnimation(m_btnFolder, "pos", t + d, ec));
        result->addAnimation(propertyAnimation(m_btnNavigation, "pos", t + 2 * d, ec));
        result->addAnimation(propertyAnimation(m_btnCalendar, "pos", t + 3 * d, ec));
        result->addAnimation(propertyAnimation(m_btnChat, "pos", t + 4 * d, ec));
        result->addAnimation(propertyAnimation(m_btnEmail, "pos", t + 5 * d, ec));
        result->addAnimation(propertyAnimation(m_btnCamera, "pos", t + 6 * d, ec));
        result->addAnimation(propertyAnimation(m_btnGames, "pos", t + 7 * d, ec));
        result->addAnimation(propertyAnimation(m_btnSettings, "pos", t + 8 * d, ec));
        result->addAnimation(propertyAnimation(m_btnTwitter, "pos", t + 9 * d, ec));
        result->addAnimation(propertyAnimation(m_btnWeb, "pos", t + 10 * d, ec));
        result->addAnimation(propertyAnimation(m_btnMusic, "pos", t + 11 * d, ec));

        connect(result, SIGNAL(finished()), SIGNAL(transitionOutFinished()));
    }

    return result;
}
