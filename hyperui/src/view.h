/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef VIEW_H
#define VIEW_H

#include <QGraphicsWidget>

#include "pageview.h"


class View : public QGraphicsWidget
{
    Q_OBJECT

public:
    View(QGraphicsItem *parent = 0);

    QString title() const;
    void setTitle(const QString &title);

    PageView *pageView();

public slots:
    virtual void doTransitionIn();
    virtual void doTransitionOut();

signals:
    void titleChanged();

    void transitionInStarted();
    void transitionInFinished();

    void transitionOutStarted();
    void transitionOutFinished();

protected:
    void setPageView(PageView *view);

private:
    QString m_title;
    PageView *m_pageView;

    friend class PageView;
};

#endif
