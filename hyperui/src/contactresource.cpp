/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QFile>
#include <QTextStream>
#include <QStringList>

#include "contactresource.h"


ContactResource::ContactResource()
{
    QFile fp(":/contactlist.txt");
    fp.open(QIODevice::ReadOnly);

    QTextStream buffer(&fp);

    QHash<int, QString> values;

    while (!buffer.atEnd()) {
        const QString &line = buffer.readLine().trimmed();

        if (line.isEmpty())
            continue;

        const QStringList &parts = line.split(':');

        values[0] = parts[0];
        values[1] = parts[1];

        if (parts[2].isEmpty()) {
            values[2] = QString();
            values[3] = QString();
        } else {
            values[2] = QString("list_photo_%1").arg(parts[2]);
            values[3] = QString("call_photo_%1").arg(parts[2]);
        }

        m_values.append(values);
    }

    fp.close();
}

ContactResource::~ContactResource()
{

}

QString ContactResource::data(int index, int role) const
{
    if (index < 0 || index >= m_values.count())
        return QString();
    else
        return m_values[index].value(role, QString());
}

ContactResource *ContactResource::instance()
{
    static ContactResource result;
    return &result;
}

int ContactResource::count()
{
    return instance()->m_values.count();
}

QString ContactResource::name(int index)
{
    return instance()->data(index, 0);
}

QString ContactResource::phone(int index)
{
    return instance()->data(index, 1);
}

QString ContactResource::photo(int index, PhotoSize type)
{
    return instance()->data(index, (type == SmallPhoto) ? 2 : 3);
}

int ContactResource::indexFromPhone(const QString &phone)
{
    ContactResource *d = instance();

    int total = count();
    for (int i = 0; i < total; i++) {
        if (phone == d->data(i, 1))
            return i;
    }

    return -1;
}
