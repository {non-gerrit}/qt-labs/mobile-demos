/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGraphicsWidget>

class PageView;
class ClockWidget;
class DraggablePreview;

QT_BEGIN_NAMESPACE
class QPainter;
class QGraphicsRectItem;
class QStyleOptionGraphicsItem;
QT_END_NAMESPACE


class MainWindow : public QGraphicsWidget
{
    Q_OBJECT

public:
    MainWindow(QGraphicsItem *parent = 0);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);

protected:
    void timerEvent(QTimerEvent *event);

private slots:
    void onDragModeIn();
    void onDragModeOut();
    void onMaximizeFinished();

private:
    void updateTime();
    void createDummyDailyEvents();

    QPixmap m_background;
    PageView *m_mainView;
    ClockWidget *m_clockWidget;
    DraggablePreview *m_preview;
    QGraphicsRectItem *m_overlay;
    QGraphicsSimpleTextItem *m_hourLabel;
};

#endif
