/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "view.h"


View::View(QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      m_pageView(0)
{

}

PageView *View::pageView()
{
    return m_pageView;
}

void View::setPageView(PageView *widget)
{
    m_pageView = widget;
}

QString View::title() const
{
    return m_title;
}

void View::setTitle(const QString &title)
{
    if (m_title != title) {
        m_title = title;
        emit titleChanged();
    }
}

void View::doTransitionIn()
{
    emit transitionInStarted();
}

void View::doTransitionOut()
{
    emit transitionOutStarted();
}
