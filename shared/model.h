/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QHash>
#include <QVariant>


class AbstractModel : public QObject
{
    Q_OBJECT

public:
    AbstractModel(QObject *parent = 0);

    virtual int count() const = 0;

    virtual QVariant data(int index, const QString &role) const = 0;

signals:
    void updated();

    void itemAdded(int index);
    void itemChanged(int index);
    void itemRemoved(int index);
    void itemMoved(int oldIndex, int newIndex);
};


class StandardModel : public AbstractModel
{
    Q_OBJECT

public:
    StandardModel(QObject *parent = 0);

    void clear();

    int count() const;

    void append(const QHash<QString, QVariant> &values);

    void insert(int index, const QHash<QString, QVariant> &values);

    void remove(int index);

    void move(int from, int to);

    QVariant data(int index, const QString &role) const;

    void setData(int index, const QString &role, const QVariant &value);

    void setData(int index, const QHash<QString, QVariant> &values);

private:
    QList<QHash<QString, QVariant> > m_items;
};

#endif
