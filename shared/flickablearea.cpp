/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

#include "flickablearea.h"


FlickableArea::FlickableArea(QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      m_hthreshold(50),
      m_vthreshold(50),
      m_isPressed(false),
      m_isSliding(false),
      m_isFlickable(true)
{

}

int FlickableArea::threshold(Qt::Orientation orientation) const
{
    return (orientation == Qt::Vertical) ? m_vthreshold : m_hthreshold;
}

void FlickableArea::setThreshold(Qt::Orientation orientation, int value)
{
    if (orientation == Qt::Vertical)
        m_vthreshold = value;
    else
        m_hthreshold = value;
}

bool FlickableArea::isFlickable() const
{
    return m_isFlickable;
}

void FlickableArea::setFlickable(bool enabled)
{
    if (m_isFlickable != enabled) {
        m_isFlickable = enabled;

        if (!enabled) {
            m_isPressed = false;
            m_isSliding = false;
        }
    }
}

QRectF FlickableArea::flickableRect() const
{
    return m_flickableRect;
}

void FlickableArea::setFlickableRect(const QRectF &rect)
{
    m_flickableRect = rect;
}

QVariant FlickableArea::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemSceneChange) {
        QGraphicsScene *oldScene = scene();

        if (oldScene)
            oldScene->removeEventFilter(this);

        if (value.canConvert<QGraphicsScene *>()) {
            QGraphicsScene *newScene = value.value<QGraphicsScene *>();

            if (newScene)
                newScene->installEventFilter(this);
        }
    }

    return QGraphicsWidget::itemChange(change, value);
}

bool FlickableArea::eventFilter(QObject *object, QEvent *event)
{
    Q_UNUSED(object);

    if (!m_isFlickable)
        return false;

    const QEvent::Type type = event->type();

    if (type != QEvent::GraphicsSceneMouseMove &&
        type != QEvent::GraphicsSceneMousePress &&
        type != QEvent::GraphicsSceneMouseRelease)
        return false;

    QRectF realRect;
    if (m_flickableRect.isEmpty())
        realRect = mapToScene(boundingRect()).boundingRect();
    else
        realRect = mapToScene(m_flickableRect).boundingRect();

    QGraphicsSceneMouseEvent *e = static_cast<QGraphicsSceneMouseEvent *>(event);

    const QPointF &scenePos = e->scenePos();

    switch(type) {
    case QEvent::GraphicsSceneMousePress:
        if (realRect.contains(scenePos)) {
            m_isPressed = true;
            m_clickPos = scenePos;
        }
        break;
    case QEvent::GraphicsSceneMouseMove:
        if (m_isPressed) {
            if (m_isSliding)
                mouseSlideMoved(scenePos);
            else if (qAbs(scenePos.x() - m_clickPos.x()) >= m_hthreshold ||
                     qAbs(scenePos.y() - m_clickPos.y()) >= m_vthreshold) {
                m_isSliding = true;
                mouseSlideStarted(scenePos);
            }
        }
        break;
    case QEvent::GraphicsSceneMouseRelease:
        if (m_isPressed) {
            mouseSlideFinished(scenePos);
            m_isPressed = false;
            m_isSliding = false;
        }
        break;
    default:
        break;
    }

    return false;
}
