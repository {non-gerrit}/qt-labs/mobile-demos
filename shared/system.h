/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef SYSTEM_H
#define SYSTEM_H

#include <QObject>


class QWidget;


class System : public QObject
{
    Q_OBJECT

public:
    enum ViewMode { PortraitMode, LandscapeMode };

    static void setViewMode(QWidget *window, ViewMode mode);

#ifdef Q_OS_SYMBIAN
private slots:
    void setPortraitMode();
    void setLandscapeMode();
#endif

private:
    System();
    ~System();

    static System *instance();
};

#endif
