symbian {
    LIBS += -lconnmon -lcone -lavkon

}

INCLUDEPATH += ../shared

HEADERS += \
           ../shared/utils.h \
           ../shared/button.h \
           ../shared/label.h \
           ../shared/scrollarea.h \
           ../shared/pixmapwidget.h \
           ../shared/kineticscroll.h \
           ../shared/dataresource.h \
           ../shared/system.h

SOURCES += \
           ../shared/utils.cpp \
           ../shared/button.cpp \
           ../shared/label.cpp \
           ../shared/scrollarea.cpp \
           ../shared/pixmapwidget.cpp \
           ../shared/kineticscroll.cpp \
           ../shared/dataresource.cpp \
           ../shared/system.cpp
