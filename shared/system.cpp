/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QTimer>
#include <QWidget>

#include "system.h"

#ifdef Q_OS_SYMBIAN
#include <eikenv.h>
#include <coemain.h>
#include <aknappui.h>
#endif


System::System()
    : QObject()
{

}

System::~System()
{

}

System *System::instance()
{
    static System result;
    return &result;
}

void System::setViewMode(QWidget *window, ViewMode mode)
{
#if defined(Q_OS_SYMBIAN)
    Q_UNUSED(window);

    // we need to use singleshot since CAknAppUi object is only
    // available in the main loop
    if (mode == PortraitMode)
        QTimer::singleShot(0, instance(), SLOT(setPortraitMode()));
    else
        QTimer::singleShot(0, instance(), SLOT(setLandscapeMode()));

#elif defined(Q_WS_MAEMO_5)
    if (mode == PortraitMode)
        window->setAttribute(Qt::WA_Maemo5PortraitOrientation, true);
    else if (mode == LandscapeMode)
        window->setAttribute(Qt::WA_Maemo5LandscapeOrientation, true);
    else
        window->setAttribute(Qt::WA_Maemo5AutoOrientation, true);
#else
    Q_UNUSED(window);
    Q_UNUSED(mode);
#endif
}

#ifdef Q_OS_SYMBIAN

void System::setPortraitMode()
{
    CAknAppUi *aknAppUi = dynamic_cast<CAknAppUi *>(CEikonEnv::Static()->AppUi());

    if (aknAppUi)
        aknAppUi->SetOrientationL(CAknAppUi::EAppUiOrientationPortrait);
}

void System::setLandscapeMode()
{
    CAknAppUi *aknAppUi = dynamic_cast<CAknAppUi *>(CEikonEnv::Static()->AppUi());

    if (aknAppUi)
        aknAppUi->SetOrientationL(CAknAppUi::EAppUiOrientationLandscape);
}

#endif
