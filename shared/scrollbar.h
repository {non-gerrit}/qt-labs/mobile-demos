/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef SCROLLBAR_H
#define SCROLLBAR_H

#include "pixmapwidget.h"

class QPixmap;
class QGraphicsItem;
class ScrollBarPrivate;


class ScrollBar : public PixmapWidget
{
    Q_OBJECT

public:
    ScrollBar(const QPixmap &background, const QPixmap &foreground,
              QGraphicsItem *parent = 0);

    int value() const;
    void setValue(int value);

    int maximum() const;
    void setMaximum(int maximum);

    int pageSize() const;
    void setPageSize(int pageSize);

    void setKnobBorders(int left, int top, int right, int bottom);

protected:
    void resizeEvent(QGraphicsSceneResizeEvent *event);

private:
    ScrollBarPrivate *d;
    friend class ScrollBarPrivate;
};

#endif
