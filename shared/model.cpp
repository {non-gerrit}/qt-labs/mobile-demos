/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QDir>

#include "model.h"


AbstractModel::AbstractModel(QObject *parent)
    : QObject(parent)
{

}



StandardModel::StandardModel(QObject *parent)
    : AbstractModel(parent)
{

}

int StandardModel::count() const
{
    return m_items.count();
}

void StandardModel::clear()
{
    m_items.clear();
    emit updated();
}

void StandardModel::insert(int index, const QHash<QString, QVariant> &values)
{
    m_items.insert(index, values);
    emit itemAdded(index);
}

void StandardModel::remove(int index)
{
    if (index >= 0 && index < m_items.count()) {
        m_items.removeAt(index);
        emit itemRemoved(index);
    }
}

void StandardModel::move(int from, int to)
{
    if (from >= 0 && from < m_items.count() && to >= 0 && to < m_items.count()) {
        QHash<QString, QVariant> values = m_items[from];
        m_items.removeAt(from);
        m_items.insert(to, values);

        emit itemMoved(from, to);
    }
}

void StandardModel::append(const QHash<QString, QVariant> &values)
{
    m_items.append(values);
    emit itemAdded(m_items.count() - 1);
}

QVariant StandardModel::data(int index, const QString &role) const
{
    return m_items[index].value(role);
}

void StandardModel::setData(int index, const QString &role, const QVariant &value)
{
    m_items[index][role] = value;
    itemChanged(index);
}

void StandardModel::setData(int index, const QHash<QString, QVariant> &values)
{
    m_items[index] = values;
    itemChanged(index);
}
