/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QPixmap>

#include "scrollbar.h"


class ScrollBarPrivate
{
public:
    ScrollBarPrivate(ScrollBar *qptr);
    void updateKnob();

    ScrollBar *q;
    int value;
    int pageSize;
    int maximum;
    PixmapWidget *knob;
};

ScrollBarPrivate::ScrollBarPrivate(ScrollBar *qptr)
    : q(qptr), value(0), pageSize(10), maximum(100)
{

}

void ScrollBarPrivate::updateKnob()
{
    qreal th = q->size().height();
    qreal fh = ((qreal)pageSize / qMax(pageSize + maximum, 1)) * th;

    int tb, bb;
    knob->getBorders(0, &tb, 0, &bb);
    knob->resize(knob->preferredWidth(), qBound<qreal>(tb + bb, fh, th));

    qreal max = qMax<qreal>(0, th - knob->size().height());
    knob->setY((value * max) / qMax(1, maximum));
}


ScrollBar::ScrollBar(const QPixmap &background,
                     const QPixmap &foreground,
                     QGraphicsItem *parent)
    : PixmapWidget(background, parent),
      d(new ScrollBarPrivate(this))
{
    setCacheMode(QGraphicsItem::ItemCoordinateCache);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

    d->knob = new PixmapWidget(foreground, this);
    d->knob->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    d->knob->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

    d->knob->setPos(0, 0);
    d->updateKnob();
}

int ScrollBar::value() const
{
    return d->value;
}

void ScrollBar::setValue(int value)
{
    if (d->value != value) {
        d->value = value;
        d->updateKnob();
    }
}

int ScrollBar::maximum() const
{
    return d->maximum;
}

void ScrollBar::setMaximum(int maximum)
{
    if (d->maximum != maximum) {
        d->maximum = maximum;
        d->updateKnob();
    }
}

int ScrollBar::pageSize() const
{
    return d->pageSize;
}

void ScrollBar::setPageSize(int pageSize)
{
    if (d->pageSize != pageSize) {
        d->pageSize = pageSize;
        d->updateKnob();
    }
}

void ScrollBar::setKnobBorders(int left, int top, int right, int bottom)
{
    d->knob->setBorders(left, top, right, bottom);
}

void ScrollBar::resizeEvent(QGraphicsSceneResizeEvent *event)
{
    PixmapWidget::resizeEvent(event);
    d->updateKnob();
}
