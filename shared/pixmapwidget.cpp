/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QPixmap>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>

#include "pixmapwidget.h"
#include "utils.h"


class PixmapWidgetPrivate
{
public:
    PixmapWidgetPrivate();

    int topBorder;
    int leftBorder;
    int rightBorder;
    int bottomBorder;

    QPixmap pixmap;
};

PixmapWidgetPrivate::PixmapWidgetPrivate()
    : topBorder(0), leftBorder(0), rightBorder(0), bottomBorder(0)
{

}


PixmapWidget::PixmapWidget(const QPixmap &pixmap, QGraphicsItem *parent)
    : QGraphicsWidget(parent),
      d(new PixmapWidgetPrivate)
{
    setPixmap(pixmap);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

PixmapWidget::~PixmapWidget()
{
    delete d;
}

QPixmap PixmapWidget::pixmap() const
{
    return d->pixmap;
}

void PixmapWidget::setPixmap(const QPixmap &pixmap)
{
    d->pixmap = pixmap;

    if (pixmap.isNull())
        setPreferredSize(QSize());
    else
        setPreferredSize(pixmap.size());

    update();
    updateGeometry();
}

void PixmapWidget::getBorders(int *left, int *top, int *right, int *bottom) const
{
    if (left)
        *left = d->leftBorder;
    if (top)
        *top = d->topBorder;
    if (right)
        *right = d->rightBorder;
    if (bottom)
        *bottom = d->bottomBorder;
}

void PixmapWidget::setBorders(int left, int top, int right, int bottom)
{
    d->leftBorder = left;
    d->topBorder = top;
    d->rightBorder = right;
    d->bottomBorder = bottom;
    update();
}

void PixmapWidget::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                         QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    bDrawPixmap(painter, d->pixmap, boundingRect(), d->leftBorder,
                d->topBorder, d->rightBorder, d->bottomBorder);
}
