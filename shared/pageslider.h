/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef PAGESLIDER_H
#define PAGESLIDER_H

#include <QGraphicsWidget>

#include "flickablearea.h"

class Page;
class PageSliderPrivate;


class Page : public QGraphicsWidget
{
    Q_OBJECT

public:
    Page(QGraphicsItem *parent = 0);

    virtual QRectF flickableRect() const;

    bool isFullOpaque() const;
    void setFullOpaque(bool opaque);

    QPainterPath opaqueArea() const;

private:
    bool m_fullOpaque;
};


class PageSlider : public FlickableArea
{
    Q_OBJECT

public:
    PageSlider(QGraphicsItem *parent = 0);
    ~PageSlider();

    bool addPage(Page *view);
    bool removePage(Page *view);

    int currentPage() const;
    void setCurrentPage(int index);

    int pageCount() const;

    int adjustmentThreshold() const;
    void setAdjustmentThreshold(int value);

    bool moveLeft();
    bool moveRight();

protected:
    void mouseSlideStarted(const QPointF &pos);
    void mouseSlideMoved(const QPointF &pos);
    void mouseSlideFinished(const QPointF &pos);

    void resizeEvent(QGraphicsSceneResizeEvent *event);

private slots:
    void transitionFinished();

private:
    PageSliderPrivate *d;
};

#endif
