/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef SCROLLAREA_H
#define SCROLLAREA_H

#include <QGraphicsWidget>

class KineticScroll;
class ScrollAreaPrivate;


class ScrollArea : public QGraphicsWidget
{
    Q_OBJECT

public:
    ScrollArea(QGraphicsItem *parent = 0);
    ~ScrollArea();

    QGraphicsWidget *widget() const;
    void setWidget(QGraphicsWidget *widget);

    int offset() const;
    void setOffset(int offset);

    int maximumOffset() const;

    void stopKinetic();

signals:
    void offsetChanged();
    void maximumOffsetChanged();

protected slots:
    bool kineticMove(int value);

protected:
    bool eventFilter(QObject *watched, QEvent *event);
    void resizeEvent(QGraphicsSceneResizeEvent *event);

    void mousePressEvent(QGraphicsSceneMouseEvent *e);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *e);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *e);

private:
    ScrollAreaPrivate *d;
    friend class ScrollAreaPrivate;
};

#endif
