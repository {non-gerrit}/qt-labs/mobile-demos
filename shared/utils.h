/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef UTILS_H
#define UTILS_H

class QSize;
class QRect;
class QRectF;
class QPixmap;
class QPainter;

QRect bTileRectAt(int order, const QSize &size, int leftBorder,
                  int topBorder, int rightBorder, int bottomBorder);

void bDrawPixmap(QPainter *painter, const QPixmap &pixmap, const QRectF &boundingRect,
                 int leftBorder = 0, int topBorder = 0, int rightBorder = 0,
                 int bottomBorder = 0);

#endif
