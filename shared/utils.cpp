/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QRect>
#include <QRectF>
#include <QPixmap>
#include <QPainter>

#include "utils.h"

QRect bTileRectAt(int order, const QSize &size, int leftBorder,
                  int topBorder, int rightBorder, int bottomBorder)
{
    int w = size.width();
    int h = size.height();

    switch (order) {
    case 0:
        return QRect(0, 0, leftBorder, topBorder);
    case 1:
        return QRect(leftBorder, 0,
                     w - leftBorder - rightBorder, topBorder);
    case 2:
        return QRect(w - rightBorder, 0, rightBorder, topBorder);
    case 3:
        return QRect(0, topBorder,
                     leftBorder, h - topBorder - bottomBorder);
    case 4:
        return QRect(leftBorder, topBorder,
                     w - leftBorder - rightBorder,
                     h - topBorder - bottomBorder);
    case 5:
        return QRect(w - rightBorder, topBorder,
                     rightBorder, h - topBorder - bottomBorder);
    case 6:
        return QRect(0, h - bottomBorder, leftBorder, bottomBorder);
    case 7:
        return QRect(leftBorder, h - bottomBorder,
                     w - leftBorder - rightBorder, bottomBorder);
    case 8:
        return QRect(w - rightBorder, h - bottomBorder,
                     rightBorder, bottomBorder);
    default:
        return QRect();
    }
}


void bDrawPixmap(QPainter *painter, const QPixmap &pixmap, const QRectF &boundingRect,
                 int leftBorder, int topBorder, int rightBorder, int bottomBorder)
{
    if (pixmap.isNull())
        return;

    if (leftBorder <= 0 && rightBorder <= 0 && topBorder <= 0 && bottomBorder <= 0)
        painter->drawPixmap(boundingRect.toRect(), pixmap);
    else {
        for (int i = 0; i < 9; i++) {
            QRect oRect = bTileRectAt(i, pixmap.size(), leftBorder, topBorder,
                                      rightBorder, bottomBorder);
            QRect dRect = bTileRectAt(i, boundingRect.size().toSize(), leftBorder,
                                      topBorder, rightBorder, bottomBorder);

            dRect.translate(boundingRect.x(), boundingRect.y());

            if (!oRect.isEmpty() && !dRect.isEmpty())
                painter->drawPixmap(dRect, pixmap, oRect);
        }
    }
}
