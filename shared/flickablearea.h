/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef FLICKABLEAREA_H
#define FLICKABLEAREA_H

#include <QGraphicsWidget>


class FlickableArea : public QGraphicsWidget
{
    Q_OBJECT

public:
    FlickableArea(QGraphicsItem *parent = 0);

    bool isFlickable() const;
    void setFlickable(bool enabled);

    int threshold(Qt::Orientation orientation) const;
    void setThreshold(Qt::Orientation orientation, int value);

    QRectF flickableRect() const;
    void setFlickableRect(const QRectF &rect);

protected:
    bool eventFilter(QObject *object, QEvent *event);
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

    virtual void mouseSlideStarted(const QPointF &pos) = 0;
    virtual void mouseSlideMoved(const QPointF &pos) = 0;
    virtual void mouseSlideFinished(const QPointF &pos) = 0;

private:
    int m_hthreshold;
    int m_vthreshold;
    bool m_isPressed;
    bool m_isSliding;
    bool m_isFlickable;
    QPointF m_clickPos;
    QRectF m_flickableRect;
};

#endif
