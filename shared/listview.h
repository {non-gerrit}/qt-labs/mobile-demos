/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef LISTVIEW_H
#define LISTVIEW_H

#include <QPointF>
#include <QGraphicsWidget>

#include "model.h"

class ListView;
class ListViewItem;
class ListViewPrivate;
class ListViewItemPrivate;


class AbstractListViewCreator
{
public:
    virtual ListViewItem *create() = 0;
};


template <typename T>
class ListViewCreator : public AbstractListViewCreator
{
public:
    ListViewItem *create() { return new T(); }
};


class ListViewItem : public QGraphicsItem
{
public:
    ListViewItem(QGraphicsItem *parent = 0);
    ~ListViewItem();

    int index() const;
    bool isPressed() const;

    QSizeF size() const;
    QRectF boundingRect() const;

    QSizeF preferredSize() const;
    void setPreferredSize(const QSizeF &size);

protected:
    ListView *listView() const;
    void resize(const QSizeF &value);

    virtual void contentsChanged();
    virtual void selectionChanged();
    virtual void indexChange(int oldIndex, int newIndex);

private:
    ListViewItemPrivate *d;

    friend class ListView;
    friend class ListViewPrivate;
    friend class ListViewItemPrivate;
};


class ListView : public QGraphicsWidget
{
    Q_OBJECT

public:
    ListView(QGraphicsItem *parent = 0);
    ~ListView();

    AbstractModel *model() const;
    void setModel(AbstractModel *model);

    int offset() const;
    int maximumOffset() const;

    void setCreator(AbstractListViewCreator *creator);

    int indexAtOffset(int offset);
    ListViewItem *itemFromIndex(int index);

signals:
    void offsetChanged();
    void maximumOffsetChanged();
    void itemClicked(int index);

public slots:
    void setOffset(int position);

protected:
    ListViewPrivate *d_ptr;

    ListView(ListViewPrivate &dptr, QGraphicsItem *parent = 0);

    virtual void modelChanged();
    void resizeEvent(QGraphicsSceneResizeEvent *event);
    void setItemPressed(ListViewItem *item, bool pressed);

protected slots:
    void reconfigureRenderers();

    virtual void itemAdded(int index);
    virtual void itemChanged(int index);
    virtual void itemRemoved(int index);
    virtual void itemMoved(int oldIndex, int newIndex);

private:
    ListViewPrivate *d;
    friend class ListViewPrivate;
};



class KineticListViewPrivate;

class KineticListView : public ListView
{
    Q_OBJECT

public:
    KineticListView(QGraphicsItem *parent = 0);
    ~KineticListView();

public Q_SLOTS:
    void kineticMove(int);

protected:
    KineticListView(KineticListViewPrivate &dptr, QGraphicsItem *parent = 0);

    void modelChanged();
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    KineticListViewPrivate *d;
    friend class KineticListViewPrivate;
};

#endif
