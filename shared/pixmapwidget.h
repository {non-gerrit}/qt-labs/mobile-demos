/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef PIXMAPWIDGET_H
#define PIXMAPWIDGET_H

#include <QSizeF>
#include <QGraphicsWidget>

class QPixmap;
class QGraphicsSceneMouseEvent;
class QStyleOptionGraphicsItem;

class PixmapWidgetPrivate;


class PixmapWidget : public QGraphicsWidget
{
    Q_OBJECT

public:
    PixmapWidget(const QPixmap &pixmap = QPixmap(), QGraphicsItem *parent = 0);
    ~PixmapWidget();

    QPixmap pixmap() const;
    void setPixmap(const QPixmap &pixmap);

    void getBorders(int *left, int *top, int *right, int *bottom) const;
    void setBorders(int left, int top, int right, int bottom);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);

private:
    PixmapWidgetPrivate *d;
    friend class PixmapWidgetPrivate;
};

#endif
