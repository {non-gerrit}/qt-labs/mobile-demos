TEMPLATE = app
TARGET =

# All generated files goes same directory
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build
DESTDIR = build

INSTALLS    += desktop
desktop.path  = /usr/share/applications/hildon
desktop.files  = data/shoplist.desktop

INSTALLS    += icon64
icon64.path  = /usr/share/icons/hicolor/64x64/apps
icon64.files  = data/icon-shoplist.png

include(src/src.pri)
include(../shared/shared.pri)

target.path = $$PREFIX/bin
INSTALLS += target

RESOURCES += resources.qrc
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.5

# S60
symbian {
    TARGET.EPOCHEAPSIZE = 0x20000 \
        0x2000000
    TARGET.UID3 = 0xe1234568
    ICON = data/icon-shoplist.svg
}

# Maemo 5
linux-g++-maemo5{
  # Targets for debian source and binary package creation
  debian-src.commands = dpkg-buildpackage -S -r -us -uc -d
  debian-bin.commands = dpkg-buildpackage -b -r -uc -d
  debian-all.depends = debian-src debian-bin


  # Clean all but Makefile
  compiler_clean.commands = -$(DEL_FILE) $(TARGET)

  QMAKE_EXTRA_TARGETS += debian-all debian-src debian-bin compiler_clean
}
