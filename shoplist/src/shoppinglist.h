/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/
#ifndef SHOPPINGLIST_H
#define SHOPPINGLIST_H

#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>
#include "contentlist.h"

class ShoppingListPixmap : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity);
public:
    ShoppingListPixmap(QGraphicsItem *parent = 0)
        : QGraphicsPixmapItem(parent) {}
public slots:
    void doHide() { hide(); }
};

class ShoppingList : public ContentList
{
    Q_OBJECT
public:
    ShoppingList(QGraphicsItem *parent = 0);
    QAbstractAnimation *getInsertAnimation(int idx, qreal height);
    QAbstractAnimation *getRemoveAnimation(int idx);

    void setActive(bool active) { m_active = active; }
    bool active() const { return m_active; }

signals:
    void completed();

public slots:
    void clearChecks();

private:
    friend class ShoppingListItemClickActivity;

    int m_checkedItems;
    bool m_active;

    QAbstractAnimation *getItemMoveAnimation(int idx, qreal offset);
    QAbstractAnimation *createCompoundAnimation(QList<QAbstractAnimation*>);
};

class ShoppingListItem : public ContentListItem
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity);
public:
    ShoppingListItem(const QString& text, ShoppingList *list,
                     int pos, QGraphicsItem *parent = 0);

    qreal contentHeight() const;
    QAbstractAnimation *getShowAnimation();
    QAbstractAnimation *getHideAnimation();

    bool checked() const { return m_image2->isVisible(); }
    int listPos() const { return m_pos; }
    void uncheck();

    void updateUI();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    ShoppingListPixmap *m_image1;
    ShoppingListPixmap *m_image2;
    ShoppingListPixmap *m_line;
    ShoppingListPixmap *m_selected;
    QGraphicsTextItem *m_text;
    QAbstractAnimation *getSwitchAnimation();
    QAbstractAnimation *addAnimationPause(QAbstractAnimation *animation, int msecs);
    template<class T> QAbstractAnimation *getFadeAnimation(T *target, bool hide, int msecs);
    void updatePixmapItem(ShoppingListPixmap *item, const QString &key, qreal zValue);

private slots:
    void doHide() { hide(); }

private:
    friend class ShoppingListItemClickActivity;
    QPointer<ShoppingList> m_list;
    const int m_pos;
    qreal m_contentHeight;

};

#endif // SHOPPINGLIST_H
