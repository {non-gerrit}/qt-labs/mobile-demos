/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "designinformation.h"
#include <QDebug>
#include <cmath>

static const char *groceriesItemsText[] = {
    "Apples","Avocado","Bacon","Bagels","Baking powder","Balsamic vinegar","Bananas",
    "Basil","Basmati rice","Beans","Beef","Bell peppers","Biscuits","Blackberries","Blueberries",
    "Brats","Bread (not white)","Broccoli","Brownie mix","Brussels sprouts","Burger patties",
    "Butter","Cabbage","Cake mix","Canned tomatoes","Cantaloupe","Carrots","Cauliflower","Cereal",
    "Cheddar","Cheese sticks","Cherries","Chicken","Chips","Chocolate chips","Cookie dough",
    "Cookie mix","Cookies","Corn","Cottage cheese","Couscous","Crackers","Cream cheese",
    "Cucumbers","Eggs","English muffins","Fish","Flour","Fruit snacks","Garlic","Ginger",
    "Graham crackers","Granola bars","Grapefruit","Grapes","Green beans","Ground beef",
    "Ground turkey","Ham","Hamburger buns","Honey","Hot cereal","Hot dogs","Hot-dog buns",
    "Jello","Kiwi","Lasagna noodles","Lemons","Lettuce","Limes","Mac & Cheese","Marshmallow",
    "Melon","Mozzarella","Muffin mix","Mushrooms","Mustard","Nectarines","Noodle & Sauce Mix",
    "Oatmeal","Olive Oil","Onions","Oranges","Pancake mix","Pancake syrup","Parmesan",
    "Pasta (whole wheat)","Peaches","Peanut butter","Pears","Peas","Peppers","Pizza sauce",
    "Plums","Popcorn","Pork chops","Pork roast","Potatoes","Pudding","Quinoa","Raisins",
    "Raspberries","Raw nuts","Rice","RiceARoni","Risotto","Sausage","Shredded cheese",
    "Sliced cheese","Snap peas","Soup","Sour cream","Spaghetti sauce","Spaghetti",
    "Squash","Steaks","Strawberries","String beans","Sugar","Tomato sauce","Tomatoes",
    "Tortillas","Tuna","Veal","White bread","White vinegar","Whole wheat bread","Yogurt","Zucchini",
    0
};

static const char *toiletriesItemsText[] = {
    "Antiseptic cream","Baby lotion","Band aids","Bath soap","Body wash ","Conditioner",
    "Contact solution","Deodorant","Facial soap","Facial tissue","Floss","Hair dye",
    "Hand lotion","Leave-in conditioner","Moisturizer","Mouthwash","Pads","Rash creams",
    "Razors","Shampoo","Shaving cream","Soap","Suntan lotion","Tampons","Toilet paper",
    "Toothpaste","Vitamins","Wipes","Scented toilet paper",
    0
};

static const char *cleaningItemsText[] = {
    "Aluminum foil","Bleach","Cleanser","Dishwasher soap","Disinfectant","Dixie cups",
    "Dusting spray","Fabric softener","Kleenex","Laundry detergent","Napkins","Paper towels",
    "Plastic wrap","Toilet paper","Trash bags","Ziplock bags","Wax paper","Floor wax",
    0
};

static const char *beveragesItemsText[] = {
    "Apple juice","Beer","Black tea","Bottled water","Champagne","Cordial","Green tea","Ice tea",
    "Juice boxes","KoolAid","Lemonade","Milk","Orange juice","Coca-Cola","Powerade",
    "Raspberry juice","Red wine","Skinned milk","Sparkling water","Tea","Vodka","White wine",
    "Coke Zero",
    0
};

static const char *petItemsText[] = {
    "Bird food","Bone","Cat food","Cat Litter","Catnip","Dog biscuit","Dog food","Dog toys",
    "Dog treats","Fish food","Fish tanks pebbles","Hamster food","Hamster hammock","Pet wash",
    "Turtle soap",
    0
};

static const char *gardenItemsText[] = {
    "Fertilizer","Flower seeds","Hose","Leaf bags","Manure","Scissors","Shovel","Vegetable seeds",
    "Pesticide","Mouse trap",
    0
};

DesignInformation::DesignInformation()
    : m_scaleFactor(1, 1)
{

    m_stringLists["category.string_list.beverages"] = beveragesItemsText;
    m_stringLists["category.string_list.cleaning"] = cleaningItemsText;
    m_stringLists["category.string_list.garden"] = gardenItemsText;
    m_stringLists["category.string_list.groceries"] = groceriesItemsText;
    m_stringLists["category.string_list.pet"] = petItemsText;
    m_stringLists["category.string_list.toiletries"] = toiletriesItemsText;

    m_pixmaps["category.pic.beverages"] = new ValueData<QPixmap, QString>(":/images/list_icon_beverages.png");
    m_pixmaps["category.shadow.beverages"] = new ValueData<QPixmap, QString>(":/images/list_shadow_beverages_50.png");
    m_pixmaps["category.header.beverages"] = new ValueData<QPixmap, QString>(":/images/list_title_beverages.png");

    m_pixmaps["category.pic.cleaning"] = new ValueData<QPixmap, QString>(":/images/list_icon_cleaning.png");
    m_pixmaps["category.shadow.cleaning"] = new ValueData<QPixmap, QString>(":/images/list_shadow_cleaning_50.png");
    m_pixmaps["category.header.cleaning"] = new ValueData<QPixmap, QString>(":/images/list_title_cleaning.png");

    m_pixmaps["category.pic.garden"] = new ValueData<QPixmap, QString>(":/images/list_icon_garden.png");
    m_pixmaps["category.shadow.garden"] = new ValueData<QPixmap, QString>(":/images/list_shadow_garden_50.png");
    m_pixmaps["category.header.garden"] = new ValueData<QPixmap, QString>(":/images/list_title_garden.png");

    m_pixmaps["category.pic.groceries"] = new ValueData<QPixmap, QString>(":/images/list_icon_groceries.png");
    m_pixmaps["category.shadow.groceries"] = new ValueData<QPixmap, QString>(":/images/list_shadow_groceries_50.png");
    m_pixmaps["category.header.groceries"] = new ValueData<QPixmap, QString>(":/images/list_title_groceries.png");

    m_pixmaps["category.pic.pet"] = new ValueData<QPixmap, QString>(":/images/list_icon_pet.png");
    m_pixmaps["category.shadow.pet"] = new ValueData<QPixmap, QString>(":/images/list_shadow_pet_50.png");
    m_pixmaps["category.header.pet"] = new ValueData<QPixmap, QString>(":/images/list_title_pet.png");

    m_pixmaps["category.pic.toiletries"] = new ValueData<QPixmap, QString>(":/images/list_icon_toiletries.png");
    m_pixmaps["category.shadow.toiletries"] = new ValueData<QPixmap, QString>(":/images/list_shadow_toiletries_50.png");
    m_pixmaps["category.header.toiletries"] = new ValueData<QPixmap, QString>(":/images/list_title_toiletries.png");

    m_pixmaps["category.background"] = new ValueData<QPixmap, QString>(":/images/list_background.png");
    m_pixmaps["category.feather"] = new ValueData<QPixmap, QString>(":/images/list_feather.png");

    m_positions["category.pic"] = new ValueData<QPointF>(QPointF(3, 0));
    m_positions["category.header"] = new ValueData<QPointF>(QPointF(110, 54));
    m_positions["category.background"] = new ValueData<QPointF>(QPointF(0, 39));
    m_positions["category.shadow"] = new ValueData<QPointF>(QPointF(3, -1));
    m_positions["category.list"] = new ValueData<QPointF>(QPointF(2, m_listTop));
    m_positions["category.feather"] = new ValueData<QPointF>(QPointF(3, m_listTop));
    m_positions["category.scrollbar"] = new ValueData<QPointF>(QPointF(415, 154));
    
    m_verticalSizes["category.list_height"] = new ValueData<qreal>(800.0);

    // ---------------------------------------------------------------------

    m_pixmaps["list_item.checked"] = new ValueData<QPixmap, QString>(":/images/list_button_checked.png");
    m_positions["list_item.checked"] = new ValueData<QPointF>(QPointF(27, 28));

    m_pixmaps["list_item.not_checked"] = new ValueData<QPixmap, QString>(":/images/list_button_check.png");
    m_positions["list_item.not_checked"] = new ValueData<QPointF>(QPointF(27, 28));

    m_pixmaps["list_item.bottom_line"] = new ValueData<QPixmap, QString>(":/images/list_line.png");
    m_positions["list_item.bottom_line"] = new ValueData<QPointF>(QPointF(1, 88));

    m_pixmaps["list_item.selected"] = new ValueData<QPixmap, QString>(":/images/list_selecteditem_background.png");
    m_positions["list_item.selected"] = new ValueData<QPointF>(QPointF(1, 0));

    m_horizontalSizes["list_item.text_offset"] = new ValueData<qreal>(20.0);

    m_verticalSizes["list_item.height"] = new ValueData<qreal>(m_referenceItemHeight);

    m_fontSizes["list_item.font_size"] = new ValueData<int>(30);

    m_verticalSizes["list.vertical_speed"] = new ValueData<qreal>(1500.0);
    

    // ---------------------------------------------------------------------

    m_pixmaps["background"] = new ValueData<QPixmap, QString>(":/images/background.png");

    m_fontSizes["title"] = new ValueData<int>(18);
    m_positions["title"] = new ValueData<QPointF>(QPointF(184, 7));

    m_pixmaps["closeButton"] = new ValueData<QPixmap, QString>(":/images/button_close.png");
    m_positions["closeButton"] = new ValueData<QPointF>(QPointF(426, 8));
    m_horizontalSizes["close_button_spacing"] = new ValueData<qreal>(28.0);

    m_horizontalSizes["listset.center_position"] = new ValueData<qreal>(17.0);

    m_pixmaps["list.scrollbar.landscape"] = new ValueData<QPixmap, QString>(":/images/list_landscape_scrollbar.png");
    m_pixmaps["list.scrollbar.portrait"] = new ValueData<QPixmap, QString>(":/images/list_portrait_scrollbar.png");
    m_pixmaps["list.scrollbar.knob"] = new ValueData<QPixmap, QString>(":/images/list_scrollbar_knob.png");

    // ---------------------------------------------------------------------

    m_pixmaps["alldone.background"] = new ValueData<QPixmap, QString>(":/images/list_alldone_background.png");

    m_pixmaps["alldone.startagain_off"] = new ValueData<QPixmap, QString>(":/images/list_alldone_bt_startagain_off.png");
    m_positions["alldone.startagain_off"] = new ValueData<QPointF>(QPointF(-4, 54));

    m_pixmaps["alldone.close_button"] = new ValueData<QPixmap, QString>(":/images/button_close.png");
    m_positions["alldone.close_button"] = new ValueData<QPointF>(QPointF(100, -95));
}

DesignInformation::~DesignInformation()
{
    qDeleteAll(m_pixmaps.values());
    qDeleteAll(m_positions.values());
    qDeleteAll(m_verticalSizes.values());
    qDeleteAll(m_horizontalSizes.values());
    qDeleteAll(m_fontSizes.values());
}

template<class T, class R>
void DesignInformation::resetValues(QMap<QString, ValueData<T,R>* > map)
{
    QList<ValueData<T,R>*> values = map.values();
    for (int i = 0; i < values.count(); ++i) {
        values[i]->m_upToDate = false;
        values[i]->m_value = T();
    }

}

void DesignInformation::setScaleFactor(QSizeF factor)
{
    DesignInformation *obj = DesignInformation::instance();
    resetValues(obj->m_pixmaps);
    resetValues(obj->m_positions);
    resetValues(obj->m_verticalSizes);
    resetValues(obj->m_horizontalSizes);
    resetValues(obj->m_fontSizes);
    obj->m_scaleFactor = QSizeF(factor.width(), 1.0);
    
    DesignInformation::getPixmap("list_item.bottom_line");
    obj->m_scaleFactor = factor;
}

qreal DesignInformation::calculateScaleFactor(QSizeF screenSize)
{
    qreal result = 1.0;
    if (screenSize.width() < m_referenceWidth) {
        result = m_referenceWidth / screenSize.width();
        screenSize.setWidth(result * screenSize.width());
        screenSize.setHeight(result * screenSize.height());
    }

    qreal referenceHeight = m_listTop + m_minimumVisibleItems * m_referenceItemHeight;

    if (screenSize.height() < referenceHeight) {
        result *= referenceHeight / screenSize.height();
        screenSize.setWidth(result * screenSize.width());
        screenSize.setHeight(result * screenSize.height());
    }
    return qMin(qreal(1.0) / result, qreal(1.0));
}

QSizeF DesignInformation::getScaleFactor(QSizeF screenSize)
{
    qreal scale1 = calculateScaleFactor(screenSize);
    qreal scale2 = calculateScaleFactor(QSizeF(screenSize.height(), screenSize.width()));

    qreal factor = qMin(scale1, scale2);
    return QSizeF(factor, factor);
}

static QPixmap pixmapScale(QString reference, QSizeF scale)
{
    QPixmap ref(reference);
    return ref.scaled(scale.width() * ref.width(), scale.height() * ref.height(),
                      Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
}

void DesignInformation::setScreenSize(QSizeF size)
{
    DesignInformation *obj = DesignInformation::instance();
        if (obj->m_pixmaps.contains("background")) {
            ValueData<QPixmap, QString>* data = obj->m_pixmaps["background"];
            QString file = size.width() > size.height() ? ":/images/background.png"
                                                        : ":/images/background_landscape.png";
            QPixmap ref = QPixmap(file);
            data->m_value = ref.scaled(size.width(), size.height(),
                                       Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            data->m_upToDate = true;
	}
	
	QPixmap pixmap = getPixmap("category.background");
	
	if (obj->m_horizontalSizes.contains("listset.center_position")) {
            ValueData<qreal> *data = obj->m_horizontalSizes["listset.center_position"];
            data->m_value = (size.width() - pixmap.width()) / 2;
            data->m_upToDate = true;
	}
	
	if (obj->m_verticalSizes.contains("category.list_height")) {
            ValueData<qreal> *data = obj->m_verticalSizes["category.list_height"];

            QPointF backgroundPos = getPosition("category.background");
            QPointF listPos = getPosition("category.list");

            data->m_value = backgroundPos.y() + pixmap.height() - listPos.y();
            data->m_value = qMin(size.height() - listPos.y(), data->m_value);
            data->m_upToDate = true;
	}

	if (obj->m_pixmaps.contains("list.scrollbar.portrait")) {
            ValueData<QPixmap, QString>* data = obj->m_pixmaps["list.scrollbar.portrait"];
            QPixmap ref = QPixmap(":images/list_portrait_scrollbar.png");
            QSizeF  scale = obj->m_scaleFactor;
            data->m_value = ref.scaled(scale.width() * ref.width(),
                                       getVerticalSize("category.list_height") - 20,
                                           Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            data->m_upToDate = true;
	}
	
	
}

DesignInformation *DesignInformation::instance()
{
    static DesignInformation *obj(new DesignInformation());
    return obj;
}

template<class T, class R, class F>
T DesignInformation::getValue(QMap<QString, ValueData<T,R>* > map,
                              QString key, F factory, QSizeF scale)
{
    if (map.contains(key)) {
        ValueData<T,R> *data = map.value(key);
        if (data->m_upToDate)
            return data->m_value;
        data->m_value = factory(data->m_reference, scale);
        data->m_upToDate = true;
        return data->m_value;
    }
    qWarning() << "key" << key << "not found";
    return T();
}

QPixmap DesignInformation::getPixmap(const QString &name)
{
    DesignInformation *obj = DesignInformation::instance();
    return getValue(obj->m_pixmaps, name, pixmapScale, obj->m_scaleFactor);
}

static QPointF pointFScale(QPointF reference, QSizeF scale)
{
    return QPointF(reference.x() * scale.width(), reference.y() * scale.height());
}

QPointF DesignInformation::getPosition(const QString &name)
{
    DesignInformation *obj = DesignInformation::instance();
    return getValue(obj->m_positions, name, pointFScale, obj->m_scaleFactor);
}

static qreal verticalScale(qreal reference, QSizeF scale)
{
    return reference * scale.height();
}

qreal DesignInformation::getVerticalSize(const QString &name)
{
    DesignInformation *obj = DesignInformation::instance();
    return getValue(obj->m_verticalSizes, name, verticalScale, obj->m_scaleFactor);
}

static qreal horizontalScale(qreal reference, QSizeF scale)
{
    return reference * scale.width();
}

qreal DesignInformation::getHorizontalSize(const QString &name)
{
    DesignInformation *obj = DesignInformation::instance();
    return getValue(obj->m_horizontalSizes, name, horizontalScale, obj->m_scaleFactor);
}

static int fontSizeScale(int reference, QSizeF scale)
{
    return reference * sqrt(scale.width() * scale.height());
}

int DesignInformation::getFontSize(const QString name)
{
    DesignInformation *obj = DesignInformation::instance();
    return getValue(obj->m_fontSizes, name, fontSizeScale, obj->m_scaleFactor);

}

QStringList DesignInformation::getStringList(const QString &name)
{
    DesignInformation *obj = DesignInformation::instance();
    if (obj && obj->m_stringLists.contains(name)) {
        QStringList result;
        for (const char **item = obj->m_stringLists.value(name); *item; ++item)
            result << *item;
        return result;
    }
    qWarning() << "position" << name << "not found";
    return QStringList();
}
