/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QtGui>
#include <QDebug>

#include "system.h"
#include "maincontainer.h"
#include "designinformation.h"
#include "shoppinglistcategoryui.h"
#include "shoppinglistview.h"


static ShoppingListCategoryUI *createList(const QString &category, int itemCount)
{
    ShoppingListCategoryUI *result = new ShoppingListCategoryUI(category);
    QString textsKey = "category.string_list." + category;
    QStringList texts = DesignInformation::getStringList(textsKey);

    QList<ContentListItem*> items;

    for (int i = 0; i < itemCount && i < texts.count(); ++i) {
        ShoppingListItem *item = new ShoppingListItem(QString::number(i + 1) + " - " + texts[i],
                                                      result->list(), i);
        items.append(item);
    }
    result->list()->appendItems(items);

    return result;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainContainer* lists = new MainContainer();

    QGraphicsScene scene;
    ShoppingListView view(lists);
    view.setScene(&scene);


    QObject::connect(QApplication::desktop(), SIGNAL(resized(int)), &view, SLOT(resized(int)));

    view.adjustScalingFactor();
    view.adjustScreenSize();

    lists->add(createList("groceries", 60));
    lists->add(createList("beverages", 20));
    lists->add(createList("cleaning", 60));
    lists->add(createList("garden", 80));
    lists->add(createList("pet", 100));
    lists->add(createList("toiletries", 200));

    scene.addItem(lists);

    System::setViewMode(&view, System::PortraitMode);

#if defined(Q_OS_SYMBIAN) || defined(Q_WS_MAEMO_5)
    view.showFullScreen();
#else
    view.show();
#endif

    return app.exec();
}
