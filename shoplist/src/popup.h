/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef POPUP_H
#define POPUP_H

#include <QObject>
#include <QRect>
#include <QGraphicsObject>

class PopupCloseButton: public QGraphicsPixmapItem
{
public:
    PopupCloseButton(QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    QPainterPath shape() const;
};

class PopUp : public QObject, public QGraphicsRectItem
{
    Q_OBJECT

public:
    PopUp(QGraphicsItem* parent = 0);
    ~PopUp() {}

    void updateUI();

signals:
    void listStartAgain();
    void deletePopUp();

protected:
    bool sceneEventFilter(QGraphicsItem *item, QEvent *event);

private:
    QGraphicsPixmapItem* m_allDone;
    QGraphicsPixmapItem* m_startAgain;
    PopupCloseButton* m_close;
};

#endif // POPUP_H
