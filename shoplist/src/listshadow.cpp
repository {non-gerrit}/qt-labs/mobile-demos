/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "listshadow.h"
#include <QPropertyAnimation>
#include "designinformation.h"

ListShadow::ListShadow(QGraphicsItem *parent)
    : QGraphicsPixmapItem(parent)
    , m_focused(false)
    , m_completed(false)
    , m_animation(0)
{
    setOpacity(getTargetOpacity());
    setVisible(opacity() != 0.0);
}

ListShadow::~ListShadow()
{
    if (m_animation)
        m_animation->deleteLater();
}

void ListShadow::setFocused(bool focused)
{
    if (focused != m_focused) {
        m_focused = focused;
        createAnimation(getTargetOpacity());
    }
}

void ListShadow::setCompleted(bool completed)
{
    if (m_completed != completed) {
        m_completed = completed;
        createAnimation(getTargetOpacity());
    }
}

qreal ListShadow::getTargetOpacity()
{
    return !m_focused ? 1.0 : m_completed ? 0.2 : 0.0;
}

void ListShadow::createAnimation(qreal target)
{
    if (m_animation)
        m_animation->deleteLater();

    QPropertyAnimation* animation = new QPropertyAnimation(this, "opacity");
    animation->setEasingCurve(QEasingCurve::OutExpo);
    animation->setEndValue(target);
    animation->setDuration(500);
    if (target == 0.0)
        connect(animation, SIGNAL(finished()), this, SLOT(doHide()));
    connect(animation, SIGNAL(finished()), this, SLOT(animationEnd()));

    m_animation = animation;
    m_animation->start(QAbstractAnimation::DeleteWhenStopped);
    show();
}
