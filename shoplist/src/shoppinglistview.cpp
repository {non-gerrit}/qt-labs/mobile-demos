/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "shoppinglistview.h"

#include <QApplication>
#include <QDesktopWidget>

ShoppingListView::ShoppingListView(MainContainer *container)
    : m_mainContainer(container)
{
    setRenderHints(QPainter::TextAntialiasing);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFrameShape(QFrame::NoFrame);
    setWindowTitle("Shopping List");
    setViewportUpdateMode(BoundingRectViewportUpdate);

}

void ShoppingListView::keyPressEvent(QKeyEvent *event)
{
    event->ignore();
    if (event->key() == Qt::Key_F5)
        adjustScreenSize();
    if (event->key() == Qt::Key_Left)
        m_mainContainer->moveLeft();
    if (event->key() == Qt::Key_Right)
        m_mainContainer->moveRight();
}

void ShoppingListView::adjustScreenSize()
{

    QSizeF screenSize(QApplication::desktop()->screenGeometry().size());
    if (screenSize.width() > 480)
        screenSize.setWidth(480);
    if (screenSize.height() > 864)
        screenSize.setHeight(864);

    /*
#ifdef Q_OS_SYMBIAN
    QSizeF screenSize(QApplication::desktop()->screenGeometry().size());
#else
    QSizeF screenSize(geometry().size());
#endif
    */
    DesignInformation::setScreenSize(screenSize);
    resize(screenSize.toSize());
    if (scene())
    	scene()->setSceneRect(0.0, 0.0, screenSize.width(), screenSize.height());
    centerOn(m_mainContainer);
    m_mainContainer->updateUI();
}

void ShoppingListView::adjustScalingFactor()
{

    QSizeF screenSize(QApplication::desktop()->screenGeometry().size());
    if (screenSize.width() > 480)
        screenSize.setWidth(480);
    if (screenSize.height() > 864)
        screenSize.setHeight(864);

 /*
#ifdef Q_OS_SYMBIAN
    QSizeF screenSize(QApplication::desktop()->screenGeometry().size());
#else
    QSizeF screenSize(geometry().size());
#endif
*/
    screenSize.setWidth(qMin(int(screenSize.width()), 480));
    screenSize.setHeight(qMin(int(screenSize.height()), 864));
    DesignInformation::setScaleFactor(DesignInformation::getScaleFactor(screenSize));
}

void ShoppingListView::resized(int screen)
{
    Q_UNUSED(screen);
    adjustScreenSize();
}

