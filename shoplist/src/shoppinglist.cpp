/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "shoppinglist.h"
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QSequentialAnimationGroup>
#include <QGraphicsSceneMouseEvent>
#include <QFont>
#include <QDebug>
#include "designinformation.h"

class ShoppingListItemClickActivity : public ContentListActivity
{
public:
    ShoppingListItemClickActivity(ShoppingListItem *item, bool check, ContentList &list)
        : ContentListActivity(list)
        , m_item(item)
        , m_check(check)
    {}
    bool run();
private:
    ShoppingListItem *m_item;
    const bool m_check;
};

bool ShoppingListItemClickActivity::run()
{
    int idx = m_list.getItemIndex(m_item);
    if (idx >= 0 && m_item->checked() != m_check) {

        m_item->m_list->m_checkedItems += m_item->checked() ? -1 : 1;

        addActivity(new AnimationActivity(m_item->getSwitchAnimation(),m_list));

        if (m_check)
            addActivity(new MoveActivity(idx, m_list.itemCount(), m_list));

        if (!m_check) {
            int target = m_list.itemCount();
            for (int i = 0; target == m_list.itemCount() && i < m_list.itemCount(); ++i) {
                ShoppingListItem *item = dynamic_cast<ShoppingListItem*>(m_list.getItem(i));
                if (!item || item == m_item)
                    continue;
                if (item->checked()) {
                    if (idx > i)
                        addActivity(new MoveActivity(idx, i, m_list));
                    break;
                }
                if (item->listPos() > m_item->listPos()) {
                    addActivity(new MoveActivity(idx, i, m_list));
                    break;
                }
            }
        }

        if (m_item->m_list->m_checkedItems == m_item->m_list->itemCount()) {
            SignalActivity *activity = new SignalActivity(m_list);
            connect(activity, SIGNAL(notify()), m_item->m_list, SIGNAL(completed()));
            addActivity(activity);
        }
    }
    return false;
}

// ShoppingListItem

ShoppingListItem::ShoppingListItem(const QString& text, ShoppingList *list,
                                   int pos, QGraphicsItem *parent)
    : ContentListItem(parent)
    , m_list(list)
    , m_pos(pos)
    , m_contentHeight(DesignInformation::getVerticalSize("list_item.height"))
{
    m_image1 = new ShoppingListPixmap(this);
    m_image2 = new ShoppingListPixmap(this);
    m_line = new ShoppingListPixmap(this);
    m_selected = new ShoppingListPixmap(this);

    m_image2->hide();
    m_image2->setOpacity(0);
    m_selected->hide();
    m_selected->setOpacity(0);

    m_text = new QGraphicsTextItem(this);
    QString html = "<span style=\"font-family:Nokia sans; color:#616161\">";
    html += text +  "</span>";
    m_text->setHtml(html);

    updateUI();
}

void ShoppingListItem::updateUI()
{
    m_contentHeight = DesignInformation::getVerticalSize("list_item.height");

    updatePixmapItem(m_image1, "list_item.not_checked", 1);
    updatePixmapItem(m_image2, "list_item.checked", 1);
    updatePixmapItem(m_line, "list_item.bottom_line", 1);
    updatePixmapItem(m_selected, "list_item.selected", 0);

    QFont font = m_text->font();
    font.setPixelSize(DesignInformation::getFontSize("list_item.font_size"));
    m_text->setFont(font);

    qreal textOffset = m_image1->pos().x() + m_image1->pixmap().width() +
                       DesignInformation::getHorizontalSize("list_item.text_offset");

    qreal textTop = (contentHeight() - m_text->boundingRect().height()) / 2;

    m_text->setPos(textOffset, textTop);
}

void ShoppingListItem::updatePixmapItem(ShoppingListPixmap *item, const QString &key, qreal zValue)
{
    item->setPixmap(DesignInformation::getPixmap(key));
    item->setPos(DesignInformation::getPosition(key));
    item->setZValue(zValue);
}

qreal ShoppingListItem::contentHeight() const
{
    return m_contentHeight;
}

QAbstractAnimation *ShoppingListItem::getShowAnimation()
{
    return getFadeAnimation(this, false, 150);
}

QAbstractAnimation *ShoppingListItem::getHideAnimation()
{
    return getFadeAnimation(this, true, 150);
}

template<class T>
QAbstractAnimation *ShoppingListItem::getFadeAnimation(T *target, bool hide, int msecs)
{

    target->setOpacity(hide ? 1.0 : 0.0);
    target->show();

    QPropertyAnimation* lResult = new QPropertyAnimation(target, "opacity");
    lResult->setEasingCurve(QEasingCurve::OutExpo);
    lResult->setStartValue(hide ? 1.0 : 0.0);
    lResult->setEndValue(hide ? 0.0 : 1.0);
    lResult->setDuration(msecs);
    if (hide)
        connect(lResult, SIGNAL(finished()), target, SLOT(doHide()));
    return lResult;
}

QAbstractAnimation *ShoppingListItem::getSwitchAnimation()
{
    ShoppingListPixmap *in = checked() ? m_image1 : m_image2;
    ShoppingListPixmap *out = in == m_image1 ? m_image2 : m_image1;

    QParallelAnimationGroup *result = new QParallelAnimationGroup();

    QAbstractAnimation *background = getFadeAnimation(m_selected, checked(), 150);
    result->addAnimation(addAnimationPause(background, 50));

    result->addAnimation(getFadeAnimation(in, false, 150));
    result->addAnimation(getFadeAnimation(out, true, 150));

    return result;
}

QAbstractAnimation *ShoppingListItem::addAnimationPause(QAbstractAnimation *animation, int msecs)
{
    QSequentialAnimationGroup *result = new QSequentialAnimationGroup();
    result->addPause(msecs);
    result->addAnimation(animation);
    return result;
}

void ShoppingListItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    if (m_list && !m_list->busy() && m_list->active())
        m_list->addActivity(new ShoppingListItemClickActivity(this, !checked(), *m_list));
}

void ShoppingListItem::uncheck()
{
    m_image1->show();
    m_image1->setOpacity(1.0);
    m_image2->hide();
    m_image2->setOpacity(0);
    m_selected->hide();
    m_selected->setOpacity(0);
}

// ShoppingList

ShoppingList::ShoppingList(QGraphicsItem *parent)
    : ContentList(parent)
    , m_checkedItems(0)
    , m_active(true)
{
}

QAbstractAnimation *ShoppingList::getInsertAnimation(int idx, qreal height)
{
    if (idx < 0 || idx >= itemCount())
        return 0;
    QList<QAbstractAnimation*> list;
    for (int i = idx; i < itemCount(); ++i)
        list.append(getItemMoveAnimation(i, height));
    return createCompoundAnimation(list);
}

QAbstractAnimation *ShoppingList::getRemoveAnimation(int idx)
{
    if (idx < 0 || idx >= itemCount())
        return 0;
    qreal offset = -getItem(idx)->contentHeight();
    QList<QAbstractAnimation*> list;
    for (int i = idx + 1; i < itemCount(); ++i)
        list.append(getItemMoveAnimation(i, offset));
    return createCompoundAnimation(list);
}

QAbstractAnimation *ShoppingList::createCompoundAnimation(QList<QAbstractAnimation*> list)
{
    if (list.count() == 0)
        return 0;
    if (list.count() == 1)
        return list[0];
    QParallelAnimationGroup *result = new QParallelAnimationGroup();
    for (int i = 0; i < list.count(); ++i)
        result->addAnimation(list[i]);
    return result;

}

QAbstractAnimation *ShoppingList::getItemMoveAnimation(int idx, qreal offset)
{
    ContentListItem *item = getItem(idx);
    if (!item)
        return 0;

    qreal itemTop = item->property(ITEM_TOP_PROPERTY_NAME).toReal();

    QPropertyAnimation* lResult = new QPropertyAnimation(item, ITEM_TOP_PROPERTY_NAME);

    lResult->setEasingCurve(QEasingCurve::OutExpo);
    lResult->setStartValue(itemTop);
    lResult->setEndValue(itemTop + offset);
    lResult->setDuration(150);
    return lResult;
}

static bool shoppingListItemCompare(const ContentListItem* s1, const ContentListItem* s2)
{
    const ShoppingListItem *item1 =dynamic_cast<const ShoppingListItem*>(s1);
    const ShoppingListItem *item2 =dynamic_cast<const ShoppingListItem*>(s2);
    return !item1 || !item2 ? false : item1->listPos() < item2->listPos();
}

void ShoppingList::clearChecks()
{
    foreach(ContentListItem *item, getItems())
        dynamic_cast<ShoppingListItem*>(item)->uncheck();
    m_checkedItems = 0;
    sortItems(shoppingListItemCompare);
}

