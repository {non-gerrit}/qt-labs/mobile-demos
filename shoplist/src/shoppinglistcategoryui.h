/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef SHOPPINGLISTCATEGORYUI_H
#define SHOPPINGLISTCATEGORYUI_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QBasicTimer>
#include <QTime>
#include "shoppinglist.h"
#include "scrollbar.h"
#include "gesturebox.h"
#include "popup.h"
#include "listshadow.h"

class ShoppingListScrollBox : public QObject, public GestureBox
{
    Q_OBJECT
public:
    ShoppingListScrollBox(QGraphicsItem *content, QGraphicsItem *parent = 0);
    void setActive(bool active) { m_active = active; }
    bool active() const { return m_active; }

signals:
    void updated(qreal, qreal, qreal);

protected:
    void gestureMousePress(QPointF pos, bool &startGesture, bool &acceptClick);
    void gestureStart(QPointF pos);
    void gestureMove(QPointF pos, QPointF movement, QPointF speed);
    void gestureEnd(QPointF pos, QPointF speed);

    void timerEvent(QTimerEvent *event);

private:
    QGraphicsItem * const m_content;
    QBasicTimer m_ticker;
    qreal m_speed;
    qreal m_acceleration;
    QTime m_time;
    bool m_active;
    QPointF m_startPoint;
    bool move(QPointF movement);
    void contentPositionUpdated();
};

class ShoppingListCategoryUI : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    ShoppingListCategoryUI(const QString &category, QGraphicsItem *parent = 0);
    ShoppingList *list() { return m_shoppingList; }
    void updateUI();

    bool focused() const { return m_shadow->focused(); }
    void setFocused(bool focused);

public slots:
    void updated(qreal, qreal, qreal);

private:
    QGraphicsPixmapItem *m_pic;
    QGraphicsPixmapItem *m_header;
    QGraphicsPixmapItem *m_background;
    QGraphicsPixmapItem *m_feather;
    ShoppingList *m_shoppingList;
    ScrollBar* m_scroll;
    ShoppingListScrollBox *m_scrollBox;
    PopUp *m_popup;
    ListShadow *m_shadow;
    const QString m_category;

    void updateListState();
    void setCompleted(bool completed);

private slots:
    void showPopup();
    void hidePopup();
    void clearList();
};

#endif // SHOPPINGLISTCATEGORYUI_H
