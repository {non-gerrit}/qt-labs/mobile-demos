/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include "shoppinglistcategoryui.h"
#include "designinformation.h"
#include "scrollbar.h"

#include <QDebug>
#include <QtGlobal>
#include <cmath>

// ShoppingListScrollBox

#ifdef Q_OS_SYMBIAN
static const int acceleration_factor = 12;
#else
static const int acceleration_factor = 64;
#endif


static const int max_gesture_time_ms = 500;
static const qreal degrees = 30.0;
static const qreal limit_angle_tg = tan(degrees * 3.141592 / 180.0);
static const qreal min_gesture_length = 100.0;

ShoppingListScrollBox::ShoppingListScrollBox(QGraphicsItem *content, QGraphicsItem *parent)
    : GestureBox(parent)
    , m_content(content)
    , m_active(true)
{
    setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
    if (m_content)
        m_content->setParentItem(this);

    QObject* obj = dynamic_cast<QObject*>(parent);
    if (obj)
        connect(this, SIGNAL(updated(qreal, qreal, qreal)),
                obj, SLOT(updated(qreal, qreal, qreal)));
}

void ShoppingListScrollBox::gestureMousePress(QPointF pos, bool &startGesture, bool &acceptClick)
{
    Q_UNUSED(pos);
    startGesture = m_active;
    acceptClick = !m_ticker.isActive();
    if (m_ticker.isActive()) {
        m_ticker.stop();
        m_speed = 0.0;
    }
}

void ShoppingListScrollBox::gestureStart(QPointF pos)
{
    m_startPoint = pos;
}

void ShoppingListScrollBox::contentPositionUpdated()
{
    qreal max = m_content->boundingRect().height();

    QPointF delta = boundingRect().topLeft() - mapFromItem(m_content, QPointF(0, 0));
    qreal cur = delta.y();

    qreal step = boundingRect().height();

    emit updated(max, cur, step);
}

bool ShoppingListScrollBox::move(QPointF movement)
{
    if (m_content && m_content->boundingRect().height() > boundingRect().height()) {

        qreal dist = movement.y();

        if (dist > 0 && m_content->pos().y() < 0) {
            qreal top = m_content->pos().y() + dist;
            m_content->setPos(m_content->pos().x(), top > 0 ? 0 : top);
            contentPositionUpdated();
            return true;
        }

        if (dist < 0) {
            qreal top = m_content->pos().y() + dist;
            if (top + m_content->boundingRect().height() >= boundingRect().height()) {
                m_content->setPos(m_content->pos().x(), top);
                contentPositionUpdated();
                return true;
            }
        }

    }
    return false;
}

void ShoppingListScrollBox::gestureMove(QPointF pos, QPointF movement, QPointF speed)
{
    Q_UNUSED(pos);
    Q_UNUSED(speed);
    move(movement);
}

void ShoppingListScrollBox::gestureEnd(QPointF pos, QPointF speed)
{
    qreal x = pos.x() - m_startPoint.x();
    qreal y = pos.y() - m_startPoint.y();
    if (fabs(x / y) > limit_angle_tg || fabs(y) < min_gesture_length)
        return;

    qreal topSpeed = DesignInformation::getVerticalSize("list.vertical_speed");
    m_acceleration = topSpeed / acceleration_factor;
    m_speed = qBound(-topSpeed, speed.y(), topSpeed);
    if (m_speed != 0) {
        m_time = QTime::currentTime();
        m_ticker.start(20, this);
    }
}

void ShoppingListScrollBox::timerEvent(QTimerEvent *event)
{
    bool stopTimer = true;
    if (m_speed != 0.0) {
        QTime now(QTime::currentTime());
        qreal movement = m_speed * (qreal)m_time.msecsTo(now) / 1000.0;
        m_time = now;
        stopTimer = !move(QPointF(0, movement));
        if (!stopTimer) {
            m_speed = m_speed > 0.0 ? qMax(qreal(0.0), m_speed - m_acceleration)
                                    : qMin(qreal(0.0), m_speed + m_acceleration);
            stopTimer = m_speed == 0.0;
        }
    }
    if (stopTimer)
        m_ticker.stop();
    QObject::timerEvent(event);
}

// ShoppingListCategoryUI

ShoppingListCategoryUI::ShoppingListCategoryUI(const QString &category, QGraphicsItem *parent)
    : QGraphicsRectItem(parent)
    , m_pic(0)
    , m_header(0)
    , m_background(0)
    , m_shoppingList(0)
    , m_scroll(0)
    , m_scrollBox(0)
    , m_popup(0)
    , m_shadow(0)
    , m_category(category)
{
    setFlag(QGraphicsItem::ItemHasNoContents, true);

    m_background = new QGraphicsPixmapItem(this);
    m_pic = new QGraphicsPixmapItem(this);
    m_header = new QGraphicsPixmapItem(this);
    m_feather = new QGraphicsPixmapItem(this);
    m_shoppingList = new ShoppingList(0);
    m_scrollBox = new ShoppingListScrollBox(m_shoppingList, this);
    m_shadow = new ListShadow(this);
    m_scroll = new ScrollBar(this);

    m_shadow->setZValue(3);
    m_background->setZValue(0);
    m_pic->setZValue(1);
    m_header->setZValue(1);
    m_feather->setZValue(2);
    m_scrollBox->setZValue(1);
    m_scroll->setZValue(2);

    connect(m_shoppingList, SIGNAL(completed()), this, SLOT(showPopup()));

    updateUI();
}

void ShoppingListCategoryUI::updateUI()
{
    m_shadow->setPixmap(DesignInformation::getPixmap("category.shadow." + m_category));
    m_shadow->setPos(DesignInformation::getPosition("category.shadow"));

    m_background->setPixmap(DesignInformation::getPixmap("category.background"));
    m_background->setPos(DesignInformation::getPosition("category.background"));

    m_pic->setPixmap(DesignInformation::getPixmap("category.pic." + m_category));
    m_pic->setPos(DesignInformation::getPosition("category.pic"));

    m_header->setPixmap(DesignInformation::getPixmap("category.header." + m_category));
    m_header->setPos(DesignInformation::getPosition("category.header"));

    m_feather->setPixmap(DesignInformation::getPixmap("category.feather"));
    m_feather->setPos(DesignInformation::getPosition("category.feather"));

    QPointF listPos = DesignInformation::getPosition("category.list");

    qreal listHeight = DesignInformation::getVerticalSize("category.list_height");

    m_scrollBox->setRect(listPos.x(), listPos.y(),
                         m_background->pixmap().width() - listPos.x(), listHeight);

    m_shoppingList->setPos(0.0, 0.0);
    m_shoppingList->setWidth(m_background->pixmap().width() - listPos.x());

    setRect(0, 0, m_background->pixmap().width(), listPos.y() + listHeight);

    m_scroll->setPos(DesignInformation::getPosition("category.scrollbar"));
    m_scroll->updateUI();

    for (int i = 0; i < m_shoppingList->itemCount(); ++i) {
        ShoppingListItem *item = dynamic_cast<ShoppingListItem*>(m_shoppingList->getItem(i));
        if (item)
            item->updateUI();
    }
    m_shoppingList->updateItems();

    if (m_popup)
        m_popup->updateUI();
}

void ShoppingListCategoryUI::updated(qreal max, qreal cur, qreal step)
{
    m_scroll->setMaximum(max - step);
    m_scroll->updatePosition(cur);
}

void ShoppingListCategoryUI::showPopup()
{
    if (m_popup)
        return;

    m_popup = new PopUp(this);
    m_popup->setZValue(10);

    connect(m_popup, SIGNAL(listStartAgain()), this, SLOT(clearList()));
    connect(m_popup, SIGNAL(deletePopUp()), this, SLOT(hidePopup()));
    setCompleted(true);
}

void ShoppingListCategoryUI::clearList()
{
    m_shoppingList->clearChecks();
    hidePopup();
}

void ShoppingListCategoryUI::hidePopup()
{
    if (!m_popup)
        return;
    setCompleted(false);
    m_popup->deleteLater();
    m_popup = 0;
}

void ShoppingListCategoryUI::updateListState()
{
    m_scrollBox->setActive(m_shadow->focused() && !m_shadow->completed());
    m_shoppingList->setActive(m_shadow->focused() && !m_shadow->completed());

}

void ShoppingListCategoryUI::setFocused(bool focused)
{
    m_shadow->setFocused(focused);
    updateListState();
}

void ShoppingListCategoryUI::setCompleted(bool completed)
{
    m_shadow->setCompleted(completed);
    updateListState();
}
