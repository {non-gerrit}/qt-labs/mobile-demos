/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef LISTSHADOW_H
#define LISTSHADOW_H

#include <QAbstractAnimation>
#include <QGraphicsPixmapItem>

class ListShadow : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity);
public:
    ListShadow(QGraphicsItem *parent = 0);
    ~ListShadow();

    bool focused() const { return m_focused; }
    bool completed() const { return m_completed; }

    void setFocused(bool);
    void setCompleted(bool);

private slots:
    void doHide() { hide(); }
    void animationEnd() { m_animation = 0; }

private:
    bool m_focused;
    bool m_completed;
    QAbstractAnimation *m_animation;

    qreal getTargetOpacity();
    void createAnimation(qreal opacity);
};

#endif // LISTSHADOW_H
