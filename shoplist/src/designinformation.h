/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef DESIGNINFORMATION_H
#define DESIGNINFORMATION_H

#include <QString>
#include <QMap>
#include <QPixmap>

class DesignInformation
{
public:
    static QPixmap getPixmap(const QString &name);
    static QPointF getPosition(const QString &name);
    static qreal getVerticalSize(const QString &name);
    static qreal getHorizontalSize(const QString &name);
    static int getFontSize(const QString name);
    static QStringList getStringList(const QString &name);
    static void setScaleFactor(QSizeF factor);
    static QSizeF getScaleFactor(QSizeF screenSize);
    static void setScreenSize(QSizeF size);
private:
    template<class T, class R = T> class ValueData
    {
    public:
        ValueData(R reference) : m_upToDate(false), m_reference(reference) {}
        bool m_upToDate;
        const R m_reference;
        T m_value;
    };
private:
    QSizeF m_scaleFactor;
    QMap<QString, ValueData<QPixmap, QString>* > m_pixmaps;
    QMap<QString, ValueData<QPointF>* > m_positions;
    QMap<QString, ValueData<qreal>* > m_verticalSizes;
    QMap<QString, ValueData<qreal>* > m_horizontalSizes;
    QMap<QString, ValueData<int>* > m_fontSizes;

    QMap<QString, const char**> m_stringLists;

    static const int m_listTop = 142;
    static const int m_referenceWidth = 480;
    static const int m_minimumVisibleItems = 3;
    static const int m_referenceItemHeight = 90;

    DesignInformation();
    ~DesignInformation();
    static inline DesignInformation *instance();
    static qreal calculateScaleFactor(QSizeF screenSize);

    template<class T, class R, class F>
    static T getValue(QMap<QString, ValueData<T,R>* > map, QString key, F factory, QSizeF scale);
    template<class T, class R>
    static void resetValues(QMap<QString, ValueData<T,R>* > map);
};

#endif // DESIGNINFORMATION_H
