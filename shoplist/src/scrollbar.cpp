/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QObject>
#include <QString>
#include <QTimer>
#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>
#include <QAbstractAnimation>

#include "designinformation.h"
#include "scrollbar.h"

/*! \class ScrollBar
 *
 * \brief Fade in/out animations and position update callback
 *
 * Stick this over your items viewport and send \c updatePosition notifications
 * to render the knob into the new position. It doesn't handle user input.
 */
ScrollBar::ScrollBar(QGraphicsItem* parent)
        : QObject()
        , QGraphicsPixmapItem(parent)
        , m_state(Hidden)
        , m_maximum(0)
        , m_value(0)
        , m_timer()
        , m_animation(0)
        , m_knob(this)
{
    hide();
    setValue(m_value);
    setOpacity(0);

    m_timer.setSingleShot(true);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(timeout()));

    updateUI();
}

void ScrollBar::updateUI()
{
    setPixmap(DesignInformation::getPixmap("list.scrollbar.portrait"));
    m_knob.setPixmap(DesignInformation::getPixmap("list.scrollbar.knob"));
}

qreal ScrollBar::maximum() { return m_maximum; }

void ScrollBar::setMaximum(qreal max)
{
    if (max >= 0)
        m_maximum = max;
}

qreal ScrollBar::value() { return m_value; }

void ScrollBar::setValue(qreal value)
{
    if (value < 0 || value > m_maximum)
        return;

    m_value = value;

    int min = boundingRect().topLeft().y();
    int max = boundingRect().bottomLeft().y() - m_knob.boundingRect().height();

    m_knob.setPos(0, (value * (max - min)) / m_maximum);
}

void ScrollBar::updatePosition(qreal value)
{
    switch (m_state) {
    case Visible:
        startTimer();
    case ShowAnimation:
        break;
    case HideAnimation:
        m_animation->stop();
    case Hidden:
        QAbstractAnimation* anim = showAnimation();
        anim->start(QAbstractAnimation::DeleteWhenStopped);
        m_state = ShowAnimation;
        break;
    }

    show();
    setValue(value);
}

void ScrollBar::setLandscapeOrientation()
{
    setPixmap(DesignInformation::getPixmap("list.scrollbar.landscape"));
    setValue(m_value);
}

void ScrollBar::setPortraitOrientation()
{
    setPixmap(DesignInformation::getPixmap("list.scrollbar.portrait"));
    setValue(m_value);
}

void ScrollBar::startTimer()
{
    if (m_timer.isActive())
        m_timer.stop();

    m_timer.start(1000);
}

void ScrollBar::timeout()
{
    if (Visible != m_state)
        return;

    m_state = HideAnimation;
    QAbstractAnimation* anim = hideAnimation();
    m_animation = anim;
    anim->start(QAbstractAnimation::DeleteWhenStopped);
}

void ScrollBar::animationFinished()
{
    switch(m_state) {
    case ShowAnimation:
        startTimer();
        m_state = Visible;
        break;
    case HideAnimation:
        hide();
        m_state = Hidden;
        m_animation = 0;
        break;
    default:
        break;
    }
}

QPropertyAnimation* ScrollBar::propertyAnimation(const char* name)
{
    QPropertyAnimation* anim = new QPropertyAnimation();
    anim->setTargetObject(this);
    anim->setPropertyName(name);
    anim->setEasingCurve(QEasingCurve::OutExpo);
    anim->setDuration(500);

    connect(anim, SIGNAL(finished()), this, SLOT(animationFinished()));

    return anim;
}

QAbstractAnimation* ScrollBar::showAnimation()
{
    QPropertyAnimation* anim = propertyAnimation("opacity");
    anim->setEndValue(1);

    return anim;
}

QAbstractAnimation* ScrollBar::hideAnimation()
{
    QPropertyAnimation* anim = propertyAnimation("opacity");
    anim->setEndValue(0);

    return anim;
}
