/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef SCROLLBAR_H
#define SCROLLBAR_H

#include <QObject>
#include <QString>
#include <QTimer>
#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>
#include <QAbstractAnimation>

class ScrollBar: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    ScrollBar(QGraphicsItem* parent);
    virtual ~ScrollBar() {}

    void updateUI();

    qreal maximum();
    void setMaximum(qreal max);

    qreal value();
    void setValue(qreal);

    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity);

public slots:
    void updatePosition(qreal);
    void setLandscapeOrientation();
    void setPortraitOrientation();

private slots:
    void animationFinished();
    void timeout();

private:
    QPropertyAnimation* propertyAnimation(const char* name);
    QAbstractAnimation* showAnimation();
    QAbstractAnimation* hideAnimation();

    void startTimer();

    enum State {
        Hidden,
        ShowAnimation,
        Visible,
        HideAnimation
    };
    State m_state;

    qreal m_maximum;
    qreal m_value;
    QTimer m_timer;
    QAbstractAnimation* m_animation;
    QGraphicsPixmapItem m_knob;
};

#endif /* SCROLLBAR_H */
