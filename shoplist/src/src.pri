HEADERS = src/scrollbar.h \
    src/maincontainer.h \
    src/designinformation.h \
    src/contentlist.h \
    src/shoppinglist.h \
    src/shoppinglistcategoryui.h \
    src/popup.h \
    src/gesturebox.h \
    src/listshadow.h \
    src/gesturebox_p.h \
    src/shoppinglistview.h
SOURCES = src/scrollbar.cpp \
    src/main.cpp \
    src/maincontainer.cpp \
    src/contentlist.cpp \
    src/designinformation.cpp \
    src/shoppinglist.cpp \
    src/shoppinglistcategoryui.cpp \
    src/popup.cpp \
    src/gesturebox.cpp \
    src/listshadow.cpp \
    src/shoppinglistview.cpp
