/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef SHOPPINGLISTVIEW_H
#define SHOPPINGLISTVIEW_H

#include <QGraphicsView>
#include "maincontainer.h"
#include "designinformation.h"

class ShoppingListView : public QGraphicsView
{
    Q_OBJECT
public:
    ShoppingListView(MainContainer *container);
    void adjustScreenSize();
    void adjustScalingFactor();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    MainContainer *m_mainContainer;

private slots:
    void resized(int screen);
};

#endif // SHOPPINGLISTVIEW_H
