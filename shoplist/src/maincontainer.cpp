/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QKeyEvent>
#include <QPointF>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QEvent>
#include <QGraphicsSceneMouseEvent>
#include <QCoreApplication>
#include <QGraphicsTextItem>
#include <QFont>

#include <QAbstractAnimation>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <cmath>
#include <QDebug>
#include <QPainter>

#include "maincontainer.h"
#include "designinformation.h"

static const qreal listOffset = -1;

// CloseButton

CloseButton::CloseButton(QGraphicsItem *parent)
        : QGraphicsPixmapItem(parent)
{
    setShapeMode(BoundingRectShape);
    updateUI();
}

void CloseButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    QCoreApplication::instance()->quit();
}

void CloseButton::updateUI()
{
    setPixmap(DesignInformation::getPixmap("closeButton"));
    if (parentItem()) {
        qreal top = DesignInformation::getPosition("closeButton").y();
        qreal left = parentItem()->boundingRect().width() - pixmap().width();
        left -= DesignInformation::getHorizontalSize("close_button_spacing");
        setPos(left, top);
    }
    else
        setPos(DesignInformation::getPosition("closeButton"));
    prepareGeometryChange();
    update();
}

QPainterPath CloseButton::shape() const
{
    return QGraphicsItem::shape();
}

QRectF CloseButton::boundingRect() const
{
#ifdef Q_WS_MAEMO_5
    static const qreal minSize = 120.0;
#else
    static const qreal minSize = 80.0;
#endif
    QRectF result(QGraphicsPixmapItem::boundingRect());
    qreal hMargin = minSize < result.width() ? 0 : (minSize - result.width()) / 2;
    qreal vMargin = minSize < result.height() ? 0 : (minSize - result.height()) / 2;
    result.adjust(-hMargin, -vMargin, hMargin, vMargin);
    return result;
}

// ListChangeGestureBox

static const int max_gesture_time_ms = 500;
static const qreal degrees = 30.0;
static const qreal limit_angle_tg = tan(degrees * 3.141592 / 180.0);
static const qreal min_gesture_length = 60.0;

ListChangeGestureBox::ListChangeGestureBox(QGraphicsItem *parent)
    : GestureBox(parent)
    , m_active(true)
    , m_startPoint(0.0, 0.0)
{
}

void ListChangeGestureBox::setActivate(bool active)
{
    m_active = active;
}

void ListChangeGestureBox::gestureMousePress(QPointF pos, bool &startGesture, bool &acceptClick)
{
    Q_UNUSED(pos);
    Q_UNUSED(acceptClick);
    startGesture = m_active;
}

void ListChangeGestureBox::gestureStart(QPointF pos)
{
    m_startPoint = pos;
    m_startTime = QTime::currentTime();
}

void ListChangeGestureBox::gestureMove(QPointF pos, QPointF movement, QPointF speed)
{
    Q_UNUSED(pos);
    Q_UNUSED(movement);
    Q_UNUSED(speed);
}

void ListChangeGestureBox::gestureEnd(QPointF pos, QPointF speed)
{
    Q_UNUSED(speed);
    qreal x = pos.x() - m_startPoint.x();
    qreal y = pos.y() - m_startPoint.y();
    if (!m_active
        || fabs(y / x) > limit_angle_tg
        || m_startTime.msecsTo(QTime::currentTime()) > max_gesture_time_ms
        || fabs(x) < min_gesture_length) {
        return;
    }

    if (x > 0)
        emit moveLeft();
    else
        emit moveRight();
}

// MainContainer

MainContainer::MainContainer()
        : m_animation(0)
        , m_lists()
        , m_current(0)
        , m_hold(new QGraphicsRectItem(this))
        , m_closeButton(new CloseButton(this))
        , m_gestureBox(new ListChangeGestureBox(this))
{
    setPixmap(DesignInformation::getPixmap("background"));
    m_hold->setFlags(QGraphicsItem::ItemHasNoContents);
    m_hold->setPos(DesignInformation::getHorizontalSize("listset.center_position"), 0.0);

    m_gestureBox->setRect(boundingRect());

    m_title = new QGraphicsTextItem(this);
    QString html = "<span style=\"font-family:Nokia sans; color:#f8f8f8\">";
    html += "Shopping List </span>";
    m_title->setHtml(html);

    connect(m_gestureBox,SIGNAL(moveLeft()), this, SLOT(moveLeft()));
    connect(m_gestureBox,SIGNAL(moveRight()), this, SLOT(moveRight()));
}

void MainContainer::adjustGestureBox()
{
    m_gestureBox->setActivate(m_current >= 0 && m_current < m_lists.count());
}

void MainContainer::add(ShoppingListCategoryUI *list)
{

    if (m_lists.indexOf(list) >= 0)
        return;

    list->setParentItem(m_hold);
    m_lists.append(list);
    if (m_lists.count() == 1)
        m_lists[0]->setFocused(true);

    adjustListsPositions();
    adjustGestureBox();
}

void MainContainer::adjustListsPositions()
{
    qreal listPos = 0.0;

    for (int i = 0; i < m_lists.count(); ++i) {
        m_lists[i]->setPos(listPos, 0);
        listPos += m_lists[i]->boundingRect().width() + listOffset;
        m_lists[i]->setFocused(i == m_current);
    }
}

void MainContainer::updateUI()
{
    setPixmap(DesignInformation::getPixmap("background"));
    foreach(ShoppingListCategoryUI *list, m_lists)
        list->updateUI();

    QFont font = m_title->font();
    font.setPixelSize(DesignInformation::getFontSize("title"));
    m_title->setFont(font);
    QPointF titlePos = DesignInformation::getPosition("title");
    qreal titleLeft = (boundingRect().width() - m_title->boundingRect().width()) / 2;
    m_title->setPos(titleLeft, titlePos.y());

    m_closeButton->updateUI();

    adjustListsPositions();

    qreal holdOffset = DesignInformation::getHorizontalSize("listset.center_position");
    if (m_current >= 0 && m_current < m_lists.count()) {
        for (int i = 0; i < m_current; ++i)
            holdOffset -= m_lists[i]->boundingRect().width();
    }
    m_hold->setPos(holdOffset, 0);

    m_gestureBox->setRect(boundingRect());
    adjustGestureBox();
}

void MainContainer::moveLeft()
{
    startMovement(false);
}

void MainContainer::moveRight()
{
    startMovement(true);
}

void MainContainer::startMovement(bool leftToRight)
{
    int target = m_current + (leftToRight ? 1 : -1);
    if (m_animation || target < 0 || target >= m_lists.count())
        return;

    QAbstractAnimation* anim = sideAnimation(leftToRight);
    anim->start(QAbstractAnimation::DeleteWhenStopped);
    m_animation = anim;
    m_current = target;

    m_gestureBox->setActivate(false);
    m_lists[m_current]->setFocused(true);
    if (m_current > 0)
        m_lists[m_current - 1]->setFocused(false);
    if (m_current < m_lists.count() - 1)
        m_lists[m_current + 1]->setFocused(false);
}

void MainContainer::animationFinished()
{
    m_animation = 0;
    adjustGestureBox();
}

QAbstractAnimation* MainContainer::sideAnimation(bool leftToRight)
{
    QParallelAnimationGroup* group = new QParallelAnimationGroup();

    qreal left = m_lists[m_current]->boundingRect().width() + listOffset;
    left = m_hold->pos().x() +(leftToRight ? -left : left);
    
    QPropertyAnimation* anim = new QPropertyAnimation(this, "position");
    anim->setEasingCurve(QEasingCurve::OutExpo);
    anim->setDuration(500);
    anim->setEndValue(QPointF(left, 0));

    group->addAnimation(anim);

    connect(group, SIGNAL(finished()), this, SLOT(animationFinished()));

    return group;
}
