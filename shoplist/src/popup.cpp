/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#include <QBrush>
#include <QEvent>
#include <QPointF>
#include <QGraphicsPixmapItem>

#include "popup.h"
#include "designinformation.h"

// PopupCloseButton

PopupCloseButton::PopupCloseButton(QGraphicsItem *parent)
        : QGraphicsPixmapItem(parent)
{
    setShapeMode(BoundingRectShape);
}

QPainterPath PopupCloseButton::shape() const
{
    return QGraphicsItem::shape();
}

QRectF PopupCloseButton::boundingRect() const
{
    static const qreal minSize = 80.0;
    QRectF result(QGraphicsPixmapItem::boundingRect());
    qreal hMargin = minSize < result.width() ? 0 : (minSize - result.width()) / 2;
    qreal vMargin = minSize < result.height() ? 0 : (minSize - result.height()) / 2;
    result.adjust(-hMargin, -vMargin, hMargin, vMargin);
    return result;
}

// PopUp

PopUp::PopUp(QGraphicsItem* parent)
    : QGraphicsRectItem(parent)
{
    m_allDone = new QGraphicsPixmapItem(this);
    m_allDone->installSceneEventFilter(this);

    m_startAgain = new QGraphicsPixmapItem(m_allDone);
    m_startAgain->installSceneEventFilter(this);

    m_close = new PopupCloseButton(m_allDone);
    m_close->installSceneEventFilter(this);

    updateUI();
}

void PopUp::updateUI()
{
    m_allDone->setPixmap(DesignInformation::getPixmap("alldone.background"));
    m_allDone->setPos((parentItem()->boundingRect().bottomRight()/2) -
            (m_allDone->boundingRect().bottomRight()/2));

    m_startAgain->setPixmap(DesignInformation::getPixmap("alldone.startagain_off"));
    m_startAgain->setPos((m_allDone->boundingRect().bottomRight()/2) -
            (m_startAgain->boundingRect().bottomRight()/2) +
            DesignInformation::getPosition("alldone.startagain_off"));

    m_close->setPixmap(DesignInformation::getPixmap("alldone.close_button"));
    m_close->setPos((m_allDone->boundingRect().bottomRight()/2) -
            (m_close->boundingRect().bottomRight()/2) +
    DesignInformation::getPosition("alldone.close_button"));
}

bool PopUp::sceneEventFilter(QGraphicsItem *item, QEvent *event)
{
    if ((event->type() == QEvent::GraphicsSceneMousePress) and (item == m_startAgain))
        emit listStartAgain();
    else if ((event->type() == QEvent::GraphicsSceneMousePress) and (item == m_close))
        emit deletePopUp();
    return true;
}
