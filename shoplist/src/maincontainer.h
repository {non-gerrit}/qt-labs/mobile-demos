/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: qt-info@nokia.com
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  You may use this software in
** accordance with the terms and conditions contained in the Qt Phone
** Demo License Agreement.
**
****************************************************************************/

#ifndef _MAINCONTAINER_H_
#define _MAINCONTAINER_H_

#include <QPointF>
#include <QObject>
#include <QKeyEvent>
#include <QAbstractAnimation>
#include <QPropertyAnimation>

#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>

#include "shoppinglistcategoryui.h"
#include "gesturebox.h"

class CloseButton: public QGraphicsPixmapItem
{
public:
    CloseButton(QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void updateUI();
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

class ListChangeGestureBox : public QObject, public GestureBox
{
    Q_OBJECT
public:
    ListChangeGestureBox(QGraphicsItem *parent = 0);
    void setActivate(bool active);

signals:
    void moveLeft();
    void moveRight();

protected:
    void gestureMousePress(QPointF pos, bool &startGesture, bool &acceptClick);
    void gestureStart(QPointF pos);
    void gestureMove(QPointF pos, QPointF movement, QPointF speed);
    void gestureEnd(QPointF pos, QPointF speed);

private:
    bool m_active;
    QPointF m_startPoint;
    QTime m_startTime;
};

class MainContainer: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF position READ position WRITE setPosition);

public:
    MainContainer();
    virtual ~MainContainer() {};

    void add(ShoppingListCategoryUI *);

    void updateUI();



public slots:
    void moveLeft();
    void moveRight();

private slots:
    void animationFinished();

private:
    QPointF position() { return m_hold->pos(); }
    void setPosition(QPointF pos) { m_hold->setPos(pos); }

    QAbstractAnimation* m_animation;
    QAbstractAnimation* sideAnimation(bool leftToRight);

    QList<ShoppingListCategoryUI*> m_lists;
    int m_current;
    QGraphicsRectItem* m_hold;
    CloseButton *m_closeButton;
    ListChangeGestureBox *m_gestureBox;
    QGraphicsTextItem* m_title;

    void adjustGestureBox();
    void startMovement(bool leftToRight);
    void adjustListsPositions();
};

#endif /* _MAINCONTAINER_H_ */
