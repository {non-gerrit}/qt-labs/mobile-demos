#!/bin/bash -e
# Script based on git://gitorious.org/qt-codecheck/qt-codecheck.git
# by Milton Soares Filho

die ()
{
	echo "FAIL!: $*"
	exit 1
}

ASTYLE=$(which astyle)

[ -z "${ASTYLE}" ] && die "missing astyle binary"

ASTYLE_SUFFIX=.astyle.orig

ASTYLE_OPTIONS="-n --indent=spaces=4 --brackets=linux \
    --indent-labels --indent-namespaces \
    --pad-oper --pad-header --keep-one-line-statements \
    --keep-one-line-blocks --convert-tabs --indent-preprocessor \
    --align-pointer=name --suffix=${ASTYLE_SUFFIX}"

# this may not work with absolute paths
# FIXME: Check it later
#cd "$(dirname $0)/.."
#SUBDIRS="src tests"

FILTERIN="-name '*.cpp' -o -name '*.cc' -o -name '*.h'"

# Filter out moc generated files
FILTEROUT="-not -name 'moc_*'"

SOURCES=$(find -type f -name '*.h' -o -name '*.cc' -o -name '*.cpp' -not -name 'moc_*' -not -name 'qrc_*')
sandboxdir=$(mktemp -dt astyle.XXXXXXXX)

[ -z ${sandboxdir} ] && die "could not create tempdir"

for f in ${SOURCES}; do
	cp --parents $f ${sandboxdir}
done

# many commands below have non-zero exit code
set +e

pushd ${sandboxdir} > /dev/null
	find . -type f -exec ${ASTYLE} ${ASTYLE_OPTIONS} {} \; | grep formatted
popd > /dev/null

PATCH_FILE=astyle.patch
diff -ru . ${sandboxdir} | grep -ve '^Only in ' > ${PATCH_FILE}
count=$(cat ${PATCH_FILE} | wc -l)

[ ${count} -eq 0 ] || die "bad formatting, check ${PATCH_FILE}"

rm -f ${PATCH_FILE}
echo "PASS: coding style ok"
