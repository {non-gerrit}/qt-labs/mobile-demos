/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include <QHBoxLayout>
#include <QPushButton>
#include <QRadioButton>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>

#include "networkclient.h"

#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , m_client(0)
    , m_server(0)
{
    setupWidgets();

    setupLayouts();

    setupConnections();
}

void Widget::onStartButtonClicked()
{
    m_startButton->setEnabled(false);
    m_serverButton->setEnabled(false);
    m_clientButton->setEnabled(false);

    if (m_serverButton->isChecked()) {
        m_server = new NetworkServer(this);
        connect(m_server, SIGNAL(messageSent(QHostAddress, quint16, quint16)),
                this, SLOT(onMessageSent(QHostAddress, quint16, quint16)));
        connect(m_server, SIGNAL(messageReceived(QHostAddress, quint16, quint16)),
                this, SLOT(onMessageReceived(QHostAddress, quint16, quint16)));
        connect(m_server, SIGNAL(error(NetworkServer::ServerError)),
                this, SLOT(onServerError(NetworkServer::ServerError)));

        m_textEdit->append("----- Server started -----");

        m_server->start();
    } else {
        m_client = new NetworkClient(this);
        connect(m_client, SIGNAL(messageSent(QHostAddress, quint16, quint16)),
                this, SLOT(onMessageSent(QHostAddress, quint16, quint16)));
        connect(m_client, SIGNAL(messageReceived(QHostAddress, quint16, quint16)),
                this, SLOT(onMessageReceived(QHostAddress, quint16, quint16)));

        m_textEdit->append("----- Client started -----");

        m_client->startDiscovery();
    }

    m_stopButton->setEnabled(true);
}

void Widget::onStopButtonClicked()
{
    m_stopButton->setEnabled(false);

    if (m_serverButton->isChecked()) {
        if (m_server != 0) {
            m_textEdit->append("----- Server stopped -----");

            m_server->deleteLater();
            m_server = 0;
        }
    } else {
        if (m_client != 0) {
            m_textEdit->append("----- Client stopped -----");

            m_client->stopDiscovery();

            m_client->deleteLater();
            m_client = 0;
        }
    }

    m_startButton->setEnabled(true);
    m_serverButton->setEnabled(true);
    m_clientButton->setEnabled(true);
}

void Widget::onMessageSent(QHostAddress address, quint16 content, quint16 senderListenPort)
{
    QString string;

    string = "Sent \"" + QString::number(content) + "\" to " + address.toString()
             + " at port " + QString::number(senderListenPort);

    m_textEdit->append(string);
}

void Widget::onMessageReceived(QHostAddress address, quint16 content, quint16 port)
{
    QString string;

    string = "Received \"" + QString::number(content) + "\" from " + address.toString()
             + " at port " + QString::number(port);

    m_textEdit->append(string);
}

void Widget::onServerError(NetworkServer::ServerError error)
{
    if (error == NetworkServer::AnotherServerRunningError)
        m_textEdit->append("Can't start a new server.. there is another one already running");
}

void Widget::setupWidgets()
{
    m_serverButton = new QRadioButton("Server");
    m_clientButton = new QRadioButton("Client");
    m_startButton = new QPushButton("Start");
    m_stopButton = new QPushButton("Stop");
    m_textEdit = new QTextEdit();

    m_serverButton->setChecked(true);
    m_startButton->setMinimumHeight(40);
    m_stopButton->setMinimumHeight(40);
    m_stopButton->setEnabled(false);
    m_textEdit->setMaximumHeight(100);
    m_textEdit->setReadOnly(true);
}

void Widget::setupLayouts()
{
    m_verticalLayout = new QVBoxLayout();
    m_verticalLayout->setContentsMargins(0, 0, 0, 0);
    m_verticalLayout->setSpacing(0);

    m_horizontalLayout = new QHBoxLayout();
    m_horizontalLayout->setContentsMargins(0, 0, 0, 0);
    m_horizontalLayout->setSpacing(0);

    m_horizontalLayout->addStretch();
    m_horizontalLayout->addWidget(m_serverButton);
    m_horizontalLayout->addStretch();
    m_horizontalLayout->addWidget(m_clientButton);
    m_horizontalLayout->addStretch();

    m_verticalLayout->addLayout(m_horizontalLayout);
    m_verticalLayout->addWidget(m_startButton);
    m_verticalLayout->addWidget(m_stopButton);
    m_verticalLayout->addWidget(m_textEdit);

    setLayout(m_verticalLayout);
}

void Widget::setupConnections()
{
    connect(m_startButton, SIGNAL(clicked()),
            this, SLOT(onStartButtonClicked()));
    connect(m_stopButton, SIGNAL(clicked()),
            this, SLOT(onStopButtonClicked()));
}
