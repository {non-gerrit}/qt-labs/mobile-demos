TEMPLATE = app

QT += network

BUILD_TESTS=$$BUILD_DEBUG
isEqual(BUILD_DEBUG, 1) {
    CONFIG += debug
}

CONFIG(debug, debug|release) {
    TARGET   = network_example_debug

    LIBS    += -Wl,-rpath,$$BUILD_TREE/lib -L$$BUILD_TREE/lib \
               -lnetwork_debug
} else {
    TARGET   = network_example

    LIBS    += -Wl,-rpath,$$BUILD_TREE/lib -L$$BUILD_TREE/lib \
               $$BUILD_TREE/lib/libnetwork.a

    DEFINES += QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT
}

INCLUDEPATH += $$SOURCE_TREE/src/network

SOURCES += \
    widget.cpp \
    main.cpp

HEADERS += \
    widget.h

DESTDIR = $$BUILD_TREE/bin
