/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7
import game.types 1.0

Rectangle {
    id: controlPad

    property bool touchEnabled: !sensorMovement.enabled
    property int rectSize: 120

    property string padColor: "lightgrey"
    property string knobColor: "steelblue"
    property string borderColor: "black"

    width: 400
    height: 480

    Rectangle {
        id: center
        width: rectSize; height: rectSize;
        anchors.centerIn: parent
    }

    Rectangle {
        id: up
        color: padColor
        width: rectSize; height: rectSize;
        anchors.bottom: center.top
        anchors.horizontalCenter: center.horizontalCenter
    }

    Rectangle {
        id: left
        color: padColor
        width: rectSize; height: rectSize;
        anchors.right: center.left
        anchors.verticalCenter: center.verticalCenter
    }

    Rectangle {
        id: right
        color: padColor
        width: rectSize; height: rectSize;
        anchors.left: center.right
        anchors.verticalCenter: center.verticalCenter
    }

    Rectangle {
        id: down
        color: padColor
        width: rectSize; height: rectSize;
        anchors.top: center.bottom
        anchors.horizontalCenter: center.horizontalCenter
    }

    MouseArea {
        hoverEnabled: true
        anchors.fill: parent

        function setDirection(direction, knobState) {
            knob.state = knobState

            if (control.direction != direction)
                control.direction = direction
        }

        function moveKnob(mouse) {
            if (!pressed || !controlPad.touchEnabled)
                return;

            var pos = mapToItem(parent, mouse.x, mouse.y)

            if (pos.x < 0 || pos.y < 0 || pos.x > parent.width || pos.y > parent.height)
                return;

            if (pos.x < center.x) {
                if (pos.y < center.y)
                    setDirection(Control.UpLeft, "UpLeft")
                else if (pos.y > down.y)
                    setDirection(Control.DownLeft, "DownLeft")
                else
                    setDirection(Control.Left, "Left")
            } else if (pos.x > right.x) {
                if (pos.y < center.y)
                    setDirection(Control.UpRight, "UpRight")
                else if (pos.y > down.y)
                    setDirection(Control.DownRight, "DownRight")
                else
                    setDirection(Control.Right, "Right")
            } else {
                if (pos.y < center.y)
                    setDirection(Control.Up, "Up")
                else if (pos.y > down.y)
                    setDirection(Control.Down, "Down")
                else
                    setDirection(Control.Stop, "Stop")
            }
        }

        function stopKnob() {
            if (!controlPad.touchEnabled)
                return;

            setDirection(Control.Stop, "Stop")
        }

        onPositionChanged: moveKnob(mouse)
        onPressed: moveKnob(mouse)
        onReleased: stopKnob()
    }

    Rectangle {
        id: knob
        color: knobColor;

        width: rectSize
        height: rectSize
        radius: rectSize
        smooth: true;

        x: center.x
        y: center.y

        Behavior on x { SpringAnimation { spring: 3; damping: 0.2 } }
        Behavior on y { SpringAnimation { spring: 3; damping: 0.2 } }

        border {
            color: borderColor
            width: 2
        }

        states {
            State {
                name: "Up";
                PropertyChanges { target: knob; x: up.x; y: up.y; }
            }
            State {
                name: "Down";
                PropertyChanges { target: knob; x: down.x; y: down.y }
            }
            State {
                name: "Right";
                PropertyChanges { target: knob; x: right.x; y: right.y }
            }
            State {
                name: "Left";
                PropertyChanges { target: knob; x: left.x; y: left.y }
            }
            State {
                name: "Stop";
                PropertyChanges { target: knob; x: center.x; y: center.y }
            }
        }

        Connections {
            target: control
            onDirectionChanged: knob.state = control.directionString
        }
    }
}
