/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

Item {
    id: slider

    height: 80

    property int maxValue: 0
    property int currentValue: 0
    property string text: "Slider"

    property alias valueText: value.text

    property string borderColor: "black"
    property string textColor: "black"
    property string knobColor: "steelblue"

    signal valueChanged(int value)

    Text {
        id: label

        color: textColor
        font.family: "NokiaSans"
        font.pixelSize: 18
        text: slider.text

        anchors {
            top: parent.top
            left: parent.left
        }
    }

    Text {
        id: value
        color: knobColor
        font.family: "NokiaSans"
        font.pixelSize: 18
        font.bold: true
        text: currentValue

        anchors {
            left: label.right
            leftMargin: 10
        }
    }

    Rectangle {
        id: line

        color: "lightgrey"
        width: slider.width
        height: knob.height + 2
        radius: 50
        smooth: true

        anchors {
            top: label.bottom
            topMargin: 5
        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                var pos = mouse.x - knob.width
                if (pos < 0)
                    pos = 0

                knob.x = pos
                knob.checkPositionChange(pos)
            }
        }
    }

    Rectangle {
        id: lineFill

        color: "steelblue"
        opacity: 0.5
        height: line.height
        radius: line.radius
        smooth: true

        anchors {
            top: line.top
            left: line.left
            right: knob.right
        }
    }

    Rectangle {
        id: knob

        function valueForPosition(pos) {
            return Math.round(pos / ((line.width - knob.width) / slider.maxValue))
        }

        function positionForValue(value) {
            return Math.round(value * ((line.width - knob.width) / slider.maxValue))
        }

        function checkPositionChange(pos) {
            var newValue = knob.valueForPosition(pos)

            slider.currentValue = newValue
            slider.valueText = currentValue
            slider.valueChanged(newValue)
        }

        color: knobColor
        width: 50
        height: 50
        radius: 50
        smooth: true

        x: positionForValue(slider.currentValue)

        border {
            color: borderColor
            width: 2
        }

        MouseArea {
            anchors.fill: parent

            drag.target: knob
            drag.axis: Drag.XAxis
            drag.minimumX: 0
            drag.maximumX: line.width - knob.width

            onPositionChanged: knob.checkPositionChange(knob.x)
        }

        anchors {
            verticalCenter: line.verticalCenter
        }

        Behavior on x { NumberAnimation { duration: 120 } }
    }
}
