/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

Rectangle {
    id: button

    property string text: "Button"

    property bool isCheckable: false
    property bool isToggled: false
    property bool enabled: true

    property string normalColor: "lightgrey"
    property string toggledColor: "steelblue"
    property string pressedColor: "lightblue"
    property string disabledColor: "white"
    property string borderColor: "black"
    property string labelColor: "black"
    property string labelDisabledColor: "grey"

    signal clicked
    signal toggled(bool isToggled)

    width: label.width + 20
    height: label.height + 20
    smooth: true
    radius: 20

    border {
        width: 2
        color: borderColor
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        enabled: button.enabled
        onClicked: {
            if (isCheckable) {
               isToggled = !isToggled
               toggled(isToggled)
            }

            button.clicked()
        }
    }

    Text {
        id: label
        anchors.centerIn: button
        color: labelColor
        text: button.text
        font.pixelSize: 18
        font.family: "NokiaSans"
    }

    states {
        State {
            name: "disabled"; when: !mouseArea.enabled
            PropertyChanges { target: button; color: disabledColor }
            PropertyChanges { target: label; color: labelDisabledColor }
        }

        State {
            name: "pressed"; when: mouseArea.pressed
            PropertyChanges { target: button; color: pressedColor }
        }

        State {
            name: "normal"; when: !isToggled
            PropertyChanges { target: button; color: normalColor }
        }

        State {
            name: "toggled"; when: isToggled
            PropertyChanges { target: button; color: toggledColor }
        }
    }

    transitions {
        Transition {
            ColorAnimation { duration: 100 }
        }
    }
}
