#ifndef TESTCONTROL_H
#define TESTCONTROL_H

#include "global.h"

#include <QDebug>
#include <QObject>

class TestControl : public QObject
{
    Q_OBJECT

public:
    TestControl(QObject *parent = 0) : QObject(parent) {}

public slots:
    void onDirectionChanged(Global::Direction direction)
    {
        qDebug() << "Direction changed:" << Global::directionString(direction);
    }
};

#endif
