TEMPLATE = app

TARGET = control_example

QT += declarative

maemo5 {
    CONFIG += mobility12
} else {
    CONFIG += mobility
}

MOBILITY += sensors

INCLUDEPATH += $$PWD/../../src/control \
               $$PWD/../../src/utils

SOURCES += \
    main.cpp \
    $$PWD/../../src/control/control.cpp \
    $$PWD/../../src/control/sensormovement.cpp \
    $$PWD/../../src/utils/global.cpp \
    $$PWD/../../src/utils/screen.cpp
HEADERS += \
    $$PWD/../../src/control/control.h \
    $$PWD/../../src/control/sensormovement.h \
    $$PWD/../../src/utils/global.h \
    $$PWD/../../src/utils/screen.h \
    testcontrol.h

RESOURCES += qml/qml.qrc

symbian {
    TARGET.UID3 = 0xE9131533

    TARGET.CAPABILITY = \
        ReadUserData \
        UserEnvironment \
        WriteUserData

    TARGET.EPOCHEAPSIZE = 0x20000 0x8000000

    TARGET.EPOCSTACKSIZE = 0x14000


    LIBS += \
        -lavkon \
        -leikcore \
        -lcone

}
