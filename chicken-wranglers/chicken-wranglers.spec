#
Name: chicken-wranglers
Summary: Chicken Wranglers
Version: 0.1
Release: 1%{?dist}
Group: Applications/Game
License: BSD
URL: http://qt.nokia.com
Source: %{name}-%{version}.tar.gz
Requires: qt
Requires: qt-mobility
BuildRequires: pkgconfig(QtOpenGL)
BuildRequires: pkgconfig(QtDeclarative)
BuildRequires: pkgconfig(QtConnectivity)
BuildRequires: pkgconfig(QtSensors)

%description
Chicken Wranglers - chicken catch game

%prep
%setup -q -n %{name}-%{version}

%build
qmake PREFIX=%{_prefix}
make %{?_smp_mflags}

%install
%qmake_install

%clean
make distclean-all
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%attr(755,root,root)%{_bindir}/chicken-wranglers
%{_datadir}/icons/hicolor/scalable/apps/chicken-wranglers.png
%{_datadir}/icons/hicolor/32x32/apps/chicken-wranglers.png
%{_datadir}/applications/chicken-wranglers.desktop
%{_datadir}/chicken-wranglers/characters/blue/*
%{_datadir}/chicken-wranglers/characters/green/*
%{_datadir}/chicken-wranglers/characters/red/*
%{_datadir}/chicken-wranglers/characters/yellow/*
%{_datadir}/chicken-wranglers/sounds/*


%changelog
* Thu Jul 14 2011 Rodrigo Gon�alves de Oliveira <rodrigo.goncalves@openbossa.org> - 0.0.4-1
- New Release
* Thu May 21 2011 Rodrigo Belem <rodrigo.belem@openbossa.org> - 0.0.3-1
- New Release
* Thu Feb 11 2011 Rodrigo Belem <rodrigo.belem@openbossa.org> - 0.0.2-1
- New Release
- Added QtOpenGL as build requires
* Thu Feb  7 2011 Rodrigo Belem <rodrigo.belem@openbossa.org> - 0.0.1-2
- Package updated to match latest game changes
* Fri Jan 14 2011 Benedito Neto <benedito.almeida@openbossa.org> - 0.0.1-1
- Packaging
