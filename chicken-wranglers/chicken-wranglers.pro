TEMPLATE = app
TARGET = chicken-wranglers

QT += opengl

CONFIG += warn_on release
MOBILITY = connectivity sensors

INSTALL_PREFIX = $$PREFIX
isEmpty (INSTALL_PREFIX){
    INSTALL_PREFIX = /usr
}

MOC_DIR = tmp
OBJECTS_DIR = tmp
RCC_DIR = tmp

SOURCE_TREE = $$PWD
SRC_PATH = $$SOURCE_TREE

INCLUDEPATH += \
    src/ai \
    src/control \
    src/controllers \
    src/main \
    src/models \
    src/network \
    src/sound \
    src/utils

include(src/ai/ai.pri)
include(src/control/control.pri)
include(src/controllers/controllers.pri)
include(src/main/main.pri)
include(src/models/models.pri)
include(src/network/network.pri)
include(src/utils/utils.pri)

RESOURCES += $$SRC_PATH/resources/common.qrc

CONFIG(debug, debug|release) {
    DEFINES += CW_DEBUG
} else {
    RESOURCES += src/qml/qml.qrc
}

unix:!symbian{
    RESOURCES += $$SRC_PATH/resources/linux.qrc
    CONFIG += link_pkgconfig
    PKGCONFIG += libpulse-simple

    include(src/sound/sound_linux.pri)

    desktop.files = data/chicken-wranglers.desktop
    desktop.path = /usr/share/applications
    maemo5: desktop.path = /usr/share/applications/hildon
    INSTALLS += desktop

    icon.files = data/chicken-wranglers.svg
    icon.path = $$INSTALL_PREFIX/share/icons/hicolor/scalable/apps
    INSTALLS += icon

    icon64.files = data/chicken-wranglers.png
    icon64.path = $$INSTALL_PREFIX/share/icons/hicolor/scalable/apps
    INSTALLS += icon64

    characters.files = characters
    characters.path = $$INSTALL_PREFIX/share/chicken-wranglers
    INSTALLS += characters

    sounds.files = sounds
    sounds.path = $$INSTALL_PREFIX/share/chicken-wranglers
    INSTALLS += sounds

    distclean-all.commands = rm -rf $$OUT_PWD/lib $$OUT_PWD/bin $$OUT_PWD/.qmake.cache
    distclean-all.depends = distclean
    !equals($$OUT_IN, $$OUT_PWD): dist-clean.commands += $$OUT_PWD

    QMAKE_EXTRA_TARGETS += distclean-all
}

symbian {
    RESOURCES += $$SRC_PATH/resources/symbian.qrc

    include(src/sound/sound.pri)

    TARGET.UID3 = 0xE9131562

    TARGET.CAPABILITY = \
        NetworkServices \
        LocalServices \
        ReadUserData \
        UserEnvironment \
        WriteUserData

    symbian-abld|symbian-sbsv2 {
        # ro-section in gui can exceed default allocated space,
        # so move rw-section a little further
        QMAKE_LFLAGS.ARMCC += --rw-base 0x800000
        QMAKE_LFLAGS.GCCE += -Tdata 0xC00000
    }

    TARGET.EPOCHEAPSIZE = 0x20000 0x8000000

    TARGET.EPOCSTACKSIZE = 0x14000

    characters.sources = characters
    characters.path = C:\\DATA\\chicken-wranglers
    DEPLOYMENT += characters

    LIBS += \
        -lavkon \
        -leikcore \
        -lcone

    ICON = $$SRC_PATH/data/chicken-wranglers.svg
}

DESTDIR += $$OUT_PWD/bin

target.path = $$INSTALL_PREFIX/bin

INSTALLS += target
