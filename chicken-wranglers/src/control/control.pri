maemo5 {
    CONFIG += mobility12
} else {
    CONFIG += mobility
}
MOBILITY += sensors

PATH = $$PWD

HEADERS += \
    $$PATH/control.h \
    $$PATH/sensormovement.h

SOURCES += \
    $$PATH/control.cpp \
    $$PATH/sensormovement.cpp
