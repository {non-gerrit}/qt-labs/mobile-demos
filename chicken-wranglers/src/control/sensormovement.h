/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef SENSORMOVEMENT_H
#define SENSORMOVEMENT_H

#include "global.h"

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QAccelerometer>

QTM_USE_NAMESPACE

class SensorMovement : public QObject, public QAccelerometerFilter
{
    Q_OBJECT

public:
    SensorMovement(QObject *parent = 0);

    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(qreal minimumRotation READ minimumRotation WRITE setMinimumRotation NOTIFY minimumRotationChanged)
    Q_PROPERTY(qreal threshold READ threshold WRITE setThreshold NOTIFY thresholdChanged)
    Q_PROPERTY(int directionTimeout READ directionTimeout WRITE setDirectionTimeout NOTIFY directionTimeoutChanged)

    /**
     * Defines if sensor is \a enabled, if it will forward direction change signals.
     */
    bool isEnabled() const { return m_enabled; }
    void setEnabled(bool enabled);

    /**
     * Defines a minimum absolute rotation value required to
     * detect a direction change in a axis. Note that DirectionStop
     * is also considered a direction.
     */
    qreal minimumRotation() const { return m_minimumRotation; }
    void setMinimumRotation(qreal minimumRotation);

    /**
     * Defines an angle interval as [-threshold, threshold], where the device
     * rotation is not considered to be at any direction.
     *
     * For instance, if threshold was set to 5 and device is moving along
     * vertical axis, it will not notify any direction change when the the
     * rotation sensor value for that axis is between -5 and 5.
     */
    qreal threshold() const { return m_threshold; }
    void setThreshold(qreal threshold);

    /**
     * Defines a time required for the device to stand in a given direction,
     * before actually notifying a direction change.
     *
     * When devices is moved to a direction A and changed to a direction B in
     * less than \a timeout miliseconds, it won't emit directionChange signal
     * for direction A.
     */
    int directionTimeout() const { return m_directionTimeout; }
    void setDirectionTimeout(int directionTimeout);

    Global::Direction direction() const { return m_direction; }

public slots:
    /**
     * Resets the device's start position.
     *
     * When this method is called, the current device position is read
     * from sensor and all the direction calculations will be based on
     * these offsets for both axis (x = 0 + m_startX, y = 0 + m_startY).
     */
    void resetStartPosition();

signals:
    void directionChanged(Global::Direction direction);
    void minimumRotationChanged();
    void thresholdChanged();
    void directionTimeoutChanged();
    void enabledChanged();

protected:
    bool filter(QAccelerometerReading *reading);

private slots:
    void notifyDirectionChanged();

private:
    Global::Direction calculateDirection(qreal x, qreal y);
    Global::Direction calculateDirectionMaemo5(qreal x, qreal y);

    QAccelerometer m_sensor;

    Global::Direction m_direction;

    qreal m_startX;
    qreal m_startY;
    qreal m_actualX;
    qreal m_actualY;

    qreal m_minimumRotation;
    qreal m_threshold;
    int m_directionTimeout;
    bool m_enabled;

    QTimer timer;
};

#endif
