/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include "sensormovement.h"

SensorMovement::SensorMovement(QObject *parent)
    : QObject(parent), m_direction(Global::DirectionStop), m_startX(0),
      m_startY(0), m_minimumRotation(0),
      m_threshold(0), m_directionTimeout(0), m_enabled(false)
{
    m_sensor.addFilter(this);
    m_sensor.start();

    setEnabled(true);

    timer.setSingleShot(true);

    connect(&timer, SIGNAL(timeout()), this, SLOT(notifyDirectionChanged()));
}

void SensorMovement::setEnabled(bool enabled)
{
    if (!m_sensor.isActive())
        return;

    m_enabled = enabled;

    emit enabledChanged();
}

void SensorMovement::setMinimumRotation(qreal minimumRotation)
{
    m_minimumRotation = minimumRotation;

    emit minimumRotationChanged();
}

void SensorMovement::setThreshold(qreal threshold)
{
    m_threshold = threshold;

    emit thresholdChanged();
}

void SensorMovement::setDirectionTimeout(int directionTimeout)
{
    m_directionTimeout = directionTimeout;

    emit directionTimeoutChanged();
}

void SensorMovement::resetStartPosition()
{
    QAccelerometerReading *reading = m_sensor.reading();

    if (!reading)
        return;

    m_startX = reading->x();
    m_startY = reading->y();
}

bool SensorMovement::filter(QAccelerometerReading *reading)
{
    if (!m_enabled)
        return true;

    Global::Direction nextDirection;
    // XXX: Axis are inverted on N900, thanks to the fact that
    // is a landscape device by definition.
    if (Global::environment() == Global::Maemo)
        nextDirection = calculateDirectionMaemo5(reading->x(), reading->y());
    else
        nextDirection = calculateDirection(reading->x(), reading->y());

    if (nextDirection != m_direction) {
        m_direction = nextDirection;

        timer.stop();
        timer.start(m_directionTimeout);
    }

    return true;
}

Global::Direction SensorMovement::calculateDirection(qreal x, qreal y)
{
    m_actualY = y - m_startY;
    m_actualX = x - m_startX;

    if (qAbs(m_actualX) > qAbs(m_actualY)) {
        if (m_actualX > 0) {
            if (m_actualX > m_minimumRotation + m_threshold)
                return Global::DirectionDown;
            else if (m_actualX < m_minimumRotation - m_threshold && m_actualX > m_threshold)
                return Global::DirectionStop;
        } else {
            if (m_actualX < -m_minimumRotation - m_threshold)
                return Global::DirectionUp;
            else if (m_actualX > -m_minimumRotation + m_threshold && m_actualX < -m_threshold)
                return Global::DirectionStop;
        }
    } else if (m_actualY > 0) {
        if (m_actualY > m_minimumRotation + m_threshold)
            return Global::DirectionRight;
        else if (m_actualY < m_minimumRotation - m_threshold && m_actualY > m_threshold)
            return Global::DirectionStop;
    } else {
        if (m_actualY < -m_minimumRotation - m_threshold)
            return Global::DirectionLeft;
        else if (m_actualY > -m_minimumRotation + m_threshold && m_actualY < -m_threshold)
            return Global::DirectionStop;
    }

    return m_direction;
}

Global::Direction SensorMovement::calculateDirectionMaemo5(qreal x, qreal y)
{
    m_actualY = y - m_startY;
    m_actualX = x - m_startX;

    if (qAbs(m_actualX) > qAbs(m_actualY)) {
        if (m_actualX > 0) {
            if (m_actualX > m_minimumRotation + m_threshold)
                return Global::DirectionLeft;
            else if (m_actualX < m_minimumRotation - m_threshold && m_actualX > m_threshold)
                return Global::DirectionStop;
        } else {
            if (m_actualX < -m_minimumRotation - m_threshold)
                return Global::DirectionRight;
            else if (m_actualX > -m_minimumRotation + m_threshold && m_actualX < -m_threshold)
                return Global::DirectionStop;
        }
    } else if (m_actualY > 0) {
        if (m_actualY > m_minimumRotation + m_threshold)
            return Global::DirectionDown;
        else if (m_actualY < m_minimumRotation - m_threshold && m_actualY > m_threshold)
            return Global::DirectionStop;
    } else {
        if (m_actualY < -m_minimumRotation - m_threshold)
            return Global::DirectionUp;
        else if (m_actualY > -m_minimumRotation + m_threshold && m_actualY < -m_threshold)
            return Global::DirectionStop;
    }

    return m_direction;
}

void SensorMovement::notifyDirectionChanged()
{
    emit directionChanged(m_direction);
}
