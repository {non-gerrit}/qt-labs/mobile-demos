/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef BLUETOOTHCONNECTION_H
#define BLUETOOTHCONNECTION_H

#include "networkconnection.h"

#include <QBluetoothSocket>
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtNetwork/QHostAddress>

QTM_USE_NAMESPACE

class BluetoothConnection : public NetworkConnection
{
    Q_OBJECT

public:
    BluetoothConnection(QObject *parent = 0, QBluetoothSocket *socket = 0);
    ~BluetoothConnection();

    void setId(const quint16 &newId);
    quint16 id() const;

    void send(NetworkMessage message);

    void connectToHost(const QString &address, const quint16 &port);
    void disconnectFromHost();

    QAbstractSocket::SocketState state() const;
    QAbstractSocket::SocketError error() const;
    QString errorString() const;

    void close();

signals:
    void connected();
    void disconnected();
    void error(QBluetoothSocket::SocketError socketError);

private slots:
    void processReadyRead();

private:
    QPointer<QBluetoothSocket> m_socket;
};

#endif
