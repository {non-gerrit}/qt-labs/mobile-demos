/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include "networkconnection.h"

#include <QtCore/QByteArray>

NetworkConnection::NetworkConnection(QObject *parent)
    : QObject(parent), m_id(0), m_message(NetworkMessage())
{
}

NetworkConnection::~NetworkConnection()
{
}

void NetworkConnection::setId(const quint16 &newId)
{
    m_id = newId;
}

quint16 NetworkConnection::id() const
{
    return m_id;
}

void NetworkConnection::processRead(QIODevice *m_socket)
{
    QByteArray buffer(m_socket->readAll());
    int counter = buffer.length();

    QDataStream dataStream(&buffer, QIODevice::ReadOnly);
    dataStream.setVersion(QDataStream::Qt_4_7);

    while (counter) {
        // Type
        if (m_message.type() == CW::NoMessageType) {
            if (counter < ((int) sizeof(quint16)))
                return;

            // Read the Message Type
            quint16 type;
            dataStream >> type;
            counter -= sizeof(quint16);

            m_message.setType((CW::MessageType) type);
        }

        // Content
        if (m_message.content() == 0) {
            if (counter < ((int) sizeof(quint16)))
                return;

            // Read the Message Content
            quint16 content;
            dataStream >> content;
            counter -= sizeof(quint16);

            m_message.setContent(content);
        }

        if (m_message.data().size() == 0) {
            if (counter < ((int) sizeof(quint32)))
                return;

            quint32 dataSize;
            dataStream >> dataSize;
            counter -= sizeof(quint32);

            if (dataSize == 0xFFFFFFFF) {
                m_message.setId(m_id);

                emit received(m_message);

                m_message = NetworkMessage();

                continue;
            } else {
                QByteArray data;
                data.resize(dataSize);
                m_message.setData(data);
            }
        }

        if (m_message.data().size() > 0) {
            if (counter < m_message.data().size())
                return;

            int dataSize = m_message.data().size();
            char *charData = new char[dataSize + 1];

            dataStream.readRawData(charData, dataSize);
            counter -= dataSize;
            charData[dataSize] = '\0';
            QByteArray data(charData);
            delete charData;

            m_message.setData(data);

            emit received(m_message);

            m_message = NetworkMessage();
        }
    }
}
