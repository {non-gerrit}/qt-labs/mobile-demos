/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#include "networkclient.h"

#include "bthandler.h"
#include "connectionfactory.h"
#include "network.h"
#include "networkconnection.h"
#include "udphandler.h"

#include <QtCore/QList>
#include <QtCore/QTimer>


NetworkClient::NetworkClient(const QString &connectivity, int discoveryTimeout,
                             int clientAttempts, QObject *parent)
    : QObject(parent), m_udpHandler(0), m_btHandler(0), m_connection(0),
      m_discoveryTimer(0), m_discoveryAttempts(0),
      m_discoveryTimeout(discoveryTimeout), m_discoveryClientAttempts(clientAttempts)
{
    if (connectivity == "lan") {
        m_udpHandler = new UdpHandler(this);
        m_udpHandler->setListenPort(m_lanUdpPort);

        connect(m_udpHandler, SIGNAL(received(QHostAddress, QByteArray, quint16, QByteArray)),
                this, SLOT(processUdp(QHostAddress, QByteArray,  quint16, QByteArray)));

        m_connection = ConnectionFactory::create(ConnectionFactory::Lan, this);
        connect(m_connection, SIGNAL(error(QAbstractSocket::SocketError)),
                this, SLOT(tcpSocketError(QAbstractSocket::SocketError)));

    } else if (connectivity == "bluetooth") {
        m_btHandler = new BtHandler(this);
        connect(m_btHandler, SIGNAL(discovered(const BluetoothDevice &)),
                this, SIGNAL(bluetoothDeviceDiscovered(const BluetoothDevice &)));

        m_connection = ConnectionFactory::create(ConnectionFactory::Bluetooth, this);
        connect(m_connection, SIGNAL(error(QBluetoothSocket::SocketError)),
                this, SLOT(bluetoothSocketError(QBluetoothSocket::SocketError)));
    }

    // Timer for broadcasting the message
    m_discoveryTimer = new QTimer(this);
    // TODO: rename this settings name to DiscoveryTimeout
    m_discoveryTimer->setInterval(m_discoveryTimeout);
    connect(m_discoveryTimer, SIGNAL(timeout()),
            this, SLOT(sendDiscovery()));

    connect(m_connection, SIGNAL(connected()),
            this, SIGNAL(connected()));
    connect(m_connection, SIGNAL(disconnected()),
            this, SIGNAL(disconnected()));
    connect(m_connection, SIGNAL(received(const NetworkMessage &)),
            this, SIGNAL(messageReceived(const NetworkMessage &)));
}

void NetworkClient::setLanPorts(int udpPort, int tcpPort)
{
    m_lanUdpPort = udpPort;
    m_lanTcpPort = tcpPort;
}

void NetworkClient::send(const NetworkMessage &message)
{
    m_connection->send(message);
}

void NetworkClient::startDiscovery()
{
    if (m_udpHandler) {
        // FIXME: Do not enable UdpHandler if it's already enabled
        m_udpHandler->setListenPort(m_lanUdpPort + 1);
        m_udpHandler->enable();

        m_discoveryAttempts = 0;
        m_discoveryTimer->start();
    } else {
        m_btHandler->enable();
    }
}

void NetworkClient::stopDiscovery()
{
    if (m_udpHandler)
        m_discoveryTimer->stop();

    if (m_btHandler)
        m_btHandler->disable();
}

void NetworkClient::connectToBluetoothServer(const QString &address)
{
    m_connection->connectToHost(address, 14);
}

void NetworkClient::processUdp(QHostAddress senderAddress, QByteArray content,
                               quint16 senderListenPort, QByteArray timestamp)
{
    Q_UNUSED(timestamp)

    quint16 contentData = content.toUShort();

    if (contentData == CW::UdpContentAckDiscovery)
        processAckDiscovery(senderAddress, senderListenPort);
}

void NetworkClient::processAckDiscovery(QHostAddress senderAddress, quint16 senderListenPort)
{
    Q_UNUSED(senderListenPort)

    stopDiscovery();

    if (m_connection->state() == QAbstractSocket::UnconnectedState) {
        // Estabilish a connection to the server
        m_connection->connectToHost(senderAddress.toString(), m_lanTcpPort);
    } else {
        qWarning("TCP Connection already open");
    }
}

void NetworkClient::sendDiscovery()
{
    if (m_discoveryAttempts < m_discoveryClientAttempts) {
        m_discoveryAttempts++;

        if (m_udpHandler) {
            m_udpHandler->sendDatagram(QHostAddress::Broadcast, m_lanUdpPort,
                                       CW::UdpSenderClient, CW::UdpContentDiscovery,
                                       m_udpHandler->listenPort());
        } else {
            // TODO: Notify upper layers about discovered bt
        }
    } else {
        stopDiscovery();

        if (m_udpHandler) {
            m_udpHandler->disable();
        } else {
            // TODO: Bluetooth stuff
        }

        emit serverDiscoveryTimeout();
    }
}

void NetworkClient::tcpSocketError(QAbstractSocket::SocketError error)
{
    qWarning("TCP Socket error [ %d ]: %s", error,
             m_connection->errorString().toUtf8().constData());

    emit socketError(error);
}

void NetworkClient::bluetoothSocketError(QBluetoothSocket::SocketError error)
{
    emit socketError(static_cast<QAbstractSocket::SocketError>(error));
}
