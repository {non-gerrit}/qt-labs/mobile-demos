/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include <QtCore/QDataStream>

#include "networkmessage.h"

NetworkMessage::NetworkMessage()
    : m_id(0), m_type(CW::NoMessageType),
      m_content(0), m_data(QByteArray())
{
}

NetworkMessage::NetworkMessage(CW::MessageType type, quint16 content)
    : m_id(0), m_type(type),
      m_content(content), m_data(QByteArray())
{
}

void NetworkMessage::setId(const quint16 &newId)
{
    m_id = newId;
}

quint16 NetworkMessage::id() const
{
    return m_id;
}

void NetworkMessage::setType(const CW::MessageType &newType)
{
    m_type = newType;
}

CW::MessageType NetworkMessage::type() const
{
    return m_type;
}

void NetworkMessage::setContent(const quint16 &newContent)
{
    m_content = newContent;
}

quint16 NetworkMessage::content() const
{
    return m_content;
}

void NetworkMessage::setData(const QByteArray &newData)
{
    m_data = newData;
}

QByteArray NetworkMessage::data() const
{
    return m_data;
}

QByteArray NetworkMessage::byteArray() const
{
    quint16 field;
    QByteArray byteArray;

    QDataStream stream(&byteArray, QIODevice::WriteOnly);
    stream.setVersion(QDataStream::Qt_4_0);

    // Type
    field = m_type;
    stream << field;

    // Content
    stream << m_content;

    // Data (optional)
    stream << m_data;

    return byteArray;
}
