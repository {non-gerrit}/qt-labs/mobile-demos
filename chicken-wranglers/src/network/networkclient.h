/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef NETWORKCLIENT_H
#define NETWORKCLIENT_H

#include "bluetoothdevice.h"
#include "networkmessage.h"

#include <QBluetoothSocket>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QHostAddress>
#include <QtCore/QByteArray>
#include <QtCore/QObject>

class BtHandler;
class NetworkConnection;
class QTimer;
class UdpHandler;

QTM_USE_NAMESPACE

class NetworkClient : public QObject
{
    Q_OBJECT

public:
    NetworkClient(const QString &connectivity = "lan", int discoveryTimeout = 0,
                  int clientAttempts = 0, QObject *parent = 0);

    void send(const NetworkMessage &message);
    void setLanPorts(int udpPort, int tcpPort);

public slots:
    void startDiscovery();
    void stopDiscovery();
    void connectToBluetoothServer(const QString &address);

signals:
    void connected();
    void disconnected();
    void socketError(QAbstractSocket::SocketError socketError);
    void messageReceived(const NetworkMessage &message);
    void serverDiscoveryTimeout();
    void bluetoothDeviceDiscovered(const BluetoothDevice &device);

private slots:
    void processUdp(QHostAddress senderAddress, QByteArray content,
                    quint16 senderListenPort, QByteArray timestamp);
    void sendDiscovery();
    void tcpSocketError(QAbstractSocket::SocketError error);
    void bluetoothSocketError(QBluetoothSocket::SocketError error);

private:
    void processAckDiscovery(QHostAddress senderAddress, quint16 senderListenPort);

    UdpHandler *m_udpHandler;
    BtHandler *m_btHandler;
    NetworkConnection *m_connection;
    QTimer *m_discoveryTimer;
    quint16 m_discoveryAttempts;
    int m_discoveryTimeout;
    int m_lanUdpPort;
    int m_lanTcpPort;
    int m_discoveryClientAttempts;
};

#endif
