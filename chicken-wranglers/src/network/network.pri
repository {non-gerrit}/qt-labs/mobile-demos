QT += network

maemo5 {
    CONFIG += mobility12
} else {
    CONFIG += mobility
}
MOBILITY += connectivity

PATH = $$PWD

HEADERS += \
    $$PATH/network.h \
    $$PATH/networkclient.h \
    $$PATH/networkserver.h \
    $$PATH/udphandler.h \
    $$PATH/networkmessage.h \
    $$PATH/networkconnection.h \
    $$PATH/bluetoothconnection.h \
    $$PATH/tcpconnection.h \
    $$PATH/connectionfactory.h \
    $$PATH/lanhandler.h \
    $$PATH/bluetoothhandler.h \
    $$PATH/bthandler.h

SOURCES += \
    $$PATH/networkclient.cpp \
    $$PATH/networkserver.cpp \
    $$PATH/udphandler.cpp \
    $$PATH/networkmessage.cpp \
    $$PATH/networkconnection.cpp \
    $$PATH/bluetoothconnection.cpp \
    $$PATH/tcpconnection.cpp \
    $$PATH/connectionfactory.cpp \
    $$PATH/lanhandler.cpp \
    $$PATH/bluetoothhandler.cpp \
    $$PATH/bthandler.cpp
