/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include <QtCore/QTime>
#include <QtNetwork/QUdpSocket>

#include "network.h"
#include "settings.h"

#include "udphandler.h"

UdpHandler::UdpHandler(QObject *parent)
    : QObject(parent), m_listenSenderType(CW::UdpSenderServer), m_listenPort(0)
{
    m_socket = new QUdpSocket(this);

    connect(m_socket, SIGNAL(readyRead()), this, SLOT(processIncomingDatagrams()));
}

UdpHandler::~UdpHandler()
{
    disable();
}

void UdpHandler::enable()
{
    for (quint16 i = 0; i < 100; i++) {
        m_listenPort += i;

        if (m_socket->bind(QHostAddress::Any, m_listenPort,
                           QUdpSocket::DontShareAddress)) {
            qWarning("UDP Socket bound to port %d", m_listenPort);

            break;
        }
    }
}

void UdpHandler::disable()
{
    m_socket->close();
}

void UdpHandler::setListenSenderType(const CW::UdpSenderType &type)
{
    m_listenSenderType = type;
}

CW::UdpSenderType UdpHandler::listenSenderType() const
{
    return m_listenSenderType;
}

void UdpHandler::setListenPort(const quint16 &port)
{
    if (m_listenPort != port)
        m_listenPort = port;
}

quint16 UdpHandler::listenPort() const
{
    return m_listenPort;
}

void UdpHandler::processIncomingDatagrams()
{
    QByteArray datagram;
    QHostAddress senderAddress;
    quint16 senderPort;

    while (m_socket->hasPendingDatagrams()) {
        datagram.resize(m_socket->pendingDatagramSize());

        if (m_socket->readDatagram(datagram.data(), datagram.size(),
                                   &senderAddress, &senderPort) == -1) {
            qWarning("UDP Error: Unable to read datagram.");
        } else {
            parseDatagram(senderAddress, senderPort, datagram);
        }

        datagram.clear();
    }
}

void UdpHandler::parseDatagram(QHostAddress address, quint16 port, QByteArray datagram)
{
    QList<QByteArray> fields = datagram.split(CW::udpMessageSeparator);
    QTime time = QTime::currentTime();

    // Message size correct (=4)
    if (fields.size() == 4) {
        // Message type received (='type specified')
        if (fields.at(0).toUShort() & m_listenSenderType) {
            qWarning("UDP Received:\t\"%s\"", datagram.constData());

            emit received(address, fields.at(1), port, fields.at(2));
        } else {
            qWarning("UDP Invalid sender type:\"%s\"", datagram.constData());
        }
    } else {
        qWarning("UDP Invalid message size:\"%s\"", datagram.constData());
    }
}

void UdpHandler::sendDatagram(QHostAddress address, quint16 port, CW::UdpSenderType senderType,
                              CW::UdpContentType content, quint16 senderListenPort)
{
    QTime time;
    QByteArray datagram;

    // Message format:
    // "client/content/senderListenPort/11:22:33.444"

    // Timestamp
    time = QTime::currentTime();

    // Message type: Client or Server
    datagram = QByteArray::number(senderType);

    // Message content: content
    datagram += CW::udpMessageSeparator + QByteArray::number((quint16) content);

    // Message content: sender listen port
    datagram += CW::udpMessageSeparator + QByteArray::number((quint16) senderListenPort);

    // Message timestamp: hours:minutes:seconds.miliseconds
    datagram += CW::udpMessageSeparator + time.toString("hh:mm:ss.zzz").toUtf8();

    m_socket->writeDatagram(datagram.data(), datagram.size(), address, port);
}
