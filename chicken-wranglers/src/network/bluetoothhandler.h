/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef BLUETOOTHHANDLER_H
#define BLUETOOTHHANDLER_H

#include "networkmessage.h"

#include <QBluetoothServiceInfo>
#include <QBluetoothSocket>
#include <QRfcommServer>
#include <QtCore/QHash>

class NetworkConnection;

QTM_USE_NAMESPACE

class BluetoothHandler : public QRfcommServer
{
    Q_OBJECT

    Q_PROPERTY(quint16 listenPort READ listenPort WRITE setListenPort NOTIFY listenPortChanged)

public:
    BluetoothHandler(QObject *parent = 0, int maxConnections = 4);
    ~BluetoothHandler();

    void start();
    void stop();

    void setListenPort(const quint16 &port);
    quint16 listenPort() const;

    void send(const NetworkMessage &message);
    void closeAll();

signals:
    void peerConnected(NetworkConnection *connection);
    void peerDisconnected(NetworkConnection *connection);
    void peerError(QAbstractSocket::SocketError socketError, quint16 id);
    void received(const NetworkMessage &message);
    void started();
    void listenPortChanged();
    void startError();

private slots:
    void newClientConnection();
    void peerDisconnected();
    void socketError(QBluetoothSocket::SocketError socketError);

protected:
    void registerService();

private:
    QHash<quint16, NetworkConnection *> m_connections;
    QBluetoothServiceInfo m_serviceInfo;
    quint16 m_listenPort;
    int m_maxConnections;
};

#endif
