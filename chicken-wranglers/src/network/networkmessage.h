/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef NETWORKMESSAGE_H
#define NETWORKMESSAGE_H

#include <QtCore/QByteArray>

namespace CW
{
    enum MessageType {
        NoMessageType = 1000,
        PlayerControlMessageType,
        PlayerInfoMessageType,
        CharacterMessageType,
        MatchMessageType
    };

    enum PlayerControl {
        PlayerControlStop = 2000,
        PlayerControlUp,
        PlayerControlDown,
        PlayerControlLeft,
        PlayerControlRight,
        PlayerControlLaserUp,
        PlayerControlLaserDown,
        PlayerControlLaserLeft,
        PlayerControlLaserRight
    };

    enum PlayerInfo {
        PlayerInfoScore = 3000,
        PlayerInfoLeader
    };

    enum Character {
        CharacterSelected = 4000,
        CharacterReleased,
        CharacterAvailableList,
        CharacterIsAvailable
    };

    enum Match {
        MatchReady = 5000,
        MatchFinished
    };
}

class NetworkMessage
{
public:
    NetworkMessage();
    NetworkMessage(CW::MessageType type, quint16 content);

    void setId(const quint16 &newId);
    quint16 id() const;

    void setType(const CW::MessageType &newType);
    CW::MessageType type() const;

    void setContent(const quint16 &newContent);
    quint16 content() const;

    void setData(const QByteArray &newData);
    QByteArray data() const;

    QByteArray byteArray() const;

private:
    quint16 m_id;
    CW::MessageType m_type;
    quint16 m_content;
    QByteArray m_data;
};

#endif
