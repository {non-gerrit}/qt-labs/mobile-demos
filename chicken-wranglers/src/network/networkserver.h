/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef NETWORKSERVER_H
#define NETWORKSERVER_H

#include "networkmessage.h"

#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtNetwork/QHostAddress>

class BluetoothHandler;
class LanHandler;
class NetworkConnection;
class QTimer;
class UdpHandler;

class NetworkServer : public QObject
{
    Q_OBJECT

    Q_ENUMS(ServerError)

public:
    NetworkServer(const QString &connectivity = "lan", int hostAttempts = 0, int discoveryTimeout = 0, QObject *parent = 0);
    ~NetworkServer();

    void send(const NetworkMessage &message);
    void setLanPorts(int udpPort, int tcpPort);

    enum ServerError {
        AnotherServerRunningError,
        UnknownError
    };

    void createConnectionHandler(const QString &connectivity);

    void closeAll();

public slots:
    void start();
    void stop();

signals:
    void peerConnected(NetworkConnection *connection);
    void peerDisconnected(NetworkConnection *connection);
    void peerError(QAbstractSocket::SocketError socketError, quint16 id);
    void messageReceived(const NetworkMessage &message);
    void error(NetworkServer::ServerError error);
    void started();

private slots:
    void processUdp(QHostAddress senderAddress, QByteArray content,
                    quint16 senderListenPort, QByteArray timestamp);
    void sendDiscovery();

    void startError();

private:
    void startDiscovery();
    void stopDiscovery();
    void processDiscovery(QHostAddress senderAddress, quint16 senderListenPort);
    void processAckDiscovery(QHostAddress senderAddress, quint16 senderListenPort);

    UdpHandler *m_udpHandler;
    LanHandler *m_lanHandler;
    BluetoothHandler *m_blueHandler;
    QTimer *m_discoveryTimer;
    quint16 m_discoveryAttempts;
    bool m_lookingForServers;
    int m_discoveryTimeout;
    int m_lanUdpPort;
    int m_lanTcpPort;
    int m_discoveryHostAttempts;
    bool m_serverBluetooth;

};

#endif
