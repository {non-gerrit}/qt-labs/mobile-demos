/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include <QtCore/QByteArray>
#include <QtNetwork/QHostAddress>

#include "tcpconnection.h"

TcpConnection::TcpConnection(QObject *parent, QTcpSocket *socket)
    : NetworkConnection(parent), m_socket(socket)
{
    if (m_socket == 0)
        m_socket = new QTcpSocket(this);

    connect(m_socket, SIGNAL(connected()),
            this, SIGNAL(connected()));
    connect(m_socket, SIGNAL(disconnected()),
            this, SIGNAL(disconnected()));
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SIGNAL(error(QAbstractSocket::SocketError)));
    connect(m_socket, SIGNAL(readyRead()),
            this, SLOT(processReadyRead()));
}

TcpConnection::~TcpConnection()
{
    if (m_socket)
        m_socket->close();
}

void TcpConnection::setId(const quint16 &newId)
{
    m_id = newId;
}

quint16 TcpConnection::id() const
{
    return m_id;
}

void TcpConnection::send(NetworkMessage message)
{
    if (m_socket->write(message.byteArray()) == -1)
        qWarning("TCP Socket write error: %s", m_socket->errorString().toUtf8().constData());
}

void TcpConnection::connectToHost(const QString &address, const quint16 &port)
{
    m_socket->connectToHost(QHostAddress(address), port);
}

QAbstractSocket::SocketState TcpConnection::state() const
{
    return m_socket->state();
}

QAbstractSocket::SocketError TcpConnection::error() const
{
    return m_socket->error();
}

QString TcpConnection::errorString() const
{
    return m_socket->errorString();
}

void TcpConnection::processReadyRead()
{
    processRead(m_socket);
}

void TcpConnection::close()
{
    m_socket.data()->close();
}
