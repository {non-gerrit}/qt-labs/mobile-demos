/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#include "global.h"

// TODO: conditional compilation module instead of ifdef's
#ifdef Q_OS_SYMBIAN
#include <eikenv.h>
#include <coemain.h>
#include <aknappui.h>
#endif

#include <QBluetoothLocalDevice>
#include <qmobilityglobal.h>
#include <QtCore/QStringList>
#include <QtGlobal>

QTM_USE_NAMESPACE

QFileInfo Global::rootDirectory()
{
#ifdef Q_OS_SYMBIAN
    return QFileInfo("c:\\data\\chicken-wranglers\\");
#endif
    QStringList pathList(QString(XSTR(DATA_PATH)).split(":", QString::SkipEmptyParts));

    foreach (QFileInfo path, pathList)
        if (path.exists() && path.isDir())
            return path;

    return QFileInfo();
}

QString Global::directionString(Global::Direction direction)
{
    switch (direction) {
    case Global::DirectionUp:
        return QString("Up");
    case Global::DirectionDown:
        return QString("Down");
    case Global::DirectionRight:
        return QString("Right");
    case Global::DirectionLeft:
        return QString("Left");
    default:
        return QString("Stop");
    }
}

void Global::setScreenOrientation(const ScreenOrientation &orientation)
{
#ifdef Q_OS_SYMBIAN
    CAknAppUi *aknAppUi = dynamic_cast<CAknAppUi *>(CEikonEnv::Static()->AppUi());
    if (!aknAppUi)
        return;

    switch (orientation) {
    case Landscape:
        aknAppUi->SetOrientationL(CAknAppUi::EAppUiOrientationLandscape);
        break;
    case Portrait:
        aknAppUi->SetOrientationL(CAknAppUi::EAppUiOrientationPortrait);
        break;
    case Auto:
        aknAppUi->SetOrientationL(CAknAppUi::EAppUiOrientationAutomatic);
    }
#endif
}

void Global::setPowerOn()
{
    QList<QBluetoothHostInfo> devices(QBluetoothLocalDevice::allDevices());
    foreach(const QBluetoothHostInfo &i, devices) {
        QBluetoothLocalDevice hci(i.getAddress());
        hci.setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        hci.powerOn();
    }
}

Global::Environment Global::environment()
{
    // Set default environment to Linux
    Environment result = Linux;
#if defined(Q_WS_MAEMO_5)
    result = Maemo;
#elif defined(Q_WS_X11)
    result = Linux;
#elif defined(Q_WS_S60)
    result = Symbian;
#elif defined(Q_WS_WIN)
    result = Windows;
#elif defined(Q_WS_MAC)
    result = Mac;
#endif

    return result;
}
