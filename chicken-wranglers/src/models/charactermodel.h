/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef CHARACTERMODEL_H
#define CHARACTERMODEL_H

#include <QtCore/QAbstractListModel>
#include <QtCore/QFileInfo>
#include <QtCore/QUrl>

class CharacterModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString path READ path NOTIFY pathChanged)
    Q_PROPERTY(bool isAvailable READ isAvailable NOTIFY isAvailableChanged)

public:
    CharacterModel(QObject *parent = 0);
    CharacterModel(const QFileInfo &characterPath, QObject *parent = 0);
    CharacterModel(const QString &characterName, QObject *parent = 0);

    bool isValid() const;

    QString path() const;
    QString name() const { return m_characterName; }

    bool isAvailable() const { return m_available; }
    void setAvailable(bool available);

    static QString basePath();

signals:
    void pathChanged(const QString &path);
    void isAvailableChanged(bool available);

private:
    QString m_characterName;
    QUrl m_path;
    bool m_available;
};

class CharacterListModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int selectedIndex READ selectedIndex WRITE setSelectedIndex NOTIFY selectedIndexChanged)

public:
    enum CharacterRole {
        PathRole = Qt::UserRole + 1,
        IsAvailableRole
    };

    explicit CharacterListModel(QObject *parent = 0);

    bool load();
    void addCharacter(CharacterModel *character);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    int selectedIndex() const { return m_selectedIndex; }
    void setSelectedIndex(int selectedIndex);

    static QStringList characterNameList();

    Q_INVOKABLE CharacterModel *get(int index) const;
    Q_INVOKABLE CharacterModel *selected() const;

signals:
    void selectedIndexChanged();

private slots:
    void onIsAvailableChanged(bool available);

private:
    QList<CharacterModel *> m_characters;
    int m_selectedIndex;
};

#endif
