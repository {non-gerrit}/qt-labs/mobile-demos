QT += declarative

PATH = $$PWD

HEADERS += \
    $$PATH/charactermodel.h \
    $$PATH/playermodel.h \
    $$PATH/bluetoothdevice.h \
    $$PATH/connectivitylistmodel.h

SOURCES += \
    $$PATH/charactermodel.cpp \
    $$PATH/playermodel.cpp \
    $$PATH/bluetoothdevice.cpp \
    $$PATH/connectivitylistmodel.cpp
