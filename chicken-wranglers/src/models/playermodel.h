/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef PLAYERMODEL_H
#define PLAYERMODEL_H

#include "global.h"

#include "networkmessage.h"

#include <QtCore/QAbstractListModel>
#include <QtCore/QStringList>
#include <QtNetwork/QHostAddress>

class CharacterModel;
class NetworkConnection;

class PlayerModel : public QObject
{
    Q_OBJECT
    Q_ENUMS(Status)

    Q_PROPERTY(QString characterPath READ characterPath NOTIFY characterPathChanged)
    Q_PROPERTY(QString direction READ directionString NOTIFY directionChanged)
    Q_PROPERTY(int score READ score NOTIFY scoreChanged)

public:
    PlayerModel(QObject *parent = 0);
    PlayerModel(NetworkConnection *connection, QObject *parent = 0);
    ~PlayerModel();

    enum Status {
        NoCharacter,
        Ready,
        Playing
    };

    QString characterPath() const;
    Status status() const { return m_status; }
    NetworkConnection *connection() const { return m_connection; }

    bool setCharacter(const QString &characterName);
    bool unsetCharacter(const QString &characterName);

    void setLeader(bool isLeader);
    bool isLeader() const { return m_isLeader; }

    void setScore(int score);
    int score() const { return m_score; }

    void reset();

    QString directionString() const { return Global::directionString(m_direction); }
    Global::Direction direction() const { return m_direction; }
    void setDirection(Global::Direction direction);

    void notifyCharacterAvailableList(const QStringList &availableCharacters);
    void notifyCharacterIsAvailable(const QString &characterName, bool available);
    void notifyPlayerLeader(bool isLeader);

    QString characterName() const;

signals:
    void characterPathChanged();
    void directionChanged();
    void scoreChanged();
    void characterSelected(const QString &characterName);
    void characterReleased(const QString &characterName);
    void laserToggleRequested(Global::LaserDirection laserDirection);

private slots:
    void onNetworkMessageReceived(const NetworkMessage &message);

private:
    void handleCharacterMessage(const NetworkMessage &message);
    void handlePlayerControlMessage(const NetworkMessage &message);

    void requestLaserToggle(Global::LaserDirection laserDirection);

    CharacterModel *m_character;
    NetworkConnection *m_connection;
    Status m_status;
    Global::Direction m_direction;
    bool m_isLeader;
    int m_score;
};

class PlayerListModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool playersReady READ playersReady NOTIFY playersReadyChanged)

public:
    enum PlayerRole {
        StatusRole = Qt::UserRole + 1,
        CharacterPathRole
    };

    explicit PlayerListModel(QObject *parent = 0);

    void newPlayer(NetworkConnection *connection);
    void removePlayer(NetworkConnection *connection);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    QStringList availableCharacters() const { return m_availableCharacters; }
    void reset();

    void notifyMatchReady();
    void notifyMatchFinished();

    // Used by QML views
    Q_INVOKABLE PlayerModel *get(int index) const;
    Q_INVOKABLE int count() const { return rowCount(); }

signals:
    void playersReadyChanged();

private slots:
    void onCharacterPathChanged();
    void onCharacterSelected(const QString &characterName);
    void onCharacterReleased(const QString &characterName);

private:
    bool playersReady() const { return m_playersReady; }
    void setPlayersReady(bool playersReady);
    void checkPlayersReady();

    QList<PlayerModel *> m_players;
    QStringList m_availableCharacters;
    bool m_playersReady;
};

#endif
