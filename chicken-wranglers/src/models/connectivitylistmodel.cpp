/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#include "connectivitylistmodel.h"

#include "settings.h"

ConnectivityListModel::ConnectivityListModel(QObject *parent)
    : QAbstractListModel(parent), m_selectedIndex(-1)
{
    QHash<int, QByteArray> roles;

    roles[NameRole] = "name";
    roles[AddressRole] = "address";

    setRoleNames(roles);
}

void ConnectivityListModel::addDevice(const BluetoothDevice &device)
{
    for (int i = 0; i < m_devices.count(); i++) {
        if (m_devices[i] == device) {
            if ((m_devices[i].name() == m_devices[i].address())
                && device.name() != device.address()) {

                m_devices[i].setName(device.name());
                emit dataChanged(index(i), index(i));
            }

            return;
        }
    }

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_devices << device;
    endInsertRows();
}

int ConnectivityListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return m_devices.count();
}

QVariant ConnectivityListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > m_devices.count())
        return QVariant();

    const BluetoothDevice &device = m_devices[index.row()];

    if (role == NameRole)
        return device.name();
    else if (role == AddressRole)
        return device.address();

    return QVariant();
}

void ConnectivityListModel::setSelectedIndex(int selectedIndex)
{
    if (selectedIndex >= m_devices.count())
        return;

    m_selectedIndex = selectedIndex;

    emit selectedIndexChanged();

    emit selectedDevice(m_devices[m_selectedIndex].address());
}

void ConnectivityListModel::clear()
{
    if (!rowCount())
        return;

    beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
    m_devices.clear();
    endRemoveRows();
}

void ConnectivityListModel::storeLastHostDevice()
{
    if (m_selectedIndex == -1 || m_selectedIndex >= m_devices.count())
        return;

    BluetoothDevice device = m_devices[m_selectedIndex];
    Settings::setBluetoothHostAddress(device.address());
    Settings::setBluetoothHostName(device.name());
}

void ConnectivityListModel::loadLastHostDevice()
{
    BluetoothDevice device;

    QString address = Settings::bluetoothHostAddress();
    QString name = Settings::bluetoothHostName();

    if (address == "00:00:00:00:00:00" || address.isEmpty())
        return;

    device.setAddress(address);
    if (!name.isEmpty())
        device.setName(name);
    else
        device.setName(address);

    addDevice(device);

    m_selectedIndex = m_devices.indexOf(device);
    emit selectedIndexChanged();
}
