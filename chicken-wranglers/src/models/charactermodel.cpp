/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#include "charactermodel.h"

#include "global.h"

#include <QtCore/QDir>
#include <QtCore/QUrl>
#include <QtDeclarative>
#include <QtGui/QPixmap>

CharacterModel::CharacterModel(QObject *parent)
    : QObject(parent)
{
}

CharacterModel::CharacterModel(const QFileInfo &characterPath, QObject *parent)
    : QObject(parent), m_path(characterPath.absoluteFilePath()), m_available(true)
{
    // TODO: Maybe we need to check it for a valid directory path
    m_characterName = characterPath.baseName();
}

CharacterModel::CharacterModel(const QString &characterName, QObject *parent)
    : QObject(parent), m_characterName(characterName),
      m_path(QFileInfo(basePath() + characterName).absoluteFilePath())
{
}

QString CharacterModel::path() const
{
    return QUrl::fromLocalFile(m_path.toString()).toString();
}

QString CharacterModel::basePath()
{
    return Global::rootDirectory().absoluteFilePath() + "/characters/";
}

bool CharacterModel::isValid() const
{
    QString iconPath = m_path.toString(QUrl::RemoveScheme) + "/select_character.png";

    if (QPixmap(iconPath).isNull())
        return false;

    return true;
}

void CharacterModel::setAvailable(bool available)
{
    if (m_available == available)
        return;

    m_available = available;

    emit isAvailableChanged(available);
}

CharacterListModel::CharacterListModel(QObject *parent)
    : QAbstractListModel(parent), m_selectedIndex(-1)
{
    QHash<int, QByteArray> roles;

    roles[PathRole] = "path";
    roles[IsAvailableRole] = "isAvailable";

    setRoleNames(roles);

    qmlRegisterType<CharacterModel>("game.types", 1, 0, "CharacterModel");
}

bool CharacterListModel::load()
{
    QDir characterDir = QDir(CharacterModel::basePath());
    if (!characterDir.exists())
        return false;

    QFlags<QDir::Filter> flags = QDir::Dirs | QDir::NoDotAndDotDot;
    QFileInfoList entries = characterDir.entryInfoList(flags, QDir::Time);

    foreach(QFileInfo entry, entries) {
        CharacterModel *character = new CharacterModel(entry);

        if (character->isValid())
            addCharacter(character);
        else
            delete character;
    }

    return true;
}

void CharacterListModel::addCharacter(CharacterModel *character)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_characters << character;
    endInsertRows();

    connect(character, SIGNAL(isAvailableChanged(bool)),
            this, SLOT(onIsAvailableChanged(bool)));
}

int CharacterListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return m_characters.count();
}

QVariant CharacterListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_characters.count())
        return QVariant();

    CharacterModel *character = m_characters[index.row()];

    if (role == PathRole)
        return character->path();
    if (role == IsAvailableRole)
        return character->isAvailable();

    return QVariant();
}

void CharacterListModel::setSelectedIndex(int selectedIndex)
{
    if (selectedIndex >= m_characters.count())
        return;

    m_selectedIndex = selectedIndex;

    selectedIndexChanged();
}

CharacterModel *CharacterListModel::get(int index) const
{
    if (index < 0 || index >= m_characters.count())
        return 0;

    return m_characters.at(index);
}

CharacterModel *CharacterListModel::selected() const
{
    if (m_selectedIndex < 0 || m_selectedIndex >= m_characters.count())
        return 0;

    return m_characters.at(m_selectedIndex);
}

QStringList CharacterListModel::characterNameList()
{
    QDir characterDir = QDir(CharacterModel::basePath());
    if (!characterDir.exists())
        return QStringList();

    QFlags<QDir::Filter> flags = QDir::Dirs | QDir::NoDotAndDotDot;
    QFileInfoList entries = characterDir.entryInfoList(flags, QDir::Time);

    QStringList nameList;

    foreach(QFileInfo entry, entries)
        nameList.append(entry.baseName());

    return nameList;
}

void CharacterListModel::onIsAvailableChanged(bool available)
{
    Q_UNUSED(available)

    CharacterModel *model = qobject_cast<CharacterModel *>(sender());

    int characterIndex = m_characters.indexOf(model);

    dataChanged(index(characterIndex), index(characterIndex));
}
