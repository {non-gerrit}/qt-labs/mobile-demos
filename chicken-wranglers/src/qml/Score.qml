/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7
import "widgets"

import "match.js" as MatchLogic
import "common.js" as Common

Item {
    id: score

    property int numberOfPlayers: playerListModel.count()
    property int scoreBoxTick: width / (numberOfPlayers + 1)

    signal close

    anchors.fill: parent
    z: 10

    function sortScore(scoreBox1, scoreBox2) {
        return (scoreBox2.score - scoreBox1.score)
    }

    function createScore() {
        var scoreBoxList = []

        for (var i = 0; i < numberOfPlayers; i++) {
            var scoreBox
            scoreBox = MatchLogic.createScoreBox()
            if (scoreBox == undefined)
                continue

            var player = playerListModel.get(i)
            if (player == null)
                continue

            scoreBox.source = Common.getImagePath(player.characterPath, "score.png")
            scoreBox.score = player.score
            scoreBox.z = 15
            scoreBox.y = 0

            scoreBoxList.push(scoreBox)
        }

        scoreBoxList.sort(sortScore)

        for (var i = 0; i < scoreBoxList.length; i++)
            scoreBoxList[i].x = (scoreBoxTick * (i + 1)) - (scoreBoxList[i].width / 2)

        closeTimer.start()
    }

    function show() {
        opacity = 1
    }

    function hide() {
        opacity = 0
    }

    Behavior on opacity {
        NumberAnimation { duration: 500 }
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.8

        MouseArea {
            anchors.fill: parent
        }
    }

    Timer {
        id: closeTimer

        interval: 5000

        onTriggered: close()
    }

    onOpacityChanged: {
        if (opacity == 1) {
            createScore()
        }
    }
}
