function start() {
    game.mode = Game.HostMode

    startViewLoader.item.showStartServerDialog("Creating server")

    // gameHosts only exists after game.mode is set to Game.HostMode
    gameHost.statusChanged.connect(onStatusChanged)
    gameHost.startConnection()
}

function quit() {
    gameHost.statusChanged.disconnect(onStatusChanged)
    startViewLoader.item.hideStartServerDialog()

    gameHost.quit();
}

function onStatusChanged() {
    switch (gameHost.status) {

    case GameHost.Start:
        startViewLoader.item.hideStartServerDialog()
        hostViewLoader.source = "HostView.qml"
        break

    case GameHost.Error:
        startViewLoader.item.hideStartServerDialog()
        handleError(gameHost.error)
        break

    default:
        console.log("Unknown server status")
    }
}

function handleError(error) {
    switch (error) {

    case GameHost.AnotherServerRunningError:
        startViewLoader.item.showErrorDialog("Server is already running")
        break

    default:
        console.log("Unknown server error")
    }
}
