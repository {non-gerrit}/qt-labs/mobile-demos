/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

import game.types 1.0
import "common.js" as Common
import "widgets"

Screen {
    id: screen

    signal back
    signal characterSelected(int index)

    Image {
        id: background

        anchors.fill: parent
        source: {
            if (game.environment() == Global.Symbian)
               "qrc:/images/general/symbian/bg_selectcharacter.png"
            else
               "qrc:/images/general/bg_selectcharacter.png"
        }
    }

    Component {
        id: characterDelegate

        Item {
            width: 191
            height: 220

            scale: PathView.iconScale

            Image {
                id: characterIcon

                y: 0
                anchors.fill: parent
                anchors.horizontalCenter: parent.horizontalCenter
                smooth: true
                source: {
                    if (isAvailable)
                        Common.getImagePath(path, "select_character.png")
                    else
                        Common.getImagePath(path, "unavailable.png")
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    view.currentIndex = index
                }
            }
        }
    }

    Item {
        id: characterHighlight

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            bottom: view.bottom
        }

        Image {
            id: name

            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                topMargin: 60
            }

            smooth: true
            source: Common.getImagePath(characterListModel.get(view.currentIndex).path, "name.png")

            // XXX: When changing source property, the sourceSize.width
            // is not updating accordingly. So we are forcing a hardcoded
            // value for now.
            onSourceChanged: sourceSize.width = 200

            Behavior on opacity {
                NumberAnimation { duration: 100 }
            }
        }

        ImageButton {
            source: "qrc:/images/general/bt_previous.png"

            anchors.right: parent.horizontalCenter
            anchors.rightMargin: 80
            anchors.verticalCenter: name.verticalCenter

            onClicked: view.decrementCurrentIndex()
        }

        ImageButton {
            source: "qrc:/images/general/bt_next.png"

            anchors.left: parent.horizontalCenter
            anchors.leftMargin: 80
            anchors.verticalCenter: name.verticalCenter

            onClicked: view.incrementCurrentIndex()
        }
    }

    PathView {
        id: view

        anchors {
            right: parent.right
            left: parent.left
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: 20
        }

        height: 220
        model: characterListModel
        delegate: characterDelegate
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        pathItemCount: 5
        highlightMoveDuration: 200
        dragMargin: 500

        onCurrentIndexChanged: {
            name.source = Common.getImagePath(characterListModel.get(currentIndex).path, "name.png")
        }

        onMovementStarted: {
            name.opacity = 0
        }

        onMovementEnded: {
            name.source = Common.getImagePath(characterListModel.get(currentIndex).path, "name.png")
            name.opacity = 1
        }

        path: Path {
            startX: -40
            startY: 100

            PathAttribute { name: "iconScale"; value: 0.3 }

            PathLine {
                x: screen.width * 0.5
                y: 100
            }

            PathAttribute { name: "iconScale"; value: 1.0 }

            // 4
            PathLine {
                x: screen.width + 40
                y: 100
            }

            PathAttribute { name: "iconScale"; value: 0.3 }
        }
    }

    Button {
        id: buttonJoin

        text: "Join"
        width: 200
        enabled: characterListModel.get(view.currentIndex).isAvailable

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            margins: 20
        }

        onClicked: characterSelected(view.currentIndex)
    }

    ImageButton {
        id: buttonBack

        anchors {
            top: parent.top
            right: parent.right
            margins: 15
        }

        name: "back"
        onClicked: back();
    }
}
