/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7
import game.types 1.0

import "client.js" as Client
import "host.js" as Host
import "widgets"

Screen {
    id: screen

    signal hostSelected
    signal clientSelected
    signal clientDiscoveryCanceled

    function showStartServerDialog() {
        startServerDialog.show()
    }

    function hideStartServerDialog() {
        startServerDialog.hide()
    }

    function showStartClientDialog() {
        if (game.connectivity == "bluetooth") {
            connectivityDialog.model = connectivityListModel
            connectivityDialog.show()
        } else {
            startClientDialog.show()
        }
    }

    function hideStartClientDialog() {
        if (game.connectivity == "bluetooth") {
            gameClient.scanCanceled()
            connectivityDialog.hide()
        } else
            startClientDialog.hide()
    }

    function showErrorDialog(msg) {
        errorDialog.text = msg
        errorDialog.show()
    }

    function hideErrorDialog() {
        errorDialog.hide()
    }

    onHostSelected: Host.start()
    onClientSelected: Client.start()

    Image {
        id: background

        smooth: true
        anchors.fill: parent

        source: {
            if (game.environment() == Global.Symbian)
                "qrc:/images/general/symbian/bg_640x360.png"
            else if (game.environment() == Global.Maemo)
                "qrc:/images/general/bg_800x480.png"
            else
                "qrc:/images/general/bg_1280x768.png"
        }
    }

    FeatherFall {
        id: featherFall
        number: {
            if (game.environment() == Global.Symbian)
               2
            else if (game.environment() == Global.Maemo)
               3
            else
               5
        }
        fallWidth:{
            if (game.environment() == Global.Symbian)
               640
            else
               800
        }
        fallHeight:{
            if (game.environment() == Global.Symbian)
               360
            else
               768
        }

        anchors.fill: parent
    }

    Item {
        anchors {
            centerIn: parent
            horizontalCenterOffset: 90
            verticalCenterOffset: 120
            margins: 100
        }

        Button {
            id: hostGameButton

            anchors {
                right: parent.right
                bottom: parent.bottom
            }

            text: "Host game"
            width: 200
            onClicked: hostSelected()
        }

        Button {
            id: joinGameButton

            anchors {
                left: parent.left
                leftMargin: 10
                bottom: parent.bottom
            }

            text: "Join game"
            width: 200
            onClicked: clientSelected()
        }
    }

    ImageButton {
        anchors {
            top: parent.top
            left: parent.left
            margins: 15
        }

        name: "close"
        onClicked: Qt.quit()
    }

    ToggleButton {
        anchors {
            top: parent.top
            right: parent.right
            margins: 15
        }
    }

    MessageDialog {
        id: startClientDialog

        text: "Searching for server"
        buttonText: "Cancel"
        opacity: 0

        Loading {
            anchors.horizontalCenter: startClientDialog.box.horizontalCenter
            anchors.bottom: startClientDialog.box.bottom
            anchors.bottomMargin: 100
        }

        onButtonClicked: clientDiscoveryCanceled()
    }

    ConnectivityDialog {
        id: connectivityDialog

        opacity: 0
        onCancel: {
            connectivityDialog.hide()
            gameClient.scanCanceled()
        }
    }

    MessageDialog {
        id: startServerDialog

        text: "Creating server"
        opacity: 0

        Loading {
            anchors.horizontalCenter: startServerDialog.box.horizontalCenter
            anchors.bottom: startServerDialog.box.bottom
            anchors.bottomMargin: 100
        }
    }

    MessageDialog {
        id: errorDialog

        text: "error"
        buttonText: "Close"
        opacity: 0

        onButtonClicked: hideErrorDialog()
    }

    onOpacityChanged: {
        if (opacity == 1)
            featherFall.start()
        else if (opacity == 0)
            featherFall.stop()
    }
}
