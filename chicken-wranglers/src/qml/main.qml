/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7
import game.types 1.0

import "common.js" as Common
import "host.js" as Host
import "client.js" as Client
import "widgets"

Rectangle {
    id: main

    width: 800
    height: 480

    focus: true

    SystemPalette { id: activePalette }

    Loader {
        id: splashScreenLoader

        signal closeFinished
        signal timeout

        anchors.fill: parent

        onLoaded: splashScreenLoader.item.show()
        onTimeout: startViewLoader.source = "StartView.qml"
        onCloseFinished: {
            splashScreenLoader.source = ""
            game.startBackgroundSound()
        }
    }

    Loader {
        id: startViewLoader

        anchors.fill: parent

        onLoaded: Common.switchScreens(splashScreenLoader.item, startViewLoader.item)
    }

    Loader {
        id: hostViewLoader

        signal closeRequested
        signal closeFinished

        anchors.fill: parent

        onLoaded: {
            focus = true
            Common.switchScreens(startViewLoader.item, hostViewLoader.item)
        }

        onCloseRequested: Common.switchScreens(hostViewLoader.item, startViewLoader.item)
        onCloseFinished: {
            Host.quit()
            hostViewLoader.source = ""
        }
    }

    Loader {
        id: clientViewLoader

        signal closeRequested
        signal closeFinished

        anchors.fill: parent

        onLoaded: {
            focus = true
            Common.switchScreens(startViewLoader.item, clientViewLoader.item)
        }

        onCloseRequested: Common.switchScreens(clientViewLoader.item, startViewLoader.item)
        onCloseFinished: {
            Client.quit()
            clientViewLoader.source = ""
        }
    }

    Component.onCompleted: splashScreenLoader.source = "Splash.qml"
}
