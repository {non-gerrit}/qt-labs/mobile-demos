/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

import "match.js" as MatchLogic
import game.types 1.0

Image {
    id: being

    property string path: ""
    property int currentRow: 0
    property int currentColumn: 0
    property int walkingStep: 1
    property string orientation: "side_"
    property string movement: "stopped_"

    // Default values
    property int moveInterval: 500
    property int changeStepsInterval: 260

    scale: matchScreen.displayScale
    smooth: true

    source: {
        if (path == "")
            return ""

        path + "/" + orientation + movement + walkingStep + ".png"
    }

    state: "stopped"

    Timer {
        id: changeSteps;
        interval: changeStepsInterval
        running: (opacity == 1) && (matchController.status != Match.Paused)
        repeat: true
        onTriggered: {
            if (walkingStep == 1)
                walkingStep = 2
            else
                walkingStep = 1
        }
    }

    states {
        State {
            name: "Up"
            PropertyChanges {
                target: being
                orientation: "back_"
                movement: "walking_"
            }
        }

        State {
            name: "Down"
            PropertyChanges {
                target: being
                orientation: "front_"
                movement: "walking_"
            }
        }

        State {
            name: "Left"
            PropertyChanges {
                target: being
                orientation: "side_"
                movement: "walking_"
            }
        }

        State {
            name: "Right"
            PropertyChanges {
                target: being
                orientation: "side_"
                movement: "walking_"
            }
            PropertyChanges {
                target: rotation
                angle: 180
            }
        }

        State {
            name: "Stop"
            PropertyChanges {
                target: being
                movement: "stopped_"
                orientation: orientation
            }
            PropertyChanges {
                target: rotation
                angle: angle
            }
        }
    }

    transform {
        Rotation {
            id: rotation
            origin.x: being.width/2; origin.y: being.height/2
            axis.x: 0; axis.y: 1; axis.z: 0
            angle: 0
        }
    }
}
