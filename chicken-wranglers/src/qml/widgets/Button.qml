/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

BorderImage {
    id: button

    property int leftBorder: 18
    property int rightBorder: 18
    property int horizontalCenterOffset: 0
    property string sourceName: "bt"
    property string text: ""
    property bool blink: false
    property bool enabled: true
    property bool italic: false

    signal clicked

    source: {
        if (!enabled)
            return "qrc:/images/general/" + sourceName + "_disabled.png"

        if (mouseArea.pressed)
            return "qrc:/images/general/" + sourceName + "_pressed.png"
        else
            return "qrc:/images/general/" + sourceName + ".png"
    }

    smooth: true

    border {
        left: leftBorder
        right: rightBorder
    }

    width: buttonText.width + 40

    PropertyAnimation {
        id: blinkAnimation

        target: button
        properties: "opacity"
        loops: 2
        from: 0
        to: 1
        duration: 200

        onRunningChanged: {
            if (!running)
                button.clicked()
        }
    }

    Text {
        id: buttonText

        text: button.text
        smooth: true

        color: {
            if (button.enabled)
                "#77262d"
            else
                "#666666"
        }

        anchors {
            centerIn: parent
            verticalCenterOffset: -3
            horizontalCenterOffset: button.horizontalCenterOffset
        }

        font {
            family: "Nokia Sans"
            pixelSize: 26
            bold: true
            italic: button.italic
        }
    }

    MouseArea {
        id: mouseArea

        enabled: button.enabled
        anchors.fill: parent
        onClicked: {
            if (blink)
                blinkAnimation.running = true
            else
                button.clicked()
        }
    }
}
