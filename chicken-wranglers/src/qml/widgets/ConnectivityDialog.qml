/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7
import game.types 1.0

BaseDialog {
    id: dialog

    property string buttonText: "Use it"
    property string cancelText: "Cancel"
    property ConnectivityListModel model: ConnectivityListModel {}
    signal cancel()

    Connections {
        target: dialog
        onHideFinished: {
            title.text = "Scanning..."
            button.enabled = true
            quitButton.enabled = true
        }
    }

    ConfirmationDialog {
        id: quitDialog

        text: "Do you want to cancel scanning?"
        opacity: 0

        onRejected: {
            quitDialog.hide()
        }

        onAccepted: {
            quitDialog.hide()
            dialog.cancel()
        }

        z: 10
    }

    Text {
        id: title

        text: "Scanning..."
        style: Text.Raised
        styleColor: "white"
        color: "#4d908f"

        font {
            family: "Nokia Sans"
            bold: true
            pixelSize: 34
        }

        anchors.top: box.top
        anchors.topMargin: 60
        anchors.left: parent.left
        anchors.right: parent.right

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    ConnectivityListView {
        id: listView

        property real scrollSize: 200

        width: 400
        height: 200
        model: dialog.model
        anchors.top: title.bottom
        anchors.topMargin: 15
        anchors.bottom: box.bottom
        anchors.bottomMargin: 80
        anchors.horizontalCenter: parent.horizontalCenter

        onContentYChanged: {
            scrollbar.setValue((contentY / scrollSize) * 100)
        }

        onMovingChanged: {
            if (moving)
                scrollbar.opacity = 1;
            else
                scrollbar.opacity = 0;
        }
    }

    Scrollbar {
        id: scrollbar

        opacity: 0
        anchors.top: title.bottom
        anchors.bottom: box.bottom
        anchors.left: listView.right
        anchors.leftMargin: 20

        Behavior on opacity {
            NumberAnimation {
                duration: 500
                easing.type: Easing.OutCirc
            }
        }
    }

    Button {
        id: button

        text: dialog.buttonText
        anchors {
            verticalCenter: box.bottom
            verticalCenterOffset: -25
            horizontalCenter: box.horizontalCenter
            horizontalCenterOffset: 90
        }

        onClicked: {
            button.enabled = false
            quitButton.enabled = false
            title.text = "Connecting..."

            if (listView.currentIndex != -1)
                listView.model.selectedIndex = listView.currentIndex
        }
    }

    Button {
        id: quitButton
        text: dialog.cancelText
        anchors {
            verticalCenter: box.bottom
            verticalCenterOffset: -25
            horizontalCenter: box.horizontalCenter
            horizontalCenterOffset: -90
        }

        onClicked: {
            if (quitDialog.opacity == 0) {
                quitDialog.show()
            }
        }
    }
}

