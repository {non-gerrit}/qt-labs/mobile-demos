/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7
import game.types 1.0

ListView {
    id: listView

    Component {
        id: delegate

        Item {
            width: lineDivisor.width
            height: 43

            Image {
               id: lineDivisor

               source: "qrc:/images/general/list_line_divisor.png"

               anchors.top: parent.top
            }

            Text {
                text: name

                width: parent.width - 110

                style: Text.Raised
                styleColor: "white"

                color: "#4d908f"

                font {
                    family: "Nokia Sans"
                    bold: true
                    pixelSize: 21
                }

                elide: Text.ElideRight

                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 23

                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    var pos = mapToItem(listView.contentItem, mouseX, mouseY)
                    var selectedIndex = listView.indexAt(pos.x, pos.y)

                    if (selectedIndex == listView.currentIndex) {
                        listView.currentIndex = -1
                    } else {
                        listView.currentIndex = selectedIndex
                    }
                }
            }
        }
    }

    model: ConnectivityListModel {}
    delegate: delegate

    focus: false
    clip: true

    Component {
        id: highlightIcon

        Item {
            width: listView.width
            height: 43

            Image {
                source: "qrc:/images/general/list_selected_icon.png"

                anchors.left: parent.right
                anchors.leftMargin: -70
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: 1
            }
        }
    }

    currentIndex: model.selectedIndex

    highlight: highlightIcon
    highlightMoveDuration: 100

    snapMode: ListView.SnapToItem
}

