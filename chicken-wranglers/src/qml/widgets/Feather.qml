/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

import game.types 1.0
import "featherfall.js" as Feather

Image {
    id: feather

    property int xMax: 0
    property int xMin: 0
    property int yMax: 768

    // the delay offset for the initial creation of each feather.
    property real delay: 0

    source: {
        if (game.environment() == Global.Symbian)
           "qrc:/images/general/symbian/feather1.png"
        else
           "qrc:/images/general/feather1.png"
    }
    smooth: true
    y: -feather.height

    function startAnimation() {
        animY.running = true
        animX.running = true
        animRotation.running = true
    }

    function stopAnimation() {
	animY.running = false
        animX.running = false
        animRotation.running = false
    }

    SequentialAnimation on y {
        id: animY

        running: false

        PauseAnimation { duration: delay }
        NumberAnimation { to: 100; easing.type: Easing.InOutSine; duration: 1050 }
        NumberAnimation { to: 300; easing.type: Easing.InOutSine; duration: 950 }
        NumberAnimation { to: 500; easing.type: Easing.InOutSine; duration: 1050 }
        NumberAnimation { to: 700; easing.type: Easing.InOutSine; duration: 950 }
        NumberAnimation { to: 900; easing.type: Easing.InOutSine; duration: 1050 }
        NumberAnimation { to: yMax + 2 * feather.height; easing.type: Easing.InOutSine; duration: 950 }

        onRunningChanged: {
            if (!running)
                feather.destroy()
        }
    }

    SequentialAnimation on x {
        id: animX

        running: false

        PauseAnimation { duration: delay }
        NumberAnimation { to: xMax; easing.type: Easing.InOutSine; duration: 950 }
        NumberAnimation { to: xMin; easing.type: Easing.InOutSine; duration: 1050 }
        NumberAnimation { to: xMax; easing.type: Easing.InOutSine; duration: 950 }
        NumberAnimation { to: xMin; easing.type: Easing.InOutSine; duration: 1050 }
        NumberAnimation { to: xMax; easing.type: Easing.InOutSine; duration: 950 }
        NumberAnimation { to: xMin; easing.type: Easing.InOutSine; duration: 1050 }
    }

    SequentialAnimation on rotation {
        id: animRotation

        running: false

        PauseAnimation { duration: delay }
        NumberAnimation { to: -40; easing.type: Easing.InOutSine; duration: 1000 }
        NumberAnimation { to: 25; easing.type: Easing.InOutSine; duration: 1000 }
        NumberAnimation { to: -40; easing.type: Easing.InOutSine; duration: 1000 }
        NumberAnimation { to: 25; easing.type: Easing.InOutSine; duration: 1000 }
        NumberAnimation { to: -40; easing.type: Easing.InOutSine; duration: 1000 }
        NumberAnimation { to: 25; easing.type: Easing.InOutSine; duration: 1000 }
        NumberAnimation { to: -40; easing.type: Easing.InOutSine; duration: 1000 }
    }
}
