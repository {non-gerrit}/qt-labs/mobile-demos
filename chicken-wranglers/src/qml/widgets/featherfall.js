var component;
var featherObjList = new Array;

function create(fallWidth, fallHeight) {
    var i, range, lowRange, highRange;

    if (component == null)
        component = Qt.createComponent("Feather.qml");

    range = fallWidth / featherFall.number;
    lowRange = -range;
    highRange = 0;

    var resourcePath = "";
    if (game.environment() == Global.Symbian)
        resourcePath = "qrc:/images/general/symbian/feather"
    else
        resourcePath = "qrc:/images/general/feather"

    for (i = 0; i < featherFall.number; i++) {
        if (component.status == Component.Ready) {

            featherObjList[i] = component.createObject(background);

            if (featherObjList[i] == null) {
                console.log("error creating featherObj");
                console.log(component.errorString());
                return false;
            }

            lowRange = lowRange + range;
            highRange = highRange + range;

            var xpos = (highRange - range) * Math.random() + lowRange;
            featherObjList[i].x = xpos;

            featherObjList[i].delay = Math.random() * 3000 * i;

            var swing = Math.random() * range;
            if (swing < 100)
                swing += 100;

            featherObjList[i].xMax = xpos + swing;
            featherObjList[i].xMin = xpos - swing;
            featherObjList[i].yMax = fallHeight;
            featherObjList[i].source = resourcePath + (Math.floor(5 * Math.random()) + 1) + ".png";

            featherObjList[i].startAnimation();
        } else {
            console.log("error loading featherObj component");
            console.log(component.errorString());
            return false;
        }
    }

    return true;
}

function destroy() {
    for (var i = 0; i < featherFall.number; i++) {
        if (featherObjList[i] == null)
            continue;

        featherObjList[i].destroy()
    }
}
