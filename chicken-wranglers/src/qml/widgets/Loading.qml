/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

Item {
    id: loading

    property int count: 0

    width: row.width
    height: row.height

    Component {
        id: dot

        Rectangle {
            id: outer

            width: 15
            height: 15
            color: "#e0fce5"
            radius: 50
            smooth: true

            Rectangle {
                id: inner

                anchors.centerIn: parent

                smooth: true
                color: "#9bbaae"
                radius: 50

                anchors.fill: parent
                anchors.margins: 3
            }

            states {
                State {
                    name: "off"
                    PropertyChanges { target: outer; scale: 1 }
                    PropertyChanges { target: inner; color: "#9bbaae" }
                }
                State {
                    name: "on"
                    PropertyChanges { target: outer; scale: 1.33 }
                    PropertyChanges { target: inner; color: "#f6c200" }
                }
            }

            transitions: Transition {
                ParallelAnimation {
                    NumberAnimation { target: outer; property: "scale"; easing.type: Easing.InOutQuad }
                    ColorAnimation { target: inner; property: "color"; easing.type: Easing.InOutQuad }
                }
            }
        }
    }

    Row {
        id: row

        spacing: 4

        children: [
            Loader { sourceComponent: dot },
            Loader { sourceComponent: dot },
            Loader { sourceComponent: dot }
        ]
    }

    Timer {
        id: timer

        repeat: true
        interval: 200
        running: true

        onTriggered: {
            if (count > 0)
                row.children[count - 1].item.state = "off"

            if (count < row.children.length)
                row.children[count].item.state = "on"

            count = (count + 1) % (row.children.length + 1)
        }
    }

    onOpacityChanged: {
        if (opacity == 1 && !timer.running) {
            timer.start()
        }
        else if (opacity == 0 && timer.running) {
            timer.stop()
        }
    }
}
