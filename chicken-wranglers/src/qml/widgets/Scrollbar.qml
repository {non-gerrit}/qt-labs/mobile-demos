/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

Item {
    id: scrollbar

    property real value: 0.0
    property real minimumValue: 0.0
    property real maximumValue: 100.0
    property real offset: 8.0
    property real minimumPos: offset
    property real maximumPos: scrollbarPath.height - scrollbarKnob.height - offset
    property real scrollSize: maximumPos - minimumPos + 1

    width: scrollbarKnob.width
    height: scrollbarPath.height

    function setValue(newValue) {
        if (newValue <= minimumValue) {
            value = minimumValue;
        } else if (newValue >= maximumValue) {
            value = maximumValue;
        } else {
            value = newValue;
        }
    }

    Image {
        id: scrollbarPath

        anchors.centerIn: parent

        source: "qrc:/images/general/list_scrollbar_path.png"
    }

    Image {
        id: scrollbarKnob

        anchors.horizontalCenter: parent.horizontalCenter

        y: ((scrollbar.value / scrollbar.maximumValue) * scrollbar.scrollSize) + scrollbar.offset

        source: "qrc:/images/general/list_scrollbar_knob.png"
    }
}

