function start() {
    game.mode = Game.ClientMode

    startViewLoader.item.showStartClientDialog("Looking for server")
    startViewLoader.item.clientDiscoveryCanceled.connect(cancelDiscovery)

    // gameClient only exists after game.mode is set to Game.ClientMode
    gameClient.statusChanged.connect(onStatusChanged)
    gameClient.startConnection()
}

function cancelDiscovery() {
    if (gameClient.status == GameClient.Joined) {
        gameClient.statusChanged.disconnect(onStatusChanged)
        startViewLoader.item.clientDiscoveryCanceled.disconnect(cancelDiscovery)
        startViewLoader.item.hideStartClientDialog()
        return
    }

    quit()
}

function quit() {
    gameClient.statusChanged.disconnect(onStatusChanged)
    startViewLoader.item.clientDiscoveryCanceled.disconnect(cancelDiscovery)
    startViewLoader.item.hideStartClientDialog()

    gameClient.quit()
}

function onStatusChanged() {
    switch (gameClient.status) {

    case GameClient.Joined:
        startViewLoader.item.hideStartClientDialog()
        clientViewLoader.source = "ClientView.qml"
        break

    case GameClient.LeftMatch:
        clientViewLoader.item.showPlayerReadyView()
        break

    case GameClient.Ready:
        clientViewLoader.item.showControlView()
        break

    case GameClient.Error:
        startViewLoader.item.hideStartClientDialog()
        handleError(gameClient.error)
        break

    default:
        console.log("Unknown client status")
    }
}

function handleError(error) {
    switch (error) {

    case GameClient.ServerDiscoveryTimeoutError:
        startViewLoader.item.showErrorDialog("Host not found")
        break

    case GameClient.SocketError:
        clientViewLoader.closeRequested()
        startViewLoader.item.showErrorDialog("Disconnected from host")
        break

    default:
        console.log("Unknown client error")
    }
}
