/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

import game.types 1.0

Image {
    id: laser

    property int step: 1
    property int maxSteps: 5
    property string orientation: ""
    property bool turnedOn: false

    property string path: "qrc:/images/laser/laser_" + orientation + "_" + step + ".png"
    source: ""

    onSourceChanged: {
        // FIXME: Laser image's width and height should not
        // be hardcoded. We are forcing this due to a bug in
        // Image element after we change source property.
        if (orientation == "vertical") {
            laser.width = 27 * matchScreen.displayScale
            laser.height = 83 * matchScreen.displayScale
        } else {
            laser.width = 83 * matchScreen.displayScale
            laser.height = 27 * matchScreen.displayScale
        }
    }

    function turnOn(cellX, cellY, cellSize, direction) {
        if (direction == Global.LaserLeft || direction == Global.LaserRight)
            orientation = "vertical"
        else
            orientation = "horizontal"

        // FIXME: Same as above.
        var diffWidth = ((83 * matchScreen.displayScale) - cellSize) / 2
        var diffHeight = (27 * matchScreen.displayScale) / 2

        if (direction == Global.LaserUp) {
            laser.x = cellX - diffWidth
            laser.y = cellY - diffHeight
        } else if (direction == Global.LaserDown) {
            laser.x = cellX - diffWidth
            laser.y = cellY + cellSize - diffHeight
        } else if (direction == Global.LaserRight) {
            laser.x = cellX + cellSize - diffHeight
            laser.y = cellY - diffWidth
        } else if (direction == Global.LaserLeft) {
            laser.x = cellX - diffHeight
            laser.y = cellY - diffWidth
        }

        step = 0
        laserOnTimer.start()
    }

    function turnOff(cell, direction) {
        step = 4
        laserOffTimer.start();
    }

    Timer {
        id: laserOnTimer

        repeat: true
        interval: 50

        onTriggered: {
            if (step == maxSteps - 1) {
                running = false
                turnedOn = true
                return
            }

            step = step + 1
            laser.source = path
        }
    }

    Timer {
        id: laserOffTimer

        repeat: true
        interval: 50

        onTriggered: {

            // FIXME: Check if laser wasn't turned on before
            // destroying the previous one
            if (step === 0) {
                running = false
                turnedOn = false
                laser.destroy()
                return
            }

            step = step - 1

            if (step == 0)
                laser.source = ""
            else
                laser.source = path
        }
    }
}
