var defaultRows = settings.battlefieldSize().height
var defaultColumns = settings.battlefieldSize().width
var defaultFieldStartX = settings.battlefieldStartPoint().x
var defaultFieldStartY = settings.battlefieldStartPoint().y
var defaultCellSize = settings.battlefieldCellSize()
var defaultHostWidth = settings.hostDisplaySize().width
var defaultHostHeight = settings.hostDisplaySize().height

// FIXME: Find better naming and place for these variables
var deltaX = 0
var deltaY = 0

// Chicken values
var chickenMoveIntervalNormal = settings.matchChickenMoveInterval()
var chickenMoveIntervalRunning = chickenMoveIntervalNormal * 0.7
var chickenChangeStepsIntervalNormal = settings.matchChickenChangeStepsInterval()
var chickenChangeStepsIntervalRunning = chickenChangeStepsIntervalNormal * 0.7

// Character values
var characterMoveInterval = settings.matchCharacterMoveInterval()
var characterChangeStepsInterval = settings.matchChickenChangeStepsInterval()

var chickenComponent = null
var playerComponent = null
var playerScoreComponent = null
var henCoopComponent = null
var laserComponent = null

var verticalLaserArray = null
var horizontalLaserArray = null
var chickenArray = new Array()
var playerArray = new Array()

function initLaserArrays()
{
    verticalLaserArray = new Array(defaultRows)
    for (var y = 0; y < defaultRows; y++)
        verticalLaserArray[y] = new Array(defaultColumns - 1)

    horizontalLaserArray = new Array(defaultRows - 1)
    for (var y = 0; y < defaultRows - 1; y++)
        horizontalLaserArray[y] = new Array(defaultColumns)

    for (var y = 0; y < defaultRows; y++)
        for (var x = 0; x < defaultColumns - 1; x++)
            verticalLaserArray[y][x] = null

    for (var y = 0; y < defaultRows - 1; y++)
        for (var x = 0; x < defaultColumns; x++)
            horizontalLaserArray[y][x] = null
}

function destroyLasers()
{
    for (var y = 0; y < defaultRows; y++)
        for (var x = 0; x < defaultColumns - 1; x++) {
            if (verticalLaserArray[y][x] != null && verticalLaserArray[y][x] != undefined) {
                verticalLaserArray[y][x].destroy()
                verticalLaserArray[y][x] = null
            }
        }

    for (var y = 0; y < defaultRows - 1; y++)
        for (var x = 0; x < defaultColumns; x++) {
            if (horizontalLaserArray[y][x] != null && horizontalLaserArray[y][x] != undefined) {
                horizontalLaserArray[y][x].destroy()
                horizontalLaserArray[y][x] = null
            }
        }
}

function getDisplayScale() {
    var scaleX = game.displaySize.width / defaultHostWidth
    var scaleY = game.displaySize.height / defaultHostHeight

    var max = Math.max(scaleX, scaleY)
    deltaX = ((max - scaleY) * defaultHostWidth) / 2
    deltaY = ((max - scaleX) * defaultHostHeight) / 2

    return Math.min(scaleX, scaleY)
}

function newMatch() {
    gameHost.status = GameHost.Match

    initLaserArrays();

    matchController.chickenCreated.connect(onChickenCreated);
    matchController.playerCreated.connect(onPlayerCreated);
    matchController.laserUpdated.connect(onLaserUpdated);
    matchController.matchFinished.connect(onMatchFinished);

    matchController.setupMatch()
    matchController.status = Match.Paused
}

function startMatch() {
    matchController.status = Match.Started

    for (var i = 0; i < playerArray.length; i++)
        playerArray[i].opacity = 1
}

function askQuitMatch() {
    if (matchController.status == Match.Running)
        matchController.status = Match.Paused

    matchScreen.showQuitDialog()
}

function resumeMatch() {
    matchController.status = Match.Running
}

function leaveMatch() {
    destroyChickens()
    destroyPlayers()
    destroyLasers()

    matchController.status = Match.Over
}

function onMatchFinished() {
    matchController.status = Match.Paused

    matchScreen.showScore()
}

function destroyChickens()
{
    for (var i = 0; i < chickenArray.length; i++)
        if (chickenArray[i] != undefined && chickenArray[i] != null) {
            chickenArray[i].destroy()
            chickenArray[i] = null
        }
}

function destroyPlayers()
{
    for (var i = 0; i < playerArray.length; i++)
        if (playerArray[i] != undefined && playerArray[i] != null) {
            playerArray[i].destroy()
            playerArray[i] = null
        }
}

function onChickenCreated(id)
{
    if (chickenComponent == null)
        chickenComponent = Qt.createComponent("Chicken.qml");

    if (chickenComponent.status == Component.Ready) {
        var dynamicObject = chickenComponent.createObject(matchScreen);
        if (dynamicObject == null) {
            console.log("error creating chicken");
            console.log(chickenComponent.errorString());
            return false;
        }

        chickenArray[id] = dynamicObject

        var chickenController = matchController.getChicken(id)
        chickenController.positionChanged.connect(dynamicObject.onPositionChanged)
        dynamicObject.controller = chickenController
        dynamicObject.x = matchScreen.fieldStartX + (chickenController.position.x * matchScreen.cellSize) + (matchScreen.cellSize - dynamicObject.width) / 2
        dynamicObject.y = matchScreen.fieldStartY + (chickenController.position.y  * matchScreen.cellSize) + (matchScreen.cellSize - dynamicObject.height) / 2
    } else {
        console.log("error loading block chickenComponent");
        console.log(chickenComponent.errorString());
        return false;
    }

    return true;
}

function onPlayerCreated(id)
{
    if (playerComponent == null)
        playerComponent = Qt.createComponent("Character.qml");

    if (playerComponent.status == Component.Ready) {
        // TODO: Only players with status Ready should be added
        var dynamicObject = playerComponent.createObject(matchScreen);
        if (dynamicObject == null) {
            console.log("error creating character");
            console.log(playerComponent.errorString());
            return false;
        }

        dynamicObject.playerNumber = id
        playerArray[id] = dynamicObject

        var playerController = matchController.getPlayer(id)
        dynamicObject.controller = playerController
        playerController.laserToggleRequested.connect(dynamicObject.onLaserToggleRequested)

        dynamicObject.model = playerListModel.get(id)
    } else {
        console.log("error loading block playerComponent");
        console.log(playerComponent.errorString());
        return false;
    }

    return true;
}

function createScoreBox()
{
    if (playerScoreComponent == null)
        playerScoreComponent = Qt.createComponent("ScoreBox.qml");

    var dynamicObject = null;

    if (playerScoreComponent.status == Component.Ready) {
        dynamicObject = playerScoreComponent.createObject(score);
        if (dynamicObject == null) {
            console.log("error creating laser");
            console.log(playerScoreComponent.errorString());
            return null;
        }
    } else {
        console.log("error loading block playerScoreComponent");
        console.log(playerScoreComponent.errorString());
        return null;
    }

    return dynamicObject

}

function createLaser()
{
    if (laserComponent == null)
        laserComponent = Qt.createComponent("Laser.qml");

    var dynamicObject = null;

    if (laserComponent.status == Component.Ready) {
        dynamicObject = laserComponent.createObject(matchScreen);
        if (dynamicObject == null) {
            console.log("error creating laser");
            console.log(laserComponent.errorString());
            return null;
        }
    } else {
        console.log("error loading block laserComponent");
        console.log(laserComponent.errorString());
        return null;
    }

    return dynamicObject
}

function removeLaser(x, y, direction)
{
    var laserObj = null

    if (direction == Global.LaserRight && x < defaultColumns - 1) {
        laserObj = verticalLaserArray[y][x]
        verticalLaserArray[y][x] = null
    } else if (direction == Global.LaserLeft && x >= 0) {
        laserObj = verticalLaserArray[y][x - 1]
        verticalLaserArray[y][x - 1] = null
    } else if (direction == Global.LaserUp && y > 0) {
        laserObj = horizontalLaserArray[y - 1][x]
        horizontalLaserArray[y - 1][x] = null
    } else if (direction == Global.LaserDown && y < defaultRows - 1) {
        laserObj = horizontalLaserArray[y][x]
        horizontalLaserArray[y][x] = null
    }

    if (laserObj == null)
        return

    var cellX = matchScreen.fieldStartX + x * (defaultCellSize * matchScreen.displayScale)
    var cellY = matchScreen.fieldStartY + y * (defaultCellSize * matchScreen.displayScale)

    laserObj.turnOff(cellX, cellY, defaultCellSize * matchScreen.displayScale, direction)
}

function addLaser(x, y, direction)
{
    var laserObj = createLaser();

    if (direction == Global.LaserRight && verticalLaserArray[y][x] == null)
        verticalLaserArray[y][x] = laserObj
    else if (direction == Global.LaserLeft && verticalLaserArray[y][x - 1] == null)
        verticalLaserArray[y][x - 1] = laserObj
    else if (direction == Global.LaserUp && horizontalLaserArray[y - 1][x] == null)
        horizontalLaserArray[y - 1][x] = laserObj
    else if (direction == Global.LaserDown && horizontalLaserArray[y][x] == null)
        horizontalLaserArray[y][x] = laserObj
    else {
        laserObj.destroy()
        return
    }

    var cellX = matchScreen.fieldStartX + x * (defaultCellSize * matchScreen.displayScale)
    var cellY = matchScreen.fieldStartY + y * (defaultCellSize * matchScreen.displayScale)

    laserObj.turnOn(cellX, cellY, defaultCellSize * matchScreen.displayScale, direction)
}

function onLaserUpdated(posX, posY, laserDirection, isOn)
{
    if (isOn)
        addLaser(posX, posY, laserDirection)
    else
        removeLaser(posX, posY, laserDirection)
}
