/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

import "match.js" as MatchLogic
import game.types 1.0

Being {
    id: character

    property int playerNumber: 0
    property PlayerModel model
    property PlayerController controller

    path: {
        if (model)
            return model.characterPath
        else
            return ""
    }

    moveInterval: MatchLogic.characterMoveInterval
    changeStepsInterval: MatchLogic.characterChangeStepsInterval

    function onLaserToggleRequested(laserDirection) {
        var column = Math.round((x - matchScreen.fieldStartX - (matchScreen.cellSize - width) / 2) / matchScreen.cellSize)
        var row = Math.round((y - matchScreen.fieldStartY - (matchScreen.cellSize - height) / 2) / matchScreen.cellSize)
        matchController.toggleLaser(column, row, laserDirection)
    }

    x: matchScreen.fieldStartX + (currentColumn * matchScreen.cellSize) + (matchScreen.cellSize - width) / 2
    y: matchScreen.fieldStartY + (currentRow * matchScreen.cellSize) + (matchScreen.cellSize - height) / 2

    Behavior on x {
        NumberAnimation {
            duration: moveInterval;
            easing.type: Easing.Linear
        }
    }

    Behavior on y {
        NumberAnimation {
            duration: moveInterval;
            easing.type: Easing.Linear
        }
    }

    Behavior on opacity {
        NumberAnimation { duration: 200 }
    }

    currentRow: {
        if (controller)
            return controller.position.y
        else
            return 4
    }

    currentColumn: {
        if (controller)
            return controller.position.x
        else
            return 4
    }

    opacity: 0

    Timer {
        id: moveTimer

        triggeredOnStart: true
        interval: moveInterval;
        running: model != null && matchController.status != Match.Paused
        repeat: true
        onTriggered: {
            state = model.direction
            if (controller)
                controller.move()
        }
    }
}
