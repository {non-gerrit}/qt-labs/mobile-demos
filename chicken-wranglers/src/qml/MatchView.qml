/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

import game.types 1.0
import "common.js" as Common
import "match.js" as MatchLogic
import "widgets"

Screen {
    id: matchScreen

    anchors.fill: parent
    color: "black"
    smooth: true
    focus: true

    property real displayScale: MatchLogic.getDisplayScale()
    property real cellSize: MatchLogic.defaultCellSize * displayScale
    property real fieldStartX: (MatchLogic.defaultFieldStartX * displayScale) + MatchLogic.deltaX
    property real fieldStartY: (MatchLogic.defaultFieldStartY * displayScale) + MatchLogic.deltaY

    function showQuitDialog() {
        quitDialog.show()
    }

    function closeQuitDialog() {
        quitDialog.hide()
    }

    function showScore() {
        score.show()
    }

    function hideScore() {
        score.hide()
    }

    Image {
        id: fieldBg
        source: {
            if (game.environment() == Global.Symbian)
               "qrc:/images/battlefield/bg_fence_stones_mobile.png"
            else
               "qrc:/images/battlefield/bg_fence_stones.png"
        }
        scale : {
            if (game.environment() == Global.Symbian)
               1.0
            else
               battleField.scale
        }
        smooth: true
        anchors.centerIn: battleField
    }

    Item {
        id: battleField
        anchors.fill: parent
        scale: displayScale

        Image {
            id: topLeftCorner

            property PlayerModel player: playerListModel.get(0)

            opacity: {
                if (player == null || player.characterPath == "")
                    return 1
                return 0
            }

            source: "qrc:/images/battlefield/fence_corner_top_left.png"
            smooth: true
            anchors {
                left: fenceLayer.left
                top: fenceLayer.top
            }
        }

        Image {
            id: bottomRightCorner

            property PlayerModel player: playerListModel.get(1)

            opacity: {
                if (player == null || player.characterPath == "")
                    return 1
                return 0
            }

            source: "qrc:/images/battlefield/fence_corner_bottom_right.png"
            smooth: true
            anchors {
                right: fenceLayer.right
                bottom: fenceLayer.bottom
            }
        }

        Image {
            id: bottomLeftCorner

            property PlayerModel player: playerListModel.get(2)

            opacity: {
                if (player == null || player.characterPath == "")
                    return 1
                return 0
            }

            source: "qrc:/images/battlefield/fence_corner_bottom_left.png"
            smooth: true
            anchors {
                left: fenceLayer.left
                bottom: fenceLayer.bottom
            }
        }

        Image {
            id: topRightCorner

            property PlayerModel player: playerListModel.get(3)

            opacity: {
                if (player == null || player.characterPath == "")
                    return 1
                return 0
            }

            source: "qrc:/images/battlefield/fence_corner_top_right.png"
            smooth: true
            anchors {
                right: fenceLayer.right
                top: fenceLayer.top
            }
        }

        Image {
            id: fenceLayer
            source: "qrc:/images/battlefield/fence_layer_over.png"
            smooth: true
            anchors.centerIn: parent
            focus: true
        }

        Image {
            id: clockContainer
            source: "qrc:/images/battlefield/clock.png"
            smooth: true
            anchors {
                top: fenceLayer.top
                horizontalCenter: parent.horizontalCenter
            }

            Text {
                id: clock

                text: matchController.elapsedTime
                color: "white"
                style: Text.Outline
                smooth: true

                font {
                    bold: true
                    family: "Nokia Sans"
                    pointSize: 20
                }

                anchors.centerIn: parent
                anchors.verticalCenterOffset: -6
            }
        }

        ImageButton {
            id: quitButton
            name: "quit"
            path: "qrc:/images/battlefield"
            smooth: true
            anchors {
                bottom: fenceLayer.bottom
                horizontalCenter: parent.horizontalCenter
            }

            onClicked: MatchLogic.askQuitMatch()
        }
    }

    HenCoop {
        playerNumber: 1

        anchors {
            top: parent.top
            topMargin: 35
            left: parent.left
            leftMargin: 29
        }
    }

    HenCoop {
        playerNumber: 2

        anchors {
            top: parent.verticalCenter
            topMargin: 36
            right: parent.right
            rightMargin: 29
        }
    }

    HenCoop {
        playerNumber: 3

        anchors {
            top: parent.verticalCenter
            topMargin: 36
            left: parent.left
            leftMargin: 29
        }
    }

    HenCoop {
        playerNumber: 4

        anchors {
            top: parent.top
            topMargin: 35
            right: parent.right
            rightMargin: 29
        }
    }

    ConfirmationDialog {
        id: quitDialog

        text: "Do you want to leave this match?"
        opacity: 0

        onRejected: {
            closeQuitDialog()
            MatchLogic.resumeMatch()
        }

        onAccepted: {
            closeQuitDialog()
            MatchLogic.leaveMatch()

            closeRequested()
        }

        z: 10
    }

    Score {
        id: score

        opacity: 0

        onClose: {
            MatchLogic.leaveMatch()
            hideScore()

            closeRequested()
        }
    }

    Rectangle {
        id: background

        anchors.fill: parent
        color: "black"
        opacity: 0.7
        z: 49

        MouseArea {
            anchors.fill: parent
        }

        Behavior on opacity {
            PropertyAnimation { duration: 200 }
        }
    }

    Countdown {
        id: countdown

        z: 50

        anchors.fill: parent
        onTimeout: {
            background.opacity = 0
            MatchLogic.startMatch()
        }
    }

    onTransitionFinished: {
        if (visible)
            countdown.start()
        else
            closeFinished()
    }

    Component.onCompleted: MatchLogic.newMatch()

    Keys.onEscapePressed: MatchLogic.askQuitMatch()
}
