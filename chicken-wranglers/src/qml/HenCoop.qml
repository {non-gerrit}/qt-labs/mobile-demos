/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7
import game.types 1.0

Item {
    id: henCoop

    property int playerNumber: 1
    property PlayerModel player: playerListModel.get(playerNumber - 1)
    property string side: getSide()

    scale: matchScreen.displayScale

    function getSide () {
        if ((playerNumber % 2) == 0)
            return "right"

        return "left"
    }

    opacity: {
        if (henCoop.player == null || henCoop.player.characterPath == "")
            return 0
        return 1
    }

    Image {
        id: plate
        source: {
            if (henCoop.player == null || henCoop.player.characterPath == "")
                return ""
            "qrc:/images/battlefield/hencoop_plate.png"
        }

        smooth: true
        anchors {
            top: parent.top
            left: {
                if (henCoop.side == "left")
                    return parent.left
                else
                    return undefined
            }
            right: {
                if (henCoop.side == "right")
                    return parent.right
                else
                    return undefined
            }
        }
    }

    Image {
        id: house

        smooth: true
        source: {
            if (henCoop.player == null || henCoop.player.characterPath == "")
                return ""
            return henCoop.player.characterPath + "/hencoop.png"
        }

        anchors {
            top: parent.top
            left: {
                if (henCoop.side == "left")
                    return parent.left
                else
                    return undefined
            }
            right: {
                if (henCoop.side == "right")
                    return parent.right
                else
                    return undefined
            }
            leftMargin: 12
        }

        Text {
            id: score

            text: {
                if (player != null)
                    return player.score
                else
                    return "0"
            }

            color: "white"
            style: Text.Outline
            styleColor: "black"
            smooth: true

            font {
                family: "Nokia Sans"
                pointSize: 30
                bold: true
            }

            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                topMargin: 20
            }
        }
    }
}
