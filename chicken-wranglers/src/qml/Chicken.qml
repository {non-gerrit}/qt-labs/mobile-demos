/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7
import "match.js" as MatchLogic
import game.types 1.0

Being {
    id: chicken

    property ChickenController controller

    path: "qrc:/images/chicken"

    function kill() {
        chicken.opacity = 0
    }

    function onPositionChanged() {
        currentRow = controller.position.y
        currentColumn = controller.position.x

        var newX = matchScreen.fieldStartX + (currentColumn * matchScreen.cellSize) + (matchScreen.cellSize - width) / 2
        var newY = matchScreen.fieldStartY + (currentRow * matchScreen.cellSize) + (matchScreen.cellSize - height) / 2

        moveInterval = chickenMoveInterval

        if (x != newX && !xAnimation.running) {
            xAnimation.to = newX
            xAnimation.start()
        }

        if (y != newY && !yAnimation.running) {
            yAnimation.to = newY
            yAnimation.start()
        }
    }

    function move() {
        controller.move()
        chicken.state = controller.direction
    }

    opacity: 1

    currentRow: MatchLogic.defaultRows / 2
    currentColumn: MatchLogic.defaultColumns / 2

    PropertyAnimation {
        id: xAnimation

        target: chicken
        property: "x"
        duration: moveInterval
        alwaysRunToEnd: true

        onRunningChanged: {
            if (!running)
                chicken.move()
        }
    }

    PropertyAnimation {
        id: yAnimation

        target: chicken
        property: "y"
        duration: moveInterval
        alwaysRunToEnd: true

        onRunningChanged: {
            if (!running)
                chicken.move()
        }
    }

    property int chickenMoveInterval

    chickenMoveInterval: {
        if (!controller)
            return MatchLogic.chickenMoveIntervalNormal

        if (controller.isSafe)
            return MatchLogic.chickenMoveIntervalNormal
        else
            return MatchLogic.chickenMoveIntervalRunning
    }

    changeStepsInterval: MatchLogic.chickenChangeStepsIntervalNormal
    moveInterval: MatchLogic.chickenMoveIntervalNormal

    onOpacityChanged: {
        if (opacity == 0)
            chicken.destroy()
    }

    Behavior on opacity {
        NumberAnimation { duration: 500 }
    }

    Timer {
        id: move;
        interval: chickenMoveInterval
        running: state == "Stop" && matchController.status != Match.Paused
        repeat: true
        onTriggered: {
            chicken.move()

            if (currentRow < 0 || currentColumn < 0
                || currentRow >= MatchLogic.defaultRows
                || currentColumn >= MatchLogic.defaultColumns) {
                chicken.kill()
            }
        }
    }

    Component.onCompleted: {
        chicken.state = "Stop"
    }
}
