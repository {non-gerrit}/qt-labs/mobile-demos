var tempFrom
var tempTo

function switchScreens(from, to) {
    if (!from || !to)
        return

    tempFrom = from
    tempTo = to

    from.hide()
    from.opacityChanged.connect(showNextScreen)
}

function showNextScreen() {
    if (tempFrom.opacity == 0) {
        tempTo.show()
    }
}

function getImagePath(basePath, fileName) {
    if (basePath == "")
        return ""

    return basePath + "/" + fileName
}
