/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


import Qt 4.7

import game.types 1.0
import "common.js" as Common
import "widgets"

Screen {
    id: screen

    signal back
    signal matchStarted

    Keys.onReturnPressed: {
        if (buttonStart.enabled)
            matchStarted()
    }

    Image {
        id: background
        smooth: true
        anchors.fill: parent
        source:  {
            if (game.environment() == Global.Symbian)
                "qrc:/images/general/symbian/bg_640x360.png"
            else if (game.environment() == Global.Maemo)
                "qrc:/images/general/bg_800x480.png"
            else
                "qrc:/images/general/bg_1280x768.png"
        }
    }

    ImageButton {
        anchors {
            left: screen.left
            top: screen.top
            margins: 15
        }

        name: "back"
        onClicked: back()
    }


    Button {
        id: buttonStart

        anchors {
            top: screen.top
            right: screen.right
            margins: 15
        }

        width: 160
        text: "Start"
        sourceName: "bt_start"
        rightBorder: 35
        horizontalCenterOffset: -6
        italic: true

        enabled: playerListModel.playersReady;

        onClicked: matchStarted()
    }

    ListView {
        id: playerListView
        anchors {
            centerIn: parent
            horizontalCenterOffset: 30
            verticalCenterOffset: 130
        }

        height: 400
        width: 400
        model: playerListModel
        delegate: playerListDelegate
        interactive: false
    }

    Component {
        id: playerListDelegate

        Item {
            id: delegatePlayer

            scale: {
                if (game.environment() == Global.Symbian)
                    0.7
                else
                    1
            }

            width: 400
            height: 80

            property bool isReady: status == PlayerModel.Ready

            states {
                State {
                    name: "playerReady"; when: isReady
                    PropertyChanges { target: playerInfo; opacity: 1 }
                    PropertyChanges { target: playerInfoUnknown; opacity: 0 }
                }
            }

            Image {
                id: listItemBg

                anchors {
                    top: playerInfoUnknown.top
                    topMargin: 32
                }

                source: "qrc:/images/general/list_name_bg.png"
            }

            Item {
                id: playerInfoUnknown

                Image {
                    id: listItemIconUnknown

                    smooth: true

                    anchors {
                        top: parent.top
                        left: parent.left
                        leftMargin: 25
                    }

                    source: "qrc:/images/general/list_unknown_character.png"
                    fillMode: Image.PreserveAspectFit
                }

                Loading {
                    id: listItemLoading

                    anchors {
                        left: listItemIconUnknown.right
                        leftMargin: 40
                        bottom: listItemIconUnknown.bottom
                        bottomMargin: 20
                    }
                }

            }

            Item {
                id: playerInfo

                opacity: 0

                Image {
                    id: listItemIcon

                    smooth: true

                    anchors {
                        top: parent.top
                        left: parent.left
                        leftMargin: 25
                    }

                    source: Common.getImagePath(characterPath, "waitinglist.png")
                    fillMode: Image.PreserveAspectFit
                }


                Item {
                    id: listItemNameOk

                    anchors {
                        left: listItemIcon.right
                        leftMargin: 15
                        bottom: listItemIcon.bottom
                        bottomMargin: 12
                    }

                    Image {
                        id: listItemName

                        smooth: true

                        anchors {
                            left: listItemNameOk.left
                            bottom: listItemNameOk.bottom
                        }

                        source: Common.getImagePath(characterPath, "name.png")
                        fillMode: Image.PreserveAspectFit
                    }

                    Image {
                        id: listItemOk

                        smooth: true

                        anchors {
                            left: listItemName.right
                            leftMargin: 30
                            verticalCenter: listItemName.verticalCenter
                        }

                        source: "qrc:/images/general/list_status_ok.png"
                        fillMode: Image.PreserveAspectFit
                    }
                }

                ParallelAnimation {
                    id: anim

                    SequentialAnimation {
                        PropertyAnimation {
                            target: listItemIcon
                            property: "scale"
                            duration: 300
                            easing.type: Easing.InOutBack
                            to: 2
                        }
                        PropertyAnimation {
                            target: listItemIcon
                            property: "scale"
                            duration: 200
                            to: 1
                        }
                    }
                }

                onOpacityChanged: {
                    anim.running = true
                }
            }

            ListView.onAdd: SequentialAnimation {
                PropertyAction { target: delegatePlayer; property: "scale"; value: 0 }
                NumberAnimation { target: delegatePlayer; property: "scale"; to: 1; duration: 250; easing.type: Easing.InOutQuad }
            }

            ListView.onRemove: SequentialAnimation {
                PropertyAction { target: delegatePlayer; property: "ListView.delayRemove"; value: true }
                NumberAnimation { target: delegatePlayer; property: "scale"; to: 0; duration: 250; easing.type: Easing.InOutQuad }
                PropertyAction { target: delegatePlayer; property: "ListView.delayRemove"; value: false }
            }
        }
    }
}
