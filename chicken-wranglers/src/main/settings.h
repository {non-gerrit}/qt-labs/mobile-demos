/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QSize>
#include <QtCore/QPoint>

class Settings : public QSettings
{
    Q_OBJECT

public:
    static Settings *instance();

    static void update();
    static bool exists();
    static void reset();

    static QString organizationName();
    static QString organizationDomain();
    static QString applicationName();

public slots:
    // host
    static QSize hostDisplaySize();
    static int hostMaximumPlayers();

    // match
    static int matchTime();
    static int matchChickenNumber();
    static int matchChickenMoveInterval();
    static int matchChickenChangeStepsInterval();
    static int matchCharacterMoveInterval();
    static int matchCharacterChangeStepsInterval();

    // battlefield
    static QSize battlefieldSize();
    static int battlefieldCellSize();
    static QPoint battlefieldStartPoint();

    // local network
    static int lanTcpPort();
    static void setLanTcpPort(const QString port);
    static int lanUdpPort();
    static void setLanUdpPort(const QString port);
    static int lanUdpDiscoveryTimeout();
    static int lanUdpDiscoveryClientAttempts();
    static int lanUdpDiscoveryHostAttempts();

    // bluetooth
    static QString bluetoothHostAddress();
    static void setBluetoothHostAddress(const QString &address);

    static QString bluetoothHostName();
    static void setBluetoothHostName(const QString &name);

private:
    explicit Settings(QObject *parent = 0);

    static const QString m_organizationName;
    static const QString m_organizationDomain;
    static const QString m_applicationName;
};

#endif
