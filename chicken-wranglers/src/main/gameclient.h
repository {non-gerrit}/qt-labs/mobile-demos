/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtNetwork/QHostAddress>

#include "global.h"
#include "networkmessage.h"

class CharacterListModel;
class ConnectivityListModel;
class Control;
class NetworkClient;
class QDeclarativeContext;

class GameClient : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Status status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(bool isLeader READ isLeader WRITE setLeader NOTIFY isLeaderChanged)
    Q_PROPERTY(ErrorType error READ error NOTIFY errorChanged)
    Q_ENUMS(Status ErrorType)

public:
    GameClient(QObject *parent = 0);
    GameClient(QDeclarativeContext *context, const QString &connectivity = "lan",
               QObject *parent = 0);

    enum Status {
        NoStatus,
        Joined,
        LeftMatch,
        Ready,
        Playing,
        Error
    };

    enum ErrorType {
        NoError,
        ServerDiscoveryTimeoutError,
        SocketError,
        UnknownError
    };

    Status status() { return m_status; }
    void setStatus(Status status);

    void setLeader(bool isLeader);
    bool isLeader() const { return m_isLeader; }

    ErrorType error() { return m_error; }

public slots:
    void startConnection();

    void scanCanceled();

    // TODO: Use a single toggle laser method with direction parameter
    void toggleLaserUp();
    void toggleLaserDown();
    void toggleLaserLeft();
    void toggleLaserRight();

    // TODO: Use a single move method with direction parameter
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
    void moveStop();

    void quit();

signals:
    void statusChanged();
    void errorChanged();
    void isLeaderChanged();
    void ended();

private slots:
    void onServerDiscoveryTimeout();
    void onConnected();
    void onDisconnected();
    void onMessageReceived(const NetworkMessage &message);
    void onSocketError(QAbstractSocket::SocketError error);
    void onModelSelectedIndexChanged();
    void onControlDirectionChanged(Global::Direction direction);
    void forceBackLightOn();

private:
    void loadCharacters();

    void handleCharacterMessage(const NetworkMessage &message);
    void handleMatchMessage(const NetworkMessage &message);
    void handlePlayerInfoMessage(const NetworkMessage &message);

    void notifyCharacterSelected(const QString &characterName);
    void notifyCharacterReleased();
    void notifyDirectionChanged(Global::Direction);
    void notifyPlayerControlLaser(CW::PlayerControl laserDirection);

    QDeclarativeContext *m_declarativeContext;
    NetworkClient *m_networkClient;
    CharacterListModel *m_characterListModel;
    ConnectivityListModel *m_connectivityListModel;
    Control *m_control;
    Status m_status;
    ErrorType m_error;
    bool m_isLeader;
    QTimer m_backlightTimer;
};

#endif
