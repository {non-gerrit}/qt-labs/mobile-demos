/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef GAME_H
#define GAME_H

#include <QtCore/QObject>
#include <QtCore/QSize>

#include "gameclient.h"
#include "gamehost.h"

class GameClient;
class GameHost;
class QDeclarativeView;

class Game : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QSize displaySize READ displaySize NOTIFY displaySizeChanged)
    Q_PROPERTY(Mode mode READ mode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(QString connectivity READ connectivity WRITE setConnectivity)

    Q_ENUMS(Mode)

public:
    enum Mode {
        None,
        HostMode,
        ClientMode
    };

    enum GraphicsMode {
	OpenGL,
	Raster
    };

    Game(const GraphicsMode &mode = OpenGL, QObject *parent = 0);
    ~Game();

    void start(const QSize &size = QSize());

    Mode mode() { return m_mode; }
    void setMode(Mode mode);

    QSize displaySize() const;

    QString connectivity() const;
    Q_INVOKABLE void setConnectivity(const QString &type);

    Q_INVOKABLE void startBackgroundSound();
    Q_INVOKABLE int environment();

signals:
    void modeChanged();
    void displaySizeChanged();

private slots:
    void onClientModeEnded();
    void onHostModeEnded();

private:
    void startClientMode();
    void startHostMode();
    void setupSounds();

    QDeclarativeView *m_view;
    GameClient *m_gameClient;
    GameHost *m_gameHost;
    QString m_connectivity;
    Mode m_mode;
};

#endif
