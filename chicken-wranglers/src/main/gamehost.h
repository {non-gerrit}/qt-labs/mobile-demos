/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef GAMEHOST_H
#define GAMEHOST_H

#include <QtCore/QObject>

#include "networkmessage.h"
#include "networkserver.h"

class MatchController;
class PlayerListModel;
class QDeclarativeContext;

class GameHost : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Status status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(ErrorType error READ error NOTIFY errorChanged)

    Q_ENUMS(Status ErrorType GameState)

public:
    GameHost(QObject *parent = 0);
    GameHost(QDeclarativeContext *context, const QString &connectivity,
             QObject *parent = 0);
    ~GameHost();

    void setConnectivity(const QString &connectivity);

    enum Status {
        NoStatus,
        Start,
        WaitingRoom,
        Match,
        Error
    };

    enum ErrorType {
        NoError,
        AnotherServerRunningError,
        UnknownError
    };

    Status status() { return m_status; }
    void setStatus(Status status);

    ErrorType error() { return m_error; }

public slots:
    void startConnection();
    void quit();

signals:
    void statusChanged();
    void errorChanged();
    void ended();

private slots:
    void onServerError(NetworkServer::ServerError error);
    void onServerStarted();

    void onPeerConnected(NetworkConnection *connection);
    void onPeerDisconnected(NetworkConnection *connection);

    void onMatchOver();

private:
    void newMatch();
    void addPlayer(NetworkConnection *connection);
    void removePlayer(NetworkConnection *connection);

    QDeclarativeContext *m_declarativeContext;
    NetworkServer *m_networkServer;

    PlayerListModel *m_playerListModel;
    MatchController *m_matchController;
    Status m_status;
    ErrorType m_error;
};

#endif
