/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include <QtDeclarative>
#include <QtGui/QApplication>
#include <QtOpenGL/QGLWidget>

#include "charactermodel.h"
#include "connectivitylistmodel.h"
#include "global.h"
#include "settings.h"
#include "soundmanager.h"
#include "gameclient.h"
#include "gamehost.h"
#include "soundmanager.h"

#include "game.h"

Game::Game(const GraphicsMode &mode, QObject *parent)
    : QObject(parent), m_gameClient(0), m_gameHost(0),
      m_connectivity("bluetooth"), m_mode(None)
{
    qmlRegisterType<Global>("game.types", 1, 0, "Global");
    qmlRegisterType<Game>("game.types", 1, 0, "Game");
    qmlRegisterType<GameClient>("game.types", 1, 0, "GameClient");
    qmlRegisterType<GameHost>("game.types", 1, 0, "GameHost");
    // FIXME: Register this model later when GameClient is created.
    qmlRegisterType<ConnectivityListModel>("game.types", 1, 0, "ConnectivityListModel");

    m_view = new QDeclarativeView;
    if (mode == OpenGL) {
        //XXX: for some reason, setting the GLWidget is not enabling
        //ogl in Symbian
        if (Global::environment() == Global::Symbian) {
            QApplication::setGraphicsSystem("opengl");
        } else {
            m_view->setViewport(new QGLWidget);
        }
    } else {
        QApplication::setGraphicsSystem("raster");
    }

    m_view->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    m_view->rootContext()->setContextProperty("game", this);
    m_view->rootContext()->setContextProperty("settings", Settings::instance());
}

Game::~Game()
{
    delete m_gameHost;
    delete m_gameClient;
    delete m_view;
}

void Game::setMode(Mode mode)
{
    if (mode == ClientMode)
        startClientMode();
    else if (mode == HostMode)
        startHostMode();
}

void Game::startClientMode()
{
    m_mode = ClientMode;
    if (m_gameClient) {
        emit modeChanged();
        return;
    }

    if (!m_gameClient) {
        m_gameClient = new GameClient(m_view->rootContext(), m_connectivity, this);
        m_view->rootContext()->setContextProperty("gameClient", m_gameClient);
        connect(m_gameClient, SIGNAL(ended()), this, SLOT(onClientModeEnded()));
    } else
        ;//TODO: client has not a way to update the connectivity

    emit modeChanged();
}

void Game::startHostMode()
{
    m_mode = HostMode;
    if (m_gameHost) {
        m_gameHost->setConnectivity(m_connectivity);
        emit modeChanged();
        return;
    }

    if (!m_gameHost) {
        m_gameHost = new GameHost(m_view->rootContext(), m_connectivity, this);
        m_view->rootContext()->setContextProperty("gameHost", m_gameHost);
        connect(m_gameHost, SIGNAL(ended()), this, SLOT(onHostModeEnded()));
    }


    emit modeChanged();
}

void Game::start(const QSize &displaySize)
{
#ifdef CW_DEBUG
    m_view->setSource(Global::rootDirectory().absoluteFilePath() + "/src/qml/main.qml");
#else
    m_view->setSource(QUrl("qrc:/main.qml"));
#endif

    QObject::connect(m_view->engine(), SIGNAL(quit()), m_view, SLOT(close()));

    if (displaySize.isValid()) {
        m_view->resize(displaySize);
        m_view->show();
    } else {
        m_view->showFullScreen();
    }
}

QSize Game::displaySize() const
{
    return m_view->size();
}

void Game::onHostModeEnded()
{
    m_mode = None;
}

void Game::onClientModeEnded()
{
    //TODO: client has not a way to update the connectivity
    if (m_gameClient) {
        delete m_gameClient;
        m_gameClient = 0;
    }
    m_mode = None;
}

void Game::startBackgroundSound()
{
    SoundManager::play(SoundManager::Background);
}

int Game::environment()
{
    return Global::environment();
}

QString Game::connectivity() const
{
    return m_connectivity;
}

void Game::setConnectivity(const QString &type)
{
    if (m_connectivity == type)
        return;

    m_connectivity = type;
    if (m_mode == HostMode) {
        onHostModeEnded();
        startHostMode();
    } else {
        onClientModeEnded();
        startClientMode();
    }
}

