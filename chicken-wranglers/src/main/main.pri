QT += declarative

PATH = $$PWD

HEADERS += \
    $$PATH/game.h \
    $$PATH/gameclient.h \
    $$PATH/gamehost.h \
    $$PATH/argumentparser.h \
    $$PATH/settings.h

SOURCES += \
    $$PATH/main.cpp \
    $$PATH/game.cpp \
    $$PATH/gameclient.cpp \
    $$PATH/gamehost.cpp \
    $$PATH/argumentparser.cpp \
    $$PATH/settings.cpp
