/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include <QtCore/QFileInfo>
#include <QtCore/QString>

#include "settings.h"

const QString Settings::m_organizationName = "Nokia";
const QString Settings::m_organizationDomain = "nokia.com";
const QString Settings::m_applicationName = "chicken-wranglers";

// host
static const QSize defaultHostDisplaySize = QSize(1280, 768);
static const int defaultHostMaximumPlayers = 4;

// match
static const int defaultMatchChickenNumber = 40;
static const int defaultMatchTime = 180;
static const int defaultMatchChickenMoveInterval = 700;
static const int defaultMatchChickenChangeStepsInterval = 260;
static const int defaultMatchCharacterMoveInterval = 500;
static const int defaultMatchCharacterChangeStepsInterval = 260;

// battlefield
static const QSize defaultBattlefieldSize = QSize(10, 10);
static const int defaultBattlefieldCellSize = 70;
static const QPoint defaultBattlefieldStartPoint = QPoint(290, 34);

// local network
static const int defaultLanTcpPort = 35000;
static const int defaultLanUdpPort = 45000;
static const int defaultLanUdpDiscoveryTimeout = 100;
static const int defaultLanUdpDiscoveryClientAttempts = 50;
static const int defaultLanUdpDiscoveryHostAttempts = 10;

// bluetooth
static const QString defaultBluetoothHostAddress = "00:00:00:00:00:00";
static const QString defaultBluetoothHostName = "";

Settings::Settings(QObject *parent)
    : QSettings(QSettings::IniFormat, QSettings::UserScope,
                m_organizationName, m_applicationName, parent)
{
}

Settings *Settings::instance()
{
    static Settings settings;

    return &settings;
}

void Settings::update()
{
    instance()->sync();
}

QString Settings::organizationName()
{
    return m_organizationName;
}

QString Settings::organizationDomain()
{
    return m_organizationDomain;
}

QString Settings::applicationName()
{
    return m_applicationName;
}

// host

QSize Settings::hostDisplaySize()
{
    return instance()->value("host/displaySize", QSize()).toSize();
}

int Settings::hostMaximumPlayers()
{
    return instance()->value("host/maximumPlayers", defaultHostMaximumPlayers).toInt();
}

// match

int Settings::matchTime()
{
    return instance()->value("match/time", defaultMatchTime).toInt();
}

int Settings::matchChickenNumber()
{
    return instance()->value("match/chickenNumber", defaultMatchChickenNumber).toInt();
}

int Settings::matchChickenMoveInterval()
{
    return instance()->value("match/chickenMoveInterval",
                             defaultMatchChickenMoveInterval).toInt();
}

int Settings::matchChickenChangeStepsInterval()
{
    return instance()->value("match/chickenChangeStepsInterval",
                             defaultMatchChickenChangeStepsInterval).toInt();
}

int Settings::matchCharacterMoveInterval()
{
    return instance()->value("match/chickenCharacterMoveInterval",
                             defaultMatchCharacterMoveInterval).toInt();
}

int Settings::matchCharacterChangeStepsInterval()
{
    return instance()->value("match/chickenCharacterStepsInterval",
                             defaultMatchCharacterChangeStepsInterval).toInt();
}

// battlefield

QSize Settings::battlefieldSize()
{
    return instance()->value("battlefield/size", defaultBattlefieldSize).toSize();
}

int Settings::battlefieldCellSize()
{
    return instance()->value("battlefield/cellSize", defaultBattlefieldCellSize).toInt();
}

QPoint Settings::battlefieldStartPoint()
{
    return instance()->value("battlefield/startPoint", defaultBattlefieldStartPoint).toPoint();
}

// local network

int Settings::lanTcpPort()
{
    return instance()->value("lan/tcpPort", defaultLanTcpPort).toInt();
}

void Settings::setLanTcpPort(const QString port)
{
    instance()->setValue("lan/tcpPort", port);
}

int Settings::lanUdpPort()
{
    return instance()->value("lan/udpPort", defaultLanUdpPort).toInt();
}

void Settings::setLanUdpPort(const QString port)
{
    instance()->setValue("lan/udpPort", port);
}

int Settings::lanUdpDiscoveryTimeout()
{
    return instance()->value("lan/UdpDiscoveryTimeout", defaultLanUdpDiscoveryTimeout).toInt();
}

int Settings::lanUdpDiscoveryClientAttempts()
{
    return instance()->value("lan/UdpDiscoveryClientAttempts",
                             defaultLanUdpDiscoveryClientAttempts).toInt();
}

int Settings::lanUdpDiscoveryHostAttempts()
{
    return instance()->value("lan/UdpDiscoveryHostAttempts",
                             defaultLanUdpDiscoveryHostAttempts).toInt();
}

// bluetooth

QString Settings::bluetoothHostAddress()
{
    return instance()->value("bluetooth/hostAddress", QString()).toString();
}

void Settings::setBluetoothHostAddress(const QString &address)
{
    instance()->setValue("bluetooth/hostAddress", address);
}

QString Settings::bluetoothHostName()
{
    return instance()->value("bluetooth/hostName", QString()).toString();
}

void Settings::setBluetoothHostName(const QString &name)
{
    instance()->setValue("bluetooth/hostName", name);
}

bool Settings::exists()
{
    QFileInfo settingsFileInfo(instance()->fileName());

    return settingsFileInfo.exists();
}

void Settings::reset()
{
    Settings *settings = instance();

    settings->beginGroup("host");
    settings->setValue("displaySize", defaultHostDisplaySize);
    settings->setValue("hostMaximumPlayers", defaultHostMaximumPlayers);
    settings->endGroup();

    settings->beginGroup("match");
    settings->setValue("time", defaultMatchTime);
    settings->setValue("chickenNumber", defaultMatchChickenNumber);
    settings->setValue("chickenMoveInterval", defaultMatchChickenMoveInterval);
    settings->setValue("chickenChangeStepsInterval", defaultMatchChickenChangeStepsInterval);
    settings->setValue("characterMoveInterval", defaultMatchCharacterMoveInterval);
    settings->setValue("characterChangeStepsInterval", defaultMatchCharacterChangeStepsInterval);
    settings->endGroup();

    settings->beginGroup("battlefield");
    settings->setValue("size", defaultBattlefieldSize);
    settings->setValue("cellSize", defaultBattlefieldCellSize);
    settings->setValue("startPoint", defaultBattlefieldStartPoint);
    settings->endGroup();

    settings->beginGroup("lan");
    settings->setValue("tcpPort", defaultLanTcpPort);
    settings->setValue("udpPort", defaultLanUdpPort);
    settings->setValue("udpDiscoveryTimeout", defaultLanUdpDiscoveryTimeout);
    settings->setValue("udpDiscoveryClientAttempts", defaultLanUdpDiscoveryClientAttempts);
    settings->setValue("udpDiscoveryHostAttempts", defaultLanUdpDiscoveryHostAttempts);
    settings->endGroup();

    settings->beginGroup("bluetooth");
    settings->setValue("hostAddress", defaultBluetoothHostAddress);
    settings->setValue("hostName", defaultBluetoothHostName);
    settings->endGroup();

    update();
}
