/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#include "matchcontroller.h"

#include "chickencontroller.h"
#include "matchai.h"
#include "playercontroller.h"
#include "playermodel.h"
#include "settings.h"

#include <QtDeclarative>

MatchController::MatchController(QObject *parent)
    : QObject(parent)
{
}

MatchController::MatchController(PlayerListModel *model, int chickens, int elapsedTime, QObject *parent)
    : QObject(parent), m_playerListModel(model), m_matchAI(0),
      m_chickens(chickens), m_elapsedTime(elapsedTime), m_status(Ready)
{
    qmlRegisterType<ChickenController>("game.types", 1, 0, "ChickenController");
    qmlRegisterType<PlayerController>("game.types", 1, 0, "PlayerController");
}

MatchController::~MatchController()
{
    if (m_matchAI)
        delete m_matchAI;
}

void MatchController::updateElapsedTime()
{
    m_time = m_time.addSecs(-1);
    m_elapsedTime--;

    emit elapsedTimeChanged();
}

QString MatchController::elapsedTime() const
{
    return m_time.toString("mm:ss");
}

void MatchController::setStatus(Status status)
{
    if (m_status == status)
        return;

    m_status = status;

    switch (status) {
    case Started:
        start();
        break;
    case Paused:
        pause();
        break;
    case Running:
        resume();
        break;
    case Over:
        leave();
        break;
    default:
        qWarning("Invalid match status");
    }

    emit statusChanged();
}

void MatchController::start()
{
    m_timer.start();

    m_status = Running;
}

void MatchController::pause()
{
    m_timer.stop();
}

void MatchController::resume()
{
    m_timer.start();
}

void MatchController::leave()
{
    emit matchOver();
}

void MatchController::matchTimeout()
{
    m_timer.stop();

    emit matchFinished();
}

void MatchController::setupMatch()
{
    // FIXME: These should not be hardcoded
    m_playerStartPositionList.append(QPoint(0, 0));
    m_playerStartPositionList.append(QPoint(9, 9));
    m_playerStartPositionList.append(QPoint(0, 9));
    m_playerStartPositionList.append(QPoint(9, 0));

    m_matchAI = new MatchAI(QSize(10, 10));

    m_time.setHMS(0, 0, 0);
    m_time = m_time.addSecs(m_elapsedTime);
    m_timer.setInterval(1000);
    connect(&m_timer, SIGNAL(timeout()),
            this, SLOT(onTimerTick()));

    createPlayers();
    createChickens();
}

void MatchController::createChickens()
{
    m_chickenCount = m_chickens;

    for (int i = 0; i < m_chickenCount; i++) {
        ChickenController *chickenController = new ChickenController(m_matchAI, this);
        chickenControllerList.append(chickenController);

        emit chickenCreated(i);
    }
}

void MatchController::createPlayers()
{
    int i;

    for (i = 0; i < m_playerListModel->rowCount() && i < m_playerStartPositionList.size(); i++) {
        PlayerModel *playerModel = m_playerListModel->get(i);

        PlayerController *playerController = new PlayerController(playerModel, m_matchAI, this);
        playerController->setPosition(m_playerStartPositionList.at(i));
        Node *henCoop = m_matchAI->addPlayer(m_playerStartPositionList.at(i));
        if (!henCoop) {
            qWarning("Error adding player to match AI");
            playerController->deleteLater();
            continue;
        }

        connect(henCoop, SIGNAL(chickenEntered(int)),
                playerController, SLOT(onScore(int)));

        connect(henCoop, SIGNAL(chickenEntered(int)),
                this, SLOT(onChickenEntered(int)));

        playerControllerList.append(playerController);

        emit playerCreated(i);
    }
}

ChickenController *MatchController::getChicken(int id) const
{
    if (id >= chickenControllerList.size() || id < 0)
        return 0;

    return chickenControllerList.at(id);
}

PlayerController *MatchController::getPlayer(int id) const
{
    if (id >= playerControllerList.size() || id < 0)
        return 0;

    return playerControllerList.at(id);
}

void MatchController::toggleLaser(int x, int y, int laserDirection)
{
    foreach(PlayerController *playerController,  playerControllerList)
        m_matchAI->doSafe(playerController->position());

    bool isOn = m_matchAI->toggleLaser(QPoint(x, y),
                                       static_cast<Global::LaserDirection>(laserDirection));

    foreach(PlayerController *playerController,  playerControllerList)
        m_matchAI->doUnSafe(playerController->position());

    emit laserUpdated(x, y, laserDirection, isOn);
}

void MatchController::onTimerTick()
{
    updateElapsedTime();

    if (m_elapsedTime == 0)
        matchTimeout();
}

void MatchController::onChickenEntered(int)
{
    if (--m_chickenCount > 0)
        return;

    m_timer.stop();

    emit matchFinished();
}
