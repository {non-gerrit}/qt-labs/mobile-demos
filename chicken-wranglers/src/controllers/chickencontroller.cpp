/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#include "chickencontroller.h"

#include "matchai.h"

#include <QtCore/QTime>

ChickenController::ChickenController(QObject *parent)
    : QObject(parent), m_isSafe(true), m_matchAI(0)
{
    initPosition();
}

ChickenController::ChickenController(MatchAI *matchAI, QObject *parent)
    : QObject(parent), m_matchAI(matchAI)
{
    initPosition();
}

ChickenController::~ChickenController()
{
}

void ChickenController::setPosition(const QPoint &position)
{
    if (position == m_position)
        return;

    m_position = position;

    emit positionChanged();
}

void ChickenController::setDirection(const QString &direction)
{
    if (direction == m_direction)
        return;

    m_direction = direction;

    emit directionChanged();
}

void ChickenController::setSafe(bool isSafe)
{
    if (m_isSafe == isSafe)
        return;

    m_isSafe = isSafe;

    emit isSafeChanged();
}

void ChickenController::move()
{
    Q_ASSERT(m_matchAI);

    setSafe(m_matchAI->isChickenSafe(m_position));

    QPair<QPoint, Global::Direction> nextPos = m_matchAI->moveChicken(m_position);

    setDirection(Global::directionString(nextPos.second));
    setPosition(nextPos.first);
}

void ChickenController::initPosition()
{
    Q_ASSERT(m_matchAI);

    qsrand(QTime::currentTime().msec());
    do {
        m_position = QPoint(qrand() % 10, qrand() % 10);
    } while (!m_matchAI->addChicken(m_position));

    m_direction = Global::directionString(Global::DirectionStop);
}
