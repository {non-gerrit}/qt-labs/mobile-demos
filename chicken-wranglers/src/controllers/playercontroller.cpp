/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#include "playercontroller.h"

#include "matchai.h"
#include "matchcontroller.h"
#include "playermodel.h"
#include "soundmanager.h"

PlayerController::PlayerController(QObject *parent)
    : QObject(parent)
{
}

PlayerController::PlayerController(PlayerModel *model, MatchAI *matchAI, QObject *parent)
    : QObject(parent), m_model(model), m_matchAI(matchAI), m_score(0)
{
    connect(m_model, SIGNAL(laserToggleRequested(Global::LaserDirection)),
            this, SLOT(onLaserToggleRequested(Global::LaserDirection)));
}

PlayerController::~PlayerController()
{
}

void PlayerController::setPosition(const QPoint &position)
{
    if (position == m_position)
        return;

    m_position = position;

    emit positionChanged();
}

void PlayerController::onScore(int score)
{
    SoundManager::play(SoundManager::Score);

    m_model->setScore(score);
}

void PlayerController::move()
{
    Q_ASSERT(m_matchAI);

    Global::Direction direction = m_model->direction();
    QPoint nextPosition = m_matchAI->movePlayer(m_position, direction);
    setPosition(nextPosition);
}

bool PlayerController::isSameDirection(Global::LaserDirection laserDirection)
{
    Global::Direction currentDirection = m_model->direction();

    if (currentDirection == Global::DirectionStop)
        return true;

    if (laserDirection == Global::LaserUp && currentDirection == Global::DirectionUp)
        return true;

    if (laserDirection == Global::LaserDown && currentDirection == Global::DirectionDown)
        return true;

    if (laserDirection == Global::LaserLeft && currentDirection == Global::DirectionLeft)
        return true;

    if (laserDirection == Global::LaserRight && currentDirection == Global::DirectionRight)
        return true;

    return false;
}

void PlayerController::onLaserToggleRequested(Global::LaserDirection laserDirection)
{
    SoundManager::play(SoundManager::Laser);

    if (isSameDirection(laserDirection)) {
        MatchController *matchController = qobject_cast<MatchController *>(parent());
        if (!matchController) {
            qWarning("Error getting MatchController from PlayerController");
            return;
        }

        matchController->toggleLaser(m_position.x(), m_position.y(), laserDirection);
    } else
        emit laserToggleRequested(laserDirection);
}
