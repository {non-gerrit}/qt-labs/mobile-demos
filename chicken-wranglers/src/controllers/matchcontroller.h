/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef MATCHCONTROLLER_H
#define MATCHCONTROLLER_H

#include "global.h"

#include <QtCore/QObject>
#include <QtCore/QPoint>
#include <QtCore/QTime>
#include <QtCore/QTimer>

class ChickenController;
class MatchAI;
class PlayerController;
class PlayerListModel;

class MatchController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString elapsedTime READ elapsedTime NOTIFY elapsedTimeChanged)
    Q_PROPERTY(Status status READ status WRITE setStatus NOTIFY statusChanged)

    Q_ENUMS(Status)

public:
    MatchController(QObject *parent = 0);
    MatchController(PlayerListModel *model, int chickens, int elapsedTime, QObject *parent = 0);
    ~MatchController();

    enum Status {
        Ready,
        Started,
        Paused,
        Running,
        Over
    };

    QString elapsedTime() const;

    void setStatus(Status status);
    Status status() const { return m_status; }

    Q_INVOKABLE void setupMatch();
    Q_INVOKABLE ChickenController *getChicken(int id) const;
    Q_INVOKABLE PlayerController *getPlayer(int id) const;

    Q_INVOKABLE void toggleLaser(int x, int y, int laserDirection);

signals:
    void chickenCreated(int id);
    void playerCreated(int id);
    void laserUpdated(int x, int y, int laserDirection, bool isOn);
    void elapsedTimeChanged();
    void statusChanged();
    void matchFinished();
    void matchOver();

    void aborted();

private slots:
    void onTimerTick();
    void onChickenEntered(int);

private:
    void setup();
    void createChickens();
    void createPlayers();
    void updateElapsedTime();
    void start();
    void pause();
    void resume();
    void leave();
    void matchTimeout();

    QList<QPoint> m_playerStartPositionList;

    QList<ChickenController *> chickenControllerList;
    QList<PlayerController *> playerControllerList;

    PlayerListModel *m_playerListModel;
    MatchAI *m_matchAI;
    QTimer m_timer;
    QTime m_time;
    int m_chickens;
    int m_elapsedTime;
    Status m_status;
    int m_chickenCount;
};

#endif
