QT += declarative

PATH = $$PWD

HEADERS += \
    $$PATH/matchcontroller.h \
    $$PATH/chickencontroller.h \
    $$PATH/playercontroller.h

SOURCES += \
    $$PATH/matchcontroller.cpp \
    $$PATH/chickencontroller.cpp \
    $$PATH/playercontroller.cpp
