/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#ifndef CHICKENCONTROLLER_H
#define CHICKENCONTROLLER_H

#include <QtCore/QObject>
#include <QtCore/QPoint>
#include <QtCore/QString>

class MatchAI;

class ChickenController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QPoint position READ position NOTIFY positionChanged)
    Q_PROPERTY(QString direction READ direction NOTIFY directionChanged)
    Q_PROPERTY(bool isSafe READ isSafe NOTIFY isSafeChanged)

public:
    explicit ChickenController(QObject *parent = 0); // don't use it
    explicit ChickenController(MatchAI *matchAI, QObject *parent = 0);
    ~ChickenController();

    QPoint position() const { return m_position; }
    void setPosition(const QPoint &position);

    QString direction() const { return m_direction; }
    void setDirection(const QString &direction);

    bool isSafe() const { return m_isSafe; }

    Q_INVOKABLE void move();

signals:
    void positionChanged();
    void directionChanged();
    void isSafeChanged();

private:
    void initPosition();
    void setSafe(bool isSafe);

    QPoint m_position;
    QString m_direction;
    bool m_isSafe;
    MatchAI *m_matchAI;
};

#endif
