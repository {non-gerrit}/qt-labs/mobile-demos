PATH = $$PWD

HEADERS += \
    $$PATH/matchai.h \
    $$PATH/graph.h

SOURCES += \
    $$PATH/matchai.cpp \
    $$PATH/graph.cpp
