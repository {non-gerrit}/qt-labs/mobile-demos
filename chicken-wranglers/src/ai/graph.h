/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#ifndef GRAPH_H
#define GRAPH_H

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QPair>
#include <QtCore/QPoint>
#include <QtCore/QSize>

#include "global.h"

class Node : public QObject
{
    Q_OBJECT

public:
    enum Neighbor {
        Right,
        Left,
        Up,
        Down,
        Stop,
        MAX_NEIGHBOR
    };

    enum Data {
        Empty,
        Chicken,
        Player,
        Hencoop
    };

    explicit Node(QObject *parent = 0);

    bool isEmpty() { return data == Node::Empty; }
    bool isChicken() { return data == Node::Chicken; }
    bool isPlayer() { return data == Node::Player; }
    bool isHencoop() { return data == Node::Hencoop; }
    bool isScape() { return !isPlayer() && !isHencoop(); }
    bool isSafe() { return !rank; }
    void emitChickenEntered() { emit chickenEntered(chickenCount); }

    static Global::Direction neighborDirection(Node::Neighbor);
    static Node::Neighbor directionNeighbor(Global::Direction);

    Node *neighbor[MAX_NEIGHBOR];
    Data data;
    int rank;
    int chickenCount;
    QPoint pos;
    Node::Neighbor direction;

signals:
    void chickenEntered(int numChickens);
};

class Graph : public QObject
{
    Q_OBJECT

public:
    explicit Graph(QSize size, QObject *parent = 0);
    Node *node(QPoint pos);

private:
    void initMatrix();

    QSize m_size;
    Node *m_matrix[20][20];
};

#endif
