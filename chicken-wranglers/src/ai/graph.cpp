/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/


#include "graph.h"

Node::Node(QObject *parent)
    : QObject(parent), data(Node::Empty), rank(0),
      chickenCount(0), pos(0, 0), direction(Node::Stop)
{
    neighbor[Node::Stop] = this;
    neighbor[Node::Up] = this;
    neighbor[Node::Down] = this;
    neighbor[Node::Right] = this;
    neighbor[Node::Left] = this;
}

Global::Direction Node::neighborDirection(Node::Neighbor neighbor)
{
    switch (neighbor) {
    case Node::Up:
        return Global::DirectionUp;
    case Node::Down:
        return Global::DirectionDown;
    case Node::Right:
        return Global::DirectionRight;
    case Node::Left:
        return Global::DirectionLeft;
    default:
        return Global::DirectionStop;
    }
}

Node::Neighbor Node::directionNeighbor(Global::Direction direction)
{
    switch (direction) {
    case Global::DirectionUp:
        return Node::Up;
    case Global::DirectionDown:
        return Node::Down;
    case Global::DirectionRight:
        return Node::Right;
    case Global::DirectionLeft:
        return Node::Left;
    default:
        return Node::Stop;
    }
}

Graph::Graph(QSize size, QObject *parent)
    : QObject(parent), m_size(size)
{
    initMatrix();
}

Node *Graph::node(QPoint pos)
{
    if (pos.y() < m_size.height() && pos.x() < m_size.width()
            && pos.y() >= 0 && pos.x() >= 0) {
        return m_matrix[pos.y()][pos.x()];
    }

    return 0;
}

void Graph::initMatrix()
{
    if (!m_size.height() || !m_size.width())
        return;

    // FIXME: Better way to alloc the matrix
    for (int row = 0; row < m_size.height(); row++)
        for (int column = 0; column < m_size.width(); column++)
            m_matrix[row][column] = new Node(this);

    for (int row = 0; row < m_size.height(); row++) {
        for (int column = 0; column < m_size.width(); column++) {
            m_matrix[row][column]->pos = QPoint(column, row);

            if (row > 0)
                m_matrix[row][column]->neighbor[Node::Up] = m_matrix[row - 1][column];
            if (column > 0)
                m_matrix[row][column]->neighbor[Node::Left] = m_matrix[row][column - 1];
            if (row < m_size.height() - 1)
                m_matrix[row][column]->neighbor[Node::Down] = m_matrix[row + 1][column];
            if (column < m_size.width() - 1)
                m_matrix[row][column]->neighbor[Node::Right] = m_matrix[row][column + 1];
        }

    }
}
