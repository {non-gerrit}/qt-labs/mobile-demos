PATH = $$PWD

HEADERS += \
    $$PATH/soundmanager.h \
    $$PATH/sound.h

SOURCES += \
    $$PATH/soundmanager.cpp \
    $$PATH/sound.cpp
