/****************************************************************************
**
** This file is a part of QtChickenWranglers.
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
** All rights reserved.
** Contact:  Nokia Corporation (qt-info@nokia.com)
**
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in the
**     documentation and/or other materials provided with the distribution.
**
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
**  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE."
**
****************************************************************************/

#include "soundmanager.h"

#include "global.h"

#if defined(Q_WS_X11)
#include "sound_linux.h"
#else
#include "sound.h"
#endif

#include <QtCore/QDebug>

SoundManager::SoundManager(QObject *parent)
    : QObject(parent), m_isLoaded(false)
{
}

SoundManager *SoundManager::instance()
{
    static SoundManager *const soundManager = new SoundManager();

    return soundManager;
}

void SoundManager::load()
{
    if (m_isLoaded)
        return;

    QString soundPath(Global::rootDirectory().absoluteFilePath() + "/sounds/");

    QFile soundFile;

    soundFile.setFileName(soundPath + "match.raw");
    if (soundFile.open(QIODevice::ReadOnly)) {
        m_backgroundSound = soundFile.readAll();
        soundFile.close();
    } else {
        qWarning() << "Error loading sound file:" << soundFile.fileName();
    }

    soundFile.setFileName(soundPath + "laser.raw");
    if (soundFile.open(QIODevice::ReadOnly)) {
        m_laserSound = soundFile.readAll();
        soundFile.close();
    } else {
        qWarning() << "Error loading sound file:" << soundFile.fileName();
    }

    soundFile.setFileName(soundPath + "score.raw");
    if (soundFile.open(QIODevice::ReadOnly)) {
        m_scoreSound = soundFile.readAll();
        soundFile.close();
    } else {
        qWarning() << "Error loading sound file:" << soundFile.fileName();
    }

    m_isLoaded = true;
}

void SoundManager::play(SoundType type)
{
    // NOTE: Currently sounds are supported only on Linux builds.

#if defined(Q_WS_X11)
    instance()->load();

    Sound *sound = 0;

    switch (type) {
    case Background:
        sound = new Sound(instance()->m_backgroundSound, true);
        break;
    case Score:
        sound = new Sound(instance()->m_scoreSound);
        break;
    case Laser:
        sound = new Sound(instance()->m_laserSound);
        break;
    default:
        qWarning("No valid sound type");
        return;
    }

    sound->play();
#endif
}
