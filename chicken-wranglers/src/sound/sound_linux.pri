PATH = $$PWD

HEADERS += \
    $$PATH/soundmanager.h \
    $$PATH/sound_linux.h

SOURCES += \
    $$PATH/soundmanager.cpp \
    $$PATH/sound_linux.cpp
